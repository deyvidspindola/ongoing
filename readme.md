> sudo apt-get install -y php7.3 libapache2-mod-php7.3 php7.3-mcrypt php7.3-mysql php7.3-dom php7.3-xml php7.3-gd php7.3-mbstring php7.3-curl php7.3-ldap php7.3-sybase php7.3-bz2 php7.3-zip php7.3-soap

> git clone http://gitlab.artit.com.br/root/ongoing_automacao.git

> composer install

> copiar (.env.example) para (.env) e editar configurações

> php artisan key:generate

> php artisan migrate:refresh --seed

> php artisan serve

Acesso ADMIN

> user: admin

> pass: artit@2019

TEMPLATE

> METRONIC 4

> https://template.lc8.com.br 