<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'mysql1' => [
            'driver' => 'mysql',
            'host' => '10.230.231.37',
            'port' => env('DB_PORT', '3306'),
            'database' => 'ongoing_test',
            'username' => 'desenv',
            'password' => 'artit@2019',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'pgsql_31' => [
            'driver' => 'pgsql',
            'host' => '10.230.231.31',
            'port' => '5432',
            'database' => 'ongoing',
            'username' => 'ongoing',
            'password' => 'srvongoing',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

	    'oracle' => [
            'driver' => 'oracle',
            'host' => '5.17.8.14',
            'port' => '1523',
            'database' => 'ddbh13f',
            'service_name' => 'DDBH13F',
            'username' => 'prod_jd',
            'password' => 'prod_jd',
            'charset' => 'utf8',
            'prefix' => '',
	    ],

        'ora_base_net_dev' => [
            'driver' => 'oracle',
            'host' => '5.17.8.14',
            'port' => '1523',
            'database' => 'ddbh13f',
            'service_name' => 'DDBH13F',
            'username' => 'prod_jd',
            'password' => 'prod_jd',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        //SEGUE INFORMAÇÕES PARA AS BASES NET
        //    'USUARIO': 'A_T1860897',
        //    'PASSWORD': 'net2014',

            'NETABC' => [
                'driver' => 'oracle',
                'host' => '5.17.8.14',
                'port' => '1523',
                'service_name' => 'DDABC13F',
                'database' => 'DDABC13F',
                'username' => 'prod_jd',
                'password' => 'prod_jd',
                'charset' => 'utf8',
                'prefix' => '',
            ],
            'NETISP' => [
                'driver' => 'oracle',
                'host' => '5.17.8.208',
                'port' => '1523',
                'service_name' => 'MUCISP01',
                'database' => 'MUCISP01',
                'username' => 'prod_jd',
                'password' => 'prod_jd',
                'charset' => 'utf8',
                'prefix' => '',
            ],
            'NETSUL' => [
                'driver' => 'oracle',
                'host' => '5.17.8.14',
                'port' => '1523',
                'service_name' => 'DDSUL11F',
                'database' => 'DDSUL11F',
                'username' => 'prod_jd',
                'password' => 'prod_jd',
                'charset' => 'utf8',
                'prefix' => '',
            ],
            'NETBRASIL' => [
                'driver' => 'oracle',
                'host' => '5.17.8.14',
                'port' => '1523',
                'service_name' => 'DDBRA13F',
                'database' => 'DDBRA13F',
                'username' => 'prod_jd',
                'password' => 'prod_jd',
                'charset' => 'utf8',
                'prefix' => '',
            ],
            'NETBH' => [
                'driver' => 'oracle',
                'host' => '5.17.8.14',
                'port' => '1523',
                'service_name' => 'DDBH13F',
                'database' => 'ddbh13f',
                'username' => 'prod_jd',
                'password' => 'prod_jd',
                'charset' => 'utf8',
                'prefix' => '',
            ],

        //Bancos Faltantes
            'NETSOROCABA' => [
                'driver' => 'oracle',
                'host' => env('DB_ORACLE_HOST', '127.0.0.1'),
                'port' => env('DB_ORACLE_PORT', '3306'),
                'service_name' => env('DB_ORACLE_SERVICE_NAME', 'forge'),
                'database' => env('DB_ORACLE_DATABASE', 'forge'),
                'username' => env('DB_ORACLE_USERNAME', 'forge'),
                'password' => env('DB_ORACLE_PASSWORD', ''),
                'charset' => 'utf8',
                'prefix' => '',
            ],
            'NETSP' => [
                'driver' => 'oracle',
                'host' => env('DB_ORACLE_HOST', '127.0.0.1'),
                'port' => env('DB_ORACLE_PORT', '3306'),
                'service_name' => env('DB_ORACLE_SERVICE_NAME', 'forge'),
                'database' => env('DB_ORACLE_DATABASE', 'forge'),
                'username' => env('DB_ORACLE_USERNAME', 'forge'),
                'password' => env('DB_ORACLE_PASSWORD', ''),
                'charset' => 'utf8',
                'prefix' => '',
            ],



        //BANCO CTV
            'CTV' => [
                'driver' => 'oracle',
                'host' => '10.28.20.31',
                'port' => '1521',
                'service_name' => 'DCTV1HP',
                'database' => 'DCTV1HP',
                'username' => 'A_T1860897',
                'password' => 'A_T1860897',
                'charset' => 'utf8',
                'prefix' => '',
            ],

        /*
         * 'BASE': 'CTV',
         * 'USUARIO': 'OPS$OGSPTB',
         * 'PASSWORD': 'Brasil29',
         *
        'CLARO' => [
            'driver' => 'oracle',
            'host' => env('DB_ORACLE_HOST', '127.0.0.1'),
            'port' => env('DB_ORACLE_PORT', '3306'),
            'service_name' => env('DB_ORACLE_SERVICE_NAME', 'forge'),
            'database' => 'CTV',
            'username' => env('DB_ORACLE_USERNAME', 'forge'),
            'password' => env('DB_ORACLE_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

*/

//        'ora_base_net_abc' => [
//            'driver' => 'oracle',
//            'host' => 'ABC.netdatacenter.corp',
//            'port' => '1521',
//            'database' => 'ABC.NET',
//            'service_name' => 'abc',
//            'username' => 'OPS$OGSPTB',
//            'password' => 'Brasil29',
//            'charset' => 'utf8',
//            'prefix' => '',
//        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'predis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'predis'),
        ],

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

];
