<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'tns'            => env('DB_TNS', ''),
        'host'           => env('DB_HOST', ''),
        'port'           => env('DB_PORT', '1521'),
        'database'       => env('DB_DATABASE', ''),
        'username'       => env('DB_USERNAME', ''),
        'password'       => env('DB_PASSWORD', ''),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_EDITION', 'ora$base'),
        'server_version' => env('DB_SERVER_VERSION', '11g'),
    ],
    'oracle_base_net_abc' => [
        'driver'        => 'oracle',
        'tns'           => '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(host = ABC.netdatacenter.corp)(PORT = 1521))(CONNECT_DATA = (SID = abc)))',
        //'host'          => env('DB_HOST', ''),
        //'port'          => env('DB_PORT', '1521'),
        //'database'      => env('DB_DATABASE', ''),
        'username' => 'OPS$OGSPTB',
        'password' => 'Brasil29',
        'charset'       => 'AL32UTF8',
        'prefix'        => '',
        'edition'       => 'ora$base',
    ],
];