<?php

return [

    // Traduções homologadas
    'synchronism_types' => [
		'1' => 'Sincronismo',
		'2' => 'Ongoing',
		'3' => 'Sem Tratativa'
    ],

	// Grupo Resolvedor Defult para importação
	'solver_group_id' => '1',

	//
	'offender_relationships' => [
		'synchronism'
	],

	// Tipos de ações do historico
	'history_types' => [
		'I' => 'Inserção',
		'E' => 'Edição',
		'R' => 'Exclusão',
        'A' => 'Ativação',
        'N' => 'Inativação'
	],

    //
    'breadcrumb' => [
        'abrir-solicitacao' => 'Abrir Solicitação',
        'correcao-status-contrato' => 'Status Contrato',
        'envia-ativacao' => 'Envia Ativação',
        'excluir-area-local' => 'Excluir Área Local',
        'incluir-area-local' => 'Incluir Área Local',
        'incluir-area-local-ctv' => 'Incluir Área Local CTV',
        'insere-area-local' => 'Incluir Área Local',
        'historico-de-portab' => 'Histórico de Portabilidade',
        'mudanca-endereco' => 'Mudança de Endereço',
        'mudanca-de-endereco' => 'Mudança de Endereço',
        'nao-possui' => 'Não Possui',
        'portabilidade-ctv' => 'Portabilidade CTV',
        'portabilidade-intrinseca' => 'Portabilidade Intrínseca',
        'retirada-de-ptodesc-opcao' => 'Retirada de Pto. / Desc. Opção',
        'reenviar-hit' => 'Reenviar HIT',
        'reenvio-hit-de' => 'Reenvio HIT DE',
        'retirada-ponto-opcao' => 'Retirada de Pto. / Desc. Opção',
        'solicitacoes' => 'Solicitações',
        'troca-de-numero' => 'Troca de Número',
        'troca-fqdn' => 'Troca FQDN',

    ],

];
