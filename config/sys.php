<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTML - VIEW
    |--------------------------------------------------------------------------
    */

    // Título
    'title' => 'UNI.T-S - Portal OnGoing',

    // Rodapé
    'footer' => date('Y').' &copy; ART IT - <i>Intelligent Technology.</i>',

    /*
    |--------------------------------------------------------------------------
    | PARAMETROS
    |--------------------------------------------------------------------------
    */

    'parameters' => [

        // ID da empresa gênesis
        'company_id' => 1,

        // ID  do usuário root (admin) do sistema
        'admin_id' => 1,

        // USUARIO de acesso root (admin) do sistema
        'admin_user' => 'admin',

    ],

    /*
    |--------------------------------------------------------------------------
    | GRUPOS
    |--------------------------------------------------------------------------
    */

    // Ids dos grupos fixos do sistema, que em alguns módulos
    // é necessário para definir regras de negócios
    'groups' => [



    ],

    /*
    |--------------------------------------------------------------------------
    | CONFIGURAÇÃO COMPANY
    |--------------------------------------------------------------------------
    */

    // Permite o sistema ser multiempresa
    // 0 - Somente uma empresa
    // 1 - 'n' empresas
    'show_company' => 1,

    /*
    |--------------------------------------------------------------------------
    | CONFIGURAÇÃO LOCALE
    |--------------------------------------------------------------------------
    */

    // Permite ou não trocar o locale da aplicação
    // 1 - Permite
    // 2 - Não permite
    'show_locale' => 0,

    // Traduções homologadas
    'locale' => [
        'pt-br' => ['initials' => 'BR', 'name_en' => 'Brazil'  , 'name_pt-br' => 'Brasil', 'img'=>'br'],
        'en'    => ['initials' => 'US', 'name_en' => 'English' , 'name_pt-br' => 'Inglês', 'img'=>'us'],
    ],

];
