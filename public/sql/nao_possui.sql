DECLARE
  -- Variaveis dos parametros de input:
  /* IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '&COD_CIDADE';
  IN_NUM_CONTRATO PROD_JD.SN_CONTRATO.NUM_CONTRATO%TYPE := '&NUM_CONTRATO';
  IN_DDD_TERMINAL PROD_JD.SN_PORTABILIDADE.DDD_TELEFONE_VOIP%TYPE := '&DDD_TERMINAL';
  IN_NUM_TERMINAL PROD_JD.SN_PORTABILIDADE.NUM_TELEFONE_VOIP%TYPE := '&NUM_TERMINAL'; */

  IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '[OPERADORA]';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := [CONTRATO];
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('[TERMINAL]', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('[TERMINAL]', 3, 8);

  VBASE                 PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VSESSAO               PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR310';
  VAUX_ID_TRANSACAO_NET PROD_JD.PP_VOIPHIT.ID_TRANSACAO_NET%TYPE;
  V_REGS_PROCESSADOS    INTEGER := 0;
  VERRO1                VARCHAR2(1000);
  VQTD                  INTEGER := 0;
  VCONTR                INTEGER := 0;
  VEXISTEQRT            INTEGER := 0;
  P_NETFONE EXCEPTION;
  P_NETFONE1 EXCEPTION;
  P_NETFONE2 EXCEPTION;
  VPORTADO varchar(2);

  VIDOFENSOR   NUMBER(3) := 000;
  VAUXDESCOCOR VARCHAR2(400) := ';OCORRENCIA CRIADA POR TI - ONGOING.PARA ACERTO DO STATUS EMBRATEL DESSE CONTRATO PARA "NAO POSSUI"';

  /* ========================================================================= *
                           CÓDIGOS DO SCRIPT
  * ========================================================================= */
  --DETALHAR INSTRUÇÕES PARA ESTRUTURAÇÃO E MODELAGEM DE CÓDIGOS

  /* ========================================================================= *
                             ROTINAS DE LOG
  * ========================================================================= */
  --DETALHAR INSTRUÇÕES PARA UTILIZAÇÃO DE ROTINAS DE LOG  E UTILIZAR ROTINA ABAIXO

  PROCEDURE PRC_GERA_OCORRENCIA(P_COD_OPERADORA IN VARCHAR2,
                                P_NUM_CONTRATO  IN SN_CONTRATO.NUM_CONTRATO%TYPE,
                                P_ID_OFENSOR    IN NUMBER,
                                P_OBS           IN VARCHAR2) IS
  
    VCHECKSUM         NUMBER;
    VIDOCORRENCIA     SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235;
    VNOMEINFORMANTE   SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDT_OCORRENCIA    SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            SN_OCORRENCIA.ID_USR%TYPE := 'PROD_JD';
    VSIT              SN_OCORRENCIA.SIT%TYPE := 1;
    VDTRETORNO        SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5;
    VIDASSINANTE      SN_ASSINANTE.ID_ASSINANTE%TYPE;
  
  BEGIN
  
    SELECT SC.ID_ASSINANTE
      INTO VIDASSINANTE
      FROM SN_CIDADE_OPERADORA SCO, SN_CONTRATO SC
     WHERE SCO.CID_CONTRATO = SC.CID_CONTRATO
       AND SCO.COD_OPERADORA = P_COD_OPERADORA
       AND SC.NUM_CONTRATO = P_NUM_CONTRATO;
  
    SELECT P_ID_OFENSOR + TO_CHAR(SYSDATE, 'MMDD')
      INTO VCHECKSUM
      FROM DUAL;
  
    PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                       VIDTIPOOCORRENCIA,
                                       VIDASSINANTE,
                                       VNOMEINFORMANTE || VCHECKSUM,
                                       VTELINFORMANTE,
                                       VDT_OCORRENCIA,
                                       VIDUSR,
                                       VSIT,
                                       VDTRETORNO,
                                       VIDORIGEM,
                                       P_ID_OFENSOR || P_OBS);
  
  END PRC_GERA_OCORRENCIA;

  --Procedure p/ Gravação de Volumetria
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO, -- SESSAO
       PDATA, -- DATA
       PBASE, -- CAMPO1
       PMENSAGEM, -- CAMPO2
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  --CHAMADAS AOS CODIGOS E ROTINAS DEFINIDAS

BEGIN

  --DBMS_OUTPUT.DISABLE; --ENABLE(NULL);

  SELECT UPPER(NM_ALIAS)
    INTO VBASE
    FROM PROD_JD.SN_CIDADE_BASE
   WHERE COD_OPERADORA = (SELECT COD_OPERADORA
                            FROM PROD_JD.SN_CIDADE_OPERADORA
                           WHERE ROWNUM < 2);

  -- GRAVA LOG DE INICIO DE PROCESSO
  GRAVALOG(VSESSAO,
           SYSDATE,
           VBASE,
           'LOG-INICIO DE PROCESSAMENTO',
           '',
           '',
           '',
           '',
           '');

  --DBMS_OUTPUT.disable;
  VCONTR := 0;

  FOR CR_LOOP IN (SELECT SCO.COD_OPERADORA,
                         STV.NUM_CONTRATO,
                         SRSCTS.ID_STATUS_CONTRATO,
                         SRSCTS.DT_INI DT_INI_STATUS_ATUAL,
                         STV.CID_CONTRATO,
                         STV.DDD_TELEFONE_VOIP,
                         STV.NUM_TELEFONE_VOIP,
                         STV.ID_PONTO
                    FROM PROD_JD.SN_CIDADE_OPERADORA    SCO,
                         PROD_JD.SN_TELEFONE_VOIP       STV,
                         PROD_JD.SN_REL_STATUS_CTO_SERV SRSCTS,
                         PROD_JD.SN_OC                  OC
                   WHERE SCO.CID_CONTRATO = SRSCTS.CID_CONTRATO
                     AND STV.NUM_CONTRATO = SRSCTS.NUM_CONTRATO
                     AND STV.CID_CONTRATO = SRSCTS.CID_CONTRATO
                     AND SRSCTS.ID_STATUS_CONTRATO IN ('29', '20') -- Status EBT Desconectado Opção
                     AND SRSCTS.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR')
                     AND STV.ID_STATUS_TELEFONE_VOIP = 'C'
                     AND STV.FC_NUMERO_PORTADO = 'S'
                     AND STV.DT_FIM =
                        -- Alteração para contemplar casos com voip fechada
                         (SELECT MAX(TV1.DT_FIM)
                            FROM PROD_JD.SN_TELEFONE_VOIP TV1
                           WHERE TV1.DDD_TELEFONE_VOIP =
                                 STV.DDD_TELEFONE_VOIP
                             AND TV1.NUM_TELEFONE_VOIP =
                                 STV.NUM_TELEFONE_VOIP)
                     AND OC.COD_OC = -- Verifica qual a ultima OC do ponto (Habilitar ou Desabilitar)
                         (SELECT MAX(OC1.COD_OC)
                            FROM SN_OC OC1
                           WHERE OC1.ID_TIPO_OC IN (48, 49)
                             AND OC1.ID_TIPO_FECHAMENTO = 1
                             AND OC1.ID_PONTO = STV.ID_PONTO)
                     AND OC.ID_TIPO_OC = 49 -- Verifica se a ultima OC é de Desabilitar número
                        
                        /*Parametros de INPUT*/
                     AND STV.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
                     AND STV.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
                     AND STV.NUM_CONTRATO = IN_NUM_CONTRATO) LOOP
  
    BEGIN
    
      VCONTR := 1;
    
      FOR C_STATUS IN (SELECT RPP.*
                         FROM SN_REL_PONTO_PRODUTO RPP, SN_PRODUTO PROD
                        WHERE RPP.ID_PRODUTO = PROD.ID_PRODUTO
                          AND PROD.ID_CARACTERISTICA = 4
                          AND PROD.ID_TIPO_PRODUTO = 1
                          AND RPP.DT_FIM =
                              TO_DATE('30/12/2049', 'DD/MM/RRRR')
                          AND RPP.NUM_CONTRATO = CR_LOOP.NUM_CONTRATO
                          AND RPP.CID_CONTRATO = CR_LOOP.CID_CONTRATO) LOOP
      
        BEGIN
        
          --DBMS_OUTPUT.PUT_LINE(C_STATUS.ID_PONTO);
        
          UPDATE PROD_JD.SN_PONTO_HISTORICO
             SET DT_FIM = TRUNC(SYSDATE), TM_ID = NULL
           WHERE ID_PONTO = C_STATUS.ID_PONTO
             AND INSTALADO = 1
             AND DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR');
        
          UPDATE PROD_JD.SN_REL_PONTO_PRODUTO
             SET DT_FIM = TRUNC(SYSDATE)
           WHERE ID_PONTO = C_STATUS.ID_PONTO
             AND INSTALADO = 1
             AND DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR');
        
          UPDATE SN_REL_PONTO_PRODUTO_PRECO
             SET DT_FIM = TRUNC(SYSDATE)
           WHERE ID_PONTO = C_STATUS.ID_PONTO
             AND DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR');
        
          COMMIT;
        
        END;
      END LOOP;
    
      BEGIN
        --DBMS_OUTPUT.PUT_LINE('PASSOU DO FOR');
      
        UPDATE SN_REL_STATUS_CTO_SERV
           SET DT_FIM = SYSDATE
         WHERE NUM_CONTRATO = CR_LOOP.NUM_CONTRATO
           AND CID_CONTRATO = CR_LOOP.CID_CONTRATO
           AND DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR')
           AND ID_STATUS_CONTRATO = CR_LOOP.ID_STATUS_CONTRATO;
        COMMIT;
      
        INSERT INTO SN_REL_STATUS_CTO_SERV
        VALUES
          (CR_LOOP.NUM_CONTRATO,
           CR_LOOP.CID_CONTRATO,
           5,
           2,
           SYSDATE,
           TO_DATE('30/12/2049', 'DD/MM/RRRR'),
           NULL,
           SYSDATE);
      
        UPDATE SN_CONTRATO
           SET NUM_CONTRATO_EMBRATEL = NULL
         WHERE NUM_CONTRATO = CR_LOOP.NUM_CONTRATO
           AND CID_CONTRATO = CR_LOOP.CID_CONTRATO;
      
        COMMIT;
      
        PRC_GERA_OCORRENCIA(P_COD_OPERADORA => CR_LOOP.COD_OPERADORA,
                            P_NUM_CONTRATO  => CR_LOOP.NUM_CONTRATO,
                            P_ID_OFENSOR    => VIDOFENSOR,
                            P_OBS           => VAUXDESCOCOR);
      
        GRAVALOG(VSESSAO,
                 SYSDATE,
                 VBASE,
                 'OK',
                 CR_LOOP.COD_OPERADORA || '/' || CR_LOOP.NUM_CONTRATO,
                 CR_LOOP.DDD_TELEFONE_VOIP || CR_LOOP.NUM_TELEFONE_VOIP,
                 '',
                 CR_LOOP.ID_PONTO,
                 'ponto / status contrato tratados');
        RAISE P_NETFONE;
      
      END;
    END;
  
    COMMIT;
  END LOOP;
  begin
  
    SELECT UPPER(V.FC_NUMERO_PORTADO)
      INTO VPORTADO
      FROM PROD_JD.SN_TELEFONE_VOIP V
     WHERE V.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
       AND V.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
       AND V.FC_NUMERO_PORTADO IN ('N')
       AND V.DT_FIM =
           (SELECT MAX(TV1.DT_FIM)
              FROM PROD_JD.SN_TELEFONE_VOIP TV1
             WHERE TV1.DDD_TELEFONE_VOIP = V.DDD_TELEFONE_VOIP
               AND TV1.NUM_TELEFONE_VOIP = V.NUM_TELEFONE_VOIP);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VPORTADO := NULL;
  end;

  IF (VPORTADO = 'N') THEN
  
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             'ERRO',
             IN_COD_CIDADE || '/' || IN_NUM_CONTRATO,
             IN_DDD_TERMINAL || IN_NUM_TERMINAL,
             '',
             '',
             'Terminal informado trata se de um netfone');
  
    RAISE P_NETFONE2;
  
  END IF;

  IF (VCONTR = 0) THEN
  
    --DBMS_OUTPUT.PUT_LINE('GRAVA LOG SEM CONTRATO');
  
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             'ERRO',
             IN_COD_CIDADE || '/' || IN_NUM_CONTRATO,
             IN_DDD_TERMINAL || IN_NUM_TERMINAL,
             '',
             '',
             'contrato nao se encontra no cenario de nao possui');
  
    RAISE P_NETFONE1;
  
  END IF;

EXCEPTION
  WHEN P_NETFONE THEN
    RAISE_APPLICATION_ERROR(-20010,
                            'Sua solicitação foi atendida com sucesso. O status do contrato foi alterado para "não possui" conforme solicitado!');
  WHEN P_NETFONE2 THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'O terminal inserido é um NETFONE e deve ser aberto um chamado através da Ferramenta Suporte OS/OC.');
  
  WHEN P_NETFONE1 THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Não foi possível prosseguir com o atendimento do contrato informado, pois esta opção deve ser utilizada somente quando o contrato estiver status "desconexão por opção/inadimplência". usuário deseja alteração de status do contrato para "não possui". favor acessar a ferramenta netsms > aba geral e verificar se o contrato consta nesse cenário.');
  
END;
