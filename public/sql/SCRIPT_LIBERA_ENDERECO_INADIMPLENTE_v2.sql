-- ==============================================================================================================================
-- Projeto: Item 5 Liberação de Endereço Inadimplente
-- Tag: SMSP_84336_NI_001_CI_002
-- Responsável: Lucas Guadagnim
-- Função :  Script paliativo - responsavel por liberar endereços inadimplentes
-- Executar como : PROD_JD
-- ==============================================================================================================================
/*
#*******************************************************************************************************************************#
# $HeadURL:: http://svn.artit.com.br/net/branches/b_ongoing/Unit-S/back-end/sql/SCRIPT_LIBERA_ENDERECO_INADIMPLENTE_v2.sql   $: #
# $Id:: SCRIPT_LIBERA_ENDERECO_INADIMPLENTE_v2.sql 4233 2015-06-24 17:47:04Z mbattistini                                     $: #
# $Rev:: 4233                                                                                       $:  Revision of last commit #
# $Author:: mbattistini                                                                               $:  Author of last commit #
# $Date:: 2015-06-24 14:47:04 -0300 (Wed, 24 Jun 2015)                                                  $:  Date of last commit #
#**********************************************************************************************************************#
*/
DECLARE
   -- INICIO: Parametros para Preenchimento ------------------------------------------------------------------------------------
   VIDPROPOSTA PROD_JD.SN_PROP_ENDER_INADIMPLENTE.ID_PROPOSTA_ASSINANTE%TYPE := [PROPOSTA]; -- numero da proposta
   VCNPJ       PROD_JD.SN_PROP_ENDER_INADIMPLENTE.NR_CPF_CNPJ%TYPE := '[CNJP]'; -- numero do cnpj
   -- TERMINO ------------------------------------------------------------------------------------------------------------------
   VIDEDIFICACAO PROD_JD.SN_PROP_ENDER_INADIMPLENTE.ID_EDIFICACAO%TYPE := 0;
   VRETORNO      NUMBER(1) := 0;
   VMSGERRO      VARCHAR2(4000) := '0';
BEGIN
   DBMS_OUTPUT.DISABLE;
   -- Retorna Edificacao
   BEGIN
      SELECT ENDP.ID_EDIFICACAO
        INTO VIDEDIFICACAO
        FROM NETSALES.CP_ENDERECO_PROSPECT ENDP, NETSALES.CP_PROPOSTA CPA
       WHERE CPA.ID_PROPOSTA = VIDPROPOSTA
         AND ENDP.ID_EDIFICACAO IS NOT NULL
         AND ENDP.ID_PROSPECT = CPA.ID_PROSPECT
         AND ROWNUM < 2;
   EXCEPTION
      WHEN OTHERS THEN
         VIDEDIFICACAO := 0;
   END;

   -- Valida a Liberacao
   --VRETORNO := PROD_JD.PSN_ASSINANTE.PRSN_VALIDA_CPF_PROPOSTA(VIDPROPOSTA, VCNPJ, VIDEDIFICACAO);
   VRETORNO := 1;

   IF VRETORNO = 0 THEN
      VMSGERRO := 'O CPF informado não corresponde ao código de proposta';
   ELSIF VRETORNO = 2 THEN
      VMSGERRO := 'O Endereço selecionado não corresponde ao código de proposta';
   ELSE
      -- Libera o Endereco
      DELETE FROM PROD_JD.SN_ENDER_INADIMPLENTE WHERE ID_EDIFICACAO = VIDEDIFICACAO;
      -- Registra a Proposta e o CPF/CNPJ da liberacao
      INSERT INTO PROD_JD.SN_PROP_ENDER_INADIMPLENTE
      VALUES
         (PROD_JD.SQ_PROP_ENDER_INADIMPLENTE.NEXTVAL, VIDEDIFICACAO, TRUNC(SYSDATE), VIDPROPOSTA, VCNPJ);
      COMMIT;
   END IF;

   IF VMSGERRO <> '0' THEN
      DBMS_OUTPUT.PUT_LINE(VMSGERRO);
      RAISE_APPLICATION_ERROR(-20000, VMSGERRO);
   END IF;
END;
