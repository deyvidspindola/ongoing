DECLARE
  /* CONSTANTES, DEFINIDAS PELO USUARIO */
  cCodOperadora CONSTANT VARCHAR2(100) :=  '[OPERADORA]';
  cNumContrato  CONSTANT VARCHAR2(100) :=  '[CONTRATO]';
  cTerminal     CONSTANT VARCHAR2(100) :=  '[TERMINAL]';

  /* EXCEPTIONS */
  E_TERM_NAO_PORTADO EXCEPTION;
  E_TERM_NAO_PORT_DOADO EXCEPTION;
  E_NAO_EXISTE_SOLIC_CANC EXCEPTION;
  E_RET_EMTA_SEM_DESCONEXAO EXCEPTION;
  E_OC_NAO_ENCONTRADA EXCEPTION;
  E_OC_DUPLICADA EXCEPTION;
  E_SOLIC_DESAB_EXECUTADA EXCEPTION;
  E_HITS_NAO_ENCONTRADOS EXCEPTION;
  E_HIT_DE_NAO_ENCONTRADO EXCEPTION;
  E_STATUS_HIT_INVALIDO EXCEPTION;
  E_STATUS_HIT_DESCONHECIDO EXCEPTION;
  E_REENVIADO_HIT_AGUARDAR EXCEPTION;
  E_FILA_TRAVADA_SEM_MOTIVO EXCEPTION;
  E_HIST_PORTAB_NOT_FOUND EXCEPTION;
  E_OPER_NAO_ENCONTRADA EXCEPTION;
  E_ERRO_OCORRENCIA EXCEPTION;
  E_NAO_ENCONTRADO_HIT_ANTERIOR EXCEPTION;
  E_EXISTE_DESC_RETIR_NAO_EXEC EXCEPTION;
  E_HIT_REENVIADO              EXCEPTION;

  /* TYPES E SUAS VARIAVEIS */
  TYPE rSolicitacoes IS RECORD(
    id_solic        PROD_JD.SN_SOLICITACAO_ASS.id_solicitacao_ass%TYPE,
    id_tipo_solic   PROD_JD.SN_TIPO_SOLIC.id_tipo_solic%TYPE,
    desc_tipo_solic PROD_JD.SN_TIPO_SOLIC.descricao%TYPE,
    desc_tipo_fech  PROD_JD.SN_TIPO_FECHAMENTO.descricao%TYPE,
    ponto           PROD_JD.SN_PONTO_HISTORICO.identificacao%TYPE,
    id_tipo_fech    PROD_JD.SN_SOLICITACAO_ASS.id_tipo_fechamento%TYPE,
    dt_cadastro     PROD_JD.SN_SOLICITACAO_ASS.dt_cadastro%TYPE,
    dt_baixa        PROD_JD.SN_SOLICITACAO_ASS.dt_baixa%TYPE,
    usuario         PROD_JD.SN_SOLICITACAO_ASS.usr_baixa%TYPE);
  TYPE rHits IS RECORD(
    id_hit                  PROD_JD.PP_VOIPHIT.id_hit%TYPE,
    tipo_registro           PROD_JD.PP_VOIPHIT.tipo_registro%TYPE,
    status_arquivo          PROD_JD.PP_VOIPHIT.status_arquivo%TYPE,
    fc_processamento_online PROD_JD.PP_VOIPHIT.fc_processamento_online%TYPE,
    cod_oc                  PROD_JD.PP_VOIPHIT.cod_oc%TYPE);
  TYPE rHistPortab IS RECORD(
    id_hist_status_portab   PROD_JD.SN_HIST_STATUS_PORTAB.id_hist_status_portab%TYPE,
    id_status_portabilidade PROD_JD.SN_HIST_STATUS_PORTAB.id_status_portabilidade%TYPE,
    id_portabilidade        PROD_JD.SN_PORTABILIDADE.id_portabilidade%TYPE,
    tp_portabilidade        PROD_JD.SN_PORTABILIDADE.tp_portabilidade%TYPE);

  TYPE tSolicitacoes IS TABLE OF rSolicitacoes;
  TYPE tHits IS TABLE OF rHits;
  TYPE tHistPortab IS TABLE OF rHistPortab;
  TYPE tVoipHit IS TABLE OF PROD_JD.PP_VOIPHIT%ROWTYPE;

  vSolicitacoes tSolicitacoes;
  vHits         tHits;
  vHistPortab   tHistPortab;
  vVoipHit      tVoipHit;

  /* FLAGS */
  vTerminalPortado BOOLEAN;
  vTerminalDoado   BOOLEAN;
  vOcFoiExecutada  BOOLEAN;

  /* VARIAVEIS */
  vCodOC           PROD_JD.SN_OC.cod_oc%TYPE;
  vIdPortabilidade PROD_JD.SN_PORTABILIDADE.id_portabilidade%TYPE;
  vDadosException  VARCHAR2(200);
  vCidContrato     PROD_JD.SN_CIDADE_BASE.CID_CONTRATO%TYPE;
  vSessao          PROD_JD.SO_ORDENA.SESSAO%TYPE := 'AUTO_TR_348';
  vBase            PROD_JD.SN_CIDADE_BASE.NM_ALIAS%TYPE;

  PROCEDURE InsereOcorrencia(iIdAssinante IN PROD_JD.SN_CONTRATO.id_assinante%TYPE) IS
    vIdOfensor    VARCHAR(20) := '';
    vCalcChecksum VARCHAR(20);

    -- Varáveis necessárias para gerar ocorrência
    vIdOcorrencia     PROD_JD.SN_OCORRENCIA.id_ocorrencia%TYPE;
    vIdTipoOcorrencia PROD_JD.SN_TIPO_OCORRENCIA.id_tipo_ocorrencia%TYPE := 235;
    vNomeInformante   PROD_JD.SN_OCORRENCIA.nome_informante%TYPE := 'TI';
    vTelInformante    PROD_JD.SN_OCORRENCIA.tel_informante%TYPE := NULL;
    vDataOcorrencia   PROD_JD.SN_OCORRENCIA.dt_ocorrencia%TYPE := SYSDATE;
    vIdUser           PROD_JD.SN_OCORRENCIA.id_usr%TYPE := USER;
    vSit              PROD_JD.SN_OCORRENCIA.sit%TYPE := 1; --FECHADA
    vDataRetorno      PROD_JD.SN_OCORRENCIA.dt_retorno%TYPE := NULL;
    vIdOrigem         PROD_JD.SN_ORIGEM_OCORRENCIA.id_origem%TYPE := 5; --INTERNA
    vObs              PROD_JD.SN_OCORRENCIA.obs%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): TELEFONE ' ||
                                                        cTerminal ||
                                                        ' APRESENTANDO ERRO "PORTABILIDADE DESTE NÚMERO EM ANDAMENTO". FAVOR VERIFICAR SE EXISTE SOLICITAÇÃO DE DESCONEXÃO
     E DESABILITAR NÚMERO EXECUTADOS PARA O PONTO FONE REFERENTE AO TELEFONE ' ||
                                                        cTerminal || '';

  BEGIN
    SELECT TO_NUMBER(vIdOfensor) + TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO vCalcChecksum
      FROM DUAL;

    PROD_JD.PSN_OCORRENCIA.SsnIncluiOcorrencia(vIdOcorrencia,
                                               vIdTipoOcorrencia,
                                               iIdAssinante,
                                               vNomeInformante ||
                                               vCalcChecksum,
                                               vTelInformante,
                                               vDataOcorrencia,
                                               vIdUser,
                                               vSit,
                                               vDataRetorno,
                                               vIdOrigem,
                                               vIdOfensor || vObs);
    COMMIT;
  END InsereOcorrencia;

  PROCEDURE GravaLog(pSessao   PROD_JD.SO_ORDENA.sessao%TYPE,
                     pData     DATE,
                     pBase     PROD_JD.SO_ORDENA.campo1%TYPE,
                     pMensagem PROD_JD.SO_ORDENA.campo2%TYPE,
                     pCampo3   PROD_JD.SO_ORDENA.campo3%TYPE DEFAULT Null,
                     pCampo4   PROD_JD.SO_ORDENA.campo4%TYPE DEFAULT Null,
                     pCampo5   PROD_JD.SO_ORDENA.campo5%TYPE DEFAULT Null,
                     pCampo6   PROD_JD.SO_ORDENA.campo6%TYPE DEFAULT Null,
                     pCampo7   PROD_JD.SO_ORDENA.campo7%TYPE DEFAULT Null,
                     pCampo8   PROD_JD.SO_ORDENA.campo8%TYPE DEFAULT Null,
                     pCampo9   PROD_JD.SO_ORDENA.campo9%TYPE DEFAULT Null,
                     pCampo10  PROD_JD.SO_ORDENA.campo10%TYPE DEFAULT Null,
                     pCampo11  PROD_JD.SO_ORDENA.campo11%TYPE DEFAULT Null) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (sessao,
       data,
       campo1,
       campo2,
       campo3,
       campo4,
       campo5,
       campo6,
       campo7,
       campo8,
       campo9,
       campo10,
       campo11)
    VALUES
      (pSessao,
       pData,
       pBase,
       pMensagem,
       pCampo3,
       pCampo4,
       pCampo5,
       pCampo6,
       pCampo7,
       pCampo8,
       pCampo9,
       pCampo10,
       pCampo11);
    COMMIT;
  END GravaLog;

  FUNCTION ChecaTerminalPortado(oIdPortabilidade OUT PROD_JD.SN_PORTABILIDADE.id_portabilidade%TYPE)
    RETURN BOOLEAN IS
  BEGIN
    SELECT MAX(id_portabilidade)
      INTO oIdPortabilidade
      FROM PROD_JD.SN_PORTABILIDADE SP
     WHERE SP.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND SP.num_telefone_voip = Substr(cTerminal, 3, 8);

    IF oIdPortabilidade IS NULL THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
  END;

  FUNCTION ChecaTerminalDoado RETURN BOOLEAN IS
    vStatusVLR PROD_JD.TBSMS_ARQUIVO_VLR.st_vlr_atual%TYPE;
  BEGIN
    SELECT st_vlr_atual
      INTO vStatusVLR
      FROM PROD_JD.TBSMS_ARQUIVO_VLR ARQ
     WHERE ARQ.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND ARQ.num_telefone_voip = Substr(cTerminal, 3, 8);

    RETURN vStatusVLR = 'BPT';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
    WHEN OTHERS THEN
      RAISE;
  END ChecaTerminalDoado;

  PROCEDURE GeraHITDesconexao IS
    vIdTransacao NUMBER;
  BEGIN

    SELECT HIT.* BULK COLLECT
      INTO vVoipHit
      FROM PROD_JD.PP_VOIPHIT HIT
     WHERE HIT.id_hit =
           (SELECT MAX(HIT_IN.id_hit)
              FROM PROD_JD.PP_VOIPHIT HIT_IN
             WHERE HIT_IN.ddd_telefone_contr = Substr(cTerminal, 1, 2)
               AND HIT_IN.num_telefone_contr = Substr(cTerminal, 3, 8)
               AND HIT_IN.tipo_registro = 'IN');

    SELECT PROD_JD.SEQ_ID_TRANSACAO_NET.NextVal
      INTO vIdTransacao
      FROM DUAL;

    INSERT INTO PROD_JD.PP_VOIPHIT
      (id_hit, --1
       cod_oc, --2
       tipo_registro_header, --3
       data_registro_header, --4
       hora_registro_header, --5
       tipo_registro, --6
       id_transacao_net, --7
       tipo_registro_detalhe, --8
       cod_operadora_cliente, --9
       num_cliente, --10
       nome_cliente, --11
       cpf_cnpj_passaporte, --12
       tipo_cliente, --13
       ddd_telefone_contr, --14
       num_telefone_contr, --15
       data_evento, --16
       nome_eqpto_mta_fqdn, --17
       porta_equipamento, --18
       tipo_registro_trailler, --19
       qtde_registro_trailler, --20
       nome_arquivo, --21
       status_arquivo, --22
       usr_voiphit, --23
       fc_numero_portado, --24
       fc_interceptado --25
       )
    VALUES
      (PROD_JD.SEQ_ID_VOIPHIT.NextVal, --1
       '-4', --2
       vVoipHit(1).tipo_registro_header, --3
       To_Number(To_Char(SYSDATE, 'YYYYMMDD')), --4
       To_Number(To_Char(SYSDATE, 'HH24MISS')), --5
       'DE', --6
       vIdTransacao, --7
       vVoipHit(1).tipo_registro_detalhe, --8
       vVoipHit(1).cod_operadora_cliente, --9
       vVoipHit(1).num_cliente, --10
       vVoipHit(1).nome_cliente, --11
       vVoipHit(1).cpf_cnpj_passaporte, --12
       vVoipHit(1).tipo_cliente, --13
       vVoipHit(1).ddd_telefone_contr, --14
       vVoipHit(1).num_telefone_contr, --15
       To_Number(To_Char(SYSDATE, 'YYYYMMDD')), --16
       vVoipHit(1).nome_eqpto_mta_fqdn, --17
       vVoipHit(1).porta_equipamento, --18
       vVoipHit(1).tipo_registro_trailler, --19
       vVoipHit(1).qtde_registro_trailler, --20
       '0', --21
       'PND', --22
       'PROD_JD', --23
       vVoipHit(1).fc_numero_portado, --24
       vVoipHit(1).fc_interceptado); --25

    COMMIT;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE E_NAO_ENCONTRADO_HIT_ANTERIOR;
    WHEN OTHERS THEN
      RAISE;
  END;

  PROCEDURE ReenviaHITDesconexao(iHitID IN PROD_JD.PP_VOIPHIT.id_hit%TYPE) IS
    vIdTransacao NUMBER;
  BEGIN
    UPDATE PROD_JD.PP_VOIPHIT H
       SET H.status_arquivo = 'RNV'
     WHERE H.id_hit = iHitID;

    SELECT HIT.* BULK COLLECT
      INTO vVoipHit
      FROM PROD_JD.PP_VOIPHIT HIT
     WHERE HIT.id_hit = iHitID;

    SELECT PROD_JD.SEQ_ID_TRANSACAO_NET.NextVal
      INTO vIdTransacao
      FROM DUAL;

    INSERT INTO PROD_JD.PP_VOIPHIT
      (id_hit, --1
       cod_oc, --2
       tipo_registro_header, --3
       data_registro_header, --4
       hora_registro_header, --5
       filler_header, --6
       tipo_registro, --7
       id_transacao_net, --8
       tipo_registro_detalhe, --9
       cod_operadora_cliente, --10
       num_cliente, --11
       cod_operadora_cliente_novo, --12
       num_cliente_novo, --13
       nome_cliente, --14
       cpf_cnpj_passaporte, --15
       tipo_cliente, --16
       tel_residencial_cliente, --17
       tel_celular_cliente, --18
       tipo_logradouro_inst, --19
       nome_logradouro_inst, --20
       num_logradouro_inst, --21
       ref_endereco_inst, --22
       compl_endereco_inst, --23
       bairro_inst, --24
       cep_inst, --25
       cidade_inst, --26
       uf_inst, --27
       inscr_estadual_inst, --28
       nome_logradouro_cobr, --29
       bairro_cobr, --30
       cep_cobr, --31
       cidade_cobr, --32
       uf_cobr, --33
       ddd_telefone_contr, --34
       num_telefone_contr, --35
       ddd_telefone_contr_novo, --36
       num_telefone_contr_novo, --37
       indic_contratacao_cliente, --38
       indic_cobr_inst, --39
       indic_fig_ltelefonica, --40
       descr_fig_ltelefonica, --41
       plano_telefonia_local, --42
       plano_ldistancia_nacional, --43
       plano_ldistancia_internacional, --44
       data_evento, --45
       dia_vencto_cliente, --46
       nome_eqpto_mta_fqdn, --47
       porta_equipamento, --48
       nome_eqpto_mta_fqdn_novo, --49
       porta_equipamento_novo, --50
       cod_retorno, --51
       mensagem_retorno, --52
       cod_indic_facilidade_adic, --53
       indic_contr_facilidade, --54
       data_solic_facilidade, --55
       ccorrente_cliente, --56
       data_confirm, --57
       tipo_registro_trailler, --58
       qtde_registro_trailler, --59
       filler_trailler, --60
       nome_arquivo, --61
       status_arquivo, --62
       usr_voiphit, --63
       id_produto, --64
       fc_numero_portado, --65
       fc_interceptado, --66
       fc_processamento_online, --67
       id_transacao_net_tronco) --68

    VALUES
      (PROD_JD.SEQ_ID_VOIPHIT.NextVal, --1
       vVoipHit(1).cod_oc, --2
       vVoipHit(1).tipo_registro_header, --3
       To_Number(To_Char(SYSDATE, 'YYYYMMDD')), --4
       To_Number(To_Char(SYSDATE, 'HH24MISS')), --5
       vVoipHit(1).filler_header, --6
       vVoipHit(1).tipo_registro, --7
       vIdTransacao, --8
       vVoipHit(1).tipo_registro_detalhe, --9
       vVoipHit(1).cod_operadora_cliente, --10
       vVoipHit(1).num_cliente, --11
       vVoipHit(1).cod_operadora_cliente_novo, --12
       vVoipHit(1).num_cliente_novo, --13
       vVoipHit(1).nome_cliente, --14
       vVoipHit(1).cpf_cnpj_passaporte, --15
       vVoipHit(1).tipo_cliente, --16
       vVoipHit(1).tel_residencial_cliente, --17
       vVoipHit(1).tel_celular_cliente, --18
       vVoipHit(1).tipo_logradouro_inst, --19
       vVoipHit(1).nome_logradouro_inst, --20
       vVoipHit(1).num_logradouro_inst, --21
       vVoipHit(1).ref_endereco_inst, --22
       vVoipHit(1).compl_endereco_inst, --23
       vVoipHit(1).bairro_inst, --24
       vVoipHit(1).cep_inst, --25
       vVoipHit(1).cidade_inst, --26
       vVoipHit(1).uf_inst, --27
       vVoipHit(1).inscr_estadual_inst, --28
       vVoipHit(1).nome_logradouro_cobr, --29
       vVoipHit(1).bairro_cobr, --30
       vVoipHit(1).cep_cobr, --31
       vVoipHit(1).cidade_cobr, --32
       vVoipHit(1).uf_cobr, --33
       vVoipHit(1).ddd_telefone_contr, --34
       vVoipHit(1).num_telefone_contr, --35
       vVoipHit(1).ddd_telefone_contr_novo, --36
       vVoipHit(1).num_telefone_contr_novo, --37
       vVoipHit(1).indic_contratacao_cliente, --38
       vVoipHit(1).indic_cobr_inst, --39
       vVoipHit(1).indic_fig_ltelefonica, --40
       vVoipHit(1).descr_fig_ltelefonica, --41
       vVoipHit(1).plano_telefonia_local, --42
       vVoipHit(1).plano_ldistancia_nacional, --43
       vVoipHit(1).plano_ldistancia_internacional, --44
       To_Number(To_Char(SYSDATE, 'yyyymmdd')), --45
       vVoipHit(1).dia_vencto_cliente, --46
       vVoipHit(1).nome_eqpto_mta_fqdn, --47
       vVoipHit(1).porta_equipamento, --48
       vVoipHit(1).nome_eqpto_mta_fqdn_novo, --49
       vVoipHit(1).porta_equipamento_novo, --50
       Null, --51
       Null, --52
       vVoipHit(1).cod_indic_facilidade_adic, --53
       vVoipHit(1).indic_contr_facilidade, --54
       vVoipHit(1).data_solic_facilidade, --55
       vVoipHit(1).ccorrente_cliente, --56
       vVoipHit(1).data_confirm, --57
       vVoipHit(1).tipo_registro_trailler, --58
       vVoipHit(1).qtde_registro_trailler, --59
       vVoipHit(1).filler_trailler, --60
       '0', --61
       'PND', --62
       vVoipHit(1).usr_voiphit, --63
       vVoipHit(1).id_produto, --64
       vVoipHit(1).fc_numero_portado, --65
       vVoipHit(1).fc_interceptado, --66
       'N', --67
       vVoipHit(1).id_transacao_net_tronco); --68
    COMMIT;
  END;

  PROCEDURE BuscaSolicitacoes IS
  BEGIN
    SELECT DISTINCT ASS.id_solicitacao_ass solic,
                    TASS.id_tipo_solic,
                    TASS.descricao,
                    TF.descricao,
                    H.identificacao ponto,
                    ASS.id_tipo_fechamento fechamento,
                    ASS.dt_cadastro gerada,
                    ASS.dt_baixa baixa,
                    ASS.usr_baixa usuario BULK COLLECT
      INTO vSolicitacoes
      FROM PROD_JD.SN_SOLICITACAO_ASS  ASS,
           PROD_JD.SN_TELEFONE_VOIP    TV,
           PROD_JD.SN_PORTABILIDADE    SP,
           PROD_JD.SN_TIPO_SOLIC       TASS,
           PROD_JD.SN_TIPO_FECHAMENTO  TF,
           PROD_JD.SN_PONTO_HISTORICO  H,
           PROD_JD.SN_CIDADE_OPERADORA CO
     WHERE SP.ddd_telefone_voip(+) = TV.ddd_telefone_voip
       AND SP.num_telefone_voip(+) = TV.num_telefone_voip
       AND TV.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND TV.num_telefone_voip = Substr(cTerminal, 3, 8)
       AND TV.num_contrato = ASS.num_contrato
       AND TV.cid_contrato = CO.cid_contrato
       AND H.id_ponto = TV.id_ponto
       AND ASS.id_tipo_fechamento = TF.id_tipo_fechamento(+)
       AND ASS.id_tipo_solic = TASS.id_tipo_solic
       AND (EXISTS (SELECT *
               FROM PROD_JD.SN_OS OS
              WHERE ASS.id_solicitacao_ass = OS.id_solicitacao_ass
                AND OS.id_ponto = TV.id_ponto) OR EXISTS
            (SELECT *
               FROM PROD_JD.SN_OC OCC
              WHERE ASS.id_solicitacao_ass = OCC.id_solicitacao_ass
                AND OCC.id_ponto = TV.id_ponto))
     ORDER BY ASS.id_solicitacao_ass DESC;
  END;

  PROCEDURE BuscaHits IS
  BEGIN
    SELECT HIT.id_hit,
           HIT.tipo_registro,
           HIT.status_arquivo,
           HIT.fc_processamento_online,
           HIT.cod_oc BULK COLLECT
      INTO vHits
      FROM PROD_JD.PP_VOIPHIT HIT
     WHERE HIT.ddd_telefone_contr = Substr(cTerminal, 1, 2)
       AND HIT.num_telefone_contr = Substr(cTerminal, 3, 8)
     ORDER BY HIT.id_hit;
  END;

  FUNCTION OcFoiExecutada(iIdSolic IN PROD_JD.SN_OC.id_solicitacao_ass%TYPE,
                          oCodOC   OUT PROD_JD.SN_OC.cod_oc%TYPE)
    RETURN BOOLEAN IS
    vIdTpFechamento PROD_JD.SN_OC.id_tipo_fechamento%TYPE;
  BEGIN
    SELECT OC.id_tipo_fechamento, OC.cod_oc
      INTO vIdTpFechamento, oCodOC
      FROM PROD_JD.SN_OC OC
     WHERE OC.id_solicitacao_ass = iIdSolic;

    IF ((vIdTpFechamento IS NULL) OR
       ((vIdTpFechamento IS NOT NULL) AND (vIdTpFechamento <> 1))) THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE E_OC_NAO_ENCONTRADA;
    WHEN TOO_MANY_ROWS THEN
      RAISE E_OC_DUPLICADA;
    WHEN OTHERS THEN
      RAISE;
  END;

  PROCEDURE CorrigeStatusHits(iHitID IN PROD_JD.PP_VOIPHIT.id_hit%type) IS
  BEGIN
    UPDATE PROD_JD.PP_VOIPHIT HIT
       SET HIT.status_arquivo = 'CAN'
     WHERE HIT.ddd_telefone_contr = Substr(cTerminal, 1, 2)
       and HIT.num_telefone_contr = Substr(cTerminal, 3, 8)
       AND HIT.id_hit < iHitID
       AND HIT.status_arquivo = 'ESP'
        OR HIT.status_arquivo = 'ERR';

    IF SQL%ROWCOUNT > 0 THEN
      UPDATE PROD_JD.PP_VOIPHIT HIT
         SET HIT.status_arquivo = 'PND', HIT.fc_processamento_online = 'N'
       WHERE HIT.id_hit = iHitID
         AND HIT.status_arquivo = 'ESP';
      COMMIT;
    ELSE
      ROLLBACK;
      RAISE E_FILA_TRAVADA_SEM_MOTIVO;
    END IF;
  END;

  PROCEDURE GeraOcorrenciaELogs IS
    vIdAssinante PROD_JD.SN_CONTRATO.id_assinante%TYPE;
  BEGIN
    BEGIN
      SELECT CT.id_assinante
        INTO vIdAssinante
        FROM PROD_JD.SN_CONTRATO CT
       WHERE CT.cid_contrato = vCidContrato
         AND CT.num_contrato = cNumContrato;

      InsereOcorrencia(vIdAssinante);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE E_ERRO_OCORRENCIA;
    END;

    GravaLog(vSessao,
             SYSDATE,
             vBase,
             'OK - TERMINAL TRATADO',
             cTerminal,
             vCidContrato || '/' || cNumContrato);
  END GeraOcorrenciaELogs;

  PROCEDURE ChecaHitsOC(iCodOC IN PROD_JD.SN_OC.cod_oc%TYPE) IS
    vEncontrouHitOC  BOOLEAN := FALSE;
    vHitsAntERRouESP INTEGER := 0;
  BEGIN
    BuscaHits;
    IF (vHits.Count > 0) THEN
      FOR i IN REVERSE 1 .. vHits.Count LOOP
        IF (NOT vEncontrouHitOC) AND (vHits(i).cod_oc <> iCodOC) AND
           (vHits(i).tipo_registro <> 'DE') THEN
          CONTINUE;
        ELSE
          IF NOT vEncontrouHitOC THEN
            vEncontrouHitOC := True;
          END IF;

          IF (vHits(i).status_arquivo = 'EXE') OR
             (vHits(i).status_arquivo = 'ENV') OR
             (vHits(i).status_arquivo = 'PND') OR
             (vHits(i).status_arquivo = 'RNV') THEN
            RAISE E_STATUS_HIT_INVALIDO;
          ELSIF (vHits(i).status_arquivo = 'ERR') OR
                (vHits(i).status_arquivo = 'ESP') OR
                (vHits(i).status_arquivo = 'CAN') THEN
            IF (vHits(i).status_arquivo = 'CAN') OR
               (vHits(i).status_arquivo = 'ERR') THEN
              ReenviaHITDesconexao(iHitID => vHits(i).id_hit);
            ELSE
              CorrigeStatusHits(iHitID => vHits(i).id_hit);
            END IF;
            RAISE E_HIT_REENVIADO;

            GeraOcorrenciaELogs;
          ELSE
            RAISE E_STATUS_HIT_DESCONHECIDO;
          END IF;

          EXIT;
        END IF;
      END LOOP;

      IF NOT vEncontrouHitOC THEN
        RAISE E_HIT_DE_NAO_ENCONTRADO;
      END IF;
    ELSE
      RAISE E_HITS_NAO_ENCONTRADOS;
    END IF;
  END;

  FUNCTION GetCidContrato RETURN SN_CIDADE_BASE.CID_CONTRATO%TYPE IS
    vReturn SN_CIDADE_BASE.CID_CONTRATO%TYPE;
  BEGIN
    SELECT cid_contrato
      INTO vReturn
      FROM SN_CIDADE_BASE CID
     WHERE CID.cod_operadora = LPAD(cCodOperadora, 3, '0');

    RETURN vReturn;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_OPER_NAO_ENCONTRADA;
  END;

  FUNCTION GetBase RETURN SN_CIDADE_BASE.NM_ALIAS%TYPE IS
    vReturn SN_CIDADE_BASE.NM_ALIAS%TYPE;
  BEGIN
    SELECT Upper(nm_alias)
      INTO vReturn
      FROM SN_CIDADE_BASE CID
     WHERE CID.cod_operadora = LPAD(cCodOperadora, 3, '0');

    RETURN vReturn;
  END;

BEGIN
  vDadosException  := chr(10) || chr(13) || 'Terminal: ' || cTerminal ||
                      ' | Operadora: ' || cCodOperadora || ' | Contrato: ' ||
                      cNumContrato;
  vCidContrato     := GetCidContrato;
  vBase            := GetBase;
  vTerminalPortado := ChecaTerminalPortado(oIdPortabilidade => vIdPortabilidade);

  IF (NOT vTerminalPortado) THEN
    RAISE E_TERM_NAO_PORT_DOADO;
  ELSE
    vTerminalDoado := ChecaTerminalDoado;
    IF NOT vTerminalDoado THEN
      RAISE E_TERM_NAO_PORT_DOADO;
    ELSE
      BuscaSolicitacoes;
      IF vSolicitacoes.Count > 0 THEN
        IF (vSolicitacoes(1).id_tipo_solic = 233) THEN
          /* RETIRAR EMTA */
          IF (vSolicitacoes(2).id_tipo_solic = 923) OR /*DESCONEXAO POR INADIMPLENCIA EBT*/
             (vSolicitacoes(2).id_tipo_solic = 924) THEN
            /*DESCONEXAO POR OPCAO EBT*/
            GeraHITDesconexao;
          ELSE
            RAISE E_RET_EMTA_SEM_DESCONEXAO;
          END IF;
        ELSIF ((vSolicitacoes(1).id_tipo_solic = 232) OR /* RETIRAR PONTO NETFONE */
              (vSolicitacoes(1).id_tipo_solic = 4) OR /* RETIRAR PONTO */
              (vSolicitacoes(1).id_tipo_solic = 923) OR /*DESCONEXAO POR INADIMPLENCIA EBT*/
              (vSolicitacoes(1).id_tipo_solic = 924)) /*DESCONEXAO POR OPCAO EBT*/
              AND (vSolicitacoes(1).dt_baixa IS NOT NULL) THEN
          GeraHITDesconexao;
        ELSIF (vSolicitacoes(1).id_tipo_solic = 223) THEN
          /*DESABILITAR NUMERO TELEFONICO*/
          vOcFoiExecutada := OcFoiExecutada(iIdSolic => vSolicitacoes(1)
                                                       .id_solic,
                                            oCodOC   => vCodOC);
          IF vOcFoiExecutada THEN
            RAISE E_SOLIC_DESAB_EXECUTADA;
          ELSE
            ChecaHitsOC(iCodOC => vCodOC);
          END IF;
        ELSE
          FOR vCount IN 1 .. vSolicitacoes.Count LOOP
            IF ((vSolicitacoes(vCount).id_tipo_solic = 232) OR /* RETIRAR PONTO NETFONE */
               (vSolicitacoes(vCount).id_tipo_solic = 4) OR /* RETIRAR PONTO */
               (vSolicitacoes(vCount).id_tipo_solic = 923) OR /*DESCONEXAO POR INADIMPLENCIA EBT*/
               (vSolicitacoes(vCount).id_tipo_solic = 924)) /*DESCONEXAO POR OPCAO EBT*/
               AND (vSolicitacoes(vCount).dt_baixa IS NULL) THEN
              vDadosException := 'Solic: ' || vSolicitacoes(vCount)
                                .id_solic || ' | ' || vDadosException;
              RAISE E_EXISTE_DESC_RETIR_NAO_EXEC;
            END IF;
          END LOOP;
          RAISE E_NAO_EXISTE_SOLIC_CANC;
        END IF;
      END IF;
    END IF;
  END IF;
EXCEPTION
  WHEN E_TERM_NAO_PORTADO THEN
    Raise_Application_Error(-20050,
                            'Esta opção deve ser utilizada somente se o número telefônico estiver em outra operadora (esta informação pode ser verificada através do site Consulta Número). Caso o número esteja em outra operadora, cheque no NETSMS > Produtos se existe solicitações de Desconexão e Desabilitar Número executados. Caso o número seja de origem Embratel não será necessário validação em área local.' ||
                            vDadosException);
  WHEN E_TERM_NAO_PORT_DOADO THEN
    Raise_Application_Error(-20050,
                            'Esta opção deve ser utilizada somente se o número telefônico estiver em outra operadora (esta informação pode ser verificada através do site Consulta Número). Caso o número esteja em outra operadora, cheque no NETSMS > Produtos se existe solicitações de Desconexão e Desabilitar Número executados. Caso o número seja de origem Embratel não será necessário validação em área local.' ||
                            vDadosException);
  WHEN E_NAO_EXISTE_SOLIC_CANC THEN
    Raise_Application_Error(-20050,
                            'Não foi possível fazer o tratamento de sua solicitação. Favor acessar o NETSMS e realizar abertura de uma Solicitação de Desconexão por Opção / Retirada de Ponto executá-la e aguardar o processamento automático da OC de Desabilitar Número. Quando estas solicitações estiverem executadas, favor realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                            vDadosException);
  WHEN E_RET_EMTA_SEM_DESCONEXAO THEN
    Raise_Application_Error(-20000,
                            'Não foi possível fazer o tratamento de sua solicitação. Favor acessar o NETSMS e realizar abertura de uma Solicitação de Desconexão por Opção / Retirada de Ponto executá-la e aguardar o processamento automático da OC de Desabilitar Número. Quando estas solicitações estiverem executadas, favor realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                            vDadosException);
  WHEN E_OC_NAO_ENCONTRADA THEN
    Raise_Application_Error(-20050,
                            'O comando de Desabilitar Número foi reenviado para Embratel. Favor aguardar o retorno sistêmico, e assim que a solicitação de Desabilitar Número estiver executada, realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                            vDadosException);
  WHEN E_OC_DUPLICADA THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido à duplicidade da solicitação de Desabilitar Número. Favor entrar em contato com o Service Desk e abrir um chamado para a equipe NETSMS PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_SOLIC_DESAB_EXECUTADA THEN
    Raise_Application_Error(-20000,
                            'Identificamos que para o número de telefone informado existe solicitação de Desconexão por Opção / Retirada de Ponto e Desabilitar Número executados. Favor acessar a ferramenta NETSALES e realizar nova tentativa de validação em área local. Caso o erro persista, será necessário abrir chamado no Service Desk na opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_HITS_NAO_ENCONTRADOS THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_HIT_DE_NAO_ENCONTRADO THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_STATUS_HIT_INVALIDO THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_STATUS_HIT_DESCONHECIDO THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_REENVIADO_HIT_AGUARDAR THEN
    Raise_Application_Error(-20050,
                            'O comando de Desabilitar Número foi reenviado para Embratel. Favor aguardar o retorno sistêmico e assim que a solicitação de Desabilitar Número estiver executada, realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                            vDadosException);
  WHEN E_FILA_TRAVADA_SEM_MOTIVO THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_HIST_PORTAB_NOT_FOUND THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_OPER_NAO_ENCONTRADA THEN
    Raise_Application_Error(-20050,
                            'Não foram encontrados registros válidos para esta pesquisa. Favor voltar ao menu anterior e inserir os dados corretos do assinante para prosseguir com sua solicitação.' ||
                            vDadosException);
  WHEN E_ERRO_OCORRENCIA THEN
    Raise_Application_Error(-20000,
                            'O comando de Desabilitar Número foi reenviado para Embratel. Favor aguardar o retorno sistêmico e assim que a solicitação de Desabilitar Número estiver executada, realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                            vDadosException);
  WHEN E_NAO_ENCONTRADO_HIT_ANTERIOR THEN
    Raise_Application_Error(-20000,
                            'Não foi possível seguir com o atendimento de sua solicitação, devido a inconsistências encontradas na base NET para este número de telefone. Será necessário abrir chamado no Service Desk selecionando a opção 3 - PORTABILIDADE.' ||
                            vDadosException);
  WHEN E_EXISTE_DESC_RETIR_NAO_EXEC THEN
    Raise_Application_Error(-20050,
                            'Não foi possível corrigir o contrato, pois já existe uma Solicitação de Desconexão por Opção / Retirada de ponto em aberto. Favor acessar a ferramenta NETSMS, executar a solicitação de Desconexão por Opção / Retirada de Ponto em questão e aguardar o processamento automático da OC de Desabilitar Número. Após estas solicitações executadas, favor realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                            vDadosException);
  WHEN E_HIT_REENVIADO THEN
    Raise_Application_Error(-20010,
                            'Solicitação de DESABILITAR NÚMERO reenviado com sucesso. Favor aguarda o processamento ' ||
                            vDadosException);


  WHEN OTHERS THEN
    RAISE;
END;