DECLARE
  /* CONSTANTES, DEFINIDAS PELO USUARIO */
  CNUMCONTRATO CONSTANT VARCHAR2(100) := '[CONTRATO]';
  CTERMINAL    CONSTANT VARCHAR2(100) := '[TERMINAL]';

  --CNUMCONTRATO CONSTANT VARCHAR2(100) := '&CONTRATO';
  --CTERMINAL    CONSTANT VARCHAR2(100) := '&TERMINAL';

  VCEP     VARCHAR(30);
  VCIDADE  VARCHAR(100);
  VESTADO  VARCHAR(10);
  VRETORNO NUMBER;

  /* EXCEPTIONS */
  E_SUCESSO EXCEPTION ;
  E_VALIDAR_DISP_TERM EXCEPTION;
  E_VALIDAR_LINHA_ABERTA EXCEPTION;
  E_ERRO_OCORRENCIA EXCEPTION;
  E_SERVICO EXCEPTION;
  VOIPABERTA EXCEPTION;
  E_TERMINAL_JA_DISPONIVEL EXCEPTION;
  E_TERMINAL_DISPONIBILIZADO EXCEPTION;
  E_ENDERECO_NAO_ENCONTRADO EXCEPTION;

  /* FLAGS */
  VTERMINALPORTADO   BOOLEAN;
  VEXISTEPROPOSTA    BOOLEAN;
  VEXISTELINHAABERTA BOOLEAN;

  /* VARIAVEIS */
  VDADOSEXCEPTION VARCHAR2(200);
  VTERMINALDISP   BOOLEAN;

  VSESSAO VARCHAR2(100) := 'AUTO_TR_299';
  VBASE   PROD_JD.SN_CIDADE_BASE.NM_ALIAS%TYPE;

  PROCEDURE SLEEP(SEGUNDOS NUMBER) AS
    IN_TIME NUMBER := SEGUNDOS;
    V_NOW   DATE;
  BEGIN
    SELECT SYSDATE INTO V_NOW FROM DUAL;
    LOOP
      EXIT WHEN V_NOW +(IN_TIME * (1 / 86400)) = SYSDATE;
    END LOOP;
  END SLEEP;

  PROCEDURE ENVIAR_EVENTO_GESTOR AS
    CONN           UTL_TCP.CONNECTION;
    V_MSG          VARCHAR2(4000);
    V_METODO       VARCHAR2(500);
    V_URL          PROD_JD.SN_PARAMETRO.INSTRUCAO%TYPE;
    V_HOST         VARCHAR2(200);
    RC             BINARY_INTEGER;
    V_RESPONSE     VARCHAR2(30000);
    V_TMP_RESPONSE VARCHAR2(4096);
    V_XML          XMLTYPE;
    VDATAHORA      VARCHAR2(40);
    IP             VARCHAR2(20) := '10.11.255.205';
    C_INI          NUMBER;
    C_FIM          NUMBER;
  
  BEGIN
  
    --URL conf service
    V_URL := 'http://' || IP ||
             ':8081/modulointegracao/portabilidade/sms/PortabilidadeNumericaService';
  
    V_HOST   := SUBSTR(V_URL, INSTR(V_URL, '//') + 2);
    V_METODO := SUBSTR(V_HOST, INSTR(V_HOST, '/'));
    V_HOST   := SUBSTR(V_HOST, 1, INSTR(V_HOST, '/') - 1);
  
    -- XML PARA REENVIO DA INTEGRAÇÃO PELO SERVIÇO GESTOR                 
    V_MSG := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://www.viaembratel.com.br/modulointegracao/portabilidadenumerica/schema/types">';
    V_MSG := V_MSG || '   <soapenv:Header/>';
    V_MSG := V_MSG || '   <soapenv:Body>';
    V_MSG := V_MSG || '<typ:validarAreaLocalRequest>';
    V_MSG := V_MSG || '         <!--Optional:-->';
    V_MSG := V_MSG || '         <!--<typ:idProposta>?</typ:idProposta>-->';
    V_MSG := V_MSG || '         <!--Optional:-->';
    V_MSG := V_MSG || '         <typ:contrato>';
    V_MSG := V_MSG || '      <typ:cidContrato>02121</typ:cidContrato>';
    V_MSG := V_MSG || '   <typ:numContrato>' || CNUMCONTRATO ||
             '</typ:numContrato>';
    V_MSG := V_MSG || '    </typ:contrato>';
    V_MSG := V_MSG || '     <!--Optional:-->';
    V_MSG := V_MSG || '      <!--<typ:localidadeOrigem>';
    V_MSG := V_MSG ||
             '      <typ:descricaoLocalidade></typ:descricaoLocalidade>';
    V_MSG := V_MSG || '           <typ:descricaoUf></typ:descricaoUf>-->';
    V_MSG := V_MSG || '         <!--Optional:-->';
    V_MSG := V_MSG || '         <!--<typ:cep></typ:cep> ';
    V_MSG := V_MSG || '        </typ:localidadeOrigem>-->';
    V_MSG := V_MSG || '         <typ:localidadeDestino>';
    V_MSG := V_MSG || '            <typ:descricaoLocalidade>' || VCIDADE ||
             '</typ:descricaoLocalidade>';
    V_MSG := V_MSG || '            <typ:descricaoUf>' || VESTADO ||
             '</typ:descricaoUf>';
    V_MSG := V_MSG || '            <!--Optional:-->';
    V_MSG := V_MSG || '            <typ:cep>' || VCEP || '</typ:cep>';
    V_MSG := V_MSG || '         </typ:localidadeDestino>';
    V_MSG := V_MSG ||
             '   <typ:idOperadoraTelefonia>121</typ:idOperadoraTelefonia>';
    V_MSG := V_MSG || ' <typ:telefoneVoip>';
    V_MSG := V_MSG || ' <typ:dddTelefoneVoip>' || SUBSTR(CTERMINAL, 1, 2) ||
             '</typ:dddTelefoneVoip>';
    V_MSG := V_MSG || ' <typ:numTelefoneVoip>' || SUBSTR(CTERMINAL, 3, 8) ||
             '</typ:numTelefoneVoip>';
    V_MSG := V_MSG || '         </typ:telefoneVoip>';
    V_MSG := V_MSG || '      </typ:validarAreaLocalRequest>';
    V_MSG := V_MSG || '   </soapenv:Body>';
    V_MSG := V_MSG || ' </soapenv:Envelope>';
  
    --
    CONN := UTL_TCP.OPEN_CONNECTION(IP, '80');
  
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'POST ' || V_METODO || ' HTTP/1.0');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Host: ' || V_HOST);
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Accept: text/*');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Accept-Charset: *');
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Accept-Encoding:');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Content-Encoding: identity');
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Content-Type: text/xml');
    RC := RC -
          UTL_TCP.WRITE_LINE(CONN, 'Content-Length: ' || LENGTH(V_MSG));
    RC := RC + UTL_TCP.WRITE_LINE(CONN, '');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, V_MSG);
  
    UTL_TCP.FLUSH(CONN);
  
    BEGIN
      V_RESPONSE := '';
      LOOP
        RC         := RC + UTL_TCP.READ_TEXT(CONN, V_TMP_RESPONSE);
        V_RESPONSE := V_RESPONSE || V_TMP_RESPONSE;
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
      
        C_INI := INSTR(V_RESPONSE, '<mensagem>', 1);
        C_FIM := INSTR(V_RESPONSE, '</mensagem>', 1);
      
        VRETORNO := INSTR(SUBSTR(V_RESPONSE, C_INI, C_FIM - C_INI),
                          'SUCESSO',
                          1);
      
    END;
  
    UTL_TCP.CLOSE_CONNECTION(CONN);
  
  END ENVIAR_EVENTO_GESTOR;

  FUNCTION VALIDADISPONIBTERMINAL RETURN BOOLEAN IS
    VCOUNT NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO VCOUNT
      FROM PROD_JD.SN_TELEFONE_VOIP PORT
     WHERE PORT.DDD_TELEFONE_VOIP = SUBSTR(CTERMINAL, 1, 2)
       AND PORT.NUM_TELEFONE_VOIP = SUBSTR(CTERMINAL, 3, 8)
       AND PORT.ID_STATUS_TELEFONE_VOIP = 'D'
       AND PORT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND NOT EXISTS
     (SELECT 1
              FROM PROD_JD.SN_DISP_PORTABILIDADE DP
             WHERE DP.DDD_TELEFONE_VOIP = PORT.DDD_TELEFONE_VOIP
               AND DP.NUM_TELEFONE_VOIP = PORT.NUM_TELEFONE_VOIP);
  
    RETURN(VCOUNT > 0);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_VALIDAR_DISP_TERM;
  END;

  FUNCTION VALIDALINHAABERTA RETURN BOOLEAN IS
    VCOUNT NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO VCOUNT
      FROM PROD_JD.SN_TELEFONE_VOIP VOIP
     WHERE VOIP.DDD_TELEFONE_VOIP = SUBSTR(CTERMINAL, 1, 2)
       AND VOIP.NUM_TELEFONE_VOIP = SUBSTR(CTERMINAL, 3, 8)
       AND VOIP.ID_STATUS_TELEFONE_VOIP IN ('U')
       AND VOIP.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR');
  
    RETURN(VCOUNT > 0);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_VALIDAR_LINHA_ABERTA;
  END;

  PROCEDURE DISPONIBILIZALINHA IS
  BEGIN
    DELETE FROM PROD_JD.SN_DISP_PORTABILIDADE PORT
     WHERE PORT.NUM_CONTRATO = CNUMCONTRATO
       AND PORT.DDD_TELEFONE_VOIP = SUBSTR(CTERMINAL, 1, 2)
       AND PORT.NUM_TELEFONE_VOIP = SUBSTR(CTERMINAL, 3, 8);
  
    COMMIT;
  END;

  PROCEDURE INSEREOCORRENCIA(IIDASSINANTE IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
    VIDOFENSOR    VARCHAR(20) := ''; --PREENCHER COM ID_OFENSOR DO SCRIPT(PLANILHA DE OFENSORES)
    VCALCCHECKSUM VARCHAR(20);
  
    -- Varï¿½veis necessï¿½rias para gerar ocorrï¿½ncia
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDATAOCORRENCIA   PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSER           PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDATARETORNO      PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): O TELEFONE ' ||
                                                        CTERMINAL ||
                                                        '  FOI REMOVIDO DA ÁREA LOCAL (PORTABILIDADE INTRÍNSECA)';
  
  BEGIN
    SELECT TO_NUMBER(VIDOFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'mmdd'))
      INTO VCALCCHECKSUM
      FROM DUAL;
  
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               IIDASSINANTE,
                                               VNOMEINFORMANTE ||
                                               VCALCCHECKSUM,
                                               VTELINFORMANTE,
                                               VDATAOCORRENCIA,
                                               VIDUSER,
                                               VSIT,
                                               VDATARETORNO,
                                               VIDORIGEM,
                                               VIDOFENSOR || VOBS);
    COMMIT;
  END INSEREOCORRENCIA;

  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PMENSAGEM,
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  PROCEDURE GERAOCORRENCIAELOGS IS
    VIDASSINANTE PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE;
  BEGIN
    BEGIN
      SELECT CT.ID_ASSINANTE
        INTO VIDASSINANTE
        FROM PROD_JD.SN_CONTRATO CT
       WHERE CT.CID_CONTRATO = '02121'
         AND CT.NUM_CONTRATO = CNUMCONTRATO;
    
      INSEREOCORRENCIA(VIDASSINANTE);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE E_ERRO_OCORRENCIA;
    END;
  
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             'OK - TERMINAL TRATADO',
             CTERMINAL,
             '02121' || '/' || CNUMCONTRATO);
  
  END GERAOCORRENCIAELOGS;

BEGIN
  VDADOSEXCEPTION := CHR(10) || CHR(13) || 'Terminal: ' || CTERMINAL ||
                     ' | Contrato: ' || CNUMCONTRATO;

  VRETORNO := 0;

  IF (VALIDADISPONIBTERMINAL) THEN
    RAISE E_TERMINAL_JA_DISPONIVEL;
  ELSE
    IF (VALIDALINHAABERTA) THEN
      RAISE VOIPABERTA;
    ELSE
    
      BEGIN
        SELECT C.NUM_CEP, CI.NOM_CIDADE, CI.SGL_ESTADO
          INTO VCEP, VCIDADE, VESTADO
          FROM PROD_JD.SN_ASSINANTE SA,
               PROD_JD.SN_CONTRATO  CO,
               PROD_JD.SN_ENDER     EN,
               GED.HP_IMOVEL        A,
               GED.IMOVEL           B,
               GED.ENDERECO         C,
               SGS.CIDADE           CI,
               GED.LOGRADOURO       LO
         WHERE CO.ID_ASSINANTE = SA.ID_ASSINANTE
           AND CO.NUM_CONTRATO = CNUMCONTRATO
           AND SA.ID_ENDER_INSTALACAO = EN.ID_ENDER
           AND EN.ID_EDIFICACAO = A.COD_HP
           AND A.COD_IMOVEL = B.COD_IMOVEL
           AND B.COD_ENDERECO = C.COD_ENDERECO
           AND LO.COD_CIDADE_REAL = CI.COD_CIDADE
           AND C.COD_LOGRADOURO = LO.COD_LOGRADOURO;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          VCEP    := NULL;
          VCIDADE := NULL;
          VESTADO := NULL;
      END;
    
      IF VCEP IS NOT NULL THEN
        ENVIAR_EVENTO_GESTOR();
        IF VRETORNO > 0 THEN
          SLEEP(1);
          DISPONIBILIZALINHA;
          --GERAOCORRENCIAELOGS;
        ELSE
          RAISE E_SERVICO;
        END IF;
      ELSE
        RAISE E_ENDERECO_NAO_ENCONTRADO;
      END IF;
    
    END IF;
  END IF;

  RAISE E_SUCESSO;

EXCEPTION

  WHEN E_SUCESSO THEN
   RAISE_APPLICATION_ERROR(-20010,'Sua solicitação foi atendida com sucesso! Telefone foi disponibilizado como número especial no contrato.');

  WHEN VOIPABERTA THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Este número telefonico está em uso na base da Claro.  Favor verificar se a venda já foi realizada.' ||
                            VDADOSEXCEPTION);
  
  WHEN E_TERMINAL_JA_DISPONIVEL THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Telefone já se encontra disponível para resgate, favor verificar.' ||
                            VDADOSEXCEPTION);
    NULL;
  WHEN E_ENDERECO_NAO_ENCONTRADO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Não foi possível identificar os dados de endereço do contrato, favor abrir chamado para a equipe de TI Portabilidade atuar.' ||
                            VDADOSEXCEPTION);
  WHEN E_SERVICO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Erro na Chamada do serviço, favor abrir chamado para a equipe de TI Portabilidade atuar.' ||
                            VDADOSEXCEPTION);
  WHEN OTHERS THEN
    RAISE;
END;
