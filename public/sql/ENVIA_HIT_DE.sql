DECLARE
  -- TYPE T_HIT IS TABLE OF VARCHAR2(32);
  TYPE T_COD_CIDADE IS TABLE OF VARCHAR2(32);
  TYPE T_NUM_CONTRATO IS TABLE OF VARCHAR2(32);
  TYPE T_DDD_TERMINAL IS TABLE OF VARCHAR2(32);
  TYPE T_NUM_TERMINAL IS TABLE OF VARCHAR2(32);

  -----
  TYPE TOUTPUT IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
  VOUTPUT TOUTPUT;
  VINDEX  INTEGER := 0;
  ------
  -- A_HIT          T_HIT;
  A_COD_CIDADE   T_COD_CIDADE;
  A_NUM_CONTRATO T_NUM_CONTRATO;
  A_DDD_TERMINAL T_DDD_TERMINAL;
  A_NUM_TERMINAL T_NUM_TERMINAL;

  VIDHIT PROD_JD.PP_VOIPHIT.ID_HIT%TYPE;

  VDDD   PROD_JD.PP_VOIPHIT.DDD_TELEFONE_CONTR%TYPE;
  VTERM  PROD_JD.PP_VOIPHIT.NUM_TELEFONE_CONTR%TYPE;
  VCONTR PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE;
  VOP    PROD_JD.PP_VOIPHIT.COD_OPERADORA_CLIENTE%TYPE;

  VHIT           PROD_JD.PP_VOIPHIT%ROWTYPE;
  V_ERRO         VARCHAR(1000);
  G_ID_ASSINANTE SN_ASSINANTE.ID_ASSINANTE%TYPE;
  E_PROCESSO_OK EXCEPTION;
  --------------------

  -------------------------------------------------------

  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN SN_CONTRATO.ID_ASSINANTE%TYPE,
                                P_DDD   IN PROD_JD.PP_VOIPHIT.DDD_TELEFONE_CONTR%TYPE,
                                P_NUM   IN PROD_JD.PP_VOIPHIT.NUM_TELEFONE_CONTR%TYPE,
                                P_OC    IN PROD_JD.SN_OC.COD_OC%TYPE) IS
  
    L_ID_OFENSOR    VARCHAR(20) := ''; --  PROCESSAMENTO NAO EFETUADO
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VAR�VEIS NECESS�RIAS PARA GERAR OCORR�NCIA
    VIDOCORRENCIA     SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            SN_OCORRENCIA.ID_USR%TYPE := USER; --prod_jd
    VSIT              SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): O COMANDO DE DESABILITAR NÚMERO REFERENTE AO TELEFONE ' ||
                                                TO_CHAR(P_DDD) ||
                                                TO_CHAR(P_NUM) ||
                                                ' FOI ENVIADO COM A FLAG CORRETA PARA PROCESSAMENTO DA OC ' ||
                                                TO_CHAR(P_OC);
  
  BEGIN
  
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(SUBSTR(L_ID_OFENSOR, 2)) +
           TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    --CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVA��O JUNTO A EBT
    PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                       VIDTIPOOCORRENCIA,
                                       P_IDASS,
                                       VNOMEINFORMANTE || L_CALC_CHECKSUM,
                                       VTELINFORMANTE,
                                       VDTOCORRENCIA,
                                       VIDUSR,
                                       VSIT,
                                       VDTRETORNO,
                                       VIDORIGEM,
                                       L_ID_OFENSOR || VOBS);
  
    COMMIT;
  
  END PR_INSEREOCORRENCIA;

BEGIN

  DBMS_OUTPUT.ENABLE(NULL);

  --A_HIT := T_HIT(&HITS);
  VOP    := '[OPERADORA]';
  VCONTR := '[CONTRATO]';
  VDDD   := SUBSTR('[TERMINAL]', 1, 2);
  VTERM  := SUBSTR('[TERMINAL]', 3, 8);

  V_ERRO := NULL;

  BEGIN
  
    BEGIN
      SELECT HIT.*
        INTO VHIT
        FROM PROD_JD.PP_VOIPHIT         HIT,
             PROD_JD.PP_VOIPHIT_FB      FB,
             PROD_JD.SN_OC              OC,
             PROD_JD.SN_SOLICITACAO_ASS ASS,
             SN_CONTRATO                SN
       WHERE HIT.ID_TRANSACAO_NET = FB.ID_TRANSACAO_NET
         AND OC.COD_OC = HIT.COD_OC
         AND ASS.ID_SOLICITACAO_ASS = OC.ID_SOLICITACAO_ASS
         AND ASS.NUM_CONTRATO = SN.NUM_CONTRATO
         AND ASS.CID_CONTRATO = SN.CID_CONTRATO
         AND HIT.STATUS_ARQUIVO in ('ERR')
            --AND HIT.FC_NUMERO_PORTADO = 'N'
         AND HIT.mensagem_retorno IN ('DES - TEL.BLOQ.PORTABILIDADE',
              'BW-DV-DES-TEL.BLOQ.PORTABILIDA')
         and hit.tipo_registro = 'DE'
         AND HIT.COD_OC > 0
            -- AND HIT.ID_HIT = VIDHIT
         AND HIT.DDD_TELEFONE_CONTR = VDDD
         AND HIT.NUM_TELEFONE_CONTR = VTERM
         AND HIT.NUM_CLIENTE = VCONTR
         AND HIT.COD_OPERADORA_CLIENTE = VOP
            -- COLOCAR NO FOR AS VARIAVEIS DE PARAMETRO: 
         AND NOT EXISTS
       (SELECT 1
                FROM PP_VOIPHIT PP2
               WHERE HIT.DDD_TELEFONE_CONTR = PP2.DDD_TELEFONE_CONTR
                 AND HIT.NUM_TELEFONE_CONTR = PP2.NUM_TELEFONE_CONTR
                 AND HIT.COD_OC = PP2.COD_OC
                 AND PP2.ID_TRANSACAO_NET > HIT.ID_TRANSACAO_NET)
         AND NOT EXISTS
       (SELECT 1
                FROM PP_VOIPHIT VH1
               WHERE VH1.DDD_TELEFONE_CONTR = HIT.DDD_TELEFONE_CONTR
                 AND VH1.NUM_TELEFONE_CONTR = HIT.NUM_TELEFONE_CONTR
                 AND VH1.NUM_CLIENTE = HIT.NUM_CLIENTE
                 AND VH1.TIPO_REGISTRO IN ('IN', 'RI')
                 AND VH1.STATUS_ARQUIVO = 'EXE'
                 AND VH1.ID_TRANSACAO_NET > HIT.ID_TRANSACAO_NET)
         AND OC.COD_OC = (SELECT MAX(OC1.COD_OC)
                            FROM SN_OC OC1
                           WHERE OC1.ID_TIPO_OC IN (49)
                             AND OC1.ID_TIPO_FECHAMENTO is null
                             AND OC1.COD_OC = OC.COD_OC);
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        --    DBMS_OUTPUT.PUT_LINE('DDD: ' || VDDD || ' NUM: ' || VTERM || ' / CONTRATO: ' || TO_CHAR(VCONTR) || ' / OPERADORA: ' || VOP);
        VHIT.ID_HIT := NULL;
        --   V_ERRO      := 'ID_HIT N�O ENCONTRADO NO CEN�RIO';
        --   DBMS_OUTPUT.PUT_LINE(VIDHIT || ';ERRO;' || V_ERRO);
      
        Raise_application_error(-20000,
                                'Não foi encontrado comando de Desabilitar Número com erro para este número de telefone. Favor acessar a ferramenta NET SMS, selecionar a Solicitação de Desabilitar Número > Dados da OC > Comandos enviados e verificar se o comando de desabilitar apresenta erro. Caso o erro persista, abrir um chamado junto ao Help Desk para a fila de atendimento NETSMS PORTABILIDADE.' ||
                                V_ERRO);
      
    END;
  
    IF VHIT.ID_HIT IS NOT NULL THEN
    
      BEGIN
      
        update prod_jd.pp_voiphit h
           set h.status_arquivo = 'RNV'
         where h.id_transacao_net = VHIT.id_transacao_net;
        --
        insert into prod_jd.pp_voiphit
        values
          (SEQ_ID_VOIPHIT.nextval,
           VHIT.cod_oc,
           VHIT.tipo_registro_header,
           to_number(to_char(sysdate, 'yyyymmdd')),
           to_number(to_char(sysdate, 'hh24miss')),
           VHIT.filler_header,
           VHIT.tipo_registro,
           prod_jd.SEQ_ID_TRANSACAO_NET.nextval,
           VHIT.tipo_registro_detalhe,
           VHIT.cod_operadora_cliente,
           VHIT.num_cliente,
           VHIT.cod_operadora_cliente_novo,
           VHIT.num_cliente_novo,
           VHIT.nome_cliente,
           VHIT.cpf_cnpj_passaporte,
           VHIT.tipo_cliente,
           VHIT.tel_residencial_cliente,
           VHIT.tel_celular_cliente,
           VHIT.tipo_logradouro_inst,
           VHIT.nome_logradouro_inst,
           VHIT.num_logradouro_inst,
           VHIT.ref_endereco_inst,
           VHIT.compl_endereco_inst,
           VHIT.bairro_inst,
           VHIT.cep_inst,
           VHIT.cidade_inst,
           VHIT.uf_inst,
           VHIT.inscr_estadual_inst,
           VHIT.nome_logradouro_cobr,
           VHIT.bairro_cobr,
           VHIT.cep_cobr,
           VHIT.cidade_cobr,
           VHIT.uf_cobr,
           VHIT.ddd_telefone_contr,
           VHIT.num_telefone_contr,
           VHIT.ddd_telefone_contr_novo,
           VHIT.num_telefone_contr_novo,
           VHIT.indic_contratacao_cliente,
           VHIT.indic_cobr_inst,
           VHIT.indic_fig_ltelefonica,
           VHIT.descr_fig_ltelefonica,
           VHIT.plano_telefonia_local,
           VHIT.plano_ldistancia_nacional,
           VHIT.plano_ldistancia_internacional,
           to_number(to_char(sysdate, 'yyyymmdd')),
           VHIT.dia_vencto_cliente,
           VHIT.nome_eqpto_mta_fqdn,
           VHIT.porta_equipamento,
           VHIT.nome_eqpto_mta_fqdn_novo,
           VHIT.porta_equipamento_novo,
           null,
           null,
           VHIT.cod_indic_facilidade_adic,
           VHIT.indic_contr_facilidade,
           VHIT.data_solic_facilidade,
           VHIT.ccorrente_cliente,
           VHIT.data_confirm,
           VHIT.tipo_registro_trailler,
           VHIT.qtde_registro_trailler,
           VHIT.filler_trailler,
           '0',
           'PND',
           VHIT.usr_voiphit,
           VHIT.id_produto,
           'S',
           VHIT.FC_INTERCEPTADO,
           null,
           'N',
           null,
           null,
           null,
           'N',
           VHIT.id_transacao_net_tronco);
      
        SELECT CT.ID_ASSINANTE
          INTO G_ID_ASSINANTE
          FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
         WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
           AND CT.NUM_CONTRATO = VHIT.NUM_CLIENTE
           AND OP.COD_OPERADORA = VHIT.COD_OPERADORA_CLIENTE;
      
        PR_INSEREOCORRENCIA(G_ID_ASSINANTE,
                            VHIT.DDD_TELEFONE_CONTR,
                            VHIT.NUM_TELEFONE_CONTR,
                            VHIT.COD_OC);
      
        --DBMS_OUTPUT.PUT_LINE(VIDHIT || ';OK');
        RAISE E_PROCESSO_OK;
      
        COMMIT;
      
      EXCEPTION
      
        when E_PROCESSO_OK then
          Raise_application_error(-20010,
                                  'O comando de Desabilitar Número foi reenviado para Embratel. Favor aguardar o retorno sistêmico, e assim que a solicitação de Desabilitar Número estiver executada, realizar nova tentativa de validação em área local através da ferramenta NETSALES.' ||
                                  VIDHIT);
        WHEN OTHERS THEN
          V_ERRO := SQLERRM;
          -- DBMS_OUTPUT.PUT_LINE(VIDHIT || ';ERRO;' || V_ERRO);
        
          Raise_application_error(-20000,
                                  'Não foram encontrados registros válidos para esta pesquisa. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa. ' ||
                                  V_ERRO);
        
      END;
    END IF;
  END;
  -- END LOOP;
END;
