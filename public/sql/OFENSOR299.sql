DECLARE
  /* CONSTANTES, DEFINIDAS PELO USUARIO */
  cCodOperadora CONSTANT VARCHAR2(100) := '[OPERADORA]';
  cNumContrato  CONSTANT VARCHAR2(100) := '[CONTRATO]';
  cTerminal     CONSTANT VARCHAR2(100) := '[TERMINAL]';

  /* EXCEPTIONS */
  E_VALIDAR_PROPOSTA EXCEPTION;
  E_VALIDAR_DISP_TERM EXCEPTION;
  E_VALIDAR_LINHA_ABERTA EXCEPTION;
  E_OPER_NAO_ENCONTRADA EXCEPTION;
  E_ERRO_OCORRENCIA EXCEPTION;
  E_TERM_NAO_PORTADO EXCEPTION;
  E_TERMINAL_JA_DISPONIVEL EXCEPTION;
  E_TERMINAL_DISPONIBILIZADO EXCEPTION;
  E_TERMINAL_NAO_ENCONTRADO EXCEPTION;

  /* FLAGS */
  vTerminalPortado   BOOLEAN;
  vExisteProposta    BOOLEAN;
  vExisteLinhaAberta BOOLEAN;

  /* VARIAVEIS */
  vDadosException VARCHAR2(200);
  vCidContrato    SN_CIDADE_BASE.CID_CONTRATO%TYPE;
  vTerminalDisp   BOOLEAN;

  vSessao VARCHAR2(100) := 'AUTO_TR_299';
  vBase   PROD_JD.SN_CIDADE_BASE.NM_ALIAS%TYPE;

  FUNCTION ChecaTerminalPortado RETURN BOOLEAN IS
    oIdPortabilidade PROD_JD.SN_TELEFONE_VOIP.cid_contrato%TYPE;
  BEGIN
    SELECT COUNT(*)
      INTO oIdPortabilidade
      FROM SN_TELEFONE_VOIP TV
     WHERE TV.DDD_TELEFONE_VOIP = Substr(cTerminal, 1, 2)
       AND TV.NUM_TELEFONE_VOIP = Substr(cTerminal, 3, 8)
       AND TV.CID_CONTRATO =
           (SELECT CID_CONTRATO
              FROM SN_CIDADE_OPERADORA CO
             WHERE CO.COD_OPERADORA = LPAD(cCodOperadora, 3, '0'));
  
    RETURN(oIdPortabilidade > 0);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_VALIDAR_DISP_TERM;
  END;

  FUNCTION ValidaPropostaOuContrato RETURN BOOLEAN IS
    vCount NUMBER;
  BEGIN
    /*  SELECT COUNT(*)
      INTO vCount
      FROM PROD_JD.SN_DISP_PORTABILIDADE PORT
     WHERE PORT.cid_contrato = vCidContrato
       AND (PORT.num_contrato = cNumContrato OR
            PORT.id_proposta = cNumContrato);
    
    RETURN (vCount > 0);*/
  
    RETURN TRUE;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_VALIDAR_PROPOSTA;
  END;

  FUNCTION ValidaDisponibTerminal RETURN BOOLEAN IS
    vCount NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO vCount
      FROM PROD_JD.SN_DISP_PORTABILIDADE PORT
     WHERE PORT.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND PORT.num_telefone_voip = Substr(cTerminal, 3, 8);
  
    RETURN(vCount > 0);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_VALIDAR_DISP_TERM;
  END;

  FUNCTION ValidaLinhaAberta RETURN BOOLEAN IS
    vCount NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO vCount
      FROM PROD_JD.SN_TELEFONE_VOIP VOIP
     WHERE VOIP.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND VOIP.num_telefone_voip = Substr(cTerminal, 3, 8)
       AND VOIP.id_status_telefone_voip IN ('A', 'D')
       AND VOIP.dt_fim = To_Date('30/12/2049', 'DD/MM/RRRR');
  
    RETURN(vCount > 0);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_VALIDAR_LINHA_ABERTA;
  END;

  FUNCTION GetCidContrato RETURN SN_CIDADE_BASE.CID_CONTRATO%TYPE IS
    vReturn SN_CIDADE_BASE.CID_CONTRATO%TYPE;
  BEGIN
    SELECT cid_contrato
      INTO vReturn
      FROM SN_CIDADE_BASE CID
     WHERE CID.cod_operadora = LPAD(cCodOperadora, 3, '0');
  
    RETURN vReturn;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE E_OPER_NAO_ENCONTRADA;
  END;

  FUNCTION GetBase RETURN SN_CIDADE_BASE.NM_ALIAS%TYPE IS
    vReturn SN_CIDADE_BASE.NM_ALIAS%TYPE;
  BEGIN
    SELECT Upper(nm_alias)
      INTO vReturn
      FROM SN_CIDADE_BASE CID
     WHERE CID.cod_operadora = LPAD(cCodOperadora, 3, '0');
  
    RETURN vReturn;
  END;

  PROCEDURE DisponibilizaLinha IS
  BEGIN
    DELETE FROM PROD_JD.SN_DISP_PORTABILIDADE PORT
     WHERE PORT.cid_contrato = vCidContrato
       AND PORT.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND PORT.num_telefone_voip = Substr(cTerminal, 3, 8);
  
    UPDATE PROD_JD.SN_TELEFONE_VOIP VOIP
       SET VOIP.dt_fim = SYSDATE
     WHERE VOIP.ddd_telefone_voip = Substr(cTerminal, 1, 2)
       AND VOIP.num_telefone_voip = Substr(cTerminal, 3, 8)
       AND VOIP.id_status_telefone_voip IN ('A', 'D')
       AND VOIP.dt_fim = To_Date('30/12/2049', 'DD/MM/YYYY');
  
    COMMIT;
  END;

  PROCEDURE InsereOcorrencia(iIdAssinante IN PROD_JD.SN_CONTRATO.id_assinante%TYPE) IS
    vIdOfensor    VARCHAR(20) := ''; --PREENCHER COM ID_OFENSOR DO SCRIPT(PLANILHA DE OFENSORES)
    vCalcChecksum VARCHAR(20);
  
    -- Varáveis necessárias para gerar ocorrência
    vIdOcorrencia     PROD_JD.SN_OCORRENCIA.id_ocorrencia%TYPE;
    vIdTipoOcorrencia PROD_JD.SN_TIPO_OCORRENCIA.id_tipo_ocorrencia%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    vNomeInformante   PROD_JD.SN_OCORRENCIA.nome_informante%TYPE := 'TI';
    vTelInformante    PROD_JD.SN_OCORRENCIA.tel_informante%TYPE := NULL;
    vDataOcorrencia   PROD_JD.SN_OCORRENCIA.dt_ocorrencia%TYPE := SYSDATE;
    vIdUser           PROD_JD.SN_OCORRENCIA.id_usr%TYPE := USER;
    vSit              PROD_JD.SN_OCORRENCIA.sit%TYPE := 1; --FECHADA
    vDataRetorno      PROD_JD.SN_OCORRENCIA.dt_retorno%TYPE := NULL;
    vIdOrigem         PROD_JD.SN_ORIGEM_OCORRENCIA.id_origem%TYPE := 5; --INTERNA
    vObs              PROD_JD.SN_OCORRENCIA.obs%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): A INCONSISTÊNCIA NA BASE NET FOI REGULARIZADA, FAVOR PROSSEGUIR COM NOVA VALIDAÇÃO EM ÁREA LOCAL PARA O TELEFONE ' ||
                                                        cTerminal ||
                                                        ' ATRAVES DO NETSALES.';
  
  BEGIN
    SELECT TO_NUMBER(vIdOfensor) + TO_NUMBER(TO_CHAR(SYSDATE, 'mmdd'))
      INTO vCalcChecksum
      FROM DUAL;
  
    PROD_JD.PSN_OCORRENCIA.SsnIncluiOcorrencia(vIdOcorrencia,
                                               vIdTipoOcorrencia,
                                               iIdAssinante,
                                               vNomeInformante ||
                                               vCalcChecksum,
                                               vTelInformante,
                                               vDataOcorrencia,
                                               vIdUser,
                                               vSit,
                                               vDataRetorno,
                                               vIdOrigem,
                                               vIdOfensor || vObs);
    COMMIT;
  END InsereOcorrencia;

  PROCEDURE GravaLog(pSessao   PROD_JD.SO_ORDENA.sessao%TYPE,
                     pData     DATE,
                     pBase     PROD_JD.SO_ORDENA.campo1%TYPE,
                     pMensagem PROD_JD.SO_ORDENA.campo2%TYPE,
                     pCampo3   PROD_JD.SO_ORDENA.campo3%TYPE DEFAULT Null,
                     pCampo4   PROD_JD.SO_ORDENA.campo4%TYPE DEFAULT Null,
                     pCampo5   PROD_JD.SO_ORDENA.campo5%TYPE DEFAULT Null,
                     pCampo6   PROD_JD.SO_ORDENA.campo6%TYPE DEFAULT Null,
                     pCampo7   PROD_JD.SO_ORDENA.campo7%TYPE DEFAULT Null,
                     pCampo8   PROD_JD.SO_ORDENA.campo8%TYPE DEFAULT Null,
                     pCampo9   PROD_JD.SO_ORDENA.campo9%TYPE DEFAULT Null,
                     pCampo10  PROD_JD.SO_ORDENA.campo10%TYPE DEFAULT Null,
                     pCampo11  PROD_JD.SO_ORDENA.campo11%TYPE DEFAULT Null) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (sessao,
       data,
       campo1,
       campo2,
       campo3,
       campo4,
       campo5,
       campo6,
       campo7,
       campo8,
       campo9,
       campo10,
       campo11)
    VALUES
      (pSessao,
       pData,
       pBase,
       pMensagem,
       pCampo3,
       pCampo4,
       pCampo5,
       pCampo6,
       pCampo7,
       pCampo8,
       pCampo9,
       pCampo10,
       pCampo11);
    COMMIT;
  END GravaLog;

  PROCEDURE GeraOcorrenciaELogs IS
    vIdAssinante PROD_JD.SN_CONTRATO.id_assinante%TYPE;
  BEGIN
    BEGIN
      SELECT CT.id_assinante
        INTO vIdAssinante
        FROM PROD_JD.SN_CONTRATO CT
       WHERE CT.cid_contrato = vCidContrato
         AND CT.num_contrato = cNumContrato;
    
      InsereOcorrencia(vIdAssinante);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE E_ERRO_OCORRENCIA;
    END;
  
    GravaLog(vSessao,
             SYSDATE,
             vBase,
             'OK - TERMINAL TRATADO',
             cTerminal,
             vCidContrato || '/' || cNumContrato);
  
    RAISE E_TERMINAL_DISPONIBILIZADO;
  END GeraOcorrenciaELogs;

BEGIN
  vDadosException := chr(10) || chr(13) || 'Terminal: ' || cTerminal ||
                     ' | Operadora: ' || cCodOperadora; -- ||  ' | Contrato: ' ||    cNumContrato;

  vTerminalPortado := ChecaTerminalPortado;

  IF (not vTerminalPortado) THEN
    RAISE E_TERM_NAO_PORTADO;
  ELSE
    vCidContrato := GetCidContrato;
    vBase        := GetBase;
  
    vTerminalDisp := ValidaDisponibTerminal;
  
    IF vTerminalDisp THEN
      DisponibilizaLinha;
      vExisteProposta := ValidaPropostaOuContrato;
      IF vExisteProposta THEN
        vExisteLinhaAberta := ValidaLinhaAberta;
      
        IF vExisteLinhaAberta THEN
          RAISE E_TERMINAL_JA_DISPONIVEL;
        ELSE
        
          DisponibilizaLinha;
          -- GeraOcorrenciaELogs;
        
          RAISE E_TERMINAL_DISPONIBILIZADO;
        
        END IF;
      ELSE
        DisponibilizaLinha;
        --  GeraOcorrenciaELogs;
      END IF;
    ELSE
      RAISE E_TERMINAL_NAO_ENCONTRADO;
    END IF;
  END IF;
EXCEPTION
  WHEN E_VALIDAR_PROPOSTA THEN
    Raise_Application_Error(-20000,
                            'Esta opção deve ser utilizada somente para números de telefone que estão em outra operadora e ao validar em área local, ocorre à mensagem de "Erro Inesperado". Para o número de telefone informado não foram encontrados erros referentes à portabilidade. Favor realizar nova tentativa de validação em área local pela ferramenta NETSALES. Caso o erro persista, deverá ser aberto chamado através do Service Desk.' ||
                            vDadosException);
  WHEN E_VALIDAR_DISP_TERM THEN
    Raise_Application_Error(-20000,
                            'Esta opção deve ser utilizada somente para números de telefone que estão em outra operadora e ao validar em área local, ocorre à mensagem de "Erro Inesperado". Para o número de telefone informado não foram encontrados erros referentes à portabilidade. Favor realizar nova tentativa de validação em área local pela ferramenta NETSALES. Caso o erro persista, deverá ser aberto chamado através do Service Desk.' ||
                            vDadosException);
  WHEN E_VALIDAR_LINHA_ABERTA THEN
    Raise_Application_Error(-20000,
                            'Esta opção deve ser utilizada somente para números de telefone que estão em outra operadora e ao validar em área local, ocorre à mensagem de "Erro Inesperado". Para o número de telefone informado não foram encontrados erros referentes à portabilidade. Favor realizar nova tentativa de validação em área local pela ferramenta NETSALES. Caso o erro persista, deverá ser aberto chamado através do Service Desk.' ||
                            vDadosException);
  WHEN E_OPER_NAO_ENCONTRADA THEN
    Raise_Application_Error(-20000,
                            'Não foram encontrados registros válidos para esta pesquisa. Favor voltar ao menu anterior e inserir os dados corretos do assinante para prosseguir com sua solicitação.' ||
                            vDadosException);
  WHEN E_ERRO_OCORRENCIA THEN
    Raise_Application_Error(-20010,
                            'Sua solicitação foi atendida com sucesso! Favor acessar a ferramenta NETSALES e seguir com nova validação em área local.' ||
                            vDadosException);
  WHEN E_TERM_NAO_PORTADO THEN
    Raise_Application_Error(-20000,
                            'Não foi encontrado registros para esta pesquisa na base NET. Favor voltar ao menu anterior e inserir os dados corretos do assinante para prosseguir com sua solicitação' ||
                            vDadosException);
  WHEN E_TERMINAL_JA_DISPONIVEL THEN
    Raise_Application_Error(-20050,
                            'O número de telefone informado já se encontra disponível em área local. Favor acessar a ferramenta NETSALES e finalizar o processo de venda seguido de portabilidade.' ||
                            vDadosException);
    NULL;
  WHEN E_TERMINAL_DISPONIBILIZADO THEN
    Raise_Application_Error(-20010,
                            'Sua solicitação foi atendida com sucesso! Favor acessar a ferramenta NETSALES e seguir com nova validação em área local.' ||
                            vDadosException);
    NULL;
  WHEN E_TERMINAL_NAO_ENCONTRADO THEN
    Raise_Application_Error(-20000,
                            'Não foram encontrados registros válidos para esta pesquisa. Favor voltar ao menu anterior e inserir os dados corretos do assinante para prosseguir com sua solicitação.' ||
                            vDadosException);
  WHEN OTHERS THEN
    RAISE;
END;
