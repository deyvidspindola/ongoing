DECLARE
   OPERADORA VARCHAR2(3) := '[OPERADORA]';
   CONTRATO NUMBER := [CONTRATO];
   DDD VARCHAR(2) := SUBSTR('[TERMINAL]',1,2);
   FONE VARCHAR(8) := SUBSTR('[TERMINAL]',3,8);
   BILHETE VARCHAR(50) := '[BILHETE]';
   JANELA VARCHAR(50) := '[JANELA]';
   DATA DATE;
   XML VARCHAR2(4000);

BEGIN
   SELECT SYSDATE INTO DATA FROM DUAL;
   :out := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:open="http://www.openuri.org/">'||
  '<soapenv:Header>'||
  '   <ns1:NetServicoHeader xmlns:ns1="http://netservicos.com.br/NetHeader">netbrasil</ns1:NetServicoHeader>'||
  '</soapenv:Header>'||
  '<soapenv:Body>'||
  '   <open:clientRequestwithReturn>'||
  '      <!--Optional:-->'||
  '      <open:entrada>'||
  '         <CodigoEOTDoadoraPortabilidade>212</CodigoEOTDoadoraPortabilidade>'||
  '         <DataHoraPrevistaPortabilidade>2016-02-17T08:00:00.000Z</DataHoraPrevistaPortabilidade>'||
  '         <CodigoLoginReceptorPortabilidade>OPS$PORT</CodigoLoginReceptorPortabilidade>'||
  '         <NumeroDiasDuracaoJanelaPortab>10</NumeroDiasDuracaoJanelaPortab>'||
  '         <CodigoArea>21</CodigoArea>'||
  '         <NumeroInstancia>30226922</NumeroInstancia>'||
  '      </open:entrada>'||
  '   </open:clientRequestwithReturn>'||
  '</soapenv:Body>'||
  '</soapenv:Envelope>';
   Raise_Application_Error (-20050, 'Não foi possivel processar sua solicitação');
END;
