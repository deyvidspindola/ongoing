DECLARE
  -- TYPE T_HIT IS TABLE OF VARCHAR2(32);
  TYPE T_COD_CIDADE IS TABLE OF VARCHAR2(32);
  TYPE T_NUM_CONTRATO IS TABLE OF VARCHAR2(32);
  TYPE T_DDD_TERMINAL IS TABLE OF VARCHAR2(32);
  TYPE T_NUM_TERMINAL IS TABLE OF VARCHAR2(32);

  VPALIATIVO VARCHAR2(1000) := 'CRIA CENARIO DE DESATIVACAO PARA NUMEROS PORTADOS';
  VCAUSA     VARCHAR2(1000) := 'SISTEMA NAO ESTAVA PREPARADO PARA CRIACAO DO CENARIO';
  VCORRECAO  VARCHAR2(1000) := 'CRIA CENARIO BASEADO CONFORME SOLICITADO PELA EMBRATEL';
  VMSG       VARCHAR2(1000);
  VERR       VARCHAR2(1000);

  VID_CENARIO         NUMBER;
  VID_CENARIO_ANTIGO  NUMBER;
  VID_PORTABILIDADE   PROD_JD.SN_PORTABILIDADE.ID_PORTABILIDADE%TYPE;
  VID_HIT             PROD_JD.PP_VOIPHIT.ID_HIT%TYPE;
  DDD                 VARCHAR(2);
  NUM                 VARCHAR(8);
  VID_SOLICITACAO     PROD_JD.MI_ITG_INTEGRACAO.ID_SOLICITACAO%TYPE;
  VID_INTEGRACAO      PROD_JD.MI_ITG_INTEGRACAO.ID_INTEGRACAO%TYPE;
  VID_INTEGRACAO_PRIM PROD_JD.MI_ITG_INTEGRACAO.ID_INTEGRACAO%TYPE;
  VBILHETE            VARCHAR(12);
  V_ID                VARCHAR(20);
  QTDE_PORTADO        NUMBER;

  -----
  TYPE TOUTPUT IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
  VOUTPUT TOUTPUT;
  VINDEX  INTEGER := 0;
  ------
  -- A_HIT          T_HIT;
  --  A_COD_CIDADE   T_COD_CIDADE;
  A_NUM_CONTRATO T_NUM_CONTRATO;
  A_DDD_TERMINAL T_DDD_TERMINAL;
  A_NUM_TERMINAL T_NUM_TERMINAL;

  V_ID_PORTAB PROD_JD.SN_PORTABILIDADE%ROWTYPE;

  VDDD   PROD_JD.PP_VOIPHIT.DDD_TELEFONE_CONTR%TYPE;
  VTERM  PROD_JD.PP_VOIPHIT.NUM_TELEFONE_CONTR%TYPE;
  VCONTR PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE;
  VOP    PROD_JD.PP_VOIPHIT.COD_OPERADORA_CLIENTE%TYPE;

  V_ERRO VARCHAR(1000);

  E_PROCESSO_OK EXCEPTION;
  E_NETFONE VARCHAR(1000);

  -- DECLARA��O DE VARIAVEIS LOCAL
  VSESSAO PROD_JD.SO_ORDENA.SESSAO%TYPE := '000';

  P_NETFONE EXCEPTION;
  VPORTADO varchar(2);
  P_PORTADO EXCEPTION;
  P_NETFONE2 EXCEPTION;
  P_CENARIO_ERRADO EXCEPTION;
  VERRO1         VARCHAR2(1000);
  P_XML          VARCHAR2(1000);
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  -- PROCEDURE P/ GRAVA��O DE VOLUMETRIA
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PMENSAGEM,
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
  
    L_ID_OFENSOR    VARCHAR(20) := '000';
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VAR�VEIS NECESS�RIAS PARA GERAR OCORR�NCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 7181; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): PARA ENVIO DO SERVIÇO 22 PARA ALTERAR O STATUS DO BILHETE PARA ATIVO.';
  
  BEGIN
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'mmdd'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    -- CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVA��O JUNTO A EBT
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
  END PR_INSEREOCORRENCIA;

  --------------------ENVIAR_EVENTO_GESTOR_22----------------------------------
  PROCEDURE ENVIAR_EVENTO_GESTOR(ID_INTEGRACAO PROD_JD.MI_ITG_INTEGRACAO.ID_INTEGRACAO%TYPE) AS
    CONN           UTL_TCP.CONNECTION;
    V_MSG          VARCHAR2(4000);
    V_METODO       VARCHAR2(500);
    V_URL          PROD_JD.SN_PARAMETRO.INSTRUCAO%TYPE;
    V_HOST         VARCHAR2(200);
    RC             BINARY_INTEGER;
    V_RESPONSE     VARCHAR2(30000);
    V_TMP_RESPONSE VARCHAR2(4096);
    V_XML          XMLTYPE;
    VDATAHORA      VARCHAR2(40);
    IP             VARCHAR2(20) := '10.11.255.205';
  
  BEGIN
  
    --URL conf service
--     V_URL := 'http://' || IP ||
--              '/modulointegracao/contingencia/GestorIntegracaoService';
--
--     V_HOST   := SUBSTR(V_URL, INSTR(V_URL, '//') + 2);
--     V_METODO := SUBSTR(V_HOST, INSTR(V_HOST, '/'));
--     V_HOST   := SUBSTR(V_HOST, 1, INSTR(V_HOST, '/') - 1);
--
--     -- XML PARA REENVIO DA INTEGRAÇÃO PELO SERVIÇO GESTOR
--     V_MSG := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://www.viaembratel.com.br/modulointegracao/schema/types">';
--     V_MSG := V_MSG || '<soapenv:Header/>';
--     V_MSG := V_MSG || '<soapenv:Body>';
--     V_MSG := V_MSG || '<typ:reenviarIntegracaRequest>';
--     V_MSG := V_MSG || '<typ:identificadores-integracao>';
--     V_MSG := V_MSG || '<typ:id-integracao>' || ID_INTEGRACAO ||
--              '</typ:id-integracao>';
--     V_MSG := V_MSG || '</typ:identificadores-integracao>';
--     V_MSG := V_MSG || '<typ:forcar-reenvio>true</typ:forcar-reenvio>';
--     V_MSG := V_MSG ||
--              '<typ:gerar-novo-id-solicitacao>true</typ:gerar-novo-id-solicitacao>';
--     V_MSG := V_MSG || '</typ:reenviarIntegracaRequest>';
--     V_MSG := V_MSG || '</soapenv:Body>';
--     V_MSG := V_MSG || '</soapenv:Envelope>';
--
--     --
--     CONN := UTL_TCP.OPEN_CONNECTION(IP, '80');
--
--     --ESTA SOMA E SUBTRACAO É SÓ PRA SUMIR COM OS WARNINGS (O RETORNO)
--     RC := RC + UTL_TCP.WRITE_LINE(CONN, 'POST ' || V_METODO || ' HTTP/1.0');
--     RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Host: ' || V_HOST);
--     RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Accept: text/*');
--     RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Accept-Charset: *');
--     RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Accept-Encoding:');
--     RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Content-Encoding: identity');
--     RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Content-Type: text/xml');
--     RC := RC -
--           UTL_TCP.WRITE_LINE(CONN, 'Content-Length: ' || LENGTH(V_MSG));
--     RC := RC + UTL_TCP.WRITE_LINE(CONN, '');
--     RC := RC - UTL_TCP.WRITE_LINE(CONN, V_MSG);
--
--     UTL_TCP.FLUSH(CONN);
  
--     BEGIN
--       V_RESPONSE := '';
--       LOOP
--         RC         := RC + UTL_TCP.READ_TEXT(CONN, V_TMP_RESPONSE);
--         V_RESPONSE := V_RESPONSE || V_TMP_RESPONSE;
--       END LOOP;
--       -- EXCEPTION
--       -- WHEN OTHERS THEN
--       -- DBMS_OUTPUT.PUT_LINE('INTEGRACAO [' || ID_INTEGRACAO ||
--       ---                    '] REENVIADO PARA O SERVIÇO:');
--       --- DBMS_OUTPUT.PUT_LINE(V_URL);
--       --  DBMS_OUTPUT.PUT_LINE('---------------------------------------------------------------------');
--     END;
--
--     UTL_TCP.CLOSE_CONNECTION(CONN);
--
--   END ENVIAR_EVENTO_GESTOR;
  --------------------------------

BEGIN

  DBMS_OUTPUT.ENABLE(NULL);

  --A_HIT := T_HIT(&HITS);
  VCONTR := '[CONTRATO]';
  VDDD   := SUBSTR('[TERMINAL]', 1, 2);
  VTERM  := SUBSTR('[TERMINAL]', 3, 8);

  V_ERRO := NULL;

  BEGIN
  
    BEGIN
      SELECT DISTINCT SP.*
        INTO V_ID_PORTAB
        FROM PROD_JD.sn_solicitacao_ass    sola,
             PROD_JD.sn_telefone_voip      tv,
             PROD_JD.SN_OC                 OC,
             PROD_JD.SN_PORTABILIDADE      SP,
             PROD_JD.SN_HIST_STATUS_PORTAB STP,
             PROD_JD.PP_VOIPHIT            HIT
       WHERE tv.id_ponto = oc.id_ponto
         and sola.id_solicitacao_ass = oc.id_solicitacao_ass
         AND TV.DDD_TELEFONE_VOIP = HIT.DDD_TELEFONE_CONTR
         AND TV.NUM_TELEFONE_VOIP = HIT.NUM_TELEFONE_CONTR
         AND SP.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
         AND SP.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
         AND STP.ID_PORTABILIDADE = SP.ID_PORTABILIDADE
         AND hit.TIPO_REGISTRO IN ('DE', 'DI')
         AND hit.STATUS_ARQUIVO IN ('EXE','ENV')
         AND sola.id_tipo_fechamento = 1
         and tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
         and tv.id_status_telefone_voip = 'C'
         and tv.fc_numero_portado = 'S'
         AND NOT EXISTS
       (SELECT 1
                FROM PP_VOIPHIT PP2
               WHERE HIT.DDD_TELEFONE_CONTR = PP2.DDD_TELEFONE_CONTR
                 AND HIT.NUM_TELEFONE_CONTR = PP2.NUM_TELEFONE_CONTR
                 AND HIT.COD_OC = PP2.COD_OC
                 AND PP2.ID_TRANSACAO_NET > HIT.ID_TRANSACAO_NET)
         AND NOT EXISTS
       (SELECT 1
                FROM PP_VOIPHIT VH1
               WHERE VH1.DDD_TELEFONE_CONTR = HIT.DDD_TELEFONE_CONTR
                 AND VH1.NUM_TELEFONE_CONTR = HIT.NUM_TELEFONE_CONTR
                 AND VH1.NUM_CLIENTE = HIT.NUM_CLIENTE
                 AND VH1.TIPO_REGISTRO IN ('IN', 'RI')
                 AND VH1.STATUS_ARQUIVO = 'EXE'
                 AND VH1.ID_TRANSACAO_NET > HIT.ID_TRANSACAO_NET)
         AND SP.TP_PORTABILIDADE = 'PTB'
         AND SP.ID_PORTABILIDADE =
             (SELECT MAX(SN1.ID_PORTABILIDADE)
                FROM SN_PORTABILIDADE SN1
               WHERE SN1.DDD_TELEFONE_VOIP = SP.DDD_TELEFONE_VOIP
                 AND SN1.NUM_TELEFONE_VOIP = SP.NUM_TELEFONE_VOIP)
            
         AND STP.ID_STATUS_PORTABILIDADE = 7
         AND STP.ID_HIST_STATUS_PORTAB =
             (SELECT MAX(SH.ID_HIST_STATUS_PORTAB)
                FROM PROD_JD.SN_HIST_STATUS_PORTAB SH
               WHERE SH.ID_PORTABILIDADE = STP.ID_PORTABILIDADE)
         AND sola.id_tipo_solic IN (223)
         AND sola.id_solicitacao_ass =
             (SELECT MAX(sol.id_solicitacao_ass)
                FROM PROD_JD.sn_solicitacao_ass sol, PROD_JD.SN_OC OC
               WHERE sol.num_contrato = sola.num_contrato
                 AND oc.id_solicitacao_ass = sol.id_solicitacao_ass
                 AND oc.id_ponto = tv.id_ponto
                 AND sol.id_tipo_solic IN (223))
         AND EXISTS
       (SELECT 1
                FROM PROD_JD.SN_PORTABILIDADE      SP1,
                     PROD_JD.SN_REL_PTB_INTEGRACAO REL,
                     PROD_JD.MI_ITG_TP_INTEGRACAO  MIG,
                     PROD_JD.MI_ITG_INTEGRACAO     ITG
               WHERE REL.ID_PORTABILIDADE = SP1.ID_PORTABILIDADE
                 and MIG.ID_TP_INTEGRACAO = itg.ID_TP_INTEGRACAO
                 AND ITG.ID_TP_INTEGRACAO = 30
                 AND SP1.ID_PORTABILIDADE = SP.ID_PORTABILIDADE
                 and rownum < 2)
            -- COLOCAR NO FOR AS VARIAVEIS DE PARAMETRO:
         AND tv.ddd_telefone_voip = VDDD
         AND tv.num_telefone_voip = VTERM
         AND tv.num_contrato = VCONTR
         and rownum < 2;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
      
        begin
        
          SELECT COUNT(*)
            INTO QTDE_PORTADO
            FROM PROD_JD.SN_TELEFONE_VOIP TV
           WHERE TV.DDD_TELEFONE_VOIP = VDDD
             AND TV.NUM_TELEFONE_VOIP = VTERM
             and TV.FC_NUMERO_PORTADO = 'N'
             AND TV.DT_FIM =
                 (SELECT MAX(TV1.DT_FIM)
                    FROM PROD_JD.SN_TELEFONE_VOIP TV1
                   WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                     AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                     and TV.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                     and tv.id_status_telefone_voip = 'C')
             and rownum < 2;
        EXCEPTION
          WHEN OTHERS THEN
            QTDE_PORTADO := 0;
        END;
      
        IF QTDE_PORTADO > 0 THEN
        
          Raise_application_error(-20050,
                                  'O terminal inserido é um NETFONE e deve ser aberto um chamado através da Ferramenta Suporte OS/OC.' ||
                                  E_NETFONE);
        
          /*
            DBMS_OUTPUT.PUT_LINE('DDD: ' || VDDD || ' NUM: ' || VTERM || ' / CONTRATO: ' || TO_CHAR(VCONTR));
          V_ID_PORTAB.ID_PORTABILIDADE := NULL;
          --   V_ERRO      := 'ID_HIT N�O ENCONTRADO NO CEN�RIO';
          --   DBMS_OUTPUT.PUT_LINE(VIDHIT || ';ERRO;' || V_ERRO);*/
        
        end if;
      
    END;
  
    IF V_ID_PORTAB.ID_PORTABILIDADE IS NOT NULL THEN
      -- DBMS_OUTPUT.PUT_LINE(V_ID_PORTAB.ID_PORTABILIDADE || ';OK');
    
      ----- enviar servico 22----------------     
      BEGIN
        VMSG := 'SELECIONANDO DADOS DO TERMINAL';
        SELECT REL.ID_CENARIO,
               DDD_TELEFONE_VOIP,
               NUM_TELEFONE_VOIP,
               SP.NR_PROTOCOLO_BP
          INTO VID_CENARIO_ANTIGO, DDD, NUM, VBILHETE
          FROM PROD_JD.SN_PORTABILIDADE      SP,
               PROD_JD.SN_REL_PTB_INTEGRACAO REL
         WHERE SP.ID_PORTABILIDADE = REL.ID_PORTABILIDADE
           AND SP.ID_PORTABILIDADE = V_ID_PORTAB.ID_PORTABILIDADE
           AND ROWNUM < 2;
      
        VID_INTEGRACAO_PRIM := 0;
      
        FOR CUR IN (SELECT *
                      FROM PROD_JD.MI_ITG_INTEGRACAO
                     WHERE ID_CENARIO = 10431345
                       and id_tp_integracao = 52
                     ORDER BY ORDEM ASC) LOOP
        
          -- select * from mi_itg_tp_integracao
        
          VMSG := 'SETANDO TRACKING ID';
          SELECT CASE
                   WHEN CUR.ORDEM IN (1, 3) THEN
                    TO_CHAR(TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY') ||
                                      '0000000000000000') +
                            PROD_JD.SEQ_SOLICITACAO_INTEGRACAO.NEXTVAL)
                   WHEN CUR.ORDEM = 2 THEN
                    TO_CHAR((20 * POWER(10, 8) +
                            PROD_JD.SQ_NR_CHAMADO_SERVICO.NEXTVAL))
                 END AS ID_SOLICITACAO
            INTO VID_SOLICITACAO
            FROM DUAL;
        
          VMSG := 'SELECIONANDO MAIOR ID_INTEGRACAO + 1';
          SELECT MAX(ID_INTEGRACAO) + 1
            INTO VID_INTEGRACAO
            FROM PROD_JD.MI_ITG_INTEGRACAO;
        
          IF VID_INTEGRACAO_PRIM = 0 THEN
            VID_INTEGRACAO_PRIM := VID_INTEGRACAO;
          END IF;
        
          VMSG := 'INSERINDO INTEGRAÇÕES';
          INSERT INTO PROD_JD.MI_ITG_INTEGRACAO
            (ID_INTEGRACAO,
             DH_RECEPCAO,
             ID_CENARIO,
             ID_SOLICITACAO,
             MENSAGEM,
             MENSAGEM_STATUS,
             ORDEM,
             STATUS,
             ID_TP_INTEGRACAO,
             MEP,
             FLAG_REENVIO,
             DH_ENVIO)
          VALUES
            (VID_INTEGRACAO,
             NULL,
             VID_CENARIO_ANTIGO,
             VID_SOLICITACAO,
             REPLACE(CUR.MENSAGEM, '38355452', VBILHETE),
             CUR.MENSAGEM_STATUS,
             CUR.ORDEM,
             'EXE',
             CUR.ID_TP_INTEGRACAO,
             CUR.MEP,
             'N',
             NULL);
        
        --  ROLLBACK;       
        END LOOP;
      
        COMMIT;
      
        -- SELECT * FROM PROD_JD.MI_ITG_INTEGRACAO WHERE ID_CENARIO = 10431345 ORDER BY ORDEM ASC
        -- select * from mi_itg_tp_integracao where id_tp_integracao =52
        ENVIAR_EVENTO_GESTOR(VID_INTEGRACAO_PRIM);
      
        -- DBMS_OUTPUT.PUT_LINE('OK');
      
      EXCEPTION
        WHEN OTHERS THEN
          VERR := SUBSTR(SQLERRM, 1, 1000);
          ROLLBACK;
          -- DBMS_OUTPUT.PUT_LINE(VERR);
      
      END;
    
      BEGIN
      
        SELECT CT.ID_ASSINANTE
          INTO G_ID_ASSINANTE
          FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
         WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
           AND CT.NUM_CONTRATO = V_ID_PORTAB.NUM_CONTRATO
           AND OP.CID_CONTRATO = V_ID_PORTAB.CID_CONTRATO;
      
        PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
      
        --DBMS_OUTPUT.PUT_LINE(VIDHIT || ';OK');
        RAISE E_PROCESSO_OK;
        GRAVALOG(VSESSAO,
                 SYSDATE,
                 VBASE,
                 'TRATAMENTO EFETUADO',
                 VDDD || VTERM,
                 V_ID_PORTAB.CID_CONTRATO || '/' ||
                 V_ID_PORTAB.NUM_CONTRATO,
                 '');
      
        COMMIT;
      
      EXCEPTION
      
        when E_PROCESSO_OK then
          Raise_application_error(-20010,
                                  'Sua solicitação foi atendida com sucesso. Histórico de portabilidade foi regularizado, acessar a ferramenta VSALES> PORTABILIDADE> HISTÓRICO.');
        WHEN OTHERS THEN
          V_ERRO := SQLERRM;
          -- DBMS_OUTPUT.PUT_LINE(VIDHIT || ';ERRO;' || V_ERRO);
        
          Raise_application_error(-20000,
                                  'O terminal informado não consta no cenário correto para o envio de ativação do bilhete. Favor verificar se o terminal esta com status de portabilidade “Desconexão Pendente” e esteja na Embratel.' ||
                                  V_ERRO);
        
      END;
    END IF;
    Raise_application_error(-20000,
                            'O terminal informado não consta no cenário correto para o envio de ativação do bilhete. Favor verificar se o terminal esta com status de portabilidade “Desconexão Pendente” e esteja na Embratel.' ||
                            V_ERRO);
  
  END;
  -- END LOOP;
END;
