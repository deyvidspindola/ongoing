DECLARE
/*  P_COD_OPERADORA prod_jd.sn_cidade_operadora.cod_operadora%type := '&COD_CIDADE';
  P_CONTRATO      PROD_JD.SN_CONTRATO.NUM_CONTRATO%type := '&NUM_CONTRATO';
  P_DDD           prod_jd.sn_portabilidade.ddd_telefone_voip%type := '&DDD_TERMINAL';
  P_TERMINAL      prod_jd.sn_portabilidade.num_telefone_voip%type := '&NUM_TERMINAL';*/

  P_COD_OPERADORA PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE:='[OPERADORA]';
  P_CONTRATO      PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE:= [CONTRATO];
  P_DDD           VARCHAR2(2):= SUBSTR('[TERMINAL]',1,2);
  P_TERMINAL      VARCHAR2(9):= SUBSTR('[TERMINAL]',3,8);

  -- CURSOR PRINCIPAL
  CURSOR CUR_PRINCIPAL IS
    SELECT SN.NR_PROTOCOLO_BP   BP,
           TV.DDD_TELEFONE_VOIP DD,
           TV.NUM_TELEFONE_VOIP NT,
           TV.CID_CONTRATO      CID_CONTRATO,
           TV.NUM_CONTRATO      NC
      FROM PROD_JD.SN_TELEFONE_VOIP      TV,
           PROD_JD.SN_PORTABILIDADE      SN,
           PROD_JD.SN_HIST_STATUS_PORTAB SHSP
     WHERE TV.ID_STATUS_TELEFONE_VOIP = 'C'
       AND (TV.DT_INI =
           (SELECT MAX(TV1.DT_INI)
               FROM PROD_JD.SN_TELEFONE_VOIP TV1
              WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP))
       AND TV.DDD_TELEFONE_VOIP = SN.DDD_TELEFONE_VOIP
       AND TV.NUM_TELEFONE_VOIP = SN.NUM_TELEFONE_VOIP
       AND TV.NUM_CONTRATO = SN.NUM_CONTRATO
       AND TV.CID_CONTRATO = SN.CID_CONTRATO
       AND SN.NR_PROTOCOLO_BP IS NOT NULL
       AND SN.TP_PORTABILIDADE = 'PTB'
       AND SN.ID_PORTABILIDADE =
           (SELECT MAX(SN1.ID_PORTABILIDADE)
              FROM SN_PORTABILIDADE SN1
             WHERE SN1.DDD_TELEFONE_VOIP = SN.DDD_TELEFONE_VOIP
               AND SN1.NUM_TELEFONE_VOIP = SN.NUM_TELEFONE_VOIP)
       AND SHSP.ID_PORTABILIDADE = SN.ID_PORTABILIDADE
       AND SHSP.ID_STATUS_PORTABILIDADE = 7
       AND SHSP.ID_HIST_STATUS_PORTAB =
           (SELECT MAX(SH.ID_HIST_STATUS_PORTAB)
              FROM PROD_JD.SN_HIST_STATUS_PORTAB SH
             WHERE SH.ID_PORTABILIDADE = SHSP.ID_PORTABILIDADE)

          /* Variaveis de INPUT */
          -- AND HIT.COD_OPERADORA_CLIENTE = P_COD_OPERADORA
       AND TV.DDD_TELEFONE_VOIP = P_DDD
       AND TV.NUM_TELEFONE_VOIP = P_TERMINAL
       AND TV.NUM_CONTRATO = P_CONTRATO;

  -- DECLARA  O DE VARIAVEIS LOCAL
  VSESSAO PROD_JD.SO_ORDENA.SESSAO%TYPE := '373';
  V_CUR   CUR_PRINCIPAL%ROWTYPE;
  P_NETFONE EXCEPTION;
  VPORTADO varchar(2);
  P_PORTADO EXCEPTION;
  P_NETFONE2 EXCEPTION;
  P_CENARIO_ERRADO EXCEPTION;
  VERRO1         VARCHAR2(1000);
  P_XML          VARCHAR2(1000);
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  -- PROCEDURE P/ GRAVA  O DE VOLUMETRIA
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PMENSAGEM,
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS

    L_ID_OFENSOR    VARCHAR(20) := '373';
    L_CALC_CHECKSUM VARCHAR(20);

    -- VAR VEIS NECESS RIAS PARA GERAR OCORR NCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): PARA ENVIO DO SERVIÇO 22 PARA ALTERAR O STATUS DO BILHETE PARA ATIVO.';

  BEGIN
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'mmdd'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;

    -- CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVA  O JUNTO A EBT
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
  END PR_INSEREOCORRENCIA;

  -- FUNȃO QUE VERIFICA NUMERO PORTADO
  FUNCTION E_PORTADO(P_DDD_TELEFONE_VOIP IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE,
                     P_NUM_TELEFONE_VOIP IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE)
    RETURN NUMBER IS
    QTDE_PORTADO NUMBER;
  BEGIN
    BEGIN
      SELECT COUNT(1)
        INTO QTDE_PORTADO
        FROM PROD_JD.SN_TELEFONE_VOIP TV
       WHERE TV.DDD_TELEFONE_VOIP = P_DDD
         AND TV.NUM_TELEFONE_VOIP = P_TERMINAL
         AND TV.FC_NUMERO_PORTADO = 'S'
         AND TV.DT_FIM =
           (SELECT MAX(TV1.DT_FIM)
              FROM PROD_JD.SN_TELEFONE_VOIP TV1
             WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
               AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP);
    EXCEPTION
      WHEN OTHERS THEN
        QTDE_PORTADO := 0;
    END;
    RETURN(QTDE_PORTADO);
  END E_PORTADO;

  -- *INICIO DO PROCESSO* --
BEGIN

  -- BUSCA A BASE
  SELECT UPPER(NM_ALIAS)
    INTO VBASE
    FROM PROD_JD.SN_CIDADE_BASE
   WHERE COD_OPERADORA = P_COD_OPERADORA;

  IF E_PORTADO(P_DDD, P_TERMINAL) > 0 THEN
    OPEN CUR_PRINCIPAL;
    FETCH CUR_PRINCIPAL
      INTO V_CUR;

    IF (CUR_PRINCIPAL%FOUND) THEN

      IF V_CUR.BP IS NOT NULL THEN

        BEGIN
        P_XML := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:open="http://www.openuri.org/">' ||
                        '<soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' ||
                           '<ns1:NetServicoHeader xmlns:ns1="http://netservicos.com.br/NetHeader">' ||
                              VBASE ||
                           '</ns1:NetServicoHeader>' ||
                        '</soap:Header>' ||
                        '<soapenv:Body>' ||
                           '<open:clientRequestwithReturn>' ||
                              '<open:entrada>' ||
                                 '<NUMEROBILHETEPORTABILIDADE>' || V_CUR.BP || '</NUMEROBILHETEPORTABILIDADE>' ||
                                 '<CODIGOLOGINRECEPTORPORTABILIDADE>OPS$PORT</CODIGOLOGINRECEPTORPORTABILIDADE>' ||
                              '</open:entrada>' ||
                           '</open:clientRequestwithReturn>' ||
                        '</soapenv:Body>' ||
                   '</soapenv:Envelope>';

          -- RECUPERA O ID_ASSINANTE
          SELECT CT.ID_ASSINANTE
            INTO G_ID_ASSINANTE
            FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
           WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
             AND CT.CID_CONTRATO = V_CUR.CID_CONTRATO
             AND CT.NUM_CONTRATO = P_CONTRATO;

          -- INSERE OCORRENCIA
          PR_INSEREOCORRENCIA(G_ID_ASSINANTE);

          -- LOGA PROCESSO OK, PASSANDO XML NO CAMPO5
          GRAVALOG(VSESSAO,
                   SYSDATE,
                   VBASE,
                   'TRATAMENTO EFETUADO',
                   P_DDD || P_TERMINAL,
                   P_COD_OPERADORA || '/' || P_CONTRATO,
                   P_XML);

          CLOSE CUR_PRINCIPAL;

          -- RETORNO PARA O CX_ORACLE (PYTHON)
          --   :OUT := P_XML;
           RAISE P_PORTADO;
        END;
      END IF;
    
    ELSE
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'Esta opção deve ser utilizada somente para números portados, onde o bilhete de portabilidade esteja com status desconexão pendente. Sendo necessário alterar o status para ativo.',
               P_DDD || P_TERMINAL,
               P_COD_OPERADORA || '/' || P_CONTRATO);
      CLOSE CUR_PRINCIPAL;
      RAISE P_CENARIO_ERRADO;
    END IF;  
     ELSE
  
    RAISE P_NETFONE2;
  END IF;
  
  --RAISE P_CENARIO_ERRADO;
EXCEPTION
  WHEN P_NETFONE2 THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'O terminal inserido é um NETFONE e deve ser aberto um chamado através da Ferramenta Suporte OS/OC.');
  WHEN P_NETFONE THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Esta opção deve ser utilizada somente para números portados, onde o bilhete de portabilidade esteja com status desconexão pendente. Sendo necessário alterar o status para ativo.');
  WHEN P_PORTADO THEN
    RAISE_APPLICATION_ERROR(-20010,
                            'Sua solicitação foi atendida com sucesso. Histórico de portabilidade foi regularizado, acessar a ferramenta NETSALES> PORTABILIDADE> HISTÓRICO.');
    NULL;
  WHEN P_CENARIO_ERRADO THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Esta opção deve ser utilizada somente para números portados, onde o bilhete de portabilidade esteja com status desconexão pendente. Sendo necessário alterar o status para ativo.');
  WHEN OTHERS THEN
    -- GRAVA LOG DO ERRO
    VERRO1 := SQLERRM;
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             VERRO1,
             P_DDD || P_TERMINAL,
             P_COD_OPERADORA || '/' || P_CONTRATO);
    --END;
END;

