DECLARE
  -- Variaveis dos parametros de input:
  /*  IN_COD_CIDADE   prod_jd.sn_cidade_operadora.cod_operadora%type := '&COD_CIDADE';
  IN_NUM_CONTRATO PROD_JD.SN_CONTRATO.NUM_CONTRATO%type := '&NUM_CONTRATO';
  IN_DDD_TERMINAL prod_jd.sn_portabilidade.ddd_telefone_voip%type := '&DDD_TERMINAL';
  IN_NUM_TERMINAL prod_jd.sn_portabilidade.num_telefone_voip%type := '&NUM_TERMINAL';*/

  IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '[OPERADORA]';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := [CONTRATO];
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('[TERMINAL]', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('[TERMINAL]', 3, 8);

  -------------

  E_PROCESSO_OK EXCEPTION;
  vDadosException        VARCHAR2(500);
  V_ERRO                 VARCHAR(1000);
  G_ID_ASSINANTE         SN_ASSINANTE.ID_ASSINANTE%TYPE;
  VNUMERO_PORTADO        PROD_JD.SN_TELEFONE_VOIP.FC_NUMERO_PORTADO%TYPE;
  VDDD_TELEFONE_VOIP     PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE;
  VNUM_TELEFONE_VOIP     PROD_JD.SN_TELEFONE_VOIP.NUM_TELEFONE_VOIP%TYPE;
  VNUM_CONTRATO          PROD_JD.SN_TELEFONE_VOIP.NUM_CONTRATO%TYPE;
  VSTATUS                PROD_JD.SN_HIST_STATUS_PORTAB.ID_STATUS_PORTABILIDADE%TYPE;
  V_cid_contrato         PROD_JD.SN_TELEFONE_VOIP.cid_contrato%type;
  V_DDD_TELEFONE         PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE;
  V_NUM_TELEFONE         PROD_JD.SN_TELEFONE_VOIP.NUM_TELEFONE_VOIP%TYPE;
  VTM_ID                 PROD_JD.SN_TELEFONE_VOIP.TM_ID%TYPE;
  V_ID_PONTO             PROD_JD.SN_TELEFONE_VOIP.ID_PONTO%TYPE;
  V_ID_PONTO_OUTRO       PROD_JD.SN_TELEFONE_VOIP.ID_PONTO%TYPE;
  solic_modificar_fqdn   VARCHAR2(50);
  solic_modificar_EXISTE VARCHAR2(50);
  V_existe_cn            NUMBER;
  V_TERMINAL             NUMBER;
  V_cod_oc               prod_jd.sn_oc.cod_oc%type;
  V_cod_oc_57            prod_jd.sn_oc.cod_oc%type;
  V_cod_oc1              prod_jd.sn_oc.cod_oc%type;
  FQDN_NOVO              VARCHAR2(30);
  NCONTRATO              VARCHAR2(20);
  NCIDADE                VARCHAR2(20);
  NID_PONTO              VARCHAR2(20);
  NTM_ID                 VARCHAR2(20);
  NID_PRODUTO            VARCHAR2(20);
  --------------------

  --RECRIA PONTO
  PROCEDURE PRC_EXCLUIR_PONTO(P_ID_PONTO SN_PONTO_HISTORICO.ID_PONTO%TYPE) IS
  
    vRet PV_ORDEM_ATIVACAO.ID_ORDEM_ATIVACAO%TYPE;
  
  BEGIN
  
    SELECT RPP.NUM_CONTRATO,
           RPP.CID_CONTRATO,
           RPP.ID_PONTO,
           -- RPP.ID_PRODUTO,
           PH.TM_ID
      INTO NCONTRATO,
           NCIDADE,
           NID_PONTO,
           --NID_PRODUTO
           NTM_ID
      FROM SN_REL_PONTO_PRODUTO RPP, SN_PONTO_HISTORICO PH
     WHERE RPP.ID_PONTO = P_ID_PONTO
       AND PH.ID_PONTO = RPP.ID_PONTO
       AND RPP.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND RPP.INSTALADO = 1
       AND PH.ID_PONTO = RPP.ID_PONTO
       AND PH.INSTALADO = 1
       AND PH.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND PH.TM_ID IS NOT NULL;
    begin
      PROD_JD.PKG_PROCESSA_ORDEM_ATIVACAO.INCLUIORDEM(pCidContrato         => NCIDADE,
                                                      pCodOC               => NULL,
                                                      pIdTipoOC            => 43 --REMOVER EMTA
                                                     ,
                                                      pIdTipoSolic         => 220 --DEVOLUÇÃO DE EMTA
                                                     ,
                                                      pIdVenda             => NULL,
                                                      pNumContrato         => NCONTRATO,
                                                      pNumContratoAnterior => NULL,
                                                      pIdProduto           => 999,
                                                      pIdPonto             => NID_PONTO,
                                                      pIdCaracteristica    => 4,
                                                      pTM_ID               => NTM_ID,
                                                      pTM_ID_Troca         => NULL,
                                                      pTM_ID_ASSOC         => NULL,
                                                      pIdSolilcitacaoAss   => NULL,
                                                      pCdEnderecavel       => NULL,
                                                      pIdTipoOrdemAtivacao => 5,
                                                      pUSRHistorico        => 'PROD_JD',
                                                      pIdOrdemAtivacao     => NTM_ID);
    
    exception
      when others then
        rollback;
    end;
  END PRC_EXCLUIR_PONTO;

  PROCEDURE PRC_GERACAO_PONTO(P_ID_PONTO SN_PONTO_HISTORICO.ID_PONTO%TYPE) IS
  
    vRet PV_ORDEM_ATIVACAO.ID_ORDEM_ATIVACAO%TYPE;
  BEGIN
    SELECT RPP.NUM_CONTRATO,
           RPP.CID_CONTRATO,
           RPP.ID_PONTO,
           RPP.ID_PRODUTO,
           PH.TM_ID
      INTO NCONTRATO, NCIDADE, NID_PONTO, NID_PRODUTO, NTM_ID
      FROM SN_REL_PONTO_PRODUTO RPP, SN_PONTO_HISTORICO PH
     WHERE RPP.ID_PONTO = P_ID_PONTO
       AND PH.ID_PONTO = RPP.ID_PONTO
       AND RPP.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND RPP.INSTALADO = 1
       AND PH.ID_PONTO = RPP.ID_PONTO
       AND PH.INSTALADO = 1
       AND PH.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND PH.TM_ID IS NOT NULL;
    begin
      PROD_JD.PKG_PROCESSA_ORDEM_ATIVACAO.INCLUIORDEM(pCidContrato         => NCIDADE,
                                                      pCodOC               => NULL,
                                                      pIdTipoOC            => 42 -- TIPO OC INICIALIZAR E HABILITAR EMTA 
                                                     ,
                                                      pIdTipoSolic         => 3 -- TIPO SOLIC ADESAO - INSTALAR PONTO ADICIONAL   
                                                     ,
                                                      pIdVenda             => NULL,
                                                      pNumContrato         => NCONTRATO,
                                                      pNumContratoAnterior => NULL,
                                                      pIdProduto           => NID_PRODUTO,
                                                      pIdPonto             => NID_PONTO,
                                                      pIdCaracteristica    => 4,
                                                      pTM_ID               => NTM_ID,
                                                      pTM_ID_Troca         => NULL,
                                                      pTM_ID_ASSOC         => NULL,
                                                      pIdSolilcitacaoAss   => NULL,
                                                      pCdEnderecavel       => NULL,
                                                      pIdTipoOrdemAtivacao => 5,
                                                      pUSRHistorico        => 'PROD_JD',
                                                      pIdOrdemAtivacao     => vRet);
    
    exception
      when others then
        rollback;
    end;
  
  END PRC_GERACAO_PONTO;

  -- REEVNIAR TROCA
  PROCEDURE ENVIA_HIT_TROCA_FQDN(V_DDD_TELEFONE IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE,
                                 V_NUM_TELEFONE IN PROD_JD.SN_TELEFONE_VOIP.NUM_TELEFONE_VOIP%TYPE,
                                 FQDN_NOVO      IN PROD_JD.SN_TELEFONE_VOIP.FQDN%TYPE,
                                 VP_PORTA       IN PROD_JD.SN_TELEFONE_VOIP.NUM_PORTA%TYPE
                                 --V_cod_oc    IN prod_jd.sn_oc.cod_oc%type
                                 ) IS
  BEGIN
    FOR V IN (SELECT TV.*,
                     ASS.NOME_TITULAR,
                     ASS.CPF,
                     CO.COD_OPERADORA,
                     CON.NUM_CONTRATO_EMBRATEL
                FROM PROD_JD.SN_TELEFONE_VOIP    TV,
                     PROD_JD.SN_CONTRATO         CON,
                     PROD_JD.SN_ASSINANTE        ASS,
                     PROD_JD.SN_CIDADE_OPERADORA CO
               WHERE TV.DDD_TELEFONE_VOIP = V_DDD_TELEFONE
                 AND TV.NUM_TELEFONE_VOIP = V_NUM_TELEFONE
                 AND TV.CID_CONTRATO = CO.CID_CONTRATO
                 AND TV.FQDN IS NOT NULL
                 AND TV.DT_FIM =
                     (SELECT MAX(TV1.DT_FIM)
                        FROM PROD_JD.SN_TELEFONE_VOIP TV1
                       WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                         AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                         AND TV.FQDN IS NOT NULL)
                 AND ROWNUM < 2
                 AND CON.NUM_CONTRATO = TV.NUM_CONTRATO
                 AND CON.CID_CONTRATO = TV.CID_CONTRATO
                 AND ASS.ID_ASSINANTE = CON.ID_ASSINANTE) LOOP
      INSERT INTO PROD_JD.PP_VOIPHIT
        (ID_HIT,
         COD_OC,
         TIPO_REGISTRO_HEADER,
         DATA_REGISTRO_HEADER,
         HORA_REGISTRO_HEADER,
         FILLER_HEADER,
         TIPO_REGISTRO,
         ID_TRANSACAO_NET,
         TIPO_REGISTRO_DETALHE,
         COD_OPERADORA_CLIENTE,
         NUM_CLIENTE,
         COD_OPERADORA_CLIENTE_NOVO,
         NUM_CLIENTE_NOVO,
         NOME_CLIENTE,
         CPF_CNPJ_PASSAPORTE,
         TIPO_CLIENTE,
         TEL_RESIDENCIAL_CLIENTE,
         TEL_CELULAR_CLIENTE,
         TIPO_LOGRADOURO_INST,
         NOME_LOGRADOURO_INST,
         NUM_LOGRADOURO_INST,
         REF_ENDERECO_INST,
         COMPL_ENDERECO_INST,
         BAIRRO_INST,
         CEP_INST,
         CIDADE_INST,
         UF_INST,
         INSCR_ESTADUAL_INST,
         NOME_LOGRADOURO_COBR,
         BAIRRO_COBR,
         CEP_COBR,
         CIDADE_COBR,
         UF_COBR,
         DDD_TELEFONE_CONTR,
         NUM_TELEFONE_CONTR,
         DDD_TELEFONE_CONTR_NOVO,
         NUM_TELEFONE_CONTR_NOVO,
         INDIC_CONTRATACAO_CLIENTE,
         INDIC_COBR_INST,
         INDIC_FIG_LTELEFONICA,
         DESCR_FIG_LTELEFONICA,
         PLANO_TELEFONIA_LOCAL,
         PLANO_LDISTANCIA_NACIONAL,
         PLANO_LDISTANCIA_INTERNACIONAL,
         DATA_EVENTO,
         DIA_VENCTO_CLIENTE,
         NOME_EQPTO_MTA_FQDN,
         PORTA_EQUIPAMENTO,
         NOME_EQPTO_MTA_FQDN_NOVO,
         PORTA_EQUIPAMENTO_NOVO,
         COD_RETORNO,
         MENSAGEM_RETORNO,
         COD_INDIC_FACILIDADE_ADIC,
         INDIC_CONTR_FACILIDADE,
         DATA_SOLIC_FACILIDADE,
         CCORRENTE_CLIENTE,
         DATA_CONFIRM,
         TIPO_REGISTRO_TRAILLER,
         QTDE_REGISTRO_TRAILLER,
         FILLER_TRAILLER,
         NOME_ARQUIVO,
         STATUS_ARQUIVO,
         USR_VOIPHIT,
         ID_PRODUTO,
         FC_NUMERO_PORTADO,
         FC_INTERCEPTADO,
         FC_PROCESSAMENTO_ONLINE,
         ID_TRANSACAO_NET_TRONCO)
      VALUES
        (PROD_JD.SEQ_ID_VOIPHIT.NEXTVAL,
         '-4',
         'H',
         TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYMMDD')),
         TO_NUMBER(TO_CHAR(SYSDATE, 'HH24MISS')),
         '',
         'AL',
         PROD_JD.SEQ_ID_TRANSACAO_NET.NEXTVAL,
         '1',
         V.COD_OPERADORA,
         V.NUM_CONTRATO,
         '',
         '',
         V.NOME_TITULAR,
         V.CPF,
         '2',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         V.DDD_TELEFONE_VOIP,
         V.NUM_TELEFONE_VOIP,
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         '',
         TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYMMDD')),
         '',
         V.FQDN,
         V.NUM_PORTA,
         FQDN_NOVO,
         2,
         NULL,
         NULL,
         '',
         '',
         '',
         '',
         '',
         'T',
         '0',
         '',
         '0',
         'PND',
         'PORT_FQDN',
         '',
         V.FC_NUMERO_PORTADO,
         V.FC_INTERCEPTADO,
         'N',
         '');
    END LOOP;
  END ENVIA_HIT_TROCA_FQDN;

  --Procedure -- Gera SOLIC --
  PROCEDURE PRC_ABRE_SOLIC(P_CID_CONTRATO  IN PROD_JD.SN_CIDADE_OPERADORA.CID_CONTRATO%TYPE,
                           P_NUM_CONTRATO  IN PROD_JD.SN_CONTRATO.NUM_CONTRATO%TYPE,
                           P_ID_PONTO      IN PROD_JD.SN_REL_PONTO_PRODUTO.ID_PONTO%TYPE,
                           P_ID_TIPO_SOLIC IN PROD_JD.SN_TIPO_SOLIC_PROD.ID_TIPO_SOLIC%TYPE) IS
    vID_TIPO_SOLIC      PROD_JD.SN_TIPO_SOLIC_PROD.ID_TIPO_SOLIC%TYPE;
    vID_TIPO_SOLIC_PROD PROD_JD.SN_TIPO_SOLIC_PROD.ID_TIPO_SOLIC_PROD%TYPE;
    IMED                PROD_JD.SN_SOLICITACAO_ASS.IMEDIATA%TYPE;
    ID_ASS              PROD_JD.SN_SOLICITACAO_ASS.ID_SOLICITACAO_ASS%TYPE;
    vVALIDA_PTOS        BOOLEAN;
  BEGIN
    SELECT ID_TIPO_SOLIC_PROD
      INTO vID_TIPO_SOLIC_PROD
      FROM PROD_JD.SN_TIPO_SOLIC_PROD
     WHERE ID_TIPO_SOLIC = P_ID_TIPO_SOLIC
       AND ID_PROD_PARA = 999
       AND ID_PROD_DE = 999;
  
    PROD_JD.PGSN_MAN_STATUS_SERVICOS.PRSN_SOLIC_PERMITIDA_NETSMS(PNUM_CONTRATO       => P_NUM_CONTRATO,
                                                                 PCID_CONTRATO       => P_CID_CONTRATO,
                                                                 PID_TIPO_SOLIC      => P_ID_TIPO_SOLIC,
                                                                 PIDTIPOSOLICPROD    => vID_TIPO_SOLIC_PROD,
                                                                 PID_TIPO_FECHAMENTO => NULL,
                                                                 PDT_BAIXA           => NULL,
                                                                 PACAO               => 1);
    PROD_JD.PSNSOLICASS.VIDPONTO := P_ID_PONTO;
  
    ID_ASS := PROD_JD.SSN_ID_SOLICITACAO_ASSINANTE.NEXTVAL;
  
    INSERT INTO PROD_JD.SN_SOLICITACAO_ASS
      (ID_SOLICITACAO_ASS,
       NUM_CONTRATO,
       CID_CONTRATO,
       ID_TIPO_SOLIC,
       NOME_SOLIC,
       DT_CADASTRO,
       USR_CADASTRO,
       ID_PLANO_PGTO,
       ID_TIPO_SOLIC_PROD,
       IMEDIATA,
       ISENTO)
    VALUES
      (ID_ASS,
       P_NUM_CONTRATO,
       P_CID_CONTRATO,
       P_ID_TIPO_SOLIC,
       'MODIFICAR FQDN-PORTA',
       SYSDATE,
       'PROD_JD',
       23,
       vID_TIPO_SOLIC_PROD,
       0,
       1);
  
    IF P_ID_TIPO_SOLIC = 225 THEN
      UPDATE PROD_JD.SN_OS OSS
         SET OSS.IMEDIATA = 0
       WHERE OSS.ID_SOLICITACAO_ASS = ID_ASS;
    END IF;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
    
      Raise_application_error(-20000,
                              'Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa. Caso o erro persista, será necessário abrir chamado no Service Desk');
    
  END PRC_ABRE_SOLIC;

  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN SN_CONTRATO.ID_ASSINANTE%TYPE) IS
  
    L_ID_OFENSOR    VARCHAR(20) := ''; --  PROCESSAMENTO NAO EFETUADO
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VAR�VEIS NECESS�RIAS PARA GERAR OCORR�NCIA
    VIDOCORRENCIA     SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            SN_OCORRENCIA.ID_USR%TYPE := USER; --prod_jd
    VSIT              SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S):  SOLICITAÇÃO DE MODIFICAR FQDN-PORTA GERADA PARA CORREÇÃO DO FQDN-PORTA DO CONTRATO';
  
  BEGIN
  
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(SUBSTR(L_ID_OFENSOR, 2)) +
           TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    --CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVA��O JUNTO A EBT
    PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                       VIDTIPOOCORRENCIA,
                                       P_IDASS,
                                       VNOMEINFORMANTE || L_CALC_CHECKSUM,
                                       VTELINFORMANTE,
                                       VDTOCORRENCIA,
                                       VIDUSR,
                                       VSIT,
                                       VDTRETORNO,
                                       VIDORIGEM,
                                       L_ID_OFENSOR || VOBS);
  
    COMMIT;
  
  END PR_INSEREOCORRENCIA;

BEGIN

  -- DBMS_OUTPUT.ENABLE(NULL);

  V_ERRO := NULL;

  begin
    begin
    
      SELECT TV.FC_NUMERO_PORTADO,
             TV.DDD_TELEFONE_VOIP,
             TV.NUM_TELEFONE_VOIP,
             TV.NUM_CONTRATO,
             TV.CID_CONTRATO,
             TV.TM_ID,
             TV.ID_PONTO
        INTO VNUMERO_PORTADO,
             VDDD_TELEFONE_VOIP,
             VNUM_TELEFONE_VOIP,
             VNUM_CONTRATO,
             V_cid_contrato,
             VTM_ID,
             V_ID_PONTO
        FROM PROD_JD.SN_TELEFONE_VOIP TV, prod_jd.sn_cidade_operadora op
       WHERE tv.cid_contrato = op.cid_contrato
         and TV.NUM_CONTRATO = IN_NUM_CONTRATO
         and TV.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
         and TV.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
         and op.cod_operadora = IN_COD_CIDADE
         AND TV.ID_STATUS_TELEFONE_VOIP = ('U')
         AND TV.TM_ID IS NOT NULL
         AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
         AND ROWNUM < 2;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        VNUMERO_PORTADO    := NULL;
        VDDD_TELEFONE_VOIP := NULL;
        VNUM_TELEFONE_VOIP := NULL;
        VNUM_CONTRATO      := NULL;
        V_cid_contrato     := NULL;
        VTM_ID             := NULL;
        V_ID_PONTO         := NULL;
      
    end;
    BEGIN
      SELECT sola.id_solicitacao_ass
        INTO solic_modificar_EXISTE
        FROM PROD_JD.sn_solicitacao_ass sola, PROD_JD.sn_telefone_voip tv
       WHERE sola.num_contrato = tv.num_contrato
         AND sola.id_tipo_solic IN (225)
         AND sola.id_tipo_fechamento IS NULL
         AND tv.ddd_telefone_voip = IN_DDD_TERMINAL
         AND tv.num_telefone_voip = IN_NUM_TERMINAL
         AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
         AND tv.num_contrato = IN_NUM_CONTRATO
         AND sola.id_solicitacao_ass =
             (SELECT MAX(sol.id_solicitacao_ass)
                FROM PROD_JD.sn_solicitacao_ass sol
               WHERE sol.num_contrato = sola.num_contrato
                 AND sol.id_tipo_solic IN (225));
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        solic_modificar_EXISTE := NULL;
      
    end;
    -- verificar se existe CN EXE
    BEGIN
      SELECT HIT.DDD_TELEFONE_CONTR
        into V_existe_cn
        FROM PROD_JD.PP_VOIPHIT HIT
       WHERE HIT.ID_HIT =
             (SELECT MAX(HIT1.ID_HIT)
                FROM PROD_JD.PP_VOIPHIT HIT1
               WHERE HIT1.NUM_TELEFONE_CONTR = HIT.NUM_TELEFONE_CONTR
                 AND HIT1.DDD_TELEFONE_CONTR = HIT.DDD_TELEFONE_CONTR
                 AND HIT1.STATUS_ARQUIVO = 'EXE'
                 AND HIT1.TIPO_REGISTRO IN ('CN'))
         and HIT.DDD_TELEFONE_CONTR = TO_CHAR(VDDD_TELEFONE_VOIP)
         and HIT.NUM_TELEFONE_CONTR = TO_CHAR(VNUM_TELEFONE_VOIP)
         and hit.NUM_CLIENTE = TO_CHAR(VNUM_CONTRATO)
         AND NOT EXISTS
       (SELECT 1
                FROM PP_VOIPHIT VH1
               WHERE VH1.DDD_TELEFONE_CONTR = HIT.DDD_TELEFONE_CONTR
                 AND VH1.NUM_TELEFONE_CONTR = HIT.NUM_TELEFONE_CONTR
                 AND VH1.NUM_CLIENTE = HIT.NUM_CLIENTE
                 AND VH1.TIPO_REGISTRO IN ('IN', 'RI', 'DE')
                 AND VH1.STATUS_ARQUIVO = 'EXE'
                 AND VH1.ID_TRANSACAO_NET > HIT.ID_TRANSACAO_NET);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_existe_cn := NULL;
      
    end;
  
    begin
    
      select SHSP.ID_STATUS_PORTABILIDADE
        INTO VSTATUS
        FROM PROD_JD.SN_TELEFONE_VOIP      TV,
             PROD_JD.SN_PORTABILIDADE      SN,
             PROD_JD.SN_HIST_STATUS_PORTAB SHSP
       WHERE TV.ID_STATUS_TELEFONE_VOIP = 'U'
         AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
         AND (TV.DT_INI =
             (SELECT MAX(TV1.DT_INI)
                 FROM PROD_JD.SN_TELEFONE_VOIP TV1
                WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                  AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP))
         AND TV.DDD_TELEFONE_VOIP = SN.DDD_TELEFONE_VOIP
         AND TV.NUM_TELEFONE_VOIP = SN.NUM_TELEFONE_VOIP
         AND TV.CID_CONTRATO = SN.CID_CONTRATO
         and TV.DDD_TELEFONE_VOIP = VDDD_TELEFONE_VOIP
         and TV.NUM_TELEFONE_VOIP = VNUM_TELEFONE_VOIP
         AND TV.NUM_CONTRATO = VNUM_CONTRATO
         AND TV.CID_CONTRATO = V_cid_contrato
         AND SN.NR_PROTOCOLO_BP IS NOT NULL
         AND SN.TP_PORTABILIDADE = 'PTB'
         AND SHSP.ID_PORTABILIDADE = SN.ID_PORTABILIDADE
         AND SHSP.ID_STATUS_PORTABILIDADE = 2
         AND SHSP.ID_HIST_STATUS_PORTAB =
             (SELECT MAX(SH.ID_HIST_STATUS_PORTAB)
                FROM PROD_JD.SN_HIST_STATUS_PORTAB SH
               WHERE SH.ID_PORTABILIDADE = SHSP.ID_PORTABILIDADE)
         AND not EXISTS
       (SELECT (1)
                FROM PROD_JD.SN_PORTABILIDADE      SN2,
                     PROD_JD.SN_HIST_STATUS_PORTAB SMP3
               WHERE SN2.DDD_TELEFONE_VOIP = SN.DDD_TELEFONE_VOIP
                 AND SN2.NUM_TELEFONE_VOIP = SN.NUM_TELEFONE_VOIP
                 AND SMP3.ID_PORTABILIDADE = SN2.ID_PORTABILIDADE
                 AND SN2.NR_PROTOCOLO_BP > SN.NR_PROTOCOLO_BP
                 AND SN2.TP_PORTABILIDADE = 'DOA'
                 AND SMP3.ID_STATUS_PORTABILIDADE >
                     SHSP.ID_STATUS_PORTABILIDADE
                 AND SMP3.ID_STATUS_PORTABILIDADE IN (8, 2))
         and rownum < 2;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        VSTATUS := NULL;
      
    end;
  
    begin
      SELECT VOIP.DDD_TELEFONE_VOIP, VOIP.NUM_TELEFONE_VOIP, VOIP.ID_PONTO
        INTO V_DDD_TELEFONE, V_NUM_TELEFONE, V_ID_PONTO_OUTRO
        FROM PROD_JD.SN_TELEFONE_VOIP VOIP
       WHERE VOIP.NUM_CONTRATO = VNUM_CONTRATO
         AND VOIP.CID_CONTRATO = V_cid_contrato
         AND VOIP.DDD_TELEFONE_VOIP = VDDD_TELEFONE_VOIP
         AND VOIP.NUM_TELEFONE_VOIP <> VNUM_TELEFONE_VOIP
         AND VOIP.ID_STATUS_TELEFONE_VOIP = 'U'
         and VOIP.TM_ID = VTM_ID
         AND VOIP.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
         AND VOIP.ID_STATUS_TELEFONE_VOIP = 'U'
         AND ROWNUM < 2;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_DDD_TELEFONE   := NULL;
        V_NUM_TELEFONE   := NULL;
        V_ID_PONTO_OUTRO := NULL;
      
    end;
  
    begin
      SELECT count(*)
        INTO V_TERMINAL
        FROM PROD_JD.SN_TELEFONE_VOIP VOIP
       WHERE VOIP.NUM_CONTRATO = VNUM_CONTRATO
         AND VOIP.CID_CONTRATO = V_cid_contrato
         AND VOIP.DDD_TELEFONE_VOIP = VDDD_TELEFONE_VOIP
         AND VOIP.NUM_TELEFONE_VOIP <> VNUM_TELEFONE_VOIP
         AND VOIP.ID_STATUS_TELEFONE_VOIP = 'U'
         and VOIP.TM_ID = VTM_ID
         AND VOIP.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
         AND VOIP.ID_STATUS_TELEFONE_VOIP = 'U'
         AND ROWNUM < 2;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_TERMINAL := NULL;
      
    end;
  
    IF VNUMERO_PORTADO IS NULL THEN
      begin
      
        Raise_Application_Error(-20050,
                                'Não foram encontrados registros válidos para esta pesquisa. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa. O termimnal informado não esta ativo ou não possui equipamento vinculado ao ponto');
      
      END;
    
    ELSIF solic_modificar_EXISTE IS NOT NULL THEN
      begin
      
        Raise_Application_Error(-20050,
                                'Já existe uma solicitação: ' ||
                                solic_modificar_existe ||
                                ' aberta para o terminal informado. Favor acessar a ferramenta netsms > aba produtos e realizar abaixa dessa solicitação. Para que possa gerar uma nova solicitação para o termimnal informado');
      
      END;
      -- END IF;
    
    ELSIF VNUMERO_PORTADO = 'S' and VSTATUS is not null THEN
      -- TRATAMENTO TERMINAIS PORTADOS 
      BEGIN
        IF V_DDD_TELEFONE IS NOT NULL THEN
          BEGIN
            BEGIN
              FOR v IN (SELECT tv.*
                          FROM prod_jd.sn_telefone_voip tv
                         WHERE tv.dt_fim =
                               to_date('30/12/2049', 'dd/mm/rrrr')
                           AND tv.id_status_telefone_voip = 'U'
                           AND tv.ddd_telefone_voip =
                               to_char(VDDD_TELEFONE_VOIP)
                           AND tv.num_telefone_voip =
                               to_char(VNUM_TELEFONE_VOIP)
                           AND rownum < 2) LOOP
              
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2049', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              
                INSERT INTO prod_jd.sn_telefone_voip
                  (cid_contrato,
                   num_contrato,
                   id_ponto,
                   ddd_telefone_voip,
                   num_telefone_voip,
                   fqdn,
                   num_porta,
                   dt_ini,
                   dt_fim,
                   dt_alteracao,
                   id_status_telefone_voip,
                   golden,
                   tm_id,
                   id_escolhido,
                   publicar,
                   nome_publicacao,
                   num_contrato_avaliacao,
                   id_sistema_externo,
                   cid_contrato_origem,
                   id_softx,
                   fc_numero_portado,
                   fc_interceptado)
                VALUES
                  (v.cid_contrato,
                   v.num_contrato,
                   v.id_ponto,
                   v.ddd_telefone_voip,
                   v.num_telefone_voip,
                   v.fqdn,
                   v.num_porta,
                   sysdate,
                   to_date('30/12/2049', 'dd/mm/rrrr'),
                   sysdate,
                   'U',
                   v.golden,
                   v.tm_id,
                   v.id_escolhido,
                   v.publicar,
                   v.nome_publicacao,
                   v.num_contrato_avaliacao,
                   v.id_sistema_externo,
                   v.cid_contrato_origem,
                   v.id_softx,
                   v.fc_numero_portado,
                   v.fc_interceptado);
              END LOOP;
              COMMIT;
            end;
          
            --terminias com mas de um termimnal no mesmo tm_id
          
            PRC_ABRE_SOLIC(V_cid_contrato, VNUM_CONTRATO, V_ID_PONTO, 225); -- Abre MODIFICAR FQDN-PORTA           
          
            -- abrir a solic gerada e pegar o id da solic gerada segundo terminal 
          
            begin
              SELECT sola.id_solicitacao_ass
                INTO solic_modificar_fqdn
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento = 1
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 and tv.id_ponto = V_ID_PONTO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                solic_modificar_fqdn := NULL;
              
            end;
          
            --SEGUNDA OC
            begin
              SELECT oc.cod_oc
                INTO V_cod_oc_57
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento IS NULL
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_cod_oc_57 := NULL;
              
            end;
          
            --  DBMS_OUTPUT.PUT_LINE(solic_modificar_fqdn); 
          
            UPDATE prod_jd.sn_solicitacao_ass sa
               SET sa.dt_baixa           = '',
                   sa.id_tipo_fechamento = '',
                   sa.usr_baixa          = ''
             WHERE sa.id_solicitacao_ass = solic_modificar_fqdn;
            commit;
          
            UPDATE prod_jd.sn_oc oc
               SET OC.dt_baixa           = sysdate,
                   OC.id_tipo_fechamento = 1,
                   OC.usr_baixa          = 'PROD_JD'
             WHERE OC.COD_OC = V_cod_oc_57;
            commit;
          
            BEGIN
              -- GERA FQDN NOVO PARA OS DOIS TERMINAIS
              SELECT PROD_JD.PSN_MANTELEFONE_VOIP.GERA_FQDN_TELEFONE_VOIP(TV.CID_CONTRATO)
                INTO FQDN_NOVO
                FROM PROD_JD.SN_TELEFONE_VOIP    TV,
                     PROD_JD.SN_CIDADE_OPERADORA CID
               WHERE TV.CID_CONTRATO = CID.CID_CONTRATO
                 AND TV.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND TV.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
            
              UPDATE PROD_JD.SN_TELEFONE_VOIP HIT -- FAZ UPDATE DO NOVO FQDN NO OUTRO TN DO CONTRATO
                 SET HIT.FQDN = FQDN_NOVO, HIT.NUM_PORTA = 1
               WHERE HIT.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND HIT.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND HIT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
              COMMIT;
            
              UPDATE PP_VOIPHIT PVHX
                 SET PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO,
                     PVHX.PORTA_EQUIPAMENTO_NOVO   = 1
              --PVHX.FC_NUMERO_PORTADO        = 'S'
              --PVHX.COD_OC                   = V_cod_oc
               WHERE PVHX.STATUS_ARQUIVO in ('ESP', 'PND')
                 AND PVHX.TIPO_REGISTRO = 'AL'
                 AND PVHX.DDD_TELEFONE_CONTR = VDDD_TELEFONE_VOIP
                 AND PVHX.NUM_TELEFONE_CONTR = VNUM_TELEFONE_VOIP;
              --AND PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO;
              COMMIT;
              begin
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = sysdate
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              end;
            
              BEGIN
                PRC_EXCLUIR_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
                PRC_GERACAO_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
              END;
            
            END;
            IF V_cod_oc_57 IS NOT NULL THEN
            
              --PRC_ABRE_SOLIC(V_cid_contrato, VNUM_CONTRATO, V_ID_PONTO_OUTRO, 225); -- Abre MODIFICAR FQDN-PORTA
              ENVIA_HIT_TROCA_FQDN(V_DDD_TELEFONE,
                                   V_NUM_TELEFONE,
                                   FQDN_NOVO,
                                   2);
            
              UPDATE PROD_JD.SN_TELEFONE_VOIP HIT -- FAZ UPDATE DO NOVO FQDN NO OUTRO TN DO CONTRATO
                 SET HIT.FQDN = FQDN_NOVO, HIT.NUM_PORTA = 2
               WHERE HIT.DDD_TELEFONE_VOIP = TO_CHAR(V_DDD_TELEFONE)
                 AND HIT.NUM_TELEFONE_VOIP = TO_CHAR(V_NUM_TELEFONE)
                 AND HIT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
              COMMIT;
            
              BEGIN
                PRC_EXCLUIR_PONTO(V_ID_PONTO_OUTRO); /*id_ponto a qual foi realizado a ação*/
                PRC_GERACAO_PONTO(V_ID_PONTO_OUTRO); /*id_ponto a qual foi realizado a ação*/
              END;
            
            END IF;
          
            BEGIN
            
              SELECT CT.ID_ASSINANTE
                INTO G_ID_ASSINANTE
                FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
               WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                 AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
                 AND OP.COD_OPERADORA = IN_COD_CIDADE;
            
              PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
            
              RAISE E_PROCESSO_OK;
            
              COMMIT;
            
            EXCEPTION
              when E_PROCESSO_OK then
                Raise_application_error(-20010,
                                        'Sua solicitação foi atendida com sucesso. A solicitação de MODIFICAR FQDN-PORTA foi gerado com sucesso!');
              
              WHEN OTHERS THEN
                V_ERRO := SQLERRM;
              
                Raise_application_error(-20000,
                                        'Erro ao gerar solicitação de MODIFICAR FQDN-PORTA. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa.Caso o erro persista,  será necessário abrir chamado no Service Desk.');
              
            END;
          
          END;
        
        ELSIF V_DDD_TELEFONE IS NULL then
          BEGIN
            -- CODIGO
          
            BEGIN
              FOR v IN (SELECT tv.*
                          FROM prod_jd.sn_telefone_voip tv
                         WHERE tv.dt_fim =
                               to_date('30/12/2049', 'dd/mm/rrrr')
                           AND tv.id_status_telefone_voip = 'U'
                           AND tv.ddd_telefone_voip =
                               to_char(VDDD_TELEFONE_VOIP)
                           AND tv.num_telefone_voip =
                               to_char(VNUM_TELEFONE_VOIP)
                           AND rownum < 2) LOOP
              
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2049', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              
                INSERT INTO prod_jd.sn_telefone_voip
                  (cid_contrato,
                   num_contrato,
                   id_ponto,
                   ddd_telefone_voip,
                   num_telefone_voip,
                   fqdn,
                   num_porta,
                   dt_ini,
                   dt_fim,
                   dt_alteracao,
                   id_status_telefone_voip,
                   golden,
                   tm_id,
                   id_escolhido,
                   publicar,
                   nome_publicacao,
                   num_contrato_avaliacao,
                   id_sistema_externo,
                   cid_contrato_origem,
                   id_softx,
                   fc_numero_portado,
                   fc_interceptado)
                VALUES
                  (v.cid_contrato,
                   v.num_contrato,
                   v.id_ponto,
                   v.ddd_telefone_voip,
                   v.num_telefone_voip,
                   v.fqdn,
                   v.num_porta,
                   sysdate,
                   to_date('30/12/2049', 'dd/mm/rrrr'),
                   sysdate,
                   'U',
                   v.golden,
                   v.tm_id,
                   v.id_escolhido,
                   v.publicar,
                   v.nome_publicacao,
                   v.num_contrato_avaliacao,
                   v.id_sistema_externo,
                   v.cid_contrato_origem,
                   v.id_softx,
                   v.fc_numero_portado,
                   v.fc_interceptado);
              END LOOP;
              COMMIT;
            end;
          
            PRC_ABRE_SOLIC(V_cid_contrato, VNUM_CONTRATO, V_ID_PONTO, 225); -- Abre MODIFICAR FQDN-PORTA  
            -- abrir a solic gerada e pegar o id da solic gerada segundo terminal 
          
            begin
              SELECT sola.id_solicitacao_ass
                INTO solic_modificar_fqdn
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento = 1
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 and tv.id_ponto = V_ID_PONTO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                solic_modificar_fqdn := NULL;
              
            end;
          
            --SEGUNDA OC
            begin
              SELECT oc.cod_oc
                INTO V_cod_oc_57
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento IS NULL
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_cod_oc_57 := NULL;
              
            end;
          
            --  DBMS_OUTPUT.PUT_LINE(solic_modificar_fqdn); 
          
            UPDATE prod_jd.sn_solicitacao_ass sa
               SET sa.dt_baixa           = '',
                   sa.id_tipo_fechamento = '',
                   sa.usr_baixa          = ''
             WHERE sa.id_solicitacao_ass = solic_modificar_fqdn;
            commit;
          
            UPDATE prod_jd.sn_oc oc
               SET OC.dt_baixa           = sysdate,
                   OC.id_tipo_fechamento = 1,
                   OC.usr_baixa          = 'PROD_JD'
             WHERE OC.COD_OC = V_cod_oc_57;
            commit;
          
            BEGIN
              -- GERA FQDN NOVO PARA OS DOIS TERMINAIS
              SELECT PROD_JD.PSN_MANTELEFONE_VOIP.GERA_FQDN_TELEFONE_VOIP(TV.CID_CONTRATO)
                INTO FQDN_NOVO
                FROM PROD_JD.SN_TELEFONE_VOIP    TV,
                     PROD_JD.SN_CIDADE_OPERADORA CID
               WHERE TV.CID_CONTRATO = CID.CID_CONTRATO
                 AND TV.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND TV.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
            
              UPDATE PROD_JD.SN_TELEFONE_VOIP HIT -- FAZ UPDATE DO NOVO FQDN NO OUTRO TN DO CONTRATO
                 SET HIT.FQDN = FQDN_NOVO, HIT.NUM_PORTA = 1
               WHERE HIT.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND HIT.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND HIT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
              COMMIT;
            
              UPDATE PP_VOIPHIT PVHX
                 SET PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO,
                     PVHX.PORTA_EQUIPAMENTO_NOVO   = 1
              --PVHX.FC_NUMERO_PORTADO        = 'S'
              --PVHX.COD_OC                   = V_cod_oc
               WHERE PVHX.STATUS_ARQUIVO in ('ESP', 'PND')
                 AND PVHX.TIPO_REGISTRO = 'AL'
                 AND PVHX.DDD_TELEFONE_CONTR = VDDD_TELEFONE_VOIP
                 AND PVHX.NUM_TELEFONE_CONTR = VNUM_TELEFONE_VOIP;
              --AND PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO;
              COMMIT;
              begin
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = sysdate
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              end;
            
              BEGIN
                PRC_EXCLUIR_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
                PRC_GERACAO_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
              END;
            
              RAISE E_PROCESSO_OK;
              begin
                SELECT CT.ID_ASSINANTE
                  INTO G_ID_ASSINANTE
                  FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
                 WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                   AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
                   AND OP.COD_OPERADORA = IN_COD_CIDADE;
              
                PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
              end;
            
              COMMIT;
            
            EXCEPTION
              when E_PROCESSO_OK then
                Raise_application_error(-20010,
                                        'Sua solicitação foi atendida com sucesso. A solicitação de MODIFICAR FQDN-PORTA foi gerado com sucesso!');
              
              WHEN OTHERS THEN
                V_ERRO := SQLERRM;
              
                Raise_application_error(-20000,
                                        'Erro ao gerar solicitação de MODIFICAR FQDN-PORTA. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa.Caso o erro persista,  será necessário abrir chamado no Service Desk.');
              
            END;
          
          END;
        
        end if;
      
      END;
    
      --  DBMS_OUTPUT.PUT_LINE(VDDD_TELEFONE_VOIP || VNUM_TELEFONE_VOIP);
    
      --- tratamento ok
    
    ELSIF VNUMERO_PORTADO = 'N' THEN
    
      IF V_DDD_TELEFONE IS NOT NULL THEN
        IF V_existe_cn IS NOT NULL THEN
        
          BEGIN
            FOR v IN (SELECT tv.*
                        FROM prod_jd.sn_telefone_voip tv
                       WHERE tv.dt_fim = to_date('30/12/2049', 'dd/mm/rrrr')
                         AND tv.id_status_telefone_voip = 'U'
                         AND tv.ddd_telefone_voip =
                             to_char(VDDD_TELEFONE_VOIP)
                         AND tv.num_telefone_voip =
                             to_char(VNUM_TELEFONE_VOIP)
                         AND rownum < 2) LOOP
            
              UPDATE prod_jd.sn_telefone_voip
                 SET dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
               WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                 AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                 AND dt_fim = to_date('30/12/2049', 'dd/mm/rrrr')
                 AND id_status_telefone_voip = 'U';
              COMMIT;
            
              INSERT INTO prod_jd.sn_telefone_voip
                (cid_contrato,
                 num_contrato,
                 id_ponto,
                 ddd_telefone_voip,
                 num_telefone_voip,
                 fqdn,
                 num_porta,
                 dt_ini,
                 dt_fim,
                 dt_alteracao,
                 id_status_telefone_voip,
                 golden,
                 tm_id,
                 id_escolhido,
                 publicar,
                 nome_publicacao,
                 num_contrato_avaliacao,
                 id_sistema_externo,
                 cid_contrato_origem,
                 id_softx,
                 fc_numero_portado,
                 fc_interceptado)
              VALUES
                (v.cid_contrato,
                 v.num_contrato,
                 v.id_ponto,
                 v.ddd_telefone_voip,
                 v.num_telefone_voip,
                 v.fqdn,
                 v.num_porta,
                 sysdate,
                 to_date('30/12/2049', 'dd/mm/rrrr'),
                 sysdate,
                 'U',
                 v.golden,
                 v.tm_id,
                 v.id_escolhido,
                 v.publicar,
                 v.nome_publicacao,
                 v.num_contrato_avaliacao,
                 v.id_sistema_externo,
                 v.cid_contrato_origem,
                 v.id_softx,
                 v.fc_numero_portado,
                 v.fc_interceptado);
            END LOOP;
            COMMIT;
          end;
        
          BEGIN
            --terminias com mas de um termimnal no mesmo tm_id                    
          
            PRC_ABRE_SOLIC(V_cid_contrato, VNUM_CONTRATO, V_ID_PONTO, 225); -- Abre MODIFICAR FQDN-PORTA 
          
            -- abrir a solic gerada e pegar o id da solic gerada segundo terminal 
          
            begin
              SELECT sola.id_solicitacao_ass
                INTO solic_modificar_fqdn
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento = 1
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 and tv.id_ponto = V_ID_PONTO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                solic_modificar_fqdn := NULL;
              
            end;
          
            --SEGUNDA OC
            begin
              SELECT oc.cod_oc
                INTO V_cod_oc_57
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento is null
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_cod_oc_57 := NULL;
              
            end;
          
            --  DBMS_OUTPUT.PUT_LINE(solic_modificar_fqdn); 
          
            UPDATE prod_jd.sn_solicitacao_ass sa
               SET sa.dt_baixa           = '',
                   sa.id_tipo_fechamento = '',
                   sa.usr_baixa          = ''
             WHERE sa.id_solicitacao_ass = solic_modificar_fqdn;
            commit;
          
            UPDATE prod_jd.sn_oc oc
               SET OC.dt_baixa           = sysdate,
                   OC.id_tipo_fechamento = 1,
                   OC.usr_baixa          = 'PROD_JD'
             WHERE OC.COD_OC = V_cod_oc_57;
            commit;
          
            BEGIN
              -- GERA FQDN NOVO PARA OS DOIS TERMINAIS
              SELECT PROD_JD.PSN_MANTELEFONE_VOIP.GERA_FQDN_TELEFONE_VOIP(TV.CID_CONTRATO)
                INTO FQDN_NOVO
                FROM PROD_JD.SN_TELEFONE_VOIP    TV,
                     PROD_JD.SN_CIDADE_OPERADORA CID
               WHERE TV.CID_CONTRATO = CID.CID_CONTRATO
                 AND TV.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND TV.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
            
              UPDATE PROD_JD.SN_TELEFONE_VOIP HIT -- FAZ UPDATE DO NOVO FQDN NO OUTRO TN DO CONTRATO
                 SET HIT.FQDN = FQDN_NOVO, HIT.NUM_PORTA = 1
               WHERE HIT.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND HIT.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND HIT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
              COMMIT;
            
              UPDATE PP_VOIPHIT PVHX
                 SET PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO,
                     PVHX.PORTA_EQUIPAMENTO_NOVO   = 1
              --PVHX.FC_NUMERO_PORTADO        = 'S'
              --PVHX.COD_OC                   = V_cod_oc
               WHERE PVHX.STATUS_ARQUIVO in ('ESP', 'PND')
                 AND PVHX.TIPO_REGISTRO = 'AL'
                 AND PVHX.DDD_TELEFONE_CONTR = VDDD_TELEFONE_VOIP
                 AND PVHX.NUM_TELEFONE_CONTR = VNUM_TELEFONE_VOIP;
              --AND PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO;
              COMMIT;
            
              -- fechar a voip fechada(30/12/2048) com sysadate 
              begin
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = sysdate
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              end;
            
              BEGIN
                PRC_EXCLUIR_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
                PRC_GERACAO_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
              END;
            
            END;
            IF V_cod_oc_57 IS NOT NULL THEN
              -- GERA HIT PARA SEGUNDO TERMINAL COM MESMO TM_ID
              --PRC_ABRE_SOLIC(V_cid_contrato, VNUM_CONTRATO, V_ID_PONTO_OUTRO, 225); -- Abre MODIFICAR FQDN-PORTA
              ENVIA_HIT_TROCA_FQDN(V_DDD_TELEFONE,
                                   V_NUM_TELEFONE,
                                   FQDN_NOVO,
                                   2);
            
              UPDATE PROD_JD.SN_TELEFONE_VOIP HIT -- FAZ UPDATE DO NOVO FQDN NO OUTRO TN DO CONTRATO
                 SET HIT.FQDN = FQDN_NOVO, HIT.NUM_PORTA = 2
               WHERE HIT.DDD_TELEFONE_VOIP = TO_CHAR(V_DDD_TELEFONE)
                 AND HIT.NUM_TELEFONE_VOIP = TO_CHAR(V_NUM_TELEFONE)
                 AND HIT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
              COMMIT;
            
              BEGIN
                PRC_EXCLUIR_PONTO(V_ID_PONTO_OUTRO); /*id_ponto a qual foi realizado a ação*/
                PRC_GERACAO_PONTO(V_ID_PONTO_OUTRO); /*id_ponto a qual foi realizado a ação*/
              END;
            
            END IF;
          
            BEGIN
            
              SELECT CT.ID_ASSINANTE
                INTO G_ID_ASSINANTE
                FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
               WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                 AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
                 AND OP.COD_OPERADORA = IN_COD_CIDADE;
            
              PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
            
              RAISE E_PROCESSO_OK;
            
              COMMIT;
            
            EXCEPTION
              when E_PROCESSO_OK then
                Raise_application_error(-20010,
                                        'Sua solicitação foi atendida com sucesso. A solicitação de MODIFICAR FQDN-PORTA foi gerado com sucesso!');
              
              WHEN OTHERS THEN
                V_ERRO := SQLERRM;
              
                Raise_application_error(-20000,
                                        'Erro ao gerar solicitação de MODIFICAR FQDN-PORTA. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa.Caso o erro persista,  será necessário abrir chamado no Service Desk.');
              
            END;
          
          END;
        end if;
      end if;
    
      IF V_DDD_TELEFONE IS NULL then
        --DBMS_OUTPUT.PUT_LINE('NETFONE COM SU');
      
        IF V_existe_cn IS NOT NULL THEN
          --DBMS_OUTPUT.PUT_LINE('NETFONE COM SU');
          BEGIN
            BEGIN
              FOR v IN (SELECT tv.*
                          FROM prod_jd.sn_telefone_voip tv
                         WHERE tv.dt_fim =
                               to_date('30/12/2049', 'dd/mm/rrrr')
                           AND tv.id_status_telefone_voip = 'U'
                           AND tv.ddd_telefone_voip =
                               to_char(VDDD_TELEFONE_VOIP)
                           AND tv.num_telefone_voip =
                               to_char(VNUM_TELEFONE_VOIP)
                           AND rownum < 2) LOOP
              
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2049', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              
                INSERT INTO prod_jd.sn_telefone_voip
                  (cid_contrato,
                   num_contrato,
                   id_ponto,
                   ddd_telefone_voip,
                   num_telefone_voip,
                   fqdn,
                   num_porta,
                   dt_ini,
                   dt_fim,
                   dt_alteracao,
                   id_status_telefone_voip,
                   golden,
                   tm_id,
                   id_escolhido,
                   publicar,
                   nome_publicacao,
                   num_contrato_avaliacao,
                   id_sistema_externo,
                   cid_contrato_origem,
                   id_softx,
                   fc_numero_portado,
                   fc_interceptado)
                VALUES
                  (v.cid_contrato,
                   v.num_contrato,
                   v.id_ponto,
                   v.ddd_telefone_voip,
                   v.num_telefone_voip,
                   v.fqdn,
                   v.num_porta,
                   sysdate,
                   to_date('30/12/2049', 'dd/mm/rrrr'),
                   sysdate,
                   'U',
                   v.golden,
                   v.tm_id,
                   v.id_escolhido,
                   v.publicar,
                   v.nome_publicacao,
                   v.num_contrato_avaliacao,
                   v.id_sistema_externo,
                   v.cid_contrato_origem,
                   v.id_softx,
                   v.fc_numero_portado,
                   v.fc_interceptado);
              END LOOP;
              COMMIT;
            end;
          
            -- CODIGO
          
            PRC_ABRE_SOLIC(V_cid_contrato, VNUM_CONTRATO, V_ID_PONTO, 225); -- Abre MODIFICAR FQDN-PORTA  
            -- abrir a solic gerada e pegar o id da solic gerada segundo terminal 
          
            begin
              SELECT sola.id_solicitacao_ass
                INTO solic_modificar_fqdn
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 57
                 AND sola.id_tipo_fechamento = 1
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 and tv.id_ponto = V_ID_PONTO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                solic_modificar_fqdn := NULL;
              
            end;
          
            begin
              SELECT oc.cod_oc
                INTO V_cod_oc_57
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv,
                     prod_jd.sn_oc              oc
               WHERE sola.num_contrato = tv.num_contrato
                 and oc.id_solicitacao_ass = sola.id_solicitacao_ass
                 AND sola.id_tipo_solic IN (225)
                 and oc.id_tipo_oc = 56
                 AND sola.id_tipo_fechamento is null
                 AND oc.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip = VDDD_TELEFONE_VOIP
                 AND tv.num_telefone_voip = VNUM_TELEFONE_VOIP
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = VNUM_CONTRATO
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol
                       WHERE sol.num_contrato = sola.num_contrato
                         AND sol.id_tipo_solic IN (225));
            
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_cod_oc_57 := NULL;
              
            end;
          
            --  DBMS_OUTPUT.PUT_LINE(solic_modificar_fqdn); 
          
            UPDATE prod_jd.sn_solicitacao_ass sa
               SET sa.dt_baixa           = '',
                   sa.id_tipo_fechamento = '',
                   sa.usr_baixa          = ''
             WHERE sa.id_solicitacao_ass = solic_modificar_fqdn;
            commit;
          
            UPDATE prod_jd.sn_oc oc
               SET OC.dt_baixa           = sysdate,
                   OC.id_tipo_fechamento = 1,
                   OC.usr_baixa          = 'PROD_JD'
             WHERE OC.COD_OC = V_cod_oc_57;
            commit;
          
            BEGIN
              -- GERA FQDN NOVO PARA OS DOIS TERMINAIS
              SELECT PROD_JD.PSN_MANTELEFONE_VOIP.GERA_FQDN_TELEFONE_VOIP(TV.CID_CONTRATO)
                INTO FQDN_NOVO
                FROM PROD_JD.SN_TELEFONE_VOIP    TV,
                     PROD_JD.SN_CIDADE_OPERADORA CID
               WHERE TV.CID_CONTRATO = CID.CID_CONTRATO
                 AND TV.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND TV.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
            
              UPDATE PROD_JD.SN_TELEFONE_VOIP HIT -- FAZ UPDATE DO NOVO FQDN NO OUTRO TN DO CONTRATO
                 SET HIT.FQDN = FQDN_NOVO, HIT.NUM_PORTA = 1
               WHERE HIT.DDD_TELEFONE_VOIP = TO_CHAR(VDDD_TELEFONE_VOIP)
                 AND HIT.NUM_TELEFONE_VOIP = TO_CHAR(VNUM_TELEFONE_VOIP)
                 AND HIT.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
              COMMIT;
            
              UPDATE PP_VOIPHIT PVHX
                 SET PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO,
                     PVHX.PORTA_EQUIPAMENTO_NOVO   = 1
              --PVHX.FC_NUMERO_PORTADO        = 'S'
              --PVHX.COD_OC                   = V_cod_oc
               WHERE PVHX.STATUS_ARQUIVO in ('ESP', 'PND')
                 AND PVHX.TIPO_REGISTRO = 'AL'
                 AND PVHX.DDD_TELEFONE_CONTR = VDDD_TELEFONE_VOIP
                 AND PVHX.NUM_TELEFONE_CONTR = VNUM_TELEFONE_VOIP;
              --AND PVHX.NOME_EQPTO_MTA_FQDN_NOVO = FQDN_NOVO;
              COMMIT;
              -- fechar a voip fechada(30/12/2048) com sysadate 
              begin
                UPDATE prod_jd.sn_telefone_voip
                   SET dt_fim = sysdate
                 WHERE ddd_telefone_voip = to_char(VDDD_TELEFONE_VOIP)
                   AND num_telefone_voip = to_char(VNUM_TELEFONE_VOIP)
                   AND dt_fim = to_date('30/12/2048', 'dd/mm/rrrr')
                   AND id_status_telefone_voip = 'U';
                COMMIT;
              end;
            
              BEGIN
                PRC_EXCLUIR_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
                PRC_GERACAO_PONTO(V_ID_PONTO); /*id_ponto a qual foi realizado a ação*/
              END;
            
            END;
          
            BEGIN
            
              SELECT CT.ID_ASSINANTE
                INTO G_ID_ASSINANTE
                FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
               WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                 AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
                 AND OP.COD_OPERADORA = IN_COD_CIDADE;
            
              PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
            
              RAISE E_PROCESSO_OK;
            
              COMMIT;
            
            EXCEPTION
              when E_PROCESSO_OK then
                Raise_application_error(-20010,
                                        'Sua solicitação foi atendida com sucesso. A solicitação de MODIFICAR FQDN-PORTA foi gerado com sucesso!');
              
              WHEN OTHERS THEN
                V_ERRO := SQLERRM;
              
                Raise_application_error(-20000,
                                        'Erro ao gerar solicitação de MODIFICAR FQDN-PORTA. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa.Caso o erro persista,  será necessário abrir chamado no Service Desk.');
              
            END;
          
          END;
          --end if;
        end if;
      end if;
    end if;
  
    -- end if;
    Raise_Application_Error(-20000,
                            'Não foram encontrados registros válidos para esta pesquisa. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa. Caso o erro persista,  será necessário abrir chamado no Service Desk');
  END;
END;
