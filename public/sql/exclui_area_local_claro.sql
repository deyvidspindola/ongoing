DECLARE
  -- VARIAVEIS DOS PARAMETROS DE INPUT: 
  -- IN_COD_CIDADE PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '021';
  -- IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := 1093673;
  -- IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('1140214367', 1, 2);
  -- IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('1140214367', 3, 8);

  --IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '[OPERADORA]';
  --IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := [CONTRATO];
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('[TERMINAL]', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('[TERMINAL]', 3, 8);

  T_CID_CONTRATO NUMBER;
  T_EXCLUIDO EXCEPTION;
  T_ERROR EXCEPTION;
  T_NAO_ENCONTRADO EXCEPTION;
  T_FORA_CENARIO EXCEPTION;
  EXISTE_TN NUMBER;
  TYPE T_ARRAY IS TABLE OF PROD_JD.SN_TELEFONE_VOIP%ROWTYPE;
  Q_ARRAY T_ARRAY;
  L_I     PLS_INTEGER;

  --Variaveis SO_ORDENA (Grava Log)         
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VSESSAO        PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSCLARO_CPS';
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  --Variaveis contadores
  V_REGS_PROCESSADOS  INTEGER := 0;
  V_REGS_PROCESSADOS1 INTEGER := 0;
  VERRO1              VARCHAR2(1000);
  MSG                 VARCHAR2(1000);
  VQTD                INTEGER := 0;

  --PRCEDURE PARA ENVIAR SERVICO 41----
  PROCEDURE SLEEP(SEGUNDOS NUMBER) AS
    IN_TIME NUMBER := SEGUNDOS;
    V_NOW   DATE;
  BEGIN
    SELECT SYSDATE INTO V_NOW FROM DUAL;
    LOOP
      EXIT WHEN V_NOW +(IN_TIME * (1 / 86400)) = SYSDATE;
    END LOOP;
  END SLEEP;

  PROCEDURE ENVIAR_EVENTO_GESTOR AS
    CONN           UTL_TCP.CONNECTION;
    V_MSG          VARCHAR2(4000);
    V_METODO       VARCHAR2(500);
    V_URL          PROD_JD.SN_PARAMETRO.INSTRUCAO%TYPE;
    V_HOST         VARCHAR2(200);
    RC             BINARY_INTEGER;
    V_RESPONSE     VARCHAR2(30000);
    V_TMP_RESPONSE VARCHAR2(4096);
    V_XML          XMLTYPE;
    VDATAHORA      VARCHAR2(40);
    IP             VARCHAR2(20) := '10.11.255.205';
    C_INI          NUMBER;
    C_FIM          NUMBER;
  
  BEGIN
  
    --URL conf service
    V_URL := 'http://' || IP ||
             ':8081/modulointegracao/portabilidade/sms/PortabilidadeNumericaService';
  
    V_HOST   := SUBSTR(V_URL, INSTR(V_URL, '//') + 2);
    V_METODO := SUBSTR(V_HOST, INSTR(V_HOST, '/'));
    V_HOST   := SUBSTR(V_HOST, 1, INSTR(V_HOST, '/') - 1);
  
    -- XML PARA REENVIO DA INTEGRAÇÃO PELO SERVIÇO GESTOR
    V_MSG := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://www.viaembratel.com.br/modulointegracao/portabilidadenumerica/schema/types">';
    V_MSG := V_MSG || '   <soapenv:Header/>';
    V_MSG := V_MSG || '   <soapenv:Body>';
    V_MSG := V_MSG || '      <typ:removerReservaElementoRedeRequest>';
    V_MSG := V_MSG || '         <typ:telefoneVoip>';
    V_MSG := V_MSG || '            <typ:dddTelefoneVoip>' ||
             IN_DDD_TERMINAL || '</typ:dddTelefoneVoip>';
    V_MSG := V_MSG || '            <typ:numTelefoneVoip>' ||
             IN_NUM_TERMINAL || '</typ:numTelefoneVoip>';
    V_MSG := V_MSG || '         </typ:telefoneVoip>';
    V_MSG := V_MSG || '      </typ:removerReservaElementoRedeRequest>';
    V_MSG := V_MSG || '   </soapenv:Body>';
    V_MSG := V_MSG || '</soapenv:Envelope>';
  
    --
    CONN := UTL_TCP.OPEN_CONNECTION(IP, '80');
  
    --ESTA SOMA E SUBTRACAO É SÓ PRA SUMIR COM OS WARNINGS (O RETORNO)
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'POST ' || V_METODO || ' HTTP/1.0');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Host: ' || V_HOST);
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Accept: text/*');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Accept-Charset: *');
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Accept-Encoding:');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, 'Content-Encoding: identity');
    RC := RC + UTL_TCP.WRITE_LINE(CONN, 'Content-Type: text/xml');
    RC := RC -
          UTL_TCP.WRITE_LINE(CONN, 'Content-Length: ' || LENGTH(V_MSG));
    RC := RC + UTL_TCP.WRITE_LINE(CONN, '');
    RC := RC - UTL_TCP.WRITE_LINE(CONN, V_MSG);
  
    UTL_TCP.FLUSH(CONN);
  
    BEGIN
      V_RESPONSE := '';
      LOOP
        RC         := RC + UTL_TCP.READ_TEXT(CONN, V_TMP_RESPONSE);
        V_RESPONSE := V_RESPONSE || V_TMP_RESPONSE;
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        --  DBMS_OUTPUT.PUT_LINE('INTEGRACAO [' || 'AREA LOCAL' ||
        --                     '] REENVIADO PARA O SERVIÇO:');
        --  DBMS_OUTPUT.PUT_LINE(V_URL);
        C_INI := INSTR(V_RESPONSE, '<codigo>', 1);
        C_FIM := INSTR(V_RESPONSE, '</codigo>', 1);
        --DBMS_OUTPUT.PUT_LINE(SUBSTR(V_RESPONSE, C_INI, C_FIM - C_INI));
        C_INI := INSTR(V_RESPONSE, '<mensagem>', 1);
        C_FIM := INSTR(V_RESPONSE, '</mensagem>', 1);
        --DBMS_OUTPUT.PUT_LINE(SUBSTR(V_RESPONSE, C_INI, C_FIM - C_INI));
    
      --DBMS_OUTPUT.PUT_LINE('---------------------------------------------------------------------');
    END;
  
    UTL_TCP.CLOSE_CONNECTION(CONN);
  
  END ENVIAR_EVENTO_GESTOR;
  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  /* PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
      L_ID_OFENSOR    VARCHAR(20) := '000';
      L_CALC_CHECKSUM VARCHAR(20);
    
      -- VARÁVEIS NECESSÁRIAS PARA GERAR OCORRÊNCIA
      VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
      VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235;
      VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
      VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
      VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
      VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
      VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
      VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
      VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
      VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := ' ;OCORRENCIA CRIADA POR TI - ONGOING (UNIT-S): PARA OS CASOS ONDE É NECESSARIO EXCLUIR O TERMINAL EM AREA LOCAL COM STATUS DA PROPOSTA OU CONTRATO CANCELADO.';
    
    BEGIN
      SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
        INTO L_CALC_CHECKSUM
        FROM DUAL;
    
      PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                                 VIDTIPOOCORRENCIA,
                                                 P_IDASS,
                                                 VNOMEINFORMANTE ||
                                                 L_CALC_CHECKSUM,
                                                 VTELINFORMANTE,
                                                 VDTOCORRENCIA,
                                                 VIDUSR,
                                                 VSIT,
                                                 VDTRETORNO,
                                                 VIDORIGEM,
                                                 L_ID_OFENSOR || VOBS);
      COMMIT;
    END PR_INSEREOCORRENCIA;
  */
  --Proc para Gravação de LOG--
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PCONTRATO PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PTERMINAL PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PCONTRATO,
       PTERMINAL,
       PMENSAGEM,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

BEGIN
  begin
    --VERIFICA O ALIAS DE BASE
    SELECT UPPER(NM_ALIAS)
      INTO VBASE
      FROM PROD_JD.SN_CIDADE_BASE
     WHERE COD_OPERADORA = '021';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VBASE := NULL;
  END;
  -- (SELECT COD_OPERADORA FROM PROD_JD.SN_CIDADE_OPERADORA WHERE ROWNUM < 2);

  --BUSCA O CID_CONTRATO 
  /*  begin
    SELECT CO.CID_CONTRATO
      INTO T_CID_CONTRATO
      FROM SN_CIDADE_OPERADORA CO
     WHERE CO.COD_OPERADORA = IN_COD_CIDADE
       AND ROWNUM < 2;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      T_CID_CONTRATO := NULL;
  end;*/

  FOR EXIS_TN IN (SELECT DISP.DDD_TELEFONE_VOIP || DISP.NUM_TELEFONE_VOIP TERMINAL
                    FROM PROD_JD.SN_DISP_PORTABILIDADE DISP,
                         PROD_JD.SN_TELEFONE_VOIP      TV
                   WHERE DISP.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                     AND DISP.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                     AND TV.ID_STATUS_TELEFONE_VOIP IN ('A', 'D')
                     AND TV.DT_INI =
                         (SELECT MAX(TV1.DT_INI)
                            FROM PROD_JD.SN_TELEFONE_VOIP TV1
                           WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                             AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                             AND TV1.ID_STATUS_TELEFONE_VOIP IN ('A', 'D')
                             AND TV1.FC_NUMERO_PORTADO = 'S')
                     AND EXISTS
                   (SELECT 1
                            FROM NETSALES.CP_PROPOSTA        cp,
                                 NETSALES.CP_STATUS_PROPOSTA CSP
                           WHERE CP.ID_PROPOSTA = DISP.ID_PROPOSTA
                             AND CP.ID_STATUS_PROPOSTA =
                                 CSP.ID_STATUS_PROPOSTA
                             and CP.ID_STATUS_PROPOSTA IN (1,5,6))
                     AND DISP.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
                     AND DISP.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
                     AND DISP.CID_CONTRATO = '02121'
                     AND ROWNUM < 2) LOOP
  
    --verficar se o retorno do select é verdadeiro   
  
    IF EXIS_TN.TERMINAL IS NOT NULL THEN
      BEGIN
        UPDATE PROD_JD.SN_TELEFONE_VOIP tv
           SET tv.dt_fim = TO_DATE('30/12/2049', 'DD/MM/YYYY')
         WHERE TV.ID_STATUS_TELEFONE_VOIP IN ('A', 'D')
           AND TV.FC_NUMERO_PORTADO = 'S'
           AND tv.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
           AND tv.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
           and tv.num_contrato is null
           AND tv.CID_CONTRATO = '02121'
           AND TV.DT_INI =
               (SELECT MAX(TV1.DT_INI)
                  FROM PROD_JD.SN_TELEFONE_VOIP TV1
                 WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                   AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                   AND TV1.ID_STATUS_TELEFONE_VOIP IN ('A', 'D')
                   AND TV1.FC_NUMERO_PORTADO = 'S')
           AND ROWNUM < 2;
        commit;
      
        -- Grava os dados da sn_telefone_voip      
        SELECT * BULK COLLECT
          INTO Q_ARRAY
          FROM SN_TELEFONE_VOIP
         WHERE DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
           AND NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
         ORDER BY DT_INI;
      
        ENVIAR_EVENTO_GESTOR();
        SLEEP(1);
      
        GRAVALOG(VSESSAO,
                 SYSDATE,
                 VBASE,
                 'TRATAMENTO EFETUADO',
                 IN_DDD_TERMINAL || IN_NUM_TERMINAL,
                 '',
                 '',
                 '');
      
        /*   -- GRAVA OCORRENCIA
        SELECT CT.ID_ASSINANTE
          INTO G_ID_ASSINANTE
          FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
         WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
              -- AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
           AND OP.CID_CONTRATO = T_CID_CONTRATO;*/
      
        -- PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
      
        FOR L_I IN 1 .. Q_ARRAY.COUNT LOOP
        
          -- Retorna os dados na sn_telefone_voip  
          INSERT INTO SN_TELEFONE_VOIP
            (CID_CONTRATO,
             NUM_CONTRATO,
             ID_PONTO,
             DDD_TELEFONE_VOIP,
             NUM_TELEFONE_VOIP,
             FQDN,
             NUM_PORTA,
             DT_INI,
             DT_FIM,
             DT_ALTERACAO,
             ID_STATUS_TELEFONE_VOIP,
             GOLDEN,
             TM_ID,
             ID_ESCOLHIDO,
             PUBLICAR,
             NOME_PUBLICACAO,
             NUM_CONTRATO_AVALIACAO,
             ID_SISTEMA_EXTERNO,
             CID_CONTRATO_ORIGEM,
             ID_SOFTX,
             FC_NUMERO_PORTADO,
             FC_INTERCEPTADO,
             COD_CNL,
             FATURA_DETALHADA,
             ID_ORDEM_PUBLICACAO,
             CI_CODIGO,
             EOT_FISCAL,
             UTILIDADE_PUBLICA,
             ADMINISTRATIVO,
             GOVERNO)
          VALUES
            (Q_ARRAY(L_I).CID_CONTRATO,
             Q_ARRAY(L_I).NUM_CONTRATO,
             Q_ARRAY(L_I).ID_PONTO,
             Q_ARRAY(L_I).DDD_TELEFONE_VOIP,
             Q_ARRAY(L_I).NUM_TELEFONE_VOIP,
             Q_ARRAY(L_I).FQDN,
             Q_ARRAY(L_I).NUM_PORTA,
             Q_ARRAY(L_I).DT_INI,
             Q_ARRAY(L_I).DT_FIM,
             Q_ARRAY(L_I).DT_ALTERACAO,
             Q_ARRAY(L_I).ID_STATUS_TELEFONE_VOIP,
             Q_ARRAY(L_I).GOLDEN,
             Q_ARRAY(L_I).TM_ID,
             Q_ARRAY(L_I).ID_ESCOLHIDO,
             Q_ARRAY(L_I).PUBLICAR,
             Q_ARRAY(L_I).NOME_PUBLICACAO,
             Q_ARRAY(L_I).NUM_CONTRATO_AVALIACAO,
             Q_ARRAY(L_I).ID_SISTEMA_EXTERNO,
             Q_ARRAY(L_I).CID_CONTRATO_ORIGEM,
             Q_ARRAY(L_I).ID_SOFTX,
             Q_ARRAY(L_I).FC_NUMERO_PORTADO,
             Q_ARRAY(L_I).FC_INTERCEPTADO,
             Q_ARRAY(L_I).COD_CNL,
             Q_ARRAY(L_I).FATURA_DETALHADA,
             Q_ARRAY(L_I).ID_ORDEM_PUBLICACAO,
             Q_ARRAY(L_I).CI_CODIGO,
             Q_ARRAY(L_I).EOT_FISCAL,
             Q_ARRAY(L_I).UTILIDADE_PUBLICA,
             Q_ARRAY(L_I).ADMINISTRATIVO,
             Q_ARRAY(L_I).GOVERNO);
        
          COMMIT;
        END LOOP;
      
        UPDATE PROD_JD.SN_TELEFONE_VOIP VOIP
           SET VOIP.dt_fim = SYSDATE
         WHERE VOIP.ddd_telefone_voip = IN_DDD_TERMINAL
           AND VOIP.num_telefone_voip = IN_NUM_TERMINAL
           AND VOIP.id_status_telefone_voip IN ('A', 'D')
           AND VOIP.dt_fim = To_Date('30/12/2049', 'DD/MM/YYYY');
        COMMIT;
      
        RAISE T_EXCLUIDO;
      end;
    
    ELSIF EXIS_TN.TERMINAL IS NULL THEN
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'TRATAMENTO NAO EFETUADO',
               IN_DDD_TERMINAL || IN_NUM_TERMINAL,
               '',
               '',
               '');
    
      RAISE T_NAO_ENCONTRADO;
    
    ELSE
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'TRATAMENTO NAO EFETUADO',
               IN_DDD_TERMINAL || IN_NUM_TERMINAL,
               '',
               '',
               '');
    
      RAISE T_ERROR;
    
    END IF;
  END LOOP;
  GRAVALOG(VSESSAO,
           SYSDATE,
           VBASE,
           'TRATAMENTO NAO EFETUADO',
           IN_DDD_TERMINAL || IN_NUM_TERMINAL,
           '',
           '',
           '');

  RAISE T_FORA_CENARIO;
EXCEPTION
  WHEN T_EXCLUIDO THEN
    RAISE_APPLICATION_ERROR(-20010,
                            'O Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' foi excluido com sucesso da area local.');
  
  WHEN T_FORA_CENARIO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' está fora do cenário para excluir area local.');
  
  WHEN T_NAO_ENCONTRADO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL || ' não foi encontrado.');
  
  WHEN T_ERROR THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Erro na exclusão do terminal ' ||
                            IN_DDD_TERMINAL || IN_NUM_TERMINAL || '.');
  
END;

