/*=================================================================================================================
                                                       ART-IT
===================================================================================================================
NOME OFENSOR  : OFENSOR323_CPS
RESPONSÁVEL   : HERIALDO MAIORINO
DATA CRIAÇÃO  : 27/02/2015
CENARIO       : SOLICITAÇÃO DE ADESÃO EXECUTADA E CONTRATO PERMANECEU COM STATUS PEND. EM INSTALACAO.
TRATAMENTO    : 1. VALIDAR SE O TERMINAL É UM PORTADO E CASO NÃO SEJA EXIBIR MENSAGEM PARA O USUÁRIO QUE O TERMINAL 
                   É REFERENTE A UM NETFONE E DEVE SER ABERTO UM CHAMADO ATRAVÉS DA FERRAMENTA SUPORTE OS/OC.
                2. VERIFICAR SE PARA O TERMINAL AS ÚLTIMAS SOLICITAÇÕES SÃO DE HABILITAR NÚMERO E ADESÃO EXECUTADA. 
                3. SE SIM, O STATUS CORRETO DO CONTRATO NET É IGUAL A CONECTADO.
                
===================================================================================================================
                                                        ART-IT
==================================================================================================================*/
-- DECLARAÇÃO DE VARIAVEIS
DECLARE
  P_COD_OPERADORA varchar(3) := '[COD_OPERADORA]';
  P_CONTRATO      PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := '[CONTRATO]';
  P_DDD           VARCHAR(10) := SUBSTR('[TERMINAL]', 1, 2);
  P_TERMINAL      VARCHAR(10) := SUBSTR('[TERMINAL]', 3, 8);

  -- CURSOR PRINCIPAL
  CURSOR CUR_PRINCIPAL IS
    SELECT TV.CID_CONTRATO, TV.NUM_CONTRATO
      FROM PROD_JD.SN_TELEFONE_VOIP       TV,
           PROD_JD.SN_SOLICITACAO_ASS     ASS,
           PROD_JD.SN_OC                  OC,
           PROD_JD.SN_REL_STATUS_CTO_SERV CON,
           PROD_JD.SN_CIDADE_OPERADORA    OP
     WHERE TV.ID_STATUS_TELEFONE_VOIP = 'U'
       AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND ASS.NUM_CONTRATO = TV.NUM_CONTRATO
       AND ASS.CID_CONTRATO = TV.CID_CONTRATO
       AND ASS.ID_SOLICITACAO_ASS = OC.ID_SOLICITACAO_ASS
       AND OC.ID_PONTO = TV.ID_PONTO
       AND ASS.ID_SOLICITACAO_ASS =
           (SELECT MAX(SA.ID_SOLICITACAO_ASS)
              FROM PROD_JD.SN_SOLICITACAO_ASS SA, PROD_JD.SN_OC CO
             WHERE SA.ID_TIPO_SOLIC <> 222
               AND SA.ID_TIPO_SOLIC IN (2, 3)
                  /*AND ASS.ID_TIPO_FECHAMENTO = 1
                                                   AND */
               AND SA.NUM_CONTRATO = TV.NUM_CONTRATO
               AND SA.CID_CONTRATO = TV.CID_CONTRATO
               AND SA.ID_SOLICITACAO_ASS = CO.ID_SOLICITACAO_ASS
               AND CO.ID_PONTO = TV.ID_PONTO)
       AND CON.ID_STATUS_CONTRATO = 3
       AND CON.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND CON.NUM_CONTRATO = TV.NUM_CONTRATO
       AND CON.CID_CONTRATO = TV.CID_CONTRATO
       AND TV.CID_CONTRATO = OP.CID_CONTRATO
       AND TV.DDD_TELEFONE_VOIP = P_DDD
       AND TV.NUM_TELEFONE_VOIP = P_TERMINAL
       AND TV.NUM_CONTRATO = P_CONTRATO
       AND OP.COD_OPERADORA = P_COD_OPERADORA
       AND ASS.ID_TIPO_SOLIC IN (2, 3)
       AND ASS.ID_TIPO_FECHAMENTO = 1
    /* AND EXISTS
             (SELECT 1
                      FROM PROD_JD.SN_SOLICITACAO_ASS SSA, PROD_JD.SN_OC COO
                     WHERE SSA.ID_TIPO_SOLIC IN (2, 3)
                       AND SSA.ID_TIPO_FECHAMENTO = 1
                       AND SSA.ID_SOLICITACAO_ASS = COO.ID_SOLICITACAO_ASS
                       AND SSA.NUM_CONTRATO = TV.NUM_CONTRATO
                       AND SSA.CID_CONTRATO = TV.CID_CONTRATO
                       AND COO.ID_PONTO = TV.ID_PONTO
                       AND SSA.DT_CADASTRO = ASS.DT_CADASTRO)*/
    ;

  -- DECLARAÇÃO DE VARIAVEIS LOCAL
  VSESSAO PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR323_CPS';
  V_CUR   CUR_PRINCIPAL%ROWTYPE;
  P_NETFONE EXCEPTION;
  P_PORTADO EXCEPTION;
  P_CENARIO_ERRADO EXCEPTION;
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VERRO1         VARCHAR2(1000);
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  -- PROCEDURE P/ GRAVAÇÃO DE VOLUMETRIA
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PMENSAGEM,
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
  
    L_ID_OFENSOR    VARCHAR(20) := ''; --PREENCHER COM ID_OFENSOR DO SCRIPT(PLANILHA DE OFENSORES)
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VARÁVEIS NECESSÁRIAS PARA GERAR OCORRÊNCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): O STATUS DO CONTRATO EMBRATEL FOI ALTERADO PARA "CONECTADO"';
  
  BEGIN
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'mmdd'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    -- CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVAÇÃO JUNTO A EBT
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
  END PR_INSEREOCORRENCIA;

  -- FUNÇÃO QUE VERIFICA NUMERO PORTADO
  FUNCTION E_PORTADO(P_DDD_TELEFONE_VOIP IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE,
                     P_NUM_TELEFONE_VOIP IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE)
    RETURN NUMBER IS
    QTDE_PORTADO NUMBER;
  BEGIN
    BEGIN
      SELECT COUNT(1)
        INTO QTDE_PORTADO
        FROM PROD_JD.SN_TELEFONE_VOIP TV
       WHERE TV.DDD_TELEFONE_VOIP = P_DDD_TELEFONE_VOIP
         AND TV.NUM_TELEFONE_VOIP = P_NUM_TELEFONE_VOIP
         AND TV.FC_NUMERO_PORTADO = 'S'
         AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
    EXCEPTION
      WHEN OTHERS THEN
        QTDE_PORTADO := 0;
    END;
    RETURN(QTDE_PORTADO);
  END E_PORTADO;

  -- *INICIO DO PROCESSO* --
BEGIN

  -- BUSCA A BASE
  SELECT UPPER(NM_ALIAS)
    INTO VBASE
    FROM PROD_JD.SN_CIDADE_BASE
   WHERE COD_OPERADORA = P_COD_OPERADORA;

  IF E_PORTADO(P_DDD, P_TERMINAL) > 0 THEN
    OPEN CUR_PRINCIPAL;
    FETCH CUR_PRINCIPAL
      INTO V_CUR;
  
    IF (CUR_PRINCIPAL%FOUND) THEN
      BEGIN
        UPDATE PROD_JD.SN_REL_STATUS_CTO_SERV
           SET DT_FIM = SYSDATE
         WHERE NUM_CONTRATO = V_CUR.NUM_CONTRATO
           AND CID_CONTRATO = V_CUR.CID_CONTRATO
           AND DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
           AND ID_STATUS_CONTRATO = 3;
      
        INSERT INTO PROD_JD.SN_REL_STATUS_CTO_SERV
        VALUES
          (V_CUR.NUM_CONTRATO,
           V_CUR.CID_CONTRATO,
           1,
           2,
           SYSDATE,
           TO_DATE('30/12/2049', 'DD/MM/YYYY'),
           NULL,
           SYSDATE);
        COMMIT;
      END;
      -- RECUPERA O ID_ASSINANTE
      SELECT CT.ID_ASSINANTE
        INTO G_ID_ASSINANTE
        FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
       WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
         AND CT.CID_CONTRATO = V_CUR.CID_CONTRATO
         AND CT.NUM_CONTRATO = P_CONTRATO;
    
      -- INSERE OCORRENCIA
      PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
    
      -- LOGA PROCESSO OK
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'TRATAMENTO EFETUADO',
               P_DDD || P_TERMINAL,
               P_COD_OPERADORA || '/' || P_CONTRATO);
    
      CLOSE CUR_PRINCIPAL;
       RAISE P_PORTADO;
    ELSE
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'O CENÁRIO DO TERMINAL NAO CORRESPONDE COM A TRATATIVA',
               P_DDD || P_TERMINAL,
               P_COD_OPERADORA || '/' || P_CONTRATO);
      CLOSE CUR_PRINCIPAL;
      RAISE P_CENARIO_ERRADO;
    END IF;
  
  ELSE
    -- LOGA PROCESSO OK
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             'TERMINAL NETFONE',
             P_DDD || P_TERMINAL,
             P_COD_OPERADORA || '/' || P_CONTRATO);
    RAISE P_NETFONE;
  END IF;

EXCEPTION
  WHEN P_NETFONE THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'O número de telefone informado é um NETFONE, estes casos são atendidos por chamado abertos através da ferramenta suporte os/os. Esta opção deve ser utilizada somente para números de telefone portados.');
  WHEN P_PORTADO THEN
    RAISE_APPLICATION_ERROR(-20010, 'Sua solicitação foi atendida com sucesso. O status do contrato foi regularizado conforme solicitado!');
  WHEN P_CENARIO_ERRADO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Não foi encontrado erros de portabilidade para o número de telefone informado. Esta opção deve ser utilizada somente para casos onde a solicitação de habilitar número e adesão foi executada e o status do Contrato Embratel permanece como Pendente Instalação. Favor acessar a ferramenta NETSMS > Aba Geral e verificar se o contrato/número de telefone possuem estas características.');
  WHEN OTHERS THEN
    -- GRAVA LOG DO ERRO
    RAISE_APPLICATION_ERROR(-20000,
                            'Não foi possível prosseguir com o atendimento de sua solicitação, será necessário abrir um chamado ligando para o Service Desk e selecionando a opção 3 - Portabilidade.');
    VERRO1 := SQLERRM;
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             VERRO1,
             P_DDD || P_TERMINAL,
             P_COD_OPERADORA || '/' || P_CONTRATO);
END;
