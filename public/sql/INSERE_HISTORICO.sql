DECLARE
  VDDD        VARCHAR(2) := SUBSTR('[TERMINAL]', 1, 2);
  VTERM       VARCHAR(9) := SUBSTR('[TERMINAL]', 3, 8);
  VOP         VARCHAR(3) := '[OPERADORA]';
  VCONTR      VARCHAR(32) := '[CONTRATO]';
  P_DT_JANELA VARCHAR(32) := '[DATA_JANELA]';
  P_BILHETE   VARCHAR(32) := '[BILHETE]';
  VDATA       VARCHAR(32);
  V_DATA      VARCHAR(32);

  VIDHIT PROD_JD.SN_TELEFONE_VOIP.CID_CONTRATO%TYPE;
  /* VDDD   PROD_JD.PP_VOIPHIT.DDD_TELEFONE_CONTR%TYPE;
  VTERM  PROD_JD.PP_VOIPHIT.NUM_TELEFONE_CONTR%TYPE;
  VCONTRR PROD_JD.SN_TELEFONE_VOIP.NUM_CONTRATO%TYPE;
  VOPP    PROD_JD.SN_TELEFONE_VOIP.CID_CONTRATO%TYPE;*/
  -------------

  E_PROCESSO_OK EXCEPTION;
  vDadosException VARCHAR2(500);
  vexiste         VARCHAR(50);
  vdd             VARCHAR(32);
  vtelefone       VARCHAR(32);
  vcod            VARCHAR(32);
  vcontrato       VARCHAR(32);
  VBILHETE        VARCHAR(50);
  V_ERRO          VARCHAR(1000);
  G_ID_ASSINANTE  SN_ASSINANTE.ID_ASSINANTE%TYPE;
  --------------------

  -------------------------------------------------------

  PROCEDURE PR_INSERESTATUS(P_BILHETE  IN NUMBER,
                            P_DDD      IN NUMBER,
                            P_TELEFONE IN NUMBER,
                            --  P_STATUS_PTB           IN NUMBER,
                            P_DT_JANELA_AUX IN varchar2)
  -- P_ID_OPERADORA_DOADORA IN NUMBER,
    -- P_DT_JANELA_AUX         IN NUMBER)
  
   AS
  
    P_ID_PORTABILIDADE NUMBER;
    P_NUM_CONTRATO     NUMBER;
    P_CID_CONTRATO     VARCHAR(5);
    P_COD_CNL          NUMBER;
    P_ID_PONTO         NUMBER;
    P_DT_JANELA        VARCHAR2(20);
    P_DT_SOLIC         VARCHAR2(20);
    P_QTD_CARAC_JAN    NUMBER;
    P_QTD_CARAC_SOL    NUMBER;
  
  BEGIN
  
    --VERIFICA A QUANTIDADE DE CARACTERES TEM A DATA
    SELECT LENGTH(P_DT_JANELA_AUX) INTO P_QTD_CARAC_JAN FROM DUAL;
    SELECT LENGTH(P_DT_JANELA_AUX) INTO P_QTD_CARAC_SOL FROM DUAL;
  
    --TRATAMENTO PARA A DATA DA JANELA
    /*   IF P_QTD_CARAC_JAN <> 13 THEN
      P_DT_JANELA := SUBSTR(P_DT_JANELA_AUX, 1, 2) || '/' ||
                     SUBSTR(P_DT_JANELA_AUX, 3, 2) || '/' ||
                     SUBSTR(P_DT_JANELA_AUX, 5, 4) || ' ' ||
                     SUBSTR(P_DT_JANELA_AUX, 9, 2) || ':' ||
                     SUBSTR(P_DT_JANELA_AUX, 11, 2) || ':' ||
                     SUBSTR(P_DT_JANELA_AUX, 13, 2);
    ELSE
      P_DT_JANELA := CONCAT(0, P_DT_JANELA) ||
                     SUBSTR(P_DT_JANELA_AUX, 1, 1) || '/' ||
                     SUBSTR(P_DT_JANELA_AUX, 2, 2) || '/' ||
                     SUBSTR(P_DT_JANELA_AUX, 4, 4) || ' ' ||
                     SUBSTR(P_DT_JANELA_AUX, 8, 2) || ':' ||
                     SUBSTR(P_DT_JANELA_AUX, 10, 2) || ':' ||
                     SUBSTR(P_DT_JANELA_AUX, 12, 2);
    END IF;
    
    --TRATAMENTO PARA A DATA DA SOLIC
    IF P_QTD_CARAC_SOL <> 13 THEN
      P_DT_SOLIC := SUBSTR(P_DT_JANELA_AUX, 1, 2) || '/' ||
                    SUBSTR(P_DT_JANELA_AUX, 3, 2) || '/' ||
                    SUBSTR(P_DT_JANELA_AUX, 5, 4) || ' ' ||
                    SUBSTR(P_DT_JANELA_AUX, 9, 2) || ':' ||
                    SUBSTR(P_DT_JANELA_AUX, 11, 2) || ':' ||
                    SUBSTR(P_DT_JANELA_AUX, 13, 2);
    ELSE
      P_DT_SOLIC := CONCAT(0, P_DT_SOLIC) || SUBSTR(P_DT_JANELA_AUX, 1, 1) || '/' ||
                    SUBSTR(P_DT_JANELA_AUX, 2, 2) || '/' ||
                    SUBSTR(P_DT_JANELA_AUX, 4, 4) || ' ' ||
                    SUBSTR(P_DT_JANELA_AUX, 8, 2) || ':' ||
                    SUBSTR(P_DT_JANELA_AUX, 10, 2) || ':' ||
                    SUBSTR(P_DT_JANELA_AUX, 12, 2);
    END IF;*/
  
    P_DT_SOLIC  := P_DT_JANELA_AUX;
    P_DT_JANELA := P_DT_JANELA_AUX;
  
    --RETORNA O ID_PORTABILIDADE SEQUENCIAL
    SELECT SQ_ID_PORTABILIDADE.NEXTVAL INTO P_ID_PORTABILIDADE FROM DUAL;
  
    --RETORNA O ID_PONTO, CID_CONTRATO, NUM_CONTRATO, DT_INI
    SELECT STV.ID_PONTO, STV.CID_CONTRATO, STV.NUM_CONTRATO
      INTO P_ID_PONTO, P_CID_CONTRATO, P_NUM_CONTRATO
      FROM SN_TELEFONE_VOIP STV
     WHERE STV.DDD_TELEFONE_VOIP || STV.NUM_TELEFONE_VOIP =
           P_DDD || P_TELEFONE
       AND STV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
       AND STV.ID_STATUS_TELEFONE_VOIP IN ('U', 'C', 'T');
  
    --RETORNA O CD_CNL DA CIDADE
    SELECT CD_CNL
      INTO P_COD_CNL
      FROM SN_PORTABILIDADE
     WHERE CID_CONTRATO = P_CID_CONTRATO
       AND ROWNUM < 2;
  
    -- CRIA A SN_PORTABILIDADE
    INSERT INTO SN_PORTABILIDADE
      (ID_PORTABILIDADE,
       ID_OPERADORA_RECEPTORA,
       ID_OPERADORA_DOADORA,
       NUM_CONTRATO,
       CID_CONTRATO,
       DT_SOLICITACAO,
       FN_TIPO_PORTABILIDADE,
       DDD_TELEFONE_VOIP,
       NUM_TELEFONE_VOIP,
       TP_PORTABILIDADE,
       NR_PROTOCOLO_BP,
       DT_MIGRACAO,
       DT_DESATIVACAO,
       CD_CNL,
       FC_PORTABILIDADE_INTRINSECA)
    VALUES
      (P_ID_PORTABILIDADE, --ID_PORTABILIDADE
       121, --ID_OPERADORA_RECEPTORA
       121,
       P_NUM_CONTRATO, --NUM_CONTRATO
       P_CID_CONTRATO, --CID_CONTRATO
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS'), --DATA_SOLICITACAO
       1, --FN_TIPO_PORTABILIDADE (N�O ALTERAR)
       P_DDD, --DDD_TELEFONE_VOIP
       P_TELEFONE, --NUM_TELEFONE_VOIP
       'PTB', --TIPO_PORTABILIDADE (ALTERAR APENAS SE FOR DOA��O)
       P_BILHETE,
       TO_DATE(P_DT_JANELA, 'DD/MM/YYYY HH24:MI:SS'), --DATA_MIGRACAO
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS'), --DATA_DESATIVACAO
       P_COD_CNL, --CD_CNL ( SELECT CD_CNL FROM SN_PORTABILIDADE WHERE CID_CONTRATO = &CID_CONTRATO )
       'N'); --FC_PORTABILIDADE_INTRINSECA (N�O ALTERAR)
  
    -- CRIA O SERVI�O 10 E O STATUS 13 (REQUIRIDO)
    INSERT INTO SN_MENSAGEM_PORTABILIDADE
      (ID_MENSAGEM_PORTABILIDADE,
       ID_PORTABILIDADE,
       DT_MENSAGEM,
       ID_SERVICO_PORTABILIDADE)
    VALUES
      (SQ_ID_MENSAGEM_PORTABILIDADE.NEXTVAL, --ID_MENSAGEM_PORTABILIDADE (N�O ALTERAR)
       P_ID_PORTABILIDADE, --ID_PORTABILIDADE
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS'), --DATA DE ENVIO
       10); --EVENTO (N�O ALTERAR)
  
    INSERT INTO SN_HIST_STATUS_PORTAB
    VALUES
      (SQ_ID_HIST_STATUS_PORTAB.NEXTVAL,
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS'),
       P_ID_PORTABILIDADE,
       13);
  
    -- CRIA O SERVI�O 34 E O STATUS 3 (PENDENTE)
    INSERT INTO SN_MENSAGEM_PORTABILIDADE
      (ID_MENSAGEM_PORTABILIDADE,
       ID_PORTABILIDADE,
       DT_MENSAGEM,
       DT_AGENDAMENTO,
       ID_SERVICO_PORTABILIDADE,
       ID_MOTIVO_PORTABILIDADE,
       TP_SEGMENTO_MOTIVO,
       FC_REENVIADO)
    VALUES
      (SQ_ID_MENSAGEM_PORTABILIDADE.NEXTVAL, --ID_MENSAGEM_PORTABILIDADE (N�O ALTERAR)
       P_ID_PORTABILIDADE, --ID_PORTABILIDADE
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS') + 1 / 24 / 60 / 60, --DATA DE ENVIO
       TO_DATE(P_DT_JANELA, 'DD/MM/YYYY HH24:MI:SS'), --DATA DA POSS�VEL JANELA DE MIGRA��O
       34,
       1, --N�O ALTERAR
       'RET', --N�O ALTERAR
       'N'); --N�O ALTERAR --EVENTO (N�O ALTERAR)
  
    INSERT INTO SN_HIST_STATUS_PORTAB
    VALUES
      (SQ_ID_HIST_STATUS_PORTAB.NEXTVAL,
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS') + 2 / 24 / 60 / 60,
       P_ID_PORTABILIDADE,
       3);
  
    -- CRIA O SERVI�O 11
    INSERT INTO SN_MENSAGEM_PORTABILIDADE
      (ID_MENSAGEM_PORTABILIDADE,
       ID_PORTABILIDADE,
       DT_MENSAGEM,
       ID_SERVICO_PORTABILIDADE,
       ID_MOTIVO_PORTABILIDADE,
       TP_SEGMENTO_MOTIVO,
       FC_REENVIADO)
    VALUES
      (SQ_ID_MENSAGEM_PORTABILIDADE.NEXTVAL, --ID_MENSAGEM_PORTABILIDADE (N�O ALTERAR)
       P_ID_PORTABILIDADE, --ID_PORTABILIDADE
       TO_DATE(P_DT_SOLIC, 'DD/MM/YYYY HH24:MI:SS') + 4 / 24 / 60 / 60, --DATA DE ENVIO
       11,
       '', --(53 - N�MERO VAGO, 55 - CPF INV�LIDO, 57 - CNPJ INV�LIDO) --TIPO DO CONFLITO (VERIFICAR NO CONSULTA BILHETE)
       'REJ', --N�O ALTERAR
       'N'); --N�O ALTERAR --EVENTO (N�O ALTERAR)
  
    -- CRIA O SERVI�O 35 E O STATUS 2 OU 9 (ATIVO OU CANCELADO)       
    INSERT INTO SN_MENSAGEM_PORTABILIDADE
      (ID_MENSAGEM_PORTABILIDADE,
       ID_PORTABILIDADE,
       DT_MENSAGEM,
       ID_SERVICO_PORTABILIDADE)
    VALUES
      (SQ_ID_MENSAGEM_PORTABILIDADE.NEXTVAL, --ID_MENSAGEM_PORTABILIDADE (N�O ALTERAR)
       P_ID_PORTABILIDADE, --ID_PORTABILIDADE
       TO_DATE(P_DT_JANELA, 'DD/MM/YYYY HH24:MI:SS') + 5 / 24 / 60 / 60, --DATA DE ENVIO
       35); --EVENTO (N�O ALTERAR)
  
    INSERT INTO SN_HIST_STATUS_PORTAB
    VALUES
      (SQ_ID_HIST_STATUS_PORTAB.NEXTVAL,
       TO_DATE(P_DT_JANELA, 'DD/MM/YYYY HH24:MI:SS') + 3 / 24 / 60 / 60,
       P_ID_PORTABILIDADE,
       2); --P_STATUS_PTB);
  
    COMMIT;
  
  END PR_INSERESTATUS;

  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN SN_CONTRATO.ID_ASSINANTE%TYPE) IS
  
    L_ID_OFENSOR    VARCHAR(20) := ''; --  PROCESSAMENTO NAO EFETUADO
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VAR�VEIS NECESS�RIAS PARA GERAR OCORR�NCIA
    VIDOCORRENCIA     SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            SN_OCORRENCIA.ID_USR%TYPE := USER; --prod_jd
    VSIT              SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): O TELEFONE ' ||
                                                TO_CHAR(VDDD) ||
                                                TO_CHAR(VTERM) ||
                                                '  CONCLUIU O PROCESSO DE PORTABILIDADE INTRÍNSECA  (JANELA DE PORTABILIDADE JÁ OCORREU). O HISTÓRICO DE PORTABILIDADE FOI ATUALIZADO NA BASE NET.';
  
  BEGIN
  
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(SUBSTR(L_ID_OFENSOR, 2)) +
           TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    --CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVA��O JUNTO A EBT
    PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                       VIDTIPOOCORRENCIA,
                                       P_IDASS,
                                       VNOMEINFORMANTE || L_CALC_CHECKSUM,
                                       VTELINFORMANTE,
                                       VDTOCORRENCIA,
                                       VIDUSR,
                                       VSIT,
                                       VDTRETORNO,
                                       VIDORIGEM,
                                       L_ID_OFENSOR || VOBS);
  
    COMMIT;
  
  END PR_INSEREOCORRENCIA;

BEGIN

  DBMS_OUTPUT.ENABLE(NULL);

  V_ERRO := NULL;

  begin
    begin
    
      vexiste := P_BILHETE;
    
      SELECT sp.ddd_telefone_voip,
             sp.num_telefone_voip terminal,
             sco.cod_operadora,
             sp.num_contrato
        INTO vdd, vtelefone, vcod, vcontrato
        FROM prod_jd.sn_portabilidade sp, prod_jd.sn_cidade_operadora sco
       WHERE sco.cid_contrato = sp.cid_contrato
         and sp.nr_protocolo_bp = vexiste;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- DBMS_OUTPUT.PUT_LINE('DDD: ' || vdd || ' NUM: ' || vtelefone || ' / CONTRATO: ' || TO_CHAR(vcontrato) || ' / OPERADORA: ' || vcod);
        vexiste := NULL;
        --   V_ERRO   := ' FAVOR VERIFICAR OS DADOS DIGITADOS.';
      -- Raise_application_error(-20000,                                'prosseguir com sua solicita��o.' || ERRO);
    
      -- DBMS_OUTPUT.PUT_LINE(VIDHIT || '' || V_ERRO);  
    
    end;
  
    vDadosException := chr(10) || chr(13) ||
                       'O número do bilhete de portabilidade informado já consta cadastrado na base NET para o telefone: ' || vdd ||
                       vtelefone || '  Operadora: ' || vcod ||
                       ' Contrato: ' || vcontrato || '. ' ||
                       'Caso haja divergência de informações, favor verificar se o número de bilhete informado na pesquisa está correto.';
    IF (vcontrato is not null) THEN
    
      Raise_application_error(-20050, vDadosException);
    
    else
    
      BEGIN
        VDATA := P_DT_JANELA;
        IF (SUBSTR(VDATA, 3, 1) = '/' and SUBSTR(VDATA, 6, 1) = '/') AND
           SUBSTR(VDATA, 14, 1) = ':' THEN
          V_DATA := TO_DATE(P_DT_JANELA, 'DD/MM/YYYY HH24:MI:SS');
        ELSE
          V_DATA := NULL;
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          V_DATA := NULL;
      END;
    
      IF V_DATA IS NULL THEN
        -- DBMS_OUTPUT.PUT_LINE('Favor inserir uma data v�lida');
        Raise_Application_Error(-20050,
                                'A data informada não é válida. Favor voltar ao menu anterior e preencher o campo DATA  neste formato: DD/MM/AAAA  HH:MM');
      
      ELSE
      
        BEGIN
        
          BEGIN
            SELECT tv.cid_contrato
              INTO VBILHETE
              FROM PROD_JD.SN_TELEFONE_VOIP TV
             WHERE TV.DDD_TELEFONE_VOIP = VDDD
               AND TV.NUM_TELEFONE_VOIP = VTERM
               AND TV.ID_STATUS_TELEFONE_VOIP IN ('U')
               and TV.FC_NUMERO_PORTADO = 'S'
               AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
               AND ROWNUM < 2
               AND TV.NUM_CONTRATO = VCONTR;
          
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              --    DBMS_OUTPUT.PUT_LINE('DDD: ' || VDDD || ' NUM: ' || VTERM || ' / CONTRATO: ' || TO_CHAR(VCONTR) || ' / OPERADORA: ' || VOP);
              VBILHETE := NULL;
              --   V_ERRO   := ' FAVOR VERIFICAR OS DADOS DIGITADOS.';
              Raise_application_error(-20000,
                                      'Não foram encontrados registros válidos para esta pesquisa. Favor retornar ao menu anterior, inserir os dados corretos do assinante e realizar nova tentativa.' ||
                                      V_ERRO);
            
            --  DBMS_OUTPUT.PUT_LINE(VIDHIT || '' || V_ERRO);
          
          END;
        
          IF VBILHETE IS NOT NULL THEN
          
            BEGIN
              if LENGTH(P_DT_JANELA) = 16 then
                P_DT_JANELA := P_DT_JANELA || ':00';
              
              end IF;
            
              --  dbms_output.put_line(P_DT_JANELA);
            
              PR_INSERESTATUS(P_BILHETE, VDDD, VTERM, TO_CHAR(P_DT_JANELA));
              -- DBMS_OUTPUT.PUT_LINE(VIDHIT || 'Solicita��o atendida, bilhete de portabilidade inserido com sucesso;OK');
            
              SELECT CT.ID_ASSINANTE
                INTO G_ID_ASSINANTE
                FROM SN_CIDADE_OPERADORA OP, SN_CONTRATO CT
               WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                 AND CT.NUM_CONTRATO = VCONTR
                 AND OP.COD_OPERADORA = VOP;
            
              PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
            
              RAISE E_PROCESSO_OK;
            
              COMMIT;
            
            EXCEPTION
              when E_PROCESSO_OK then
                Raise_application_error(-20010,
                                        'Sua solicitação foi atendida com sucesso. O bilhete de portabilidade foi inserido com sucesso!' ||
                                        VIDHIT);
              
              WHEN OTHERS THEN
                V_ERRO := SQLERRM;
                --  V_ERRO := 'FAVOR VERIFICAR POIS O BILHETE J� SE ENCONTRADO INSERIDO.';         
              
                Raise_application_error(-20050, vDadosException);
              
              --    DBMS_OUTPUT.PUT_LINE(VIDHIT || ' ' || V_ERRO);
            
            END;
          END IF;
        END;
      
      end if;
    end if;
    -- END LOOP;
  END;
END;
