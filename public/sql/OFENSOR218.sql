-- Ofensor 218:
DECLARE
 /* P_COD_OPERADORA PROD_JD.PP_VOIPHIT.COD_OPERADORA_CLIENTE%TYPE := '&COD_OPERADORA'; -- 013
  P_CONTRATO      PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := '&CONTRATO'; -- 28368091
  P_DDD           VARCHAR(10) := '&DDD_TERMINAL';
  P_TERMINAL      VARCHAR(10) := '&NUM_TERMINAL';*/

  P_COD_OPERADORA PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE:='[OPERADORA]';
  P_CONTRATO      PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE:= [CONTRATO];
  P_DDD           VARCHAR2(2):= SUBSTR('[TERMINAL]',1,2);
  P_TERMINAL      VARCHAR2(9):= SUBSTR('[TERMINAL]',3,8);  

  V_OP_DOADORA PROD_JD.SN_DISP_PORTABILIDADE.ID_OPERADORA_DOADORA%TYPE := 125; -- GVT (operadora padrao em todas as bases)

  -- CURSOR PRINCIPAL
  CURSOR CUR_PRINCIPAL IS
    SELECT HIT.DDD_TELEFONE_CONTR_NOVO DD,
           HIT.NUM_TELEFONE_CONTR_NOVO NT,
           BASE.NM_ALIAS               ALIAS,
           HIT.COD_OPERADORA_CLIENTE   CO,
           HIT.NUM_CLIENTE             NC,
           BASE.CID_CONTRATO           CID_CONTRATO
      FROM PROD_JD.PP_VOIPHIT         HIT,
           PROD_JD.SN_SOLICITACAO_ASS ASS,
           PROD_JD.SN_OC              OC,
           PROD_JD.SN_CIDADE_BASE     BASE
     WHERE OC.ID_SOLICITACAO_ASS = ASS.ID_SOLICITACAO_ASS
       AND HIT.COD_OC = OC.COD_OC
       AND BASE.COD_OPERADORA = HIT.COD_OPERADORA_CLIENTE
       AND HIT.NUM_CLIENTE = ASS.NUM_CONTRATO
       AND HIT.STATUS_ARQUIVO = 'ENV'
       AND HIT.TIPO_REGISTRO = 'AL'
       AND HIT.NUM_TELEFONE_CONTR_NOVO IS NOT NULL
       AND HIT.FC_NUMERO_PORTADO = 'S'
       AND HIT.ID_TRANSACAO_NET =
           (SELECT MAX(ID_TRANSACAO_NET)
              FROM PROD_JD.PP_VOIPHIT H
             WHERE H.DDD_TELEFONE_CONTR = HIT.DDD_TELEFONE_CONTR
               AND H.NUM_TELEFONE_CONTR = HIT.NUM_TELEFONE_CONTR)
       AND HIT.DT_ENVIO_HIT < SYSDATE - 1 / 24
       AND ASS.ID_TIPO_FECHAMENTO IS NULL
       AND ASS.ID_TIPO_SOLIC IN (224, 235)
       AND NOT EXISTS(SELECT 1
                        FROM PROD_JD.SN_HIST_STATUS_PORTAB   SHSP,
                             PROD_JD.SN_PORTABILIDADE        P
                       WHERE SHSP.ID_PORTABILIDADE = P.ID_PORTABILIDADE
                         AND P.DDD_TELEFONE_VOIP = HIT.DDD_TELEFONE_CONTR_NOVO
                         AND P.NUM_TELEFONE_VOIP = HIT.NUM_TELEFONE_CONTR_NOVO
                         AND SHSP.ID_STATUS_PORTABILIDADE IN(12,13,3,1)) -- 12 - Provisório / 13 - Requerido
       /* Variaveis de INPUT */
       AND HIT.COD_OPERADORA_CLIENTE = P_COD_OPERADORA
       AND HIT.DDD_TELEFONE_CONTR_NOVO = P_DDD
       AND HIT.NUM_TELEFONE_CONTR_NOVO = P_TERMINAL
       AND HIT.NUM_CLIENTE = P_CONTRATO;

  -- DECLARAï¿½ï¿½O DE VARIAVEIS LOCAL
  VSESSAO PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR218_CPS';
  V_CUR   CUR_PRINCIPAL%ROWTYPE;
  P_NETFONE EXCEPTION;
  P_PORTADO EXCEPTION;
  P_CENARIO_ERRADO EXCEPTION;
  VERRO1         VARCHAR2(1000);
  P_XML          VARCHAR2(1000);
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  -- PROCEDURE P/ INSERIR OU NAO NA DISP
  PROCEDURE INSERE_DISP(PCID_CONT IN PROD_JD.SN_DISP_PORTABILIDADE.CID_CONTRATO%TYPE,
                        PDDD_TEL  IN PROD_JD.SN_DISP_PORTABILIDADE.DDD_TELEFONE_VOIP%TYPE,
                        PNUM_TEL  IN PROD_JD.SN_DISP_PORTABILIDADE.NUM_TELEFONE_VOIP%TYPE,
                        PNUM_CONT IN PROD_JD.SN_DISP_PORTABILIDADE.NUM_CONTRATO%TYPE,
                        POP_DOA   OUT PROD_JD.SN_DISP_PORTABILIDADE.ID_OPERADORA_DOADORA%TYPE) IS
    V_FLAG_DISP BOOLEAN := TRUE;
    SOFT_X      number := 5;
  BEGIN
    FOR VDISP IN (SELECT DISP.ID_OPERADORA_DOADORA OP
                    FROM PROD_JD.SN_DISP_PORTABILIDADE DISP
                   WHERE DISP.CID_CONTRATO = PCID_CONT
                     AND DISP.DDD_TELEFONE_VOIP = PDDD_TEL
                     AND DISP.NUM_TELEFONE_VOIP = PNUM_TEL
                     AND DISP.NUM_CONTRATO = PNUM_CONT) LOOP
      POP_DOA     := VDISP.OP;
      V_FLAG_DISP := FALSE;
    END LOOP;
  
    IF V_FLAG_DISP THEN
      -- SE NAO EXISTIR NA DISP, INSERIR
      BEGIN
        SELECT PSN_PORTABILIDADE_VOIP.FNSMS_BUSCA_ID_SOFTX(PCID_CONT,
                                                           PNUM_CONT)
          INTO SOFT_X
          FROM DUAL;
        FOR V IN (SELECT *
                    FROM SN_DISP_PORTABILIDADE V
                   WHERE CID_CONTRATO = PCID_CONT
                     AND ID_SOFTX = SOFT_X
                     AND NUM_CONTRATO IS NOT NULL
                     AND ROWNUM = 1) LOOP
        
          FOR TV IN (SELECT *
                       FROM SN_TELEFONE_VOIP TV
                      WHERE TV.CID_CONTRATO = PCID_CONT
                        AND TV.DDD_TELEFONE_VOIP = PDDD_TEL
                        AND TV.NUM_TELEFONE_VOIP = PNUM_TEL
                        AND TV.NUM_CONTRATO = PNUM_CONT
                        AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
                        AND TV.ID_STATUS_TELEFONE_VOIP IN ('U', 'T')
                        AND ROWNUM = 1) LOOP
          
            POP_DOA := V.ID_OPERADORA_DOADORA;
          
            INSERT INTO PROD_JD.SN_DISP_PORTABILIDADE
              (DDD_TELEFONE_VOIP,
               NUM_TELEFONE_VOIP,
               ID_OPERADORA_DOADORA,
               CID_CONTRATO,
               FC_AREA_LOCAL,
               NUM_CONTRATO,
               ID_PROPOSTA,
               CD_CNL,
               FN_TIPO_PORTABILIDADE,
               ID_SOFTX,
               DT_DISP_PORTABILIDADE,
               ID_OPERADORA_FISCAL,
               CD_CNL_ORIGEM,
               CD_CONTRATO_ANTERIOR,
               CD_CIDADE_ANTERIOR,
               NR_CPF)
            VALUES
              (PDDD_TEL,
               PNUM_TEL,
               V.ID_OPERADORA_DOADORA,
               V.CID_CONTRATO,
               V.FC_AREA_LOCAL,
               PNUM_CONT,
               V.ID_PROPOSTA,
               V.CD_CNL,
               V.FN_TIPO_PORTABILIDADE,
               SOFT_X,
               TV.DT_INI,
               V.ID_OPERADORA_FISCAL,
               V.CD_CNL_ORIGEM,
               V.CD_CONTRATO_ANTERIOR,
               V.CD_CIDADE_ANTERIOR,
               V.NR_CPF);
          END LOOP;
        END LOOP;
        COMMIT;
      END;
    END IF;
  END INSERE_DISP;

  -- PROCEDURE P/ GRAVAï¿½ï¿½O DE VOLUMETRIA
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PMENSAGEM,
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
  
    L_ID_OFENSOR    VARCHAR(20) := '218';
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VARï¿½VEIS NECESSï¿½RIAS PARA GERAR OCORRï¿½NCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235; -- TI1 - NF2 - INTERVENCOES NOS PONTOS DE TELEFONIA
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): A SOLICITAÇÃO DE PORTABILIDADE FOI ENVIADA PARA EMBRATEL, FAVOR AGUARDAR O RETORNO SISTÊMICO NO HISTÓRICO DE PORTABILIDADE. A SOLICITAÇÃO DE TROCA DE NÚMERO SERÁ EXECUTADA SOMENTE APÓS A JANELA DE MIGRAÇÃO (STATUS ATIVO).';
  
  BEGIN
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'mmdd'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    -- CRIACAO DA OCORRENCIA INFORMANDO A ALTERACAO DA DESATIVAï¿½ï¿½O JUNTO A EBT
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
  END PR_INSEREOCORRENCIA;

  -- FUNï¿½ï¿½O QUE VERIFICA NUMERO PORTADO
  FUNCTION E_PORTADO(P_DDD_TELEFONE_VOIP IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE,
                     P_NUM_TELEFONE_VOIP IN PROD_JD.SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE)
    RETURN NUMBER IS
    QTDE_PORTADO NUMBER;
  BEGIN
    BEGIN
      SELECT COUNT(1)
        INTO QTDE_PORTADO
        FROM PROD_JD.SN_TELEFONE_VOIP TV
       WHERE TV.DDD_TELEFONE_VOIP = P_DDD_TELEFONE_VOIP
         AND TV.NUM_TELEFONE_VOIP = P_NUM_TELEFONE_VOIP
         AND TV.FC_NUMERO_PORTADO = 'S'
         AND TV.ID_STATUS_TELEFONE_VOIP = 'T'
         AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
    EXCEPTION
      WHEN OTHERS THEN
        QTDE_PORTADO := 0;
    END;
    RETURN(QTDE_PORTADO);
  END E_PORTADO;

  -- *INICIO DO PROCESSO* --
BEGIN

  -- BUSCA A BASE
  SELECT UPPER(NM_ALIAS)
    INTO VBASE
    FROM PROD_JD.SN_CIDADE_BASE
   WHERE COD_OPERADORA = P_COD_OPERADORA;

  IF E_PORTADO(P_DDD, P_TERMINAL) > 0 THEN
    OPEN CUR_PRINCIPAL;
    FETCH CUR_PRINCIPAL
      INTO V_CUR;
    
    IF (CUR_PRINCIPAL%FOUND) THEN
    
      -- Inserir na DISP, caso ja nao exista registro
      INSERE_DISP(V_CUR.CID_CONTRATO,
                  V_CUR.DD,
                  V_CUR.NT,
                  V_CUR.NC,
                  V_OP_DOADORA);

      P_XML := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:open="http://www.openuri.org/">' ||
               CHR(10) || '    <soapenv:Header>' || CHR(10) ||
               '        <ns1:NetServicoHeader xmlns:ns1="http://netservicos.com.br/NetHeader">' ||
               VBASE || '</ns1:NetServicoHeader>' || CHR(10) ||
               '    </soapenv:Header>' || CHR(10) || '    <soapenv:Body>' ||
               CHR(10) || '        <open:clientRequestwithReturn>' ||
               CHR(10) || '        <open:entrada>' || CHR(10) ||
               '            <CodigoEOTDoadoraPortabilidade>' ||
               V_OP_DOADORA || '</CodigoEOTDoadoraPortabilidade>' ||
               CHR(10) ||
               '            <DataHoraPrevistaPortabilidade></DataHoraPrevistaPortabilidade>' ||
               CHR(10) ||
               '            <CodigoLoginReceptorPortabilidade>OPS$PORT</CodigoLoginReceptorPortabilidade>' ||
               CHR(10) ||
               '            <NumeroDiasDuracaoJanelaPortab>1</NumeroDiasDuracaoJanelaPortab>' ||
               CHR(10) || '            <CodigoArea>' || V_CUR.DD ||
               '</CodigoArea>' || CHR(10) ||
               '            <NumeroInstancia>' || V_CUR.NT ||
               '</NumeroInstancia>' || CHR(10) || '        </open:entrada>' ||
               CHR(10) || '        </open:clientRequestwithReturn>' ||
               CHR(10) || '    </soapenv:Body>' || CHR(10) ||
               '</soapenv:Envelope>';
    
      -- RECUPERA O ID_ASSINANTE
      SELECT CT.ID_ASSINANTE
        INTO G_ID_ASSINANTE
        FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
       WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
         AND CT.CID_CONTRATO = V_CUR.CID_CONTRATO
         AND CT.NUM_CONTRATO = P_CONTRATO;
    
      -- INSERE OCORRENCIA
      PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
    
      -- LOGA PROCESSO OK, PASSANDO XML NO CAMPO5
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'TRATAMENTO EFETUADO',
               P_DDD || P_TERMINAL,
               P_COD_OPERADORA || '/' || P_CONTRATO,
               P_XML);
    
      CLOSE CUR_PRINCIPAL;
    
      -- RETORNO PARA O CX_ORACLE (PYTHON)
      -- :OUT := P_XML; //REMOVIDO DIA 31/10/2019 não tem funcionalidade
      RAISE P_PORTADO;
    
    ELSE
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'Esta opção deve ser utilizada somente para números portados com solicitação de Troca de Número aberta e para o qual não existe histórico de portabilidade. Favor acessar o NETSMS e verificar se o número de telefone possui estas características antes de realizar nova tentativa.',
               P_DDD || P_TERMINAL,
               P_COD_OPERADORA || '/' || P_CONTRATO);
      CLOSE CUR_PRINCIPAL;
      RAISE P_CENARIO_ERRADO;
    END IF;
  
  ELSE
    RAISE P_NETFONE;
  END IF;

EXCEPTION
  WHEN P_NETFONE THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Esta opção deve ser utilizada somente para números portados com solicitação de Troca de Número aberta e para o qual não existe histórico de portabilidade. Favor acessar o NETSMS e verificar se o número de telefone possui estas características antes de realizar nova tentativa.');
  WHEN P_PORTADO THEN
    RAISE_APPLICATION_ERROR(-20010, 'Sua solicitação foi atendida com sucesso. Histórico de portabilidade foi regularizado, acessar a ferramenta NETSALES> PORTABILIDADE> HISTÓRICO.');
    NULL;
  WHEN P_CENARIO_ERRADO THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Esta opção deve ser utilizada somente para números portados com solicitação de Troca de Número aberta e para o qual não existe histórico de portabilidade. Favor acessar o NETSMS e verificar se o número de telefone possui estas características antes de realizar nova tentativa.');
  WHEN OTHERS THEN
    -- GRAVA LOG DO ERRO
    VERRO1 := SQLERRM;
    GRAVALOG(VSESSAO,
             SYSDATE,
             VBASE,
             VERRO1,
             P_DDD || P_TERMINAL,
             P_COD_OPERADORA || '/' || P_CONTRATO);
END;
