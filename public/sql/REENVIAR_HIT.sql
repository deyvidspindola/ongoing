DECLARE

  -- VARIAVEIS DOS PARAMETROS DE INPUT: 

  /*IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '239';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := 8618782;
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('2126973802', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('2126973802', 3, 8);*/

  IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '[OPERADORA]';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := [CONTRATO];
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('[TERMINAL]', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('[TERMINAL]', 3, 8);

  --Variaveis SO_ORDENA (Grava Log)         
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VSESSAO        PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR000_CPS';
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  --Variaveis contadores
  V_REGS_PROCESSADOS  INTEGER := 0;
  V_REGS_PROCESSADOS1 INTEGER := 0;
  VERRO1              VARCHAR2(1000);
  MSG                 VARCHAR2(1000);
  VQTD                INTEGER := 0;
  T_EXISTE_HIT        NUMBER;
  N_HIT_ENVIADO EXCEPTION;
  T_ERROR EXCEPTION;
  HIT_ENVIADO EXCEPTION;
  HIT_REENVIADO EXCEPTION;
  V_EXI_HIT      NUMBER;
  T_PROCESSO     NUMBER;
  T_CID_CONTRATO NUMBER;
  T_FORA_CENARIO EXCEPTION;
  T_PROCESSO_ERRO EXCEPTION;
  ID_HIT NUMBER;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
    L_ID_OFENSOR    VARCHAR(20) := '000';
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VARÁVEIS NECESSÁRIAS PARA GERAR OCORRÊNCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235;
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := ' ;OCORRENCIA CRIADA POR TI - ONGOING (UNIT-S): PARA OS CASOS ONDE É NECESSÁRIO REENVIAR O HIT DE TROCA DE NUMERO PARA OS CASOS DE PORTABILIDADE INTERNA.';
  
  BEGIN
    begin
      SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
        INTO L_CALC_CHECKSUM
        FROM DUAL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        L_CALC_CHECKSUM := NULL;
    END;
  
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
    COMMIT;
  END PR_INSEREOCORRENCIA;

  --Proc para Gravação de LOG--
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PCONTRATO PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PTERMINAL PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PCONTRATO,
       PTERMINAL,
       PMENSAGEM,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  PROCEDURE REENVIA_HIT(P_DDD_TERMINAL    IN PP_VOIPHIT.DDD_TELEFONE_CONTR%TYPE,
                        P_NUMERO_TERMINAL IN PP_VOIPHIT.NUM_TELEFONE_CONTR%TYPE) AS
  
  BEGIN
  
    FOR TRANSACAO IN (SELECT HIT1.ID_HIT,
                             HIT1.DDD_TELEFONE_CONTR DDD,
                             HIT1.NUM_TELEFONE_CONTR NUM
                        FROM PP_VOIPHIT                 HIT1,
                             PROD_JD.SN_SOLICITACAO_ASS ASS,
                             PROD_JD.SN_OC              OC
                       WHERE HIT1.STATUS_ARQUIVO = 'ESP'
                         AND HIT1.TIPO_REGISTRO = 'AL'
                         AND ASS.ID_TIPO_SOLIC = 224
                         AND ASS.ID_SOLICITACAO_ASS = OC.ID_SOLICITACAO_ASS
                         AND ASS.NUM_CONTRATO = HIT1.NUM_CLIENTE
                         AND OC.COD_OC = HIT1.COD_OC
                         AND ASS.ID_TIPO_FECHAMENTO IS NULL
                         AND NOT EXISTS
                       (SELECT (1)
                                FROM PP_VOIPHIT HIT2
                               WHERE HIT2.TIPO_REGISTRO IN ('DE', 'DI')
                                 AND HIT2.STATUS_ARQUIVO IN
                                     ('ESP', 'PND', 'PEG', 'ENV', 'ERR')
                                 AND HIT2.DDD_TELEFONE_CONTR =
                                     HIT1.DDD_TELEFONE_CONTR
                                 AND HIT2.NUM_TELEFONE_CONTR =
                                     HIT1.NUM_TELEFONE_CONTR
                                 AND HIT2.ID_TRANSACAO_NET <
                                     HIT1.ID_TRANSACAO_NET)
                         AND HIT1.DDD_TELEFONE_CONTR_NOVO = P_DDD_TERMINAL
                         AND HIT1.NUM_TELEFONE_CONTR_NOVO =
                             P_NUMERO_TERMINAL) LOOP
    
      UPDATE PROD_JD.PP_VOIPHIT PP
         SET PP.STATUS_ARQUIVO = 'PND', PP.FC_PROCESSAMENTO_ONLINE = 'N'
       WHERE PP.STATUS_ARQUIVO = 'ESP'
         AND PP.TIPO_REGISTRO = 'AL'
         AND PP.DDD_TELEFONE_CONTR_NOVO IS NOT NULL
         AND PP.DDD_TELEFONE_CONTR = TRANSACAO.DDD
         AND PP.NUM_TELEFONE_CONTR = TRANSACAO.NUM
         AND PP.ID_HIT = TRANSACAO.ID_HIT;
      COMMIT;
    
    END LOOP;
  END REENVIA_HIT;

  FUNCTION EXI_HIT RETURN NUMBER IS
    T_PROCESSO NUMBER;
  
  BEGIN
    --PROCURA SE EXISTE HIT ENVIADO 1 SIM - 0 NAO
    SELECT /*CASE
                                 WHEN HIT1.STATUS_ARQUIVO IN ('PEG', 'ENV') THEN
                                  1
                                 WHEN HIT1.STATUS_ARQUIVO = 'PND' AND
                                      HIT1.FC_PROCESSAMENTO_ONLINE = 'N' THEN
                                  1
                                 ELSE
                                  0
                               END TRATADO */
     COUNT(*)
      INTO T_PROCESSO
      FROM PROD_JD.PP_VOIPHIT         HIT1,
           PROD_JD.SN_SOLICITACAO_ASS ASS,
           PROD_JD.SN_OC              OC
     WHERE HIT1.DDD_TELEFONE_CONTR_NOVO = IN_DDD_TERMINAL
       AND HIT1.NUM_TELEFONE_CONTR_NOVO = IN_NUM_TERMINAL
       AND HIT1.TIPO_REGISTRO = 'AL'
       AND ((HIT1.STATUS_ARQUIVO IN ('PEG', 'ENV', 'ERR', 'RNV')) OR
           (HIT1.STATUS_ARQUIVO = 'PND' AND
           HIT1.FC_PROCESSAMENTO_ONLINE = 'N'))
       AND HIT1.DT_ENVIO_HIT IS NOT NULL
       AND ASS.ID_TIPO_SOLIC = 224
       AND ASS.ID_SOLICITACAO_ASS = OC.ID_SOLICITACAO_ASS
       AND ASS.NUM_CONTRATO = HIT1.NUM_CLIENTE
       AND OC.COD_OC = HIT1.COD_OC
       AND ASS.ID_TIPO_FECHAMENTO IS NULL
       AND NOT EXISTS
     (SELECT (1)
              FROM PP_VOIPHIT HIT2
             WHERE HIT2.TIPO_REGISTRO IN ('DE', 'DI')
               AND HIT2.STATUS_ARQUIVO IN
                   ('ESP', 'PND', 'PEG', 'ENV', 'ERR')
               AND HIT2.DDD_TELEFONE_CONTR = HIT1.DDD_TELEFONE_CONTR
               AND HIT2.NUM_TELEFONE_CONTR = HIT1.NUM_TELEFONE_CONTR
               AND HIT2.ID_TRANSACAO_NET < HIT1.ID_TRANSACAO_NET);
  
    RETURN T_PROCESSO;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE T_ERROR;
    
  END;

BEGIN

  --VERIFICA O ALIAS DE BASE
  begin
    SELECT UPPER(NM_ALIAS)
      INTO VBASE
      FROM PROD_JD.SN_CIDADE_BASE
     WHERE COD_OPERADORA = IN_COD_CIDADE;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VBASE := NULL;
  END;
  -- (SELECT COD_OPERADORA FROM PROD_JD.SN_CIDADE_OPERADORA WHERE ROWNUM < 2);

  --BUSCA O CID_CONTRATO 
  begin
    SELECT CO.CID_CONTRATO
      INTO T_CID_CONTRATO
      FROM SN_CIDADE_OPERADORA CO
     WHERE CO.COD_OPERADORA = IN_COD_CIDADE
       AND ROWNUM < 2;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      T_CID_CONTRATO := NULL;
  END;

  FOR EXISTE_HIT IN (SELECT COUNT(HIT1.DDD_TELEFONE_CONTR ||
                                  HIT1.NUM_TELEFONE_CONTR) TN
                       FROM PP_VOIPHIT HIT1
                      WHERE HIT1.STATUS_ARQUIVO = 'ESP'
                        AND HIT1.TIPO_REGISTRO = 'AL'
                        AND HIT1.DDD_TELEFONE_CONTR_NOVO IS NOT NULL
                        AND NOT EXISTS
                      (SELECT (1)
                               FROM PP_VOIPHIT HIT2
                              WHERE HIT2.TIPO_REGISTRO IN ('DE', 'DI')
                                AND HIT2.STATUS_ARQUIVO IN
                                    ('ESP', 'PND', 'PEG', 'ENV', 'ERR')
                                AND HIT2.DDD_TELEFONE_CONTR =
                                    HIT1.DDD_TELEFONE_CONTR
                                AND HIT2.NUM_TELEFONE_CONTR =
                                    HIT1.NUM_TELEFONE_CONTR
                                AND HIT2.ID_TRANSACAO_NET <
                                    HIT1.ID_TRANSACAO_NET)
                        AND HIT1.DDD_TELEFONE_CONTR_NOVO = IN_DDD_TERMINAL
                        AND HIT1.NUM_TELEFONE_CONTR_NOVO = IN_NUM_TERMINAL) LOOP
  
    V_EXI_HIT := EXI_HIT;
    IF (EXISTE_HIT.TN = 0) THEN
    
      -- NÃO EXISTE HIT AL (TROCA DE NUMERO) COM STATUS ESP      
    
      --VERIFICA SE EXISTE UM TROCA COM STATUS PEG OU ENV JÁ ENVIADA
      IF V_EXI_HIT = 1 THEN
      
        RAISE HIT_ENVIADO;
      
      ELSE
      
        RAISE N_HIT_ENVIADO;
      
      END IF;
    
    ELSIF (EXISTE_HIT.TN = 1) THEN
      -- EXISTE HIT ESP DE TROCA PARA O TERMINAL    
      --IF V_EXI_HIT = 0 THEN
      -- IF (V_EXI_HIT = 1) THEN removido por valter
    
      -- RENVIA O HIT COM O STATUS PND
      REENVIA_HIT(IN_DDD_TERMINAL, IN_NUM_TERMINAL);
    
      -- GRAVA OCORRENCIA
      SELECT CT.ID_ASSINANTE
        INTO G_ID_ASSINANTE
        FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
       WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
         AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
         AND OP.CID_CONTRATO = T_CID_CONTRATO;
    
      PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
    
      RAISE HIT_REENVIADO;
    
    ELSE
    
      RAISE N_HIT_ENVIADO;
    END IF;
  
  -- END IF;
  END LOOP;
  RAISE T_FORA_CENARIO;

EXCEPTION
  WHEN T_ERROR THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Erro no reenvio do Comando de Troca de Número para o terminal ' ||
                            IN_DDD_TERMINAL || IN_NUM_TERMINAL || '.');
  
  WHEN HIT_ENVIADO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' está fora do cenário para reenviar o Comando de Troca de Número.');
  
  WHEN N_HIT_ENVIADO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Não existe comando de Troca de Número no Terminal ' ||
                            IN_DDD_TERMINAL || IN_NUM_TERMINAL ||
                            ' para ser enviado.');
  
  WHEN HIT_REENVIADO THEN
    RAISE_APPLICATION_ERROR(-20010,
                            'Comando de Troca de Número do terminal ' ||
                            IN_DDD_TERMINAL || IN_NUM_TERMINAL ||
                            ' foi reenviado com sucesso.');
  
  WHEN T_FORA_CENARIO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' está fora do cenário para reenviar o Comando de Troca de Número.');
  
END;
