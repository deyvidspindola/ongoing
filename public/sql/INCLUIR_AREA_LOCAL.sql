--INTERNA
DECLARE

  -- VARIAVEIS DOS PARAMETROS DE INPUT: 
  /*
  IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '055';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := 10542129;
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('1139632199', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('1139632199', 3, 8);*/

  IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '[OPERADORA]';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := [CONTRATO];
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('[TERMINAL]', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('[TERMINAL]', 3, 8);

  SOFT_X              NUMBER := 5;
  V_STATUS_QUARENTENA NUMBER;
  T_CID_CONTRATO      VARCHAR(30);
  T_CONTRATO          EXCEPTION;
  T_PROPOSTA          EXCEPTION;
  T_ERROR             EXCEPTION;
  T_ERRO              EXCEPTION;
  T_VALIDADO          EXCEPTION;
  T_FORA_CENARIO      EXCEPTION;
  T_NUM_CONTRATO      NUMBER;
  NUM_PROPOSTA        NUMBER;
  V_PROPOSTA          NUMBER;
  V_CONTRATO          NUMBER;
  V_ERROR             VARCHAR2(100);
  EXIST_TERMINAL      NUMBER;
  PROPOSTA            NUMBER;
  CONTRATO            NUMBER;
  VALIDADO            VARCHAR2(100);

  --Variaveis SO_ORDENA (Grava Log)         
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VSESSAO        PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR000_CPS';
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  --Variaveis contadores
  V_REGS_PROCESSADOS  INTEGER := 0;
  V_REGS_PROCESSADOS1 INTEGER := 0;
  VERRO1              VARCHAR2(1000);
  MSG                 VARCHAR2(1000);
  VQTD                INTEGER := 0;
  valida_contrato     integer := 0;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE) IS
    L_ID_OFENSOR    VARCHAR(20) := '000';
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VARÁVEIS NECESSÁRIAS PARA GERAR OCORRÊNCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235;
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := ' ;OCORRENCIA CRIADA POR TI - ONGOING (UNIT-S): PARA OS CASOS ONDE É NECESSARIO INCLUIR O TERMINAL EM AREA LOCAL, NO CENARIO DE PORTABILIDADE INTERNA.';
  
  BEGIN
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
    COMMIT;
  END PR_INSEREOCORRENCIA;

  --Proc para Gravação de LOG--
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PCONTRATO PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PTERMINAL PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PCONTRATO,
       PTERMINAL,
       PMENSAGEM,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  PROCEDURE QUARENTENA(P_DDD_TERMINAL    IN SN_TELEFONE_VOIP.DDD_TELEFONE_VOIP%TYPE,
                       P_NUMERO_TERMINAL IN SN_TELEFONE_VOIP.NUM_TELEFONE_VOIP%TYPE) AS
    V_STATUS_QUARENTENA NUMBER;
  BEGIN
    BEGIN
    
      DELETE FROM PROD_JD.TBSMS_QUARENTENA_CMRL_VOIP
       WHERE DDD_TELEFONE_VOIP = P_DDD_TERMINAL
         AND NUM_TELEFONE_VOIP = P_NUMERO_TERMINAL
         AND DT_FIM = TO_DATE('30-12-2049', 'DD-MM-YYYY');
      COMMIT;
    
      V_STATUS_QUARENTENA := 1;
    EXCEPTION
      WHEN OTHERS THEN
        V_STATUS_QUARENTENA := 0;
    END;
  END QUARENTENA;

BEGIN

  --VERIFICA O ALIAS DE BASE
  BEGIN
    SELECT UPPER(NM_ALIAS)
      INTO VBASE
      FROM PROD_JD.SN_CIDADE_BASE
     WHERE COD_OPERADORA = IN_COD_CIDADE;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VBASE := NULL;
  END;
  -- (SELECT COD_OPERADORA FROM PROD_JD.SN_CIDADE_OPERADORA WHERE ROWNUM < 2);

  BEGIN
  
    FOR EXIST_TN IN (SELECT COUNT(1) TERMINAL
                       FROM PROD_JD.SN_TELEFONE_VOIP TV
                      WHERE TV.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
                        AND TV.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
                           -- AND TV.NUM_CONTRATO = IN_NUM_CONTRATO
                        AND TV.ID_STATUS_TELEFONE_VOIP IN ('D', 'T', 'C')
                        AND TV.DT_INI =
                            (SELECT MAX(T.DT_INI)
                               FROM PROD_JD.SN_TELEFONE_VOIP T
                              WHERE T.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                                AND T.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP)
                        AND NOT EXISTS
                            (SELECT 1
                               FROM PROD_JD.SN_DISP_PORTABILIDADE DP
                              WHERE DP.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                                AND DP.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP)) LOOP
    
      IF EXIST_TN.TERMINAL = 1 THEN
      --IF EXIST_TN.TERMINAL IN ('D', 'T', 'C') THEN
        --BUSCA O CID_CONTRATO 
        BEGIN
          SELECT CO.CID_CONTRATO
            INTO T_CID_CONTRATO
            FROM SN_CIDADE_OPERADORA CO
           WHERE CO.COD_OPERADORA = IN_COD_CIDADE
             AND ROWNUM < 2;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            T_CID_CONTRATO := NULL;
        END;
      
        SELECT PSN_PORTABILIDADE_VOIP.FNSMS_BUSCA_ID_SOFTX(T_CID_CONTRATO,
                                                           IN_NUM_CONTRATO)
          INTO SOFT_X
          FROM DUAL;
      
        QUARENTENA(IN_DDD_TERMINAL, IN_NUM_TERMINAL);
      
        BEGIN
          FOR V IN (SELECT *
                      FROM SN_DISP_PORTABILIDADE V
                     WHERE V.CID_CONTRATO = T_CID_CONTRATO
                       AND V.ID_SOFTX = SOFT_X
                       AND V.NUM_CONTRATO IS NOT NULL
                       AND ROWNUM = 1) LOOP
          
            INSERT INTO PROD_JD.SN_DISP_PORTABILIDADE
              (DDD_TELEFONE_VOIP,
               NUM_TELEFONE_VOIP,
               ID_OPERADORA_DOADORA,
               CID_CONTRATO,
               FC_AREA_LOCAL,
               NUM_CONTRATO,
               ID_PROPOSTA,
               CD_CNL,
               FN_TIPO_PORTABILIDADE,
               ID_SOFTX,
               DT_DISP_PORTABILIDADE,
               ID_OPERADORA_FISCAL,
               CD_CNL_ORIGEM,
               CD_CONTRATO_ANTERIOR,
               CD_CIDADE_ANTERIOR,
               NR_CPF)
            VALUES
              (IN_DDD_TERMINAL,
               IN_NUM_TERMINAL,
               V.ID_OPERADORA_DOADORA,
               V.CID_CONTRATO,
               'S',
               IN_NUM_CONTRATO,
               V.ID_PROPOSTA,
               V.CD_CNL,
               V.FN_TIPO_PORTABILIDADE,
               SOFT_X,
               SYSDATE,
               V.ID_OPERADORA_FISCAL,
               V.CD_CNL_ORIGEM,
               V.CD_CONTRATO_ANTERIOR,
               V.CD_CIDADE_ANTERIOR,
               V.NR_CPF);
            COMMIT;
          
            UPDATE SN_TELEFONE_VOIP
               SET DT_ALTERACAO = SYSDATE, DT_FIM = SYSDATE
             WHERE DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
               AND DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
               AND NUM_TELEFONE_VOIP = IN_NUM_TERMINAL;
            COMMIT;
          
            INSERT INTO PROD_JD.SN_TELEFONE_VOIP
              (CID_CONTRATO,
               NUM_CONTRATO,
               ID_PONTO,
               DDD_TELEFONE_VOIP,
               NUM_TELEFONE_VOIP,
               FQDN,
               NUM_PORTA,
               DT_INI,
               DT_FIM,
               DT_ALTERACAO,
               ID_STATUS_TELEFONE_VOIP,
               GOLDEN,
               TM_ID,
               ID_ESCOLHIDO,
               PUBLICAR,
               NOME_PUBLICACAO,
               NUM_CONTRATO_AVALIACAO,
               ID_SISTEMA_EXTERNO,
               CID_CONTRATO_ORIGEM,
               ID_SOFTX,
               FC_NUMERO_PORTADO,
               FC_INTERCEPTADO)
            VALUES
              (T_CID_CONTRATO,
               NULL,
               NULL,
               IN_DDD_TERMINAL,
               IN_NUM_TERMINAL,
               NULL,
               NULL,
               SYSDATE,
               TO_DATE('30/12/2049', 'DD/MM/YYYY'),
               NULL,
               'D',
               '0',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               SOFT_X,
               'S',
               'N');
            COMMIT;
          
            begin
              SELECT CT.ID_ASSINANTE
                INTO G_ID_ASSINANTE
                FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
               WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                 AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
                 AND OP.CID_CONTRATO = T_CID_CONTRATO;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                G_ID_ASSINANTE := NULL;
            END;
          
            PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
            -- GRAVA LOG
            GRAVALOG(VSESSAO,
                     SYSDATE,
                     'Terminal inserido em área local com sucesso',
                     IN_DDD_TERMINAL || IN_NUM_TERMINAL,
                     IN_COD_CIDADE,
                     IN_NUM_CONTRATO,
                     '');

            RAISE T_VALIDADO;
          END LOOP;
        END;
      ELSIF EXIST_TN.TERMINAL = 0 THEN
        --verifica se o terminal ja esta validado em área local
        BEGIN
          SELECT COUNT(TV.CID_CONTRATO)
            INTO VALIDADO
            FROM PROD_JD.SN_TELEFONE_VOIP TV
           WHERE TV.ID_STATUS_TELEFONE_VOIP IN ('A', 'D')
             AND TV.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
             AND TV.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
             AND TV.DT_FIM =
                 (SELECT MAX(TV1.DT_FIM)
                    FROM PROD_JD.SN_TELEFONE_VOIP TV1
                   WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                     AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                     AND TV1.ID_STATUS_TELEFONE_VOIP IN ('A', 'D')
                     AND TV1.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY'))
             AND EXISTS
           (SELECT 1
                    FROM PROD_JD.SN_DISP_PORTABILIDADE PORT
                   WHERE PORT.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                     AND PORT.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                     AND ROWNUM < 2);
        
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            VALIDADO := NULL;
        END;
      
        IF VALIDADO > 0 THEN
        
          RAISE T_CONTRATO;
          -- GRAVA LOG
          GRAVALOG(VSESSAO,
                   SYSDATE,
                   'Terminal já inserido em área local anteriormente',
                   IN_DDD_TERMINAL || IN_NUM_TERMINAL,
                   IN_COD_CIDADE,
                   IN_NUM_CONTRATO,
                   '');

        ELSIF (VALIDADO = 0) and (EXIST_TN.TERMINAL = 0) then
          -- DBMS_OUTPUT.PUT_LINE(VALIDADO);
        
          begin
            SELECT count(sn.cid_contrato)
              into valida_contrato
              FROM PROD_JD.SN_CONTRATO            CON,
                   prod_jd.sn_rel_status_cto_serv sn,
                   PROD_JD.sn_telefone_voip       tv
             WHERE sn.num_contrato = con.num_contrato
               and sn.num_contrato = tv.num_contrato
               AND sn.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
               and SN.ID_STATUS_CONTRATO = 1
               and sn.NUM_CONTRATO = IN_NUM_CONTRATO
                  -- and sn.cid_contrato = T_CID_CONTRATO;
               AND NOT EXISTS
             (SELECT MAX(TV1.DT_FIM)
                      FROM PROD_JD.SN_TELEFONE_VOIP TV1
                     WHERE TV1.DDD_TELEFONE_VOIP = TV.DDD_TELEFONE_VOIP
                       AND TV1.NUM_TELEFONE_VOIP = TV.NUM_TELEFONE_VOIP
                       AND TV1.ID_STATUS_TELEFONE_VOIP in ('U', 'T')
                       AND TV1.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY'));
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              valida_contrato := NULL;
          end;
          if valida_contrato > 0 then
            --  DBMS_OUTPUT.PUT_LINE(valida_contrato);
            --BUSCA O CID_CONTRATO
            begin
              SELECT CO.CID_CONTRATO
                INTO T_CID_CONTRATO
                FROM SN_CIDADE_OPERADORA CO
               WHERE CO.COD_OPERADORA = IN_COD_CIDADE
                 AND ROWNUM < 2;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                T_CID_CONTRATO := NULL;
            END;

            SELECT PSN_PORTABILIDADE_VOIP.FNSMS_BUSCA_ID_SOFTX(T_CID_CONTRATO,
                                                               IN_NUM_CONTRATO)
              INTO SOFT_X
              FROM DUAL;

            QUARENTENA(IN_DDD_TERMINAL, IN_NUM_TERMINAL);

            BEGIN
              FOR V IN (SELECT *
                          FROM SN_DISP_PORTABILIDADE V
                         WHERE V.CID_CONTRATO = T_CID_CONTRATO
                           AND V.ID_SOFTX = SOFT_X
                           AND V.NUM_CONTRATO IS NOT NULL
                           AND ROWNUM = 1) LOOP

                INSERT INTO PROD_JD.SN_DISP_PORTABILIDADE
                  (DDD_TELEFONE_VOIP,
                   NUM_TELEFONE_VOIP,
                   ID_OPERADORA_DOADORA,
                   CID_CONTRATO,
                   FC_AREA_LOCAL,
                   NUM_CONTRATO,
                   ID_PROPOSTA,
                   CD_CNL,
                   FN_TIPO_PORTABILIDADE,
                   ID_SOFTX,
                   DT_DISP_PORTABILIDADE,
                   ID_OPERADORA_FISCAL,
                   CD_CNL_ORIGEM,
                   CD_CONTRATO_ANTERIOR,
                   CD_CIDADE_ANTERIOR,
                   NR_CPF)
                VALUES
                  (IN_DDD_TERMINAL,
                   IN_NUM_TERMINAL,
                   V.ID_OPERADORA_DOADORA,
                   V.CID_CONTRATO,
                   'S',
                   IN_NUM_CONTRATO,
                   V.ID_PROPOSTA,
                   V.CD_CNL,
                   V.FN_TIPO_PORTABILIDADE,
                   SOFT_X,
                   SYSDATE,
                   V.ID_OPERADORA_FISCAL,
                   V.CD_CNL_ORIGEM,
                   V.CD_CONTRATO_ANTERIOR,
                   V.CD_CIDADE_ANTERIOR,
                   V.NR_CPF);
                COMMIT;

                UPDATE SN_TELEFONE_VOIP
                   SET DT_ALTERACAO = SYSDATE, DT_FIM = SYSDATE
                 WHERE DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY')
                   AND DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
                   AND NUM_TELEFONE_VOIP = IN_NUM_TERMINAL;
                COMMIT;

                INSERT INTO PROD_JD.SN_TELEFONE_VOIP
                  (CID_CONTRATO,
                   NUM_CONTRATO,
                   ID_PONTO,
                   DDD_TELEFONE_VOIP,
                   NUM_TELEFONE_VOIP,
                   FQDN,
                   NUM_PORTA,
                   DT_INI,
                   DT_FIM,
                   DT_ALTERACAO,
                   ID_STATUS_TELEFONE_VOIP,
                   GOLDEN,
                   TM_ID,
                   ID_ESCOLHIDO,
                   PUBLICAR,
                   NOME_PUBLICACAO,
                   NUM_CONTRATO_AVALIACAO,
                   ID_SISTEMA_EXTERNO,
                   CID_CONTRATO_ORIGEM,
                   ID_SOFTX,
                   FC_NUMERO_PORTADO,
                   FC_INTERCEPTADO)
                VALUES
                  (T_CID_CONTRATO,
                   NULL,
                   NULL,
                   IN_DDD_TERMINAL,
                   IN_NUM_TERMINAL,
                   NULL,
                   NULL,
                   SYSDATE,
                   TO_DATE('30/12/2049', 'DD/MM/YYYY'),
                   NULL,
                   'D',
                   '0',
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   SOFT_X,
                   'S',
                   'N');
                COMMIT;

                begin
                  SELECT CT.ID_ASSINANTE
                    INTO G_ID_ASSINANTE
                    FROM PROD_JD.SN_CIDADE_OPERADORA OP,
                         PROD_JD.SN_CONTRATO         CT
                   WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                     AND CT.NUM_CONTRATO = IN_NUM_CONTRATO
                     AND OP.CID_CONTRATO = T_CID_CONTRATO;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    G_ID_ASSINANTE := NULL;
                END;

                PR_INSEREOCORRENCIA(G_ID_ASSINANTE);

                -- GRAVA LOG
                GRAVALOG(VSESSAO,
                         SYSDATE,
                         'Terminal inserido em área local com sucesso',
                         IN_DDD_TERMINAL || IN_NUM_TERMINAL,
                         IN_COD_CIDADE,
                         IN_NUM_CONTRATO,
                         '');

                RAISE T_VALIDADO;
              END LOOP;
            END;
          
          ELSE
            -- GRAVA LOG
            GRAVALOG(VSESSAO,
                     SYSDATE,
                     'Terminal não esta no cenario para incluir em área local com sucesso',
                     IN_DDD_TERMINAL || IN_NUM_TERMINAL,
                     IN_COD_CIDADE,
                     IN_NUM_CONTRATO,
                     '');

            RAISE T_FORA_CENARIO;

          END IF;
        END IF;

      END IF;
    END LOOP;
  END;

  --RAISE T_FORA_CENARIO;
EXCEPTION
  WHEN T_FORA_CENARIO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' está fora do cenário para incluir em area local');
  
  WHEN T_VALIDADO THEN
    RAISE_APPLICATION_ERROR(-20010,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' foi incluido com sucesso em área local para o contrato ' ||
                            IN_NUM_CONTRATO || '.');
  
  WHEN T_ERROR THEN
    RAISE_APPLICATION_ERROR(-20000,
                            'Erro na inclusão do terminal ' ||
                            IN_DDD_TERMINAL || IN_NUM_TERMINAL ||
                            '. Erro: ' || V_ERROR || ' ');
  
  WHEN T_PROPOSTA THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' já foi incluido anteriormente para a proposta ' ||
                            V_PROPOSTA || '.');
  
  WHEN T_CONTRATO THEN
    RAISE_APPLICATION_ERROR(-20050,
                            'Terminal ' || IN_DDD_TERMINAL ||
                            IN_NUM_TERMINAL ||
                            ' já foi incluido em área local anteriormente.');
  
END;
