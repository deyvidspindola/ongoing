/*
Cenário: BILHETE DE DOAÇÃO COM STATUS CANCELADO AO TENTAR GERAR A SOLICITAÇÃO DE DESCONEXÃO/RETIRADA APRESENTA MENSAGEM "CONTRATO COM PROCESSO DE PORTABILIDADE EM ANDAMENTO PARA ESTE PONTO! PORTANTO, NÃO PODE NÃO PODE 
         ABRIR A SOLICITAÇÃO DE RETIRAR PONTO VOIP"
*/
DECLARE
   -- Variaveis dos parametros de input:
/*  IN_COD_CIDADE   prod_jd.sn_cidade_operadora.cod_operadora%type := '&COD_CIDADE';
  IN_NUM_CONTRATO PROD_JD.SN_CONTRATO.NUM_CONTRATO%type := '&NUM_CONTRATO';
  IN_DDD_TERMINAL prod_jd.sn_portabilidade.ddd_telefone_voip%type := '&DDD_TERMINAL';
  IN_NUM_TERMINAL prod_jd.sn_portabilidade.num_telefone_voip%type := '&NUM_TERMINAL';*/

  IN_COD_CIDADE   PROD_JD.SN_CIDADE_OPERADORA.COD_OPERADORA%TYPE := '[OPERADORA]';
  IN_NUM_CONTRATO PROD_JD.PP_VOIPHIT.NUM_CLIENTE%TYPE := [CONTRATO];
  IN_DDD_TERMINAL VARCHAR2(2) := SUBSTR('[TERMINAL]', 1, 2);
  IN_NUM_TERMINAL VARCHAR2(9) := SUBSTR('[TERMINAL]', 3, 8);

  --Variaveis Segregação e Regra de Negocio
  desco        prod_jd.sn_solicitacao_ass.id_solicitacao_ass%type;
  solic_os     prod_jd.sn_solicitacao_ass.id_solicitacao_ass%type;
  solic_oc     prod_jd.sn_solicitacao_ass.id_solicitacao_ass%type;
  vsts         prod_jd.sn_rel_status_cto_serv.id_status_contrato%type;
  VPORTADO     varchar(2);
  solic_cod_oc VARCHAR2(50);
  desco_retira VARCHAR2(50);
  solic        VARCHAR2(50);
  E_TERMINAL_NAO_ENCONTRADO EXCEPTION;

  --Variaveis SO_ORDENA (Grava Log)         
  VBASE          PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VSESSAO        PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR_CPS';
  G_ID_ASSINANTE PROD_JD.SN_ASSINANTE.ID_ASSINANTE%TYPE;

  --Variaveis contadores
  V_REGS_PROCESSADOS  INTEGER := 0;
  V_REGS_PROCESSADOS1 INTEGER := 0;
  VERRO1              VARCHAR2(1000);
  MSG                 VARCHAR2(1000);
  VQTD                INTEGER := 0;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS  IN PROD_JD.SN_CONTRATO.ID_ASSINANTE%TYPE,
                                P_SOLICI IN PROD_JD.SN_SOLICITACAO_ASS.ID_SOLICITACAO_ASS%TYPE) IS
    L_ID_OFENSOR    VARCHAR(20) := '';
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VARÁVEIS NECESSÁRIAS PARA GERAR OCORRÊNCIA
    VIDOCORRENCIA     PROD_JD.SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA PROD_JD.SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235;
    VNOMEINFORMANTE   PROD_JD.SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    PROD_JD.SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     PROD_JD.SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            PROD_JD.SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              PROD_JD.SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        PROD_JD.SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         PROD_JD.SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              PROD_JD.SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): FOI GERADA A SOLICITAÇÃO ' ||
                                                        P_SOLICI ||
                                                        ' DE RETIRADA DE PONTO/DESCONEXÃO POR OPÇÃO. FAVOR PROSSEGUIR COM A BAIXA DESTA SOLICITAÇÃO PARA REGULARIZAR O STATUS DO TELEFONE ' ||
                                                        IN_DDD_TERMINAL ||
                                                        IN_NUM_TERMINAL;
  
  BEGIN
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    PROD_JD.PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                               VIDTIPOOCORRENCIA,
                                               P_IDASS,
                                               VNOMEINFORMANTE ||
                                               L_CALC_CHECKSUM,
                                               VTELINFORMANTE,
                                               VDTOCORRENCIA,
                                               VIDUSR,
                                               VSIT,
                                               VDTRETORNO,
                                               VIDORIGEM,
                                               L_ID_OFENSOR || VOBS);
    COMMIT;
  END PR_INSEREOCORRENCIA;

  --Proc para Gravação de LOG--
  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PCONTRATO PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PTERMINAL PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO,
       PDATA,
       PBASE,
       PCONTRATO,
       PTERMINAL,
       PMENSAGEM,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

  --Procedure -- Gera Desconexão/Retirada de Ponto --
  PROCEDURE PRC_ABRE_SOLIC(P_CID_CONTRATO  IN PROD_JD.SN_CIDADE_OPERADORA.CID_CONTRATO%TYPE,
                           P_NUM_CONTRATO  IN PROD_JD.SN_CONTRATO.NUM_CONTRATO%TYPE,
                           P_ID_PONTO      IN PROD_JD.SN_REL_PONTO_PRODUTO.ID_PONTO%TYPE,
                           P_ID_TIPO_SOLIC IN PROD_JD.SN_TIPO_SOLIC_PROD.ID_TIPO_SOLIC%TYPE) IS
    vID_TIPO_SOLIC      PROD_JD.SN_TIPO_SOLIC_PROD.ID_TIPO_SOLIC%TYPE;
    vID_TIPO_SOLIC_PROD PROD_JD.SN_TIPO_SOLIC_PROD.ID_TIPO_SOLIC_PROD%TYPE;
    IMED                PROD_JD.SN_SOLICITACAO_ASS.IMEDIATA%TYPE;
    ID_ASS              PROD_JD.SN_SOLICITACAO_ASS.ID_SOLICITACAO_ASS%TYPE;
    vVALIDA_PTOS        BOOLEAN;
  BEGIN
    SELECT ID_TIPO_SOLIC_PROD
      INTO vID_TIPO_SOLIC_PROD
      FROM PROD_JD.SN_TIPO_SOLIC_PROD
     WHERE ID_TIPO_SOLIC = P_ID_TIPO_SOLIC
       AND ID_PROD_PARA = 999
       AND ID_PROD_DE = 999;
  
    PROD_JD.PGSN_MAN_STATUS_SERVICOS.PRSN_SOLIC_PERMITIDA_NETSMS(PNUM_CONTRATO       => P_NUM_CONTRATO,
                                                                 PCID_CONTRATO       => P_CID_CONTRATO,
                                                                 PID_TIPO_SOLIC      => P_ID_TIPO_SOLIC,
                                                                 PIDTIPOSOLICPROD    => vID_TIPO_SOLIC_PROD,
                                                                 PID_TIPO_FECHAMENTO => NULL,
                                                                 PDT_BAIXA           => NULL,
                                                                 PACAO               => 1);
    PROD_JD.PSNSOLICASS.VIDPONTO := P_ID_PONTO;
  
    ID_ASS := PROD_JD.SSN_ID_SOLICITACAO_ASSINANTE.NEXTVAL;
  
    INSERT INTO PROD_JD.SN_SOLICITACAO_ASS
      (ID_SOLICITACAO_ASS,
       NUM_CONTRATO,
       CID_CONTRATO,
       ID_TIPO_SOLIC,
       NOME_SOLIC,
       DT_CADASTRO,
       USR_CADASTRO,
       ID_PLANO_PGTO,
       ID_TIPO_SOLIC_PROD,
       IMEDIATA,
       ISENTO)
    VALUES
      (ID_ASS,
       P_NUM_CONTRATO,
       P_CID_CONTRATO,
       P_ID_TIPO_SOLIC,
       'Desconexão do ponto de telefonia.',
       SYSDATE,
       'PROD_JD',
       23,
       vID_TIPO_SOLIC_PROD,
       0,
       1);
  
    IF P_ID_TIPO_SOLIC = 924 THEN
      UPDATE PROD_JD.SN_OS OSS
         SET OSS.IMEDIATA = 1
       WHERE OSS.ID_SOLICITACAO_ASS = ID_ASS;
    END IF;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
    
      Raise_application_error(-20000,
                              'Não foi possível prosseguir com o atendimento do número de telefone informado, pois já existem solicitações abertas para o contrato que impedem o inicio da tratativa. Favor acessar a ferramenta NETSMS > Aba Produtos e verificar se para este contrato existem solicitações de Retirada de Ponto em aberto ou Desconexão por opção em aberto. Caso haja, estas solicitações devem ser executadas. Verifique também se o contrato possui dívidas antes de realizar nova tentativa de correção. ');
    
  END PRC_ABRE_SOLIC;

  --Procedure -- Muda Status do Contrato
  PROCEDURE muda_sts_contrato(pcid_contrato    IN prod_jd.sn_contrato.cid_contrato%type,
                              pnum_contrato    IN prod_jd.sn_contrato.num_contrato%type,
                              pstatus          IN prod_jd.sn_rel_status_contrato_aux.id_status%type,
                              pstatus_embratel IN prod_jd.sn_rel_status_cto_serv.id_status_contrato%type) as
  BEGIN
    BEGIN
      UPDATE prod_jd.sn_rel_status_cto_serv stc
         SET stc.dt_fim = sysdate
       WHERE stc.cid_contrato = pcid_contrato
         AND stc.num_contrato = pnum_contrato
         AND stc.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY');
    
      INSERT INTO PROD_JD.SN_REL_STATUS_CTO_SERV
      VALUES
        (pnum_contrato,
         pcid_contrato,
         pstatus_embratel,
         2,
         SYSDATE,
         TO_DATE('30/12/2049', 'DD/MM/YYYY'),
         NULL,
         SYSDATE);
      COMMIT;
    END;
  END muda_sts_contrato;

BEGIN
  --Verifica o Alias de Base
  SELECT upper(nm_alias)
    INTO vbase
    FROM PROD_JD.sn_cidade_base
   WHERE cod_operadora = IN_COD_CIDADE; -- (SELECT cod_operadora FROM PROD_JD.sn_cidade_operadora WHERE rownum < 2);

  -- Tratamento de exception, para mensagem de terminal não portado
  BEGIN
    SELECT UPPER(V.FC_NUMERO_PORTADO)
      INTO VPORTADO
      FROM PROD_JD.SN_TELEFONE_VOIP V
     WHERE V.DDD_TELEFONE_VOIP = IN_DDD_TERMINAL
       AND V.NUM_TELEFONE_VOIP = IN_NUM_TERMINAL
       AND V.FC_NUMERO_PORTADO IN ('N', 'S')
       AND V.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
  
    FOR cur_ptb IN (SELECT DISTINCT sp.ddd_telefone_voip,
                                    sp.num_telefone_voip,
                                    sp.id_portabilidade,
                                    shsp.id_status_portabilidade,
                                    tv.id_ponto,
                                    tv.num_contrato,
                                    tv.cid_contrato
                      FROM prod_jd.sn_portabilidade      sp,
                           prod_jd.sn_hist_status_portab shsp,
                           prod_jd.sn_telefone_voip      tv
                     WHERE sp.cid_contrato = tv.cid_contrato
                       AND sp.num_contrato = tv.num_contrato
                       AND sp.ddd_telefone_voip = tv.ddd_telefone_voip
                       AND sp.num_telefone_voip = tv.num_telefone_voip
                       AND sp.id_portabilidade =
                           (SELECT MAX(spa.id_portabilidade)
                              FROM prod_jd.sn_portabilidade spa
                             WHERE spa.ddd_telefone_voip =
                                   sp.ddd_telefone_voip
                               AND spa.num_telefone_voip =
                                   sp.num_telefone_voip
                               AND spa.cid_contrato = sp.cid_contrato
                               AND spa.num_contrato = sp.num_contrato
                               AND spa.tp_portabilidade IN ('DOA', 'PTB'))
                       AND shsp.id_hist_status_portab =
                           (SELECT /*+ index(shsp1, IX_HIST_STATUS_PORTABILIDADE_1)*/
                             MAX(shsp1.id_hist_status_portab)
                              FROM prod_jd.sn_hist_status_portab shsp1
                             WHERE shsp1.id_portabilidade =
                                   sp.id_portabilidade)
                       AND tv.id_ponto =
                           (SELECT MAX(tv1.id_ponto)
                              FROM prod_jd.sn_telefone_voip tv1
                             WHERE tv1.ddd_telefone_voip =
                                   sp.ddd_telefone_voip
                               AND tv1.num_telefone_voip =
                                   sp.num_telefone_voip
                               AND tv1.id_status_telefone_voip = 'U'
                               AND tv1.dt_fim =
                                   to_date('30/12/2049', 'DD/MM/YYYY'))
                       AND shsp.id_portabilidade = sp.id_portabilidade
                       AND shsp.id_status_portabilidade = 9
                       AND EXISTS
                     (SELECT /*+ no_unnest */
                             1
                              FROM prod_jd.sn_ponto_historico p
                             WHERE p.id_ponto = tv.id_ponto
                               AND p.dt_fim =
                                   to_date('30/12/2049', 'DD/MM/YYYY')
                               AND p.instalado = 1
                               AND rownum < 2)
                          /*Parametros de INPUT*/
                       AND TV.ddd_telefone_voip = IN_DDD_TERMINAL
                       AND TV.num_telefone_voip = IN_NUM_TERMINAL
                       AND TV.num_contrato = IN_NUM_CONTRATO) LOOP
    
      --- VERIFICA SE EXISTE SOLICITAÇÃO DE ADESÃO/REINSTALAÇÃO EM ABERTO
      BEGIN
      
        SELECT occ.COD_OC, acs.id_solicitacao_ass
          INTO solic_cod_oc, solic
          FROM prod_jd.sn_solicitacao_ass acs, prod_jd.sn_oc occ
         WHERE acs.num_contrato = cur_ptb.num_contrato
           AND acs.cid_contrato = cur_ptb.cid_contrato
           AND acs.id_solicitacao_ass = occ.id_solicitacao_ass
           AND occ.id_ponto = cur_ptb.id_ponto
           and occ.id_tipo_oc in (57, 42)
           AND acs.id_tipo_solic IN (1, 2, 926, 925)
           and occ.ID_TIPO_FECHAMENTO is null;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          solic_cod_oc := NULL;
          solic        := NULL;
        
      END;
    
      --- VERIFICA A QUANTIDADE DE PONTOS NO CONTRATO 
      begin
        SELECT COUNT(*)
          INTO VQTD
          FROM SN_REL_PONTO_PRODUTO RPP, SN_PRODUTO PROD
         WHERE RPP.ID_PRODUTO = PROD.ID_PRODUTO
           AND PROD.ID_CARACTERISTICA = 4
           AND PROD.ID_TIPO_PRODUTO = 1
           AND RPP.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/RRRR')
           AND RPP.NUM_CONTRATO = cur_ptb.num_contrato
           AND RPP.CID_CONTRATO = cur_ptb.cid_contrato;
      end;
    
      --Grava Status antigo do Contrato EBT
      BEGIN
        SELECT DISTINCT stc.id_status
          INTO vsts
          FROM prod_jd.sn_rel_status_contrato_aux stc
         WHERE stc.cid_contrato = cur_ptb.cid_contrato
           AND stc.num_contrato = cur_ptb.num_contrato
           AND stc.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY');
      EXCEPTION
        WHEN no_data_found THEN
          vsts := NULL;
      END;
    
      --Verifica a existencia de solicitações de Desconexões ou Retirada ou Desabilitar
      BEGIN
        SELECT /* +all_rows */
         sola.id_solicitacao_ass --, sola.id_tipo_fechamento, sola.id_tipo_solic
          INTO desco -- , tp_solic --, stst
          FROM PROD_JD.sn_solicitacao_ass sola, PROD_JD.sn_telefone_voip tv
         WHERE sola.num_contrato = tv.num_contrato
           AND sola.id_tipo_solic IN (924, 232, 923, 223)
           AND (sola.id_tipo_fechamento  <>  3 OR  sola.id_tipo_fechamento IS NULL)
           AND tv.ddd_telefone_voip = to_char(cur_ptb.ddd_telefone_voip)
           AND tv.num_telefone_voip = to_char(cur_ptb.num_telefone_voip)
           AND tv.id_ponto = cur_ptb.id_ponto
           AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
           AND tv.num_contrato = cur_ptb.num_contrato
           AND sola.id_solicitacao_ass =
               (SELECT MAX(sol.id_solicitacao_ass)
                  FROM PROD_JD.sn_solicitacao_ass sol, PROD_JD.sn_os os
                 WHERE sol.num_contrato = sola.num_contrato
                   AND os.id_solicitacao_ass = sol.id_solicitacao_ass
                   AND os.id_ponto = tv.id_ponto
                   AND sol.id_tipo_solic IN (924, 232, 923, 223))
           AND NOT EXISTS
         (SELECT (1)
                  FROM PROD_JD.sn_solicitacao_ass sol2
                 WHERE sol2.num_contrato = tv.num_contrato
                   AND sol2.id_solicitacao_ass > sola.id_solicitacao_ass
                   AND sol2.id_tipo_solic IN (222, 224, 13));
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          desco := NULL;
      END;
    
      BEGIN
        SELECT /*+ index(acs, IX_SOLICITACAO_ASS_08)*/
         acs.id_solicitacao_ass
          INTO solic_oc
          FROM PROD_JD.sn_solicitacao_ass acs, PROD_JD.sn_oc occ
         WHERE acs.num_contrato = cur_ptb.num_contrato
           AND acs.cid_contrato = cur_ptb.cid_contrato
           AND acs.id_solicitacao_ass = occ.id_solicitacao_ass
           AND occ.id_ponto = cur_ptb.id_ponto
           AND acs.id_tipo_solic IN (223, 224, 235)
           AND acs.ID_TIPO_FECHAMENTO = 1 -- EXECUTADA
              /*AND acs.id_solicitacao_ass > (SELECT MAX(sol.id_solicitacao_ass)
                                                                                                                                             FROM PROD_JD.sn_solicitacao_ass sol, PROD_JD.sn_os os
                                                                                                                                            WHERE sol.num_contrato = acs.num_contrato
                                                                                                                                              AND os.id_solicitacao_ass = sol.id_solicitacao_ass
                                                                                                                                              AND os.id_ponto = occ.id_ponto
                                                                                                                                              AND sol.id_tipo_solic IN (2,3,926,925,224,235,12,13))*/
           AND rownum < 2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          solic_oc := NULL;
      END;
    
      BEGIN
        SELECT /*+ index(acs, IX_SOLICITACAO_ASS_08)*/
         acs.id_solicitacao_ass
          INTO solic_os
          FROM prod_jd.sn_solicitacao_ass acs, PROD_JD.sn_os occ
         WHERE acs.num_contrato = cur_ptb.num_contrato
           AND acs.cid_contrato = cur_ptb.cid_contrato
           AND acs.id_solicitacao_ass = occ.id_solicitacao_ass
           AND occ.id_ponto = cur_ptb.id_ponto
           AND acs.id_tipo_solic IN (923, 924, 4, 226, 223, 232)
           and acs.ID_TIPO_FECHAMENTO = 1 -- EXECUTADA
              /*AND acs.id_solicitacao_ass > (SELECT MAX(sol.id_solicitacao_ass)
                                                                                                                                             FROM PROD_JD.sn_solicitacao_ass sol, PROD_JD.sn_os os
                                                                                                                                            WHERE sol.num_contrato = acs.num_contrato
                                                                                                                                              AND os.id_solicitacao_ass = sol.id_solicitacao_ass
                                                                                                                                              AND os.id_ponto = occ.id_ponto
                                                                                                                                              AND sol.id_tipo_solic IN (2,3,926,925,224,235,12,13))*/
           AND rownum < 2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          solic_os := NULL;
      END;
      
      IF desco is not null THEN
  
          
            Raise_application_error(-20000,
                                        'Não foi possível prosseguir com o atendimento do número de telefone informado, pois já existem solicitações abertas para o contrato que impedem o inicio da tratativa. Favor acessar a ferramenta NETSMS > Aba Produtos e verificar se para este contrato existem solicitações de Retirada de Ponto em aberto ou Desconexão por opção em aberto. Caso haja, estas solicitações devem ser executadas. Verifique também se o contrato possui dívidas antes de realizar nova tentativa de correção. ');
          
       -- end;
      
      else
      
      
      
    
      IF solic_cod_oc is not null THEN
        begin
          UPDATE SN_OC SOC
             SET SOC.ID_TIPO_FECHAMENTO = 1,
                 SOC.DT_BAIXA           = SYSDATE,
                 SOC.USR_BAIXA          = 'PROD_JD'
           WHERE SOC.COD_OC = solic_cod_oc;
        
          COMMIT;
        
          UPDATE SN_SOLICITACAO_ASS SSA
             SET SSA.ID_TIPO_FECHAMENTO = 1,
                 SSA.DT_BAIXA           = SYSDATE,
                 SSA.USR_BAIXA          = 'PROD_JD'
           WHERE SSA.ID_SOLICITACAO_ASS = solic;
        
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
          
            Raise_application_error(-20050,
                                    'Não foi possível seguir com o atendimento de sua solicitação, devido existir uma OC ' ||
                                    solic_cod_oc ||
                                    ' Favor acessar o NETSMS > Produtos se existe solicitações em aberto após a baixa tentar novamente gerar a solicitação.');
          
        end;
      
      else
      
        IF (desco IS NULL AND solic_os IS NULL AND solic_oc IS NULL and
           VQTD > 1) THEN
        
          muda_sts_contrato(cur_ptb.cid_contrato,
                            cur_ptb.num_contrato,
                            1,
                            1); -- Muda status do Contrato
          PRC_ABRE_SOLIC(cur_ptb.cid_contrato,
                         cur_ptb.num_contrato,
                         cur_ptb.id_ponto,
                         
                         232); -- Abre Desconexão Opção
          muda_sts_contrato(cur_ptb.cid_contrato,
                            cur_ptb.num_contrato,
                            vsts,
                            1); -- Retorna status do contrato      
        
          MSG := 'retirada de ponto';
          GRAVALOG(VSESSAO,
                   SYSDATE,
                   VBASE,
                   MSG,
                   cur_ptb.ddd_telefone_voip || cur_ptb.num_telefone_voip,
                   IN_COD_CIDADE || '/' || cur_ptb.num_contrato);
        
          -- GRAVA OCORRENCIA
          SELECT CT.ID_ASSINANTE
            INTO G_ID_ASSINANTE
            FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
           WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
             AND CT.NUM_CONTRATO = cur_ptb.num_contrato
             AND OP.CID_CONTRATO = cur_ptb.cid_contrato;
        
          ---pega o numero da solicitação que foi aberta        
        
          SELECT sola.id_solicitacao_ass
            INTO desco_retira
            FROM PROD_JD.sn_solicitacao_ass sola,
                 PROD_JD.sn_telefone_voip   tv
           WHERE sola.num_contrato = tv.num_contrato
             AND sola.id_tipo_solic IN (924, 232, 923, 223)
             AND sola.id_tipo_fechamento is null
             AND tv.ddd_telefone_voip = to_char(cur_ptb.ddd_telefone_voip)
             AND tv.num_telefone_voip = to_char(cur_ptb.num_telefone_voip)
             AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
             AND tv.num_contrato = cur_ptb.num_contrato
             AND sola.id_solicitacao_ass =
                 (SELECT MAX(sol.id_solicitacao_ass)
                    FROM PROD_JD.sn_solicitacao_ass sol, PROD_JD.sn_os os
                   WHERE sol.num_contrato = sola.num_contrato
                     AND os.id_solicitacao_ass = sol.id_solicitacao_ass
                     AND os.id_ponto = tv.id_ponto
                     AND sol.id_tipo_solic IN (924, 232, 923, 223));
          ---
        
          PR_INSEREOCORRENCIA(G_ID_ASSINANTE, desco_retira);
          V_REGS_PROCESSADOS1 := V_REGS_PROCESSADOS1 + 1;
        
        ELSE
        
          IF (desco IS NULL AND solic_os IS NULL AND solic_oc IS NULL and
             VQTD < 2) THEN
          
            BEGIN
              muda_sts_contrato(cur_ptb.cid_contrato,
                                cur_ptb.num_contrato,
                                1,
                                1); -- Muda status do Contrato
              PRC_ABRE_SOLIC(cur_ptb.cid_contrato,
                             cur_ptb.num_contrato,
                             cur_ptb.id_ponto,
                             924); -- Abre Desconexão Opção
              muda_sts_contrato(cur_ptb.cid_contrato,
                                cur_ptb.num_contrato,
                                vsts,
                                11); -- Retorna status do contrato
              MSG := 'Desconexão Por Opção Gerada';
              GRAVALOG(VSESSAO,
                       SYSDATE,
                       VBASE,
                       MSG,
                       cur_ptb.ddd_telefone_voip ||
                       cur_ptb.num_telefone_voip,
                       IN_COD_CIDADE || '/' || cur_ptb.num_contrato);
            
              -- GRAVA OCORRENCIA
              SELECT CT.ID_ASSINANTE
                INTO G_ID_ASSINANTE
                FROM PROD_JD.SN_CIDADE_OPERADORA OP, PROD_JD.SN_CONTRATO CT
               WHERE CT.CID_CONTRATO = OP.CID_CONTRATO
                 AND CT.NUM_CONTRATO = cur_ptb.num_contrato
                 AND OP.CID_CONTRATO = cur_ptb.cid_contrato;
            
              ---pega o numero da solicitação que foi aberta        
            
              SELECT sola.id_solicitacao_ass
                INTO desco_retira
                FROM PROD_JD.sn_solicitacao_ass sola,
                     PROD_JD.sn_telefone_voip   tv
               WHERE sola.num_contrato = tv.num_contrato
                 AND sola.id_tipo_solic IN (924, 232, 923, 223)
                 AND sola.id_tipo_fechamento is null
                 AND tv.ddd_telefone_voip =
                     to_char(cur_ptb.ddd_telefone_voip)
                 AND tv.num_telefone_voip =
                     to_char(cur_ptb.num_telefone_voip)
                 AND tv.dt_fim = to_date('30/12/2049', 'DD/MM/YYYY')
                 AND tv.num_contrato = cur_ptb.num_contrato
                 AND sola.id_solicitacao_ass =
                     (SELECT MAX(sol.id_solicitacao_ass)
                        FROM PROD_JD.sn_solicitacao_ass sol,
                             PROD_JD.sn_os              os
                       WHERE sol.num_contrato = sola.num_contrato
                         AND os.id_solicitacao_ass = sol.id_solicitacao_ass
                         AND os.id_ponto = tv.id_ponto
                         AND sol.id_tipo_solic IN (924, 232, 923, 223));
              ---
            
              PR_INSEREOCORRENCIA(G_ID_ASSINANTE, desco_retira);
              V_REGS_PROCESSADOS := V_REGS_PROCESSADOS + 1;
            
            EXCEPTION
              WHEN OTHERS THEN
                VERRO1 := SQLERRM;
                GRAVALOG(VSESSAO,
                         SYSDATE,
                         VBASE,
                         'DXC ' || VERRO1,
                         cur_ptb.ddd_telefone_voip ||
                         cur_ptb.num_telefone_voip,
                         IN_COD_CIDADE || '/' || cur_ptb.num_contrato);
                Raise_application_error(-20000,
                                        'Não foi possível prosseguir com o atendimento do número de telefone informado, pois já existem solicitações abertas para o contrato que impedem o inicio da tratativa. Favor acessar a ferramenta NETSMS > Aba Produtos e verificar se para este contrato existem solicitações de Retirada de Ponto em aberto ou Desconexão por opção em aberto. Caso haja, estas solicitações devem ser executadas. Verifique também se o contrato possui dívidas antes de realizar nova tentativa de correção. ');
            END;
            -- end; 
          END IF;
             END IF;
        END IF;
      END IF;
    END LOOP;
  
    IF V_REGS_PROCESSADOS > 0 THEN
      Raise_application_error(-20010,
                              'A solicitação de desconexão por opção foi aberta com sucesso! Favor acessar a ferramenta NETSMS e seguir com baixa como executada.');
    
    ELSE
      IF V_REGS_PROCESSADOS1 > 0 THEN
        Raise_application_error(-20010,
                                'A solicitação de retirada de ponto foi aberta com sucesso! Favor acessar a ferramenta NETSMS e seguir com baixa como executada.');
      else
      
        Raise_application_error(-20000,
                                'Não foram encontrados erros de portabilidade para o número de telefone informado. Esta opção deve ser utilizada somente quando ocorre o erro: ""Contrato com processo de portabilidade em andamento para este ponto! Portanto, não pode abrir a solicitação de ""RETIRAR PONTO NETFONE"". Este erro ocorre devido o status do bilhete de portabilidade estar como cancelado. Favor acessar a ferramenta NETSMS > Produtos > Hist Portabilidade e verificar se o número de telefone informado possui estas características.');
      END IF;
    END IF;
  
  EXCEPTION
    WHEN no_data_found THEN
      Raise_application_error(-20000,
                              'Não foram encontrados erros de portabilidade para o número de telefone informado. Esta opção deve ser utilizada somente quando ocorre o erro: ""Contrato com processo de portabilidade em andamento para este ponto! Portanto, não pode abrir a solicitação de ""RETIRAR PONTO NETFONE"". Este erro ocorre devido o status do bilhete de portabilidade estar como cancelado. Favor acessar a ferramenta NETSMS > Produtos > Hist Portabilidade e verificar se o número de telefone informado possui estas características.');
  END;
END;
