/* OFENSOR: 234
CENÁRIO: TERMINAL ESTÁ DESCONECTADO COM STATUS “DESCONEXÃO PENDENTE”. CONTRATO NÃO PERMITE ABRIR SOLICITAÇÃO DE MUDANÇA DE 
         ENDEREÇO
         
TRATAMENTO: 
•  VALIDAR SE O TERMINAL É UM PORTADO E CASO NÃO SEJA EXIBIR MENSAGEM PARA O USUÁRIO QUE O TERMINAL É REFERENTE A UM NETFONE 
   E DEVE SER ABERTO UM CHAMADO ATRAVÉS DA FERRAMENTA SUPORTE OS/OC.
•  VERIFICAR SE O TERMINAL PORTADO ESTA COM STATUS DA PORTABILIDADE ATIVO. SE NÃO, É NECESSÁRIO INCLUIR O STATUS E SOLICITAR   
   AO USUÁRIO QUE PROSSIGA COM A SOLICITAÇÃO.
*/
DECLARE

  TERMINAL       INTEGER;
  PORTADO        VARCHAR2(1) := NULL;
  VBASE          PROD_JD.SN_CIDADE_BASE.NM_ALIAS%TYPE;
  CONTRATO       PROD_JD.SN_PORTABILIDADE.NUM_CONTRATO%TYPE;
  G_ID_ASSINANTE SN_CONTRATO.ID_ASSINANTE%TYPE;
  VERRO1         PROD_JD.SO_ORDENA.CAMPO2%TYPE;

  --Variaveis SO_ORDENA (Grava Log)
  --VBASE                 PROD_JD.SO_ORDENA.CAMPO7%TYPE;
  VSESSAO               PROD_JD.SO_ORDENA.SESSAO%TYPE := 'OFENSOR234';
  VAUX_ID_TRANSACAO_NET PROD_JD.PP_VOIPHIT.ID_TRANSACAO_NET%TYPE;
  V_REGS_PROCESSADOS    NUMBER;

  -- PROCEDURE PARA GERAR OCORRENCIA NO NETSMS
  PROCEDURE PR_INSEREOCORRENCIA(P_IDASS IN SN_CONTRATO.ID_ASSINANTE%TYPE)
  
   IS
  
    L_ID_OFENSOR    VARCHAR(20) := '';
    L_CALC_CHECKSUM VARCHAR(20);
  
    -- VARÁVEIS NECESSÁRIAS PARA GERAR OCORRÊNCIA
    VIDOCORRENCIA     SN_OCORRENCIA.ID_OCORRENCIA%TYPE;
    VIDTIPOOCORRENCIA SN_TIPO_OCORRENCIA.ID_TIPO_OCORRENCIA%TYPE := 235;
    VNOMEINFORMANTE   SN_OCORRENCIA.NOME_INFORMANTE%TYPE := 'TI';
    VTELINFORMANTE    SN_OCORRENCIA.TEL_INFORMANTE%TYPE := NULL;
    VDTOCORRENCIA     SN_OCORRENCIA.DT_OCORRENCIA%TYPE := SYSDATE;
    VIDUSR            SN_OCORRENCIA.ID_USR%TYPE := USER;
    VSIT              SN_OCORRENCIA.SIT%TYPE := 1; --FECHADA
    VDTRETORNO        SN_OCORRENCIA.DT_RETORNO%TYPE := NULL;
    VIDORIGEM         SN_ORIGEM_OCORRENCIA.ID_ORIGEM%TYPE := 5; --INTERNA
    VOBS              SN_OCORRENCIA.OBS%TYPE := 'OCORRÊNCIA CRIADA POR TI - ONGOING (UNIT-S): O STATUS DA PORTABILIDADE FOI ALTERADO PARA ATIVO, FAVOR PROSSEGUIR COM A CRIAÇÃO DE SUA SOLICITAÇÃO DE MUDANÇA DE ENDEREÇO.';
  
  BEGIN
  
    -- CALCULA O CHECKSUM
    SELECT TO_NUMBER(L_ID_OFENSOR) + TO_NUMBER(TO_CHAR(SYSDATE, 'MMDD'))
      INTO L_CALC_CHECKSUM
      FROM DUAL;
  
    -- INCLUI OCORRÊNCIA
    PSN_OCORRENCIA.SSNINCLUIOCORRENCIA(VIDOCORRENCIA,
                                       VIDTIPOOCORRENCIA,
                                       P_IDASS,
                                       VNOMEINFORMANTE || L_CALC_CHECKSUM,
                                       VTELINFORMANTE,
                                       VDTOCORRENCIA,
                                       VIDUSR,
                                       VSIT,
                                       VDTRETORNO,
                                       VIDORIGEM,
                                       L_ID_OFENSOR || VOBS);
  
    COMMIT;
  
  END PR_INSEREOCORRENCIA;

  PROCEDURE GRAVALOG(PSESSAO   PROD_JD.SO_ORDENA.SESSAO%TYPE,
                     PDATA     DATE,
                     PBASE     PROD_JD.SO_ORDENA.CAMPO1%TYPE,
                     PMENSAGEM PROD_JD.SO_ORDENA.CAMPO2%TYPE,
                     PCAMPO3   PROD_JD.SO_ORDENA.CAMPO3%TYPE DEFAULT NULL,
                     PCAMPO4   PROD_JD.SO_ORDENA.CAMPO4%TYPE DEFAULT NULL,
                     PCAMPO5   PROD_JD.SO_ORDENA.CAMPO5%TYPE DEFAULT NULL,
                     PCAMPO6   PROD_JD.SO_ORDENA.CAMPO6%TYPE DEFAULT NULL,
                     PCAMPO7   PROD_JD.SO_ORDENA.CAMPO7%TYPE DEFAULT NULL,
                     PCAMPO8   PROD_JD.SO_ORDENA.CAMPO8%TYPE DEFAULT NULL,
                     PCAMPO9   PROD_JD.SO_ORDENA.CAMPO9%TYPE DEFAULT NULL,
                     PCAMPO10  PROD_JD.SO_ORDENA.CAMPO10%TYPE DEFAULT NULL,
                     PCAMPO11  PROD_JD.SO_ORDENA.CAMPO11%TYPE DEFAULT NULL) AS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    INSERT INTO PROD_JD.SO_ORDENA
      (SESSAO,
       DATA,
       CAMPO1,
       CAMPO2,
       CAMPO3,
       CAMPO4,
       CAMPO5,
       CAMPO6,
       CAMPO7,
       CAMPO8,
       CAMPO9,
       CAMPO10,
       CAMPO11)
    VALUES
      (PSESSAO, -- SESSAO
       PDATA, -- DATA
       PBASE, -- CAMPO1
       PMENSAGEM, -- CAMPO2
       PCAMPO3,
       PCAMPO4,
       PCAMPO5,
       PCAMPO6,
       PCAMPO7,
       PCAMPO8,
       PCAMPO9,
       PCAMPO10,
       PCAMPO11);
    COMMIT;
  END GRAVALOG;

BEGIN

  --Varifica o Alias de Base
  SELECT upper(nm_alias)
    INTO VBASE
    FROM sn_cidade_base
   WHERE cod_operadora =
         (SELECT cod_operadora FROM sn_cidade_operadora WHERE rownum < 2);

  GRAVALOG(VSESSAO, SYSDATE, VBASE, 'LOG-INICIO DE PROCESSAMENTO');

  FOR REG IN (SELECT P.DDD_TELEFONE_VOIP,
                     P.NUM_TELEFONE_VOIP,
                     P.NUM_CONTRATO,
                     P.ID_PORTABILIDADE,
                     P.CID_CONTRATO
                FROM PROD_JD.SN_HIST_STATUS_PORTAB   SHSP,
                     PROD_JD.SN_STATUS_PORTABILIDADE SSP,
                     PROD_JD.SN_PORTABILIDADE        P
               WHERE SHSP.ID_STATUS_PORTABILIDADE =
                     SSP.ID_STATUS_PORTABILIDADE
                 AND SHSP.ID_PORTABILIDADE = P.ID_PORTABILIDADE
                 AND SHSP.ID_STATUS_PORTABILIDADE IN (7, 9) -- DESCONEXÃO PENDENTE OU CANCELADO
                 AND SHSP.ID_HIST_STATUS_PORTAB =
                      (SELECT MAX(SHSP2.ID_HIST_STATUS_PORTAB)
                      FROM PROD_JD.SN_HIST_STATUS_PORTAB SHSP2
                     WHERE SHSP2.ID_PORTABILIDADE = SHSP.ID_PORTABILIDADE)
               AND P.DDD_TELEFONE_VOIP = SUBSTR('[TERMINAL]', 1, 2)
               AND P.NUM_TELEFONE_VOIP = SUBSTR('[TERMINAL]', 3, 8)
               AND P.NUM_CONTRATO = [CONTRATO]
               AND EXISTS
                   (SELECT UPPER(CB.NM_ALIAS)
                      FROM PROD_JD.SN_CIDADE_BASE CB
                     WHERE CB.CID_CONTRATO = P.CID_CONTRATO
                       AND CB.COD_OPERADORA = [COD_OPERADORA]
                         AND ROWNUM < 2)
                 AND ROWNUM < 2) LOOP
  
    BEGIN
      SELECT 'S'
        INTO PORTADO
        FROM SN_TELEFONE_VOIP TV
       WHERE TV.DDD_TELEFONE_VOIP = REG.DDD_TELEFONE_VOIP
         AND TV.NUM_TELEFONE_VOIP = REG.NUM_TELEFONE_VOIP
         AND TV.NUM_CONTRATO = REG.NUM_CONTRATO
         AND TV.CID_CONTRATO = REG.CID_CONTRATO
         AND TV.FC_NUMERO_PORTADO = 'S'
         AND TV.ID_STATUS_TELEFONE_VOIP = 'C'
         AND TV.DT_FIM = TO_DATE('30/12/2049', 'DD/MM/YYYY');
    EXCEPTION
      WHEN OTHERS THEN
        PORTADO := 'N';
    END;
  
    IF PORTADO = 'N' THEN
    
      RAISE_APPLICATION_ERROR(-20000,
                              'Esta opção deve ser utilizada somente para casos onde é necessário gerar a solicitação de mudança de endereço e ocorre o erro: "Contrato com processo de portabilidade em andamento"! Portanto, não pode abrir a solicitação de "MUDANCA DE ENDEREÇO - RETIRAR PONTOS". Este erro é ocasionado devido o status da portabilidade estar como desconexão pendente ou cancelado. Favor acessar a ferramenta NETSMS > Produtos > Hist Portabilidade e verificar o status do bilhete de portabilidade do seu número de telefone antes de realizar nova tentativa.' ||
                              CHR(13) || CHR(10) ||
                              '');
    
    END IF;
  
    IF PORTADO = 'S' THEN
    
      V_REGS_PROCESSADOS := V_REGS_PROCESSADOS + 1;
    
      -- INSERE STATUS PORTABILIDADE: ATIVO 
      INSERT INTO SN_HIST_STATUS_PORTAB
      VALUES
        (SQ_ID_HIST_STATUS_PORTAB.NEXTVAL,
         SYSDATE,
         REG.ID_PORTABILIDADE,
         2);
    
        COMMIT;
    
       RAISE_APPLICATION_ERROR(-20010, 'O status do bilhete foi regularizado conforme solicitado. Favor prosseguir com a solicitação de mudança de endereço!');
            
    
      -- LOGA PROCESSO OK
      GRAVALOG(VSESSAO,
               SYSDATE,
               VBASE,
               'OK',
               REG.DDD_TELEFONE_VOIP || REG.NUM_TELEFONE_VOIP,
               REG.NUM_CONTRATO,
               REG.CID_CONTRATO);
         
    END IF;
    BEGIN
    
      ------ GRAVA OCORRENCIA --------------
      SELECT CT.ID_ASSINANTE
        INTO G_ID_ASSINANTE
        FROM SN_CONTRATO CT
       WHERE CT.CID_CONTRATO = REG.CID_CONTRATO
         AND CT.NUM_CONTRATO = REG.NUM_CONTRATO;
    
      PR_INSEREOCORRENCIA(G_ID_ASSINANTE);
    
    EXCEPTION
      WHEN OTHERS THEN
        VERRO1 := SQLERRM;
        GRAVALOG(VSESSAO,
                 SYSDATE,
                 VBASE,
                 VERRO1,
                 REG.DDD_TELEFONE_VOIP || REG.NUM_TELEFONE_VOIP,
                 REG.NUM_CONTRATO,
                 REG.CID_CONTRATO,
                 'ERRO');
        --   DBMS_OUTPUT.PUT_LINE(VERRO1 || ';ERRO;');       
                 
      
    END;
  
  END LOOP;
  
if   PORTADO is null then
  RAISE_APPLICATION_ERROR(-20050,'Esta opção deve ser utilizada somente para casos onde é necessário gerar a solicitação de mudança de endereço e ocorre o erro: "Contrato com processo de portabilidade em andamento"! Portanto, não pode abrir a solicitação de "MUDANCA DE ENDEREÇO - RETIRAR PONTOS". Este erro é ocasionado devido o status da portabilidade estar como desconexão pendente ou cancelado. Favor acessar a ferramenta NETSMS > Produtos > Hist Portabilidade e verificar o status do bilhete de portabilidade do seu número de telefone antes de realizar nova tentativa.');
                              
  
 
   
  -- GRAVA LOG DE FINAL DE PROCESSO
  GRAVALOG(VSESSAO,
           SYSDATE + (1 / 24 / 3600),
           VBASE,
           'FINAL DE PROCESSAMENTO - ' || V_REGS_PROCESSADOS ||
          ' REGISTROS PROCESSADOS');
  end if;          
END;
