$(function() {

    $('.date-picker').datepicker({
        language: "pt-BR",
        rtl: false,
        orientation: "rigth bottom",
        autoclose: !0,
    });

    $(".mask_date").inputmask("d/m/y");


    $('body')
        .on('click', '.show-details', function () {
            $.get($(this).data('route'), function (data) {
                $('#modal-details').html(data);
                $('#modal-details').modal();
            });
        })
        .on('click', '#export_filter', function () {
            var route_export = $('#route-export').val();

            if($('#type_filter').val() != 'Todos') {
                $('#export_file').val('yes');
                $('#search-form').attr('action', route_export);
                $('#search-form').submit();
                $('#export_file').val('no');
            } else {
                swal('Opss.', 'Selecione um tipo de ofensor!', 'error');
            }

        })
        .on('submit', '#search-form', function () {
            var exportFile = $('#export_file').val();
            if (exportFile != 'yes'){
                oTable.draw();
                return false;
            }
        });


    // auto complete para os ofensores
    $('#offender_filter').autocomplete({
        minLength: 3,
        source: function (request, response) {
            $.ajax({
                url: 'history/search',
                type: 'GET',
                dataType: 'json',
                data: {
                    query: request.term
                },
                success: function (data) {
                    response($.map(data, function (el) {
                        if(el.type = "Externo") {
                            el.offender_id = el.offender_id.slice(0,-1);
                        }
                        return {
                            label: el.name,
                            value: el.offender_id
                        }
                    }))
                }
            });
        },
        select: function (event, ui) {
            this.value = ui.item.label;
            console.log(ui.item);
            $('#id_offender_filter').val(ui.item.value);
            event.preventDefault();
        }
    });


    var oTable = $('#offenders-history-table').DataTable({
        bPaginate: true,
        bLengthChange: false,
        bFilter: false,
        bInfo: true,
        processing: true,
        serverSide: true,
        responsive: true,
        bProcessing: false,
        language: {
            url: "/assets/global/plugins/datatables/lang/pt-BR.json"
        },
        ajax: {
            url: 'history/datatable',
            data: function (d) {
                d.type_filter           = $('select[name=type_filter]').val();
                d.id_offender_filter    = $('input[name=id_offender_filter]').val();
                d.date_ini_filter       = $('input[name=date_ini_filter]').val();
                d.date_end_filter       = $('input[name=date_end_filter]').val();
                d.solver_group_filter   = $('select[name=solver_group_filter]').val();
            }
        },
        columns: [
            { data: 'offender_id', name: 'offender_id'},
            { data: 'offender', name: 'offender'},
            { data: 'type', name: 'type'},
            { data: 'created_at', name: 'created_at'},
            { data: 'updated_at', name: 'updated_at'},
            { data: 'action', name: 'action', orderable: false, searchable: false, sClass: 'text-center'}
        ]
    });



});

