runConfirmCancel();

$(function () {
    $('.internal').hide();

    $('body')
        .on('change', '#type', function () {
            if($(this).val() == 'Interno'){
                $("#retention_level").prop('disabled', true);
                $('.external').hide();
                $('.internal').show();
            }else{
                $("#retention_level").prop('disabled', false);
                $('.internal').hide();
                $('.external').show();
            }
        })
        .on('keypress', '.autocomplete', function () {
            var action = $(this).data('action');
            var nextImputId = $(this).data('next_imput_id');
            $(this).autocomplete({
                minLength: 0,
                source: function (request, response) {
                    $.ajax({
                        url: 'search',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            acao: action,
                            query: request.term
                        },
                        success: function (data) {
                            response($.map(data, function (el) {
                                return {
                                    label: el.name,
                                    value: el.id
                                }
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    this.value = ui.item.label;
                    if (nextImputId != 'undefined') {
                        $('#'+nextImputId).val(ui.item.value);
                    }
                    event.preventDefault();
                }
            });
        })
        .on('focusout', '#offender_id', function () {
            $.get('offender-id-exists/'+$(this).val(), function (data) {
               if (data == 0){
                   $('#offender_id').focus().val('');
                   swal('Erro!', 'O Id informado para o ofensor já existe', 'error');
               }
            });
        })
        .on('focusout', '#classification', function () {
            checkClassifications();
        })
        .on('focusout', '#macro_offender', function () {
            if($("#macro_offender").val() == "") {
                $("#macro_offender_id").val('');
            }
        })
        .on('change', '#retention_level', function () {
            checkClassifications();
        });

        runValidate('#form-save');

        var checkClassifications = function () {
            var classification = $('#classification').val();
            var retention_level = $('#retention_level').val();

            if (retention_level === 'N1') {

                if (classification.toLowerCase() != 'falha de processo' && classification.toLowerCase() != 'falha na abertura do chamado') {
                    swal('Erro!', 'Quando o nivel de retenção for N1 a classificação não pode ser diferente de "Falha de Processo" ou "Falha na abertura do chamado"', 'error');
                }
            }
        }

});
