runConfirmCancel();
$(function () {

    $('body')
        .on('click', '#remove_file', function () {
            $.get($(this).data('url'), function () {
               return true;
            });
        })
        .on('keypress', '.autocomplete', function () {
            var action = $(this).data('action');
            var nextImputId = $(this).data('next_imput_id');
            $(this).autocomplete({
                minLength: 0,
                source: function (request, response) {
                    $.ajax({
                        url: '/offender/search',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            acao: action,
                            query: request.term
                        },
                        success: function (data) {
                            response($.map(data, function (el) {
                                return {
                                    label: el.name,
                                    value: el.id
                                }
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    this.value = ui.item.label;
                    if (nextImputId != 'undefined') {
                        $('#'+nextImputId).val(ui.item.value);
                    }
                    event.preventDefault();
                }
            });
        })
        .on('focusout', '#classification', function () {
            checkClassifications();
        })
        .on('focusout', '#macro_offender', function () {
            if($("#macro_offender").val() == "") {
                $("#macro_offender_id").val('');
            }
        })
        .on('change', '#retention_level', function () {
            checkClassifications();
        });


    // auto complete para os ofensores
    $('#macro_offender').autocomplete({
        minLength: 0,
        source: function (request, response) {
            $.ajax({
                url: '/offender/search',
                type: 'GET',
                dataType: 'json',
                data: {
                    acao: 'macro_offender',
                    query: request.term
                },
                success: function (data) {

                    if (data.length == 0) {
                        $('#macro_offender_id').val('');
                    }

                    response($.map(data, function (el) {
                        return {
                            label: el.name,
                            value: el.id
                        }
                    }))
                }
            });
        },
        select: function (event, ui) {
            this.value = ui.item.label;
            $('#macro_offender_id').val(ui.item.value);
            event.preventDefault();
        }
    });


    runValidate('#form-save');

    var checkClassifications = function () {
        var classification = $('#classification').val();
        var retention_level = $('#retention_level').val();

        if (retention_level === 'N1') {

            if (classification.toLowerCase() != 'falha de processo' && classification.toLowerCase() != 'falha na abertura do chamado') {
                swal('Erro!', 'Quando o nivel de retenção for N1 a classificação não pode ser diferente de "Falha de Processo" ou "Falha na abertura do chamado"', 'error');
            }
        }
    }

});