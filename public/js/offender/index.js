$(function() {

    $('.date-picker').datepicker({
        language: "pt-BR",
        rtl: false,
        orientation: "rigth bottom",
        autoclose: !0,
    });

    $(".mask_date").inputmask("d/m/y");

    $('body')
        .on('click', '.show-details', function () {
            var offender_id = $(this).data('offender_id');
            $.get('offender/show-details/'+offender_id, function (data) {
                $('#modal-details').html(data);
                $('#modal-details').modal();
            });
        })
        .on('change', '#type_filter', function () {
            if($(this).val() == 'Interno'){
                $('.hide-interno').hide();
            }else{
                $('.hide-interno').show();
            }
        })
        .on('keypress', '.autocomplete', function () {
            var route = $("#route").val();
            var action = $(this).data('action');
            var nextImputId = $(this).data('next_imput_id');
            $(this).autocomplete({
                minLength: 3,
                source: function (request, response) {
                    $.ajax({
                        url: route,
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            acao: action,
                            query: request.term
                        },
                        success: function (data) {
                            response($.map(data, function (el) {
                                return {
                                    label: el.name,
                                    value: el.id
                                }
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    this.value = ui.item.label;
                    if (nextImputId != 'undefined') {
                        $('#'+nextImputId).val(ui.item.value);
                    }
                    event.preventDefault();
                }
            });
        });


    var oTable = $('#offenders-table').DataTable({
        bPaginate: true,
        bLengthChange: false,
        bFilter: false,
        bInfo: true,
        processing: true,
        serverSide: true,
        responsive: true,
        bProcessing: false,
        language: {
            url: "/assets/global/plugins/datatables/lang/pt-BR.json"
        },
        ajax: {
            url: 'offender/datatable',
            data: function (d) {
                d.type_filter               = $('select[name=type_filter]').val();
                d.status_filter             = $('select[name=status_filter]').val();
                d.solver_group_filter       = $('select[name=solver_group_filter]').val();
                d.id_macro_offender_filter  = $('input[name=id_macro_offender_filter]').val();
                d.id_offender_filter        = $('input[name=id_offender_filter]').val();
                d.retention_level_filter    = $('input[name=retention_level_filter]').val();
                d.classification_filter     = $('input[name=classification_filter]').val();
                d.date_ini_filter           = $('input[name=date_ini_filter]').val();
                d.date_end_filter           = $('input[name=date_end_filter]').val();
            }
        },
        columns: [
            { data: 'offender_id', name: 'offender_id'},
            { data: 'offender', name: 'offender'},
            { data: 'macro_offender', name: 'macro_offender'},
            { data: 'retention_level', name: 'retention_level', sClass: 'text-center'},
            { data: 'type', name: 'type', sClass: 'text-center'},
            { data: 'created_at', name: 'created_at', sClass: 'text-center'},
            { data: 'action', name: 'action', orderable: false, searchable: false, sClass: 'text-center'}
        ]
    });

    $('#search-form').on('submit', function(e) {
        oTable.draw();
        return false;
    });

});

