runConfirmCancel();

$(function () {

    $('body')
        .on('click', '.add', function () {
            $.get('get-row', function (data) {
                $('#clones').append(data);
            })
        })
        .on('click', '.remove', function () {
            var rowId = $(this).data('rowid');
            $("#"+rowId).remove();
        })
        .on('keypress', '.offender_id', function () {
            var rowid = $(this).data('rowid');
            var url_search = $(this).data('url_search');
            $('#ppm_id_'+rowid).val('');
            $(this).autocomplete({
                minLength: 0,
                source: function (request, response) {
                    $.ajax({
                        url: url_search,
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            query: request.term
                        },
                        success: function (data) {
                            response($.map(data, function (el) {
                                return {
                                    label: el.name,
                                    value: el.id,
                                    ppm_id: el.ppm_id
                                }
                            }))
                        }
                    });
                },
                select: function (event, ui) {
                    this.value = ui.item.label;
                    $('#offender_id_'+rowid).val(ui.item.value);
                    $('#ppm_id_'+rowid).val(ui.item.ppm_id);

                    event.preventDefault();
                }
            });
        })
        .on('focusout', '.offender_id', function () {
            var rowid = $(this).data('rowid');

            if(this.value == ''){
                $('#ppm_id_'+rowid).val('');
            }
        })
        .on('focus', '.maskMoney', function () {
            $(this).maskMoney({thousands:'', decimal:'.', allowZero:true});
        })
        .on('focus', '.mask_date', function () {
            $(this).inputmask("d/m/y");
            $(this).datepicker({
                language: "pt-BR",
                rtl: false,
                orientation: "rigth bottom",
                autoclose: !0,
            });
        })
        .on('click', '.click_date', function () {
            $('.mask_date').focus();
        })
    ;


    //
    // // auto complete para os ofensores
    // $('.offender_id').autocomplete({
    //     minLength: 0,
    //     source: function (request, response) {
    //         $.ajax({
    //             url: 'search',
    //             type: 'GET',
    //             dataType: 'json',
    //             data: {
    //                 query: request.term
    //             },
    //             success: function (data) {
    //                 response($.map(data, function (el) {
    //                     return {
    //                         label: el.name,
    //                         value: el.id
    //                     }
    //                 }))
    //             }
    //         });
    //     },
    //     select: function (event, ui) {
    //         this.value = ui.item.label;
    //         $('.offender_id').val(ui.item.value);
    //         event.preventDefault();
    //     }
    // });

    runValidate('#form-save');

});