$(function() {

    $('.date-picker').datepicker({
        language: "pt-BR",
        rtl: false,
        orientation: "rigth bottom",
        autoclose: !0,
    });

    $(".mask_date").inputmask("d/m/y");


    $('body')
        .on('click', '.show-details', function () {
            var route = $(this).data('route');
            $.get(route, function (data) {
                $('#modal-details').html(data);
                $('#modal-details').modal();
            });
        });


    var url_search = $("#offender_filter").data('url_search');

    // auto complete para os ofensores
    $('#offender_filter').autocomplete({
        minLength: 0,
        source: function (request, response) {
            $.ajax({
                url: url_search,
                type: 'GET',
                dataType: 'json',
                data: {
                    query: request.term
                },
                success: function (data) {
                    response($.map(data, function (el) {
                        return {
                            label: el.name,
                            value: el.id
                        }
                    }))
                }
            });
        },
        select: function (event, ui) {
            this.value = ui.item.label;
            $('#offender_id_filter').val(ui.item.value);
            event.preventDefault();
        }
    });

    var oTable = $('#synchronism-table').DataTable({
        bPaginate: true,
        bLengthChange: false,
        bFilter: false,
        bInfo: true,
        processing: true,
        serverSide: true,
        responsive: true,
        bProcessing: false,
        language: {
            url: "/assets/global/plugins/datatables/lang/pt-BR.json"
        },
        ajax: {
            url: 'synchronism/datatable',
            data: function (d) {
                d.type_filter           = $('select[name=type_filter]').val();
                d.offender_id_filter    = $('input[name=offender_id_filter]').val();
                d.date_ini_filter       = $('input[name=date_ini_filter]').val();
                d.date_end_filter       = $('input[name=date_end_filter]').val();
            }
        },
        // },
        columns: [
            { data: 'register_date', name: 'register_date'},
            { data: 'offender', name: 'offender'},
            { data: 'type', name: 'type'},
            { data: 'action', name: 'action', orderable: false, searchable: false, sClass: 'text-center'}
        ]
    });

    $('#search-form').on('submit', function(e) {
        oTable.draw();
        return false;
    });

});

