$(function () {

    runConfirmCancel();
    hideSolverGroups();

    $('body')
        .on('click', '.create', function () {
            var id = $(this).attr('id');
            if ($(this).is(':checked')) {
                $(".solver_"+id).each(function () {
                    $(this).prop('checked', true);
                });
            } else {
                $(".solver_"+id).each(function () {
                    $(this).prop('checked', false);
                });
            }
        });

});

function hideSolverGroups()
{
    var selectedValues = $("#permissoes").val();
    if(jQuery.inArray("11", selectedValues) !== -1) {
        $("#solver_groups_table").show();
    } else {
        $("#solver_groups_table").hide();
    }
}


