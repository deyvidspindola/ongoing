$(function () {

    $('#datetimepicker').datetimepicker({
        locale: 'pt-br',
        sideBySide: true,
        // maxDate: new Date()
    });

    $('body')
        .on('change', '#city_contract', function () {

            var base_code = $(this).find(':selected').data('base_code');
            var city_contract = $(this).val();

            if (city_contract != '') {
                $('#base_code').val('Cod. Operadora: ' + base_code);
                $('#base_code_hide').val(base_code);
                $('#contract_code').prop('disabled', false); //Habilita campo do contato
            } else {
                $('#base_code').val('');
                $('#base_code_hide').val('');
                $('#contract_code').prop('disabled', true); //Bloqueia campo do contato
            }

        })
        .on('focus', '#contract_code', function () {
            // Limita o campo a apenas numeros
           $(this).mask("9#");
        })
        .on('blur', '#contract_code', function () {
            // Carrega as informações do contrato
            var contract_code = $(this).val();
            var base_code = $('#base_code_hide').val();

            if (base_code != '' && contract_code.length > 2){
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'/units/net/buscar_assinante',
                    data:{
                        operadora: base_code,
                        contrato: contract_code
                    },
                    timeout: 15000,
                    beforeSend: function (xhr, settings) {
                        $('#terminal_number').prop('disabled', true);
                        $(".loading-fs").show();
                    },
                    success:function(response){
                        $('#customer_name').val(response.data.nome_titular);
                        $("#customer_name").closest('.form-group').removeClass('has-error');
                        $("#customer_name").removeClass('has-error-text');
                        $('#terminal_number').prop('disabled', false);
                        $('#terminal_number').focus();
                    },
                    complete: function () {
                        $(".loading-fs").hide();
                    },
                    error: function(response, timeout){
                        var typeError = response.responseJSON.type_error;
                        if (typeError != 'message'){
                            swal('Erro!', response.responseJSON.message, 'error');
                        } else {
                            $('#customer_name').val(response.responseJSON.message);
                            $("#customer_name").closest('.form-group').addClass('has-error'); // mudar a cor da borda
                            $("#customer_name").addClass('has-error-text'); // mudar a cor do texto do campo
                        }
                        $(".loading-fs").hide();
                    }
                })
            }

        })
        .on('blur', '#terminal_number', function () {

            if($(this).val().length == 10) {
                // Carrega as informações do contrato
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/units/net/buscar_terminal',
                    data: {
                        'terminal': $("#terminal_number").val().replace(/\D+/g, ''),
                        'operadora': $("#base_code").val().replace(/\D+/g, '')
                    },
                    timeout: 15000,
                    beforeSend: function (xhr, settings) {
                        $(".loading-fs").show();
                    },
                    success: function (response) {
                        if(typeof(response.data) != "undefined") {
                            $('#terminal_data').val('CidContrato: ' + response.data[0].cid_contrato + ', numContrato: ' + response.data[0].num_contrato + ', portado: ' + response.data[0].fc_numero_portado + ', status: ' + response.data[0].id_status_telefone_voip);
                            $("#terminal_data").closest('.form-group').removeClass('has-error');
                            $("#terminal_data").removeClass('has-error-text');
                            $('#terminal_number').prop('disabled', false);
                            if($('#flag_fields').length) {
                                $('#ticket_number').prop('disabled', false);
                                $('#ticket_number').focus();
                            }
                        } else {
                            $('#terminal_data').val(response.message);
                            $("#terminal_data").closest('.form-group').addClass('has-error'); // mudar a cor da borda
                            $("#terminal_data").addClass('has-error-text'); // mudar a cor do texto do campo
                        }
                    },
                    complete: function () {
                        $(".loading-fs").hide();
                    },
                    error: function (response, timeout) {
                        var typeError = response.responseJSON.type_error;
                        if (typeError != 'message') {
                            swal('Erro!', response.responseJSON.message, 'error');
                        } else {
                            $('#terminal_data').val(response.responseJSON.message);
                            $("#terminal_data").closest('.form-group').addClass('has-error'); // mudar a cor da borda
                            $("#terminal_data").addClass('has-error-text'); // mudar a cor do texto do campo
                        }
                        $(".loading-fs").hide();
                    }
                })
            }

        }).on('blur', '#ticket_number', function () {
            if (this.value != '') {
                $('#date_time').prop('disabled', false);
                $('#date_time').focus();
            }
        }).on('click', '#tratar', function (e) {
            let a = document.forms["search-form"].reportValidity();

            if (a) {
                swal({
                    title: 'Certeza que quer executar essa ação?',
                    text: "Lembre-se que após executado não poderá mais ser desfeito!",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Sim, Tratar!'
                }, function () {
                    $("#search-form").submit();
                });
            }
        })
        .on('click', '#reset', function () {
            location.reload();
        });
});