$(function () {

    $('.date-picker').datepicker({
        language: "pt-BR",
        rtl: false,
        orientation: "rigth bottom",
        autoclose: !0,
    });

    $(".mask_date").inputmask("d/m/y");

    $('body')
        .on('click', '#export_filter', function () {
            var route_export = $('#route-export').val();
            $('#export_file').val('yes');
            $('#search-form').attr('action', route_export);
            $('#search-form').submit();
            $('#export_file').val('no');
        })
        .on('submit', '#search-form', function () {
            var exportFile = $('#export_file').val();
            if($('#date_ini_filter').val().length == 0 || $('#date_end_filter').val().length == 0){
                swal('Ops..', 'Informe um periodo para continuar!', 'error');
                return false;
            }
            if (exportFile != 'yes'){
                oTable.draw();
                return false;
            }
        });

    var oTable = $('#vod_shopping').DataTable({
        bPaginate: true,
        bLengthChange: false,
        bFilter: false,
        bInfo: true,
        processing: true,
        serverSide: true,
        responsive: true,
        bProcessing: true,
        language: {
            url: "/assets/global/plugins/datatables/lang/pt-BR.json"
        },
        ajax: {
            url: 'compras_vod/datatable',
            data: function (d) {
                d.type          = $('#type_filter').val(),
                d.status        = $('#status_filter').val(),
                d.shopping_id   = $('#shopping_id_filter').val(),
                d.date_ini      = $('#date_ini_filter').val(),
                d.date_end      = $('#date_end_filter').val()
            }
        },
        columns: [
            { data: 'id_compra', name: 'id_compra', orderable: false},
            { data: 'customer', name: 'customer', orderable: false},
            { data: 'smartcard', name: 'smartcard', orderable: false},
            { data: 'dt_compra', name: 'dt_compra', orderable: false, sClass: 'text-center'},
            { data: 'produto', name: 'produto', orderable: false},
            { data: 'valor', name: 'valor', orderable: false},
            { data: 'arquivo_xml', name: 'arquivo_xml', orderable: false},
            { data: 'observacao', name: 'observacao', orderable: false}
        ]
    });

});
