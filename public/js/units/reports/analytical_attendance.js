$(function () {

    $('.date-picker').datepicker({
        language: "pt-BR",
        rtl: false,
        orientation: "rigth bottom",
        autoclose: !0,
    });

    $(".mask_date").inputmask("d/m/y");

    $('body')
        .on('click', '#export_filter', function () {
            var route_export = $('#route-export').val();
            $('#export_file').val('yes');
            $('#search-form').attr('action', route_export);
            $('#search-form').submit();
            $('#export_file').val('no');
        })
        .on('submit', '#search-form', function () {
            var exportFile = $('#export_file').val();

            console.log

            if($('#date_ini_filter').val().length == 0 || $('#date_end_filter').val().length == 0){
                swal('Ops..', 'Informe um periodo para continuar!', 'error');
                return false;
            }

            if (exportFile != 'yes'){
                oTable.draw();
                return false;
            }
        });

    var oTable = $('#analytical_attendance').DataTable({
        bPaginate: true,
        bLengthChange: true,
        bFilter: false,
        bInfo: true,
        processing: true,
        serverSide: true,
        responsive: false,
        bProcessing: true,
        language: {
            url: "/assets/global/plugins/datatables/lang/pt-BR.json"
        },
        ajax: {
            url: 'atendimentos-units/datatable',
            data: function (d) {
                d.status        = $('#status_filter').val(),
                d.date_ini      = $('#date_ini_filter').val(),
                d.date_end      = $('#date_end_filter').val()
            }
        },
        columns: [
            { data: 'data', name: 'data', orderable: false, sClass: 'text-center'},
            { data: 'username', name: 'username', orderable: false },
            { data: 'nome', name: 'nome', orderable: false },
            { data: 'perfil', name: 'perfil', orderable: false },
            { data: 'ip_remoto', name: 'ip_remoto', orderable: false },
            { data: 'id_servico', name: 'id_servico', orderable: false },
            { data: 'descricao', name: 'descricao', orderable: false },
            { data: 'menu', name: 'menu', orderable: false },
            { data: 'contrato', name: 'contrato', orderable: false },
            { data: 'terminal', name: 'terminal', orderable: false },
            { data: 'bilhete', name: 'bilhete', orderable: false },
            { data: 'dth_janela', name: 'dth_janela', orderable: false },
            { data: 'status', name: 'status', orderable: false },
            { data: 'msg_processo', name: 'msg_processo', orderable: false }
        ]
    });

});














