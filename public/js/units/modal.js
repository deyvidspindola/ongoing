$(function() {

  // Recebe os parâmetros dos botões
  $('#modalInformative').on("show.bs.modal", function (e) {
    var button = $(e.relatedTarget);
    //console.log(button);
    
    //Para testes: só adicionar o atributo data-whatever="@qualquertexto" no botão
    //var recipient = button.data('whatever');
    //console.log(recipient);
    
    var title = button.data('title'); //console.log(title);
    var description = button.data('description'); //console.log(description);
    var route = button.data('route'); //console.log(route);

    var modal = $(this);
    modal.find('.modal-title').text(title);
    modal.find('.modal-description').text(description);
    $('.modal-route').attr("href", route); // envia o link para o atributo href
  });

});