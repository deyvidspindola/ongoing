$(function() {
  $('#submit_button').prop("disabled", true);
  // Date Picker
  $('#datetimepicker').datetimepicker({
    locale: 'pt-br',
    sideBySide: true,
    maxDate: new Date()
  });

  $(".only-number").mask("9#"); // máscara, apenas números
  $('.mask-phone-number').mask('(00)0000-00009');
  //$(".mask-phone-number").unmask();

  /**********************************************************
   * 
   * EVENTOS DO INPUT CITY_CONTRACT (CIDADE CONTRATO)
   **********************************************************/ 
  
  // Altera o código da operadora quando altera a cidade
  $( "#city_contract" ).change(function(e) {
    e.preventDefault();
    var id = $(this).find(':selected').data('base-code');
    //console.log(id);
    //var data = $('#city_contract').data('base-code');
    $('#base_code').val('Código da Operadora: ' + id);
    $('input#base_code_hide').val(id);
    $('#contract_code').prop('disabled', false); // habilita o campo de contrato para digitação
  });

  /**********************************************************
   * 
   * EVENTOS DO INPUT CONTRACT_CODE (NUMERO DO CONTRATO)
   **********************************************************/ 

  $("#contract_code").on("keyup", function() {
    var terminal_number = $("#terminal_number").val();
    if(terminal_number.length > 0){
      $('#terminal_number').val('');
      $('#terminal_number').prop('disabled', true);
    }
  })

  $('#contract_code').on('focus', function() {
    $('#terminal_number').prop('disabled', true);
    $('#submit_button').prop('disabled', true);
  })

  // Acionado quando o foco sai do campo do contrato
  $('#contract_code').on('blur', function() {
    var base_code = $('input#base_code_hide').val();
    var contract_code = $(this).val();

    if(contract_code && contract_code.length > 2){
      console.log("Operadora:", base_code, "Contrato:", contract_code);
      $.ajax({
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:'/units/net/buscar_assinante',
        data:{operadora:base_code, contrato:contract_code},
        timeout: 15000,
        beforeSend: function (xhr, settings) {
          $('#terminal_number').prop('disabled', true);
          $(".loading-fs").show();


          // Preciso entender porque tem isso
          //if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
              //xhr.setRequestHeader("X-CSRFToken", csrftoken)
          //}
        },
        success:function(response){
          console.log(response.success);
          console.log(response.data);

          $('#customer_name').val(response.data.nome_titular);

          $("#customer_name").closest('.form-group').removeClass('has-error');
          $("#customer_name").removeClass('has-error-text');
          $('#terminal_number').prop('disabled', false);
          $('#terminal_number').focus();
          //$('#softx').prop('disabled', false);
          //$('#softx').focus();
        },
        complete: function () {
          console.log("COMPLETO!!!!!!!!!!!");
          $(".loading-fs").hide();
        },
        error: function(response, timeout){
          console.log(timeout);
          console.log(response.error);
          $('#customer_name').val('Contrato inválido ou não pertence a cidade informada.');
          $("#customer_name").closest('.form-group').addClass('has-error'); // mudar a cor da borda
          $("#customer_name").addClass('has-error-text'); // mudar a cor do texto do campo
          $(".loading-fs").hide();
        }
     });
    
    }
  });

  /**********************************************************
   * 
   * EVENTOS DO INPUT TERMINAL_NUMBER (NUMERO DO TELEFONE)
   **********************************************************/ 
  // Não sei o que é, manterei para analisar depois.
  // window.consistTerminal = function(terminal_number)
  // {
  //   console.log(terminal_number);
  // }


  $("#terminal_number").on('blur', function () {
    $.ajax({
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/units/net/buscar_terminal',
      data:{'terminal': $("#terminal_number").val().replace(/\D+/g, ''), 'operadora': $("#base_code").val().replace(/\D+/g, '')},
      timeout: 0,
      beforeSend: function (xhr, settings) {
        $('#terminal_number').prop('disabled', true);
        $(".loading-fs").show();


        // Preciso entender porque tem isso
        //if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
        //xhr.setRequestHeader("X-CSRFToken", csrftoken)
        //}
      },
      success:function(response){
        console.log(response.success);
        console.log(response.data);

        $('#terminal_data').val('CidContrato: ' + response.data[0].cid_contrato + ', numContrato: ' + response.data[0].num_contrato + ', portado: ' + response.data[0].fc_numero_portado + ', status: ' + response.data[0].id_status_telefone_voip);

        $("#terminal_data").closest('.form-group').removeClass('has-error');
        $("#terminal_data").removeClass('has-error-text');
        $('#terminal_number').prop('disabled', false);
        // $('#terminal_number').focus();
        $('#submit_button').prop('disabled', false);
        // $('#softx').focus();
      },
      complete: function () {
        console.log("COMPLETO!!!!!!!!!!!");
        $(".loading-fs").hide();
      },
      error: function(response, timeout){
        console.log(timeout);
        console.log(response);
        $('#terminal_data').val(response.responseJSON.message);
        $("#terminal_data").closest('.form-group').addClass('has-error'); // mudar a cor da borda
        $("#terminal_data").addClass('has-error-text'); // mudar a cor do texto do campo
        $(".loading-fs").hide();
      }
    });
  });


  $("#submit_button").on('click', function() {

    var $form = $(this).closest('form');

    $('#modalInformative1').modal({
      backdrop: 'static',
      keyboard: false
    })
    .on('click', '#go', function(e) {
      e.preventDefault();
      $form.trigger('submit');
    });

    // $("#modalInformative").modal({ show: true});
    $("#modalInformativeTitle").html("Certeza que quer executar essa ação?");
    $(".modal-description").html("Lembre-se que após executado não poderá mais ser desfeito");





    console.log('ABRIU');
    // data-placement="bottom" ;
    // data-original-title="Troca de FQDN";
    // data-toggle="modal" ;
    // data-target="#modalInformative";
    // data-title="TROCA FQDN" ;
    // data-description="Esta opção deve ser utilizada somente para telefones, " +
    //     "onde há necessidade de alterar o FQDN e PORTA para telefones portados " +
    //     "e netfones. O(s) terminal(is) deve esta ativo, para que se possível realizar " +
    //     "a troca com sucesso.";

    return false;


  });

});

//onblur="consisteTerminal(document.getElementById('operadora').value,
//document.getElementById('terminal').value,0)"