$(function() {

    $('.date-picker').datepicker({
        language: "pt-BR",
        rtl: false,
        orientation: "rigth bottom",
        autoclose: !0,
    });

    $(".mask_date").inputmask("d/m/y");

    $('body')
        .on('submit', '#search-form', function () {
            if($('#date_ini_filter').val().length == 0 || $('#date_end_filter').val().length == 0){
                swal('Ops..', 'Informe um periodo para continuar!', 'error');
                return false;
            }
            oTable.draw();
            return false;
        })
        .on('click', '.show-details', function () {
            var log_active_id = $(this).data('log_active_id');

            console.log(log_active_id);

            $.get('log-atividades/show-details/'+log_active_id, function (data) {
                $('#modal-details').html(data);
                $('#modal-details').modal();
            });
        });


    var oTable = $('#activities-log-table').DataTable({
        bPaginate: true,
        bLengthChange: false,
        bFilter: false,
        bInfo: true,
        processing: true,
        serverSide: true,
        responsive: true,
        bProcessing: false,
        language: {
            url: "/assets/global/plugins/datatables/lang/pt-BR.json"
        },
        ajax: {
            url: 'log-atividades/datatable',
            data: function (d) {
                d.service_filter            = $('select[name=service_filter]').val();
                d.user_filter               = $('select[name=user_filter]').val();
                d.date_ini_filter           = $('input[name=date_ini_filter]').val();
                d.date_end_filter           = $('input[name=date_end_filter]').val();
                d.terminal_filter           = $('input[name=terminal_filter]').val();
            }
        },
        columns: [
            { data: 'id', name: 'id'},
            { data: 'created_at', name: 'created_at', sClass: 'text-center'},
            { data: 'username', name: 'username'},
            { data: 'terminal', name: 'terminal'},
            { data: 'service', name: 'service'},
            { data: 'status', name: 'status'},
            { data: 'action', name: 'action', orderable: false, searchable: false, sClass: 'text-center'}
        ]
    });

    // $('#search-form').on('submit', function(e) {
    //     oTable.draw();
    //     return false;
    // });

});

