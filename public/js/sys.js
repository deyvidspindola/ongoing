var runValidate = function(element, rules = {}){

    $(element).validate({


        ignore: [],
        errorElement: "span",
        errorClass: "help-block",
        highlight: function (element, errorClass, validClass) {
            $(element).closest('.form-group').addClass('has-error');
            $(element).next('.tagsinput').css('border-color', '#A94442');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error');
            $(element).next().next().css('border-color', '#CCCCCC');
            $(element).next().css('border-color', '#CCCCCC');
        },
        errorPlacement: function (error, element) {

            if (element.parent('.input-group').length || element.prop('type') === 'checkbox') {
                error.insertAfter(element.parent());
            }
            else if (element.prop('type') === 'radio'){
                error.insertAfter(element.parent().parent().parent());
            }
            else if (element.hasClass("tags")){
                error.insertAfter(element.next());
            }
            else if (element.hasClass("search-select")){
                error.insertAfter(element.next());
            }
            else {
                error.insertAfter(element);
            }

        },
        rules: rules
    });
};

var runEnterSubmit = function(elementForm) {

    $('.enterSubmit').keypress(function(e){

        var tecla = (e.keyCode?e.keyCode:e.which);

        // Caso tenha apertado enter
        if(tecla == 13){

            $(elementForm).submit();

        }

    });

};

var runSelect2 = function(element){

    var locale = 'pt-BR';

    $('.search-select').select2({language: locale});

};

var runSelect2Locale = function(element){

    var locale = $(element).data('locale');

    $(element).select2({language: locale});

};

var runTagsInput = function (element) {
    $(element).tagsInput({
        width: 'auto',
        text: 'teste'
    });
};

var runMultiselect = function(element){

    $(element).multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Filtrar" /> <br>',
            right: '<input type="text" name="q" class="form-control" placeholder="Filtrar" /><br>',
        }
    });

};

var runSelectMultiple = function(name, firstName, lastName){

    var filter = $('select[name="'+name+'[]"]').data('filter');

    $('select[name="'+name+'[]"]').bootstrapDualListbox({
        nonSelectedListLabel: '<br><span style="color: #6C7F92; font-size: 18px;">'+firstName+'</span>',
        selectedListLabel: '<br><span style="color: #6C7F92; font-size: 18px;">'+lastName+'</span>',
        selectorMinimalHeight: 250,
        filterPlaceHolder: filter,
        infoText: false
    });

};

var runSelectMultipleHeight = function(name, firstName, lastName, height){

    var filter = $('select[name="'+name+'[]"]').data('filter');

    $('select[name="'+name+'[]"]').bootstrapDualListbox({
        nonSelectedListLabel: '<br><span style="color: #6C7F92; font-size: 18px;">'+firstName+'</span>',
        selectedListLabel: '<br><span style="color: #6C7F92; font-size: 18px;">'+lastName+'</span>',
        selectorMinimalHeight: height,
        filterPlaceHolder: filter,
        infoText: false
    });

};


var runDataTable = function(element, columnOrder) {

    $(element).dataTable({
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sProcessing": "Processando...",
            "sLengthMenu": "Exibindo _MENU_ linhas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            },
            "sZeroRecords": "Não foram encontrados resultados",
            "sEmptyTable": "Não foram encontrados resultados",
            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "",
            "sInfoFiltered": "",
        },
        "aaSorting": [
            [columnOrder, 'desc']
        ],
        "aLengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 10,
    });
    $(element + '_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Pesquisar");
    // modify table search input
    $(element + '_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $(element + '_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $(element + '_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

};


var runChartPie = function(element, dataPie){

    $.plot(element, dataPie, {
        series: {
            pie: {
                show: true,
                radius: 1,
                tilt: 0.5,
                label: {
                    show: true,
                    radius: 1,
                    formatter: labelFormatter,
                    threshold: 0.1,
                    background: {
                        opacity: 0.8
                    }
                }
            }
        },
        legend: {
            show: false
        }
    });

    function labelFormatter(label, series) {
        return "<div style='font-size:9pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
    }

};

var runDynatree = function(ajaxUrl, element, hiddenInput, errorMsg){

    $(element).dynatree({

        initAjax: {
            url: ajaxUrl
        },

        onActivate: function (node) {
            $(hiddenInput).val(node.data.key);
            $(errorMsg).hide();
        }

    });

};

var runbackstretch = function(images){

    $.backstretch(images, {
        fade: 1000,
        duration: 8000
    });

};



$(".type-number").keypress(function(e) {

    var key = e.which;

    return ( ( key > 47 && key < 58) || key == 8 || key == 0 ) ? true : false;

});

// $(".remove-spaces").keypress(function(e) {
//
//     var key = e.which;
//
//     return ( ( key >= 48 && key <= 57 ) || ( key >= 65 && key <= 90 ) || ( key >= 97 && key <= 122 ) || key == 95  ) ? true : false;
//
// });

// $(".remove-underline").keypress(function(e) {
//
//     var key = e.which;
//
//     return (key != 95) ? true : false;
//
// });

// $(".remove-spaces-with-point").keypress(function(e) {
//
//     var key = e.which;
//
//     return ( ( key >= 48 && key <= 57 ) || ( key >= 65 && key <= 90 ) || ( key >= 97 && key <= 122 ) || key == 95 || key == 46 ) ? true : false;
//
// });


var btnConfirmAlert = function(element, message) {

    $('body').on('click', element, function(event) {

        return (confirm(message)) ? true : false;

    });


};

// -- ----------------------------------------------------------------------------
// -- ALERTAS
// -- ----------------------------------------------------------------------------

// Confirmação de Exclusão - @runConfirmDelete (Apelido)

var runConfirmDelete = function(){ swalAlert('.btn-delete', 1); };

// Confirmação de Cancelamento - @runConfirmCancel (Apelido)

var runConfirmCancel = function(){ swalAlert('.btn-cancel', 0); };

// Renderização das propriedades do plugin (swal)

var swalAlert = function (element, showCancel){

    $(element).click(function(){

        var href = $(this).attr('href');

        var sa_title = $(this).data('sa-title');
        var sa_message = $(this).data('sa-message');
        var sa_confirmButtonText = $(this).data('sa-confirmbuttontext');
        var sa_cancelButtonText = $(this).data('sa-cancelbuttontext');
        var sa_popupTitleCancel = $(this).data('sa-popuptitlecancel');
        var sa_popupMessageCancel = '';

        var sa_type = 'warning';
        var sa_confirmButtonClass = 'green-meadow';
        var sa_cancelButtonClass = 'btn-danger';
        var sa_allowOutsideClick = false;
        var sa_showConfirmButton = true;
        var sa_showCancelButton = true;
        var sa_closeOnConfirm = false;
        var sa_closeOnCancel = (showCancel ? false : true);

        swal({
                title: sa_title,
                text: sa_message,
                type: sa_type,
                allowOutsideClick: sa_allowOutsideClick,
                showConfirmButton: sa_showConfirmButton,
                showCancelButton: sa_showCancelButton,
                confirmButtonClass: sa_confirmButtonClass,
                cancelButtonClass: sa_cancelButtonClass,
                closeOnConfirm: sa_closeOnConfirm,
                closeOnCancel: sa_closeOnCancel,
                confirmButtonText: sa_confirmButtonText,
                cancelButtonText: sa_cancelButtonText
            },
            function(isConfirm){
                if (isConfirm){

                    window.location.href = href;

                }else if(showCancel) {
                    swal(sa_popupTitleCancel, sa_popupMessageCancel, "error");
                }
            });

    });

};

var runAlertError = function(title, text){

    swal({
        title: title,
        text: text,
        type: 'error',
        allowOutsideClick: true,
        confirmButtonClass: 'btn-danger'
    });

};

var runAlertSuccess = function(title, text){

    swal({
        title: title,
        text: text,
        type: 'success',
        allowOutsideClick: true,
        confirmButtonClass: 'btn-success'
    });

};

var runAlertWarning = function(title, text){

    swal({
        title: title,
        text: text,
        type: 'warning',
        allowOutsideClick: true,
        confirmButtonClass: 'btn-warning'
    });

};

// -- ----------------------------------------------------------------------------
// -- BLOQUEIO DE PAGINA LOADING
// -- ----------------------------------------------------------------------------

var startLoadingBlock = function(text){

    var icon = '<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>';

    /*var html = '';

     html += '<span class="fa-stack fa-lg">';
     html += '    <i class="fa fa-cog fa-spin fa-stack-2x text-inverse"></i>';
     html += '    <i class="fa fa-laptop fa-stack-1x text-primary"></i>';
     html += '</span>';

     icon = html;*/

    //$.blockUI({ message: '<table width="100%"><tr><td><i class="fa fa-user fa-spin fa-3x fa-fw"></i></td><td class="text-left">'+text+'</td></table>', css: {
    $.blockUI({ message: '<table width="100%"><tr><td>'+icon+'</td><td class="text-left">'+text+'</td></table>', css: {
        border: 'none',
        padding: '15px',
        'font-size': '12pt',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .7,
        color: '#fff'
    } });

};

var stopLoadingBlock = function(){
    $.unblockUI();
};


// -- ----------------------------------------------------------------------------
// -- POST VIA JAVA SCRIPT
// -- ----------------------------------------------------------------------------


var postJS = function(path, params) {

    var form = document.createElement("form");
    form.setAttribute("method", 'POST');
    form.setAttribute("action", path);
    form.setAttribute("target", '_blank');

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
};

// -- ----------------------------------------------------------------------------
// -- VISUALIZAR E ESCONDER SENHA
// -- ----------------------------------------------------------------------------

var changeViewPass = function(){

    $('.change-view-pass').click(function(){

        var type = $(this).attr('type');

        var input = $('#'+$(this).attr('input_id'));

        // Exibir senha
        if(type == 'show'){

            var icon = '<i class="fa fa-eye-slash font-red-flamingo"></i>';

            var type = 'hide';

            input.attr("type", "text");

        }

        // Esconder senha
        else {

            var icon = '<i class="fa fa-eye"></i>';

            var type = 'show';

            input.attr("type", "password");

        }

        $(this).attr('type', type);

        $(this).html(icon);

        input.focus();

    });

};

// -- ----------------------------------------------------------------------------
// -- SLIM SCROLL
// -- ----------------------------------------------------------------------------

var runSlimScroll = function(element) {

    App.initSlimScroll(element);
};

var runSlimScrollDestroy = function(element) {

    App.destroySlimScroll(element);

};

