Dropzone.autoDiscover = false;

$(function () {
    // Now that the DOM is fully loaded, create the dropzone, and setup the
    // event listeners
    var myDropzone = new Dropzone("#my-dropzone");
    myDropzone.options.autoProcessQueue = false;
    myDropzone.on("addedfile", function () {
        if (this.files[1] != null) {
            this.removeFile(this.files[0]);
        }

        this.files[0] != null ? $('#send-import').prop("disabled", false) : $('#send-import').prop("disabled", true)

    });

    myDropzone.on("sending", function () {

        $('<input>').attr({
            type: 'hidden',
            id: 'solver_group_filter',
            name: 'solver_group_filter',
            value: $('#solver_group_filter').val()
        }).appendTo('#my-dropzone');

        $('#loading').show();
    });

    myDropzone.on("removedfile", function () {
        this.files.lenght == 0 ? $('#send-import').prop("disabled", false) : $('#send-import').prop("disabled", true)
    });

    myDropzone.on("success", function (file, response) {


        $('#loading').hide();
        //location.reload();

        // var obj = jQuery.parseJSON(response);
        //
        // this.removeFile(this.files[0]);
        //
        // $('#myTable tr:last').after(
        //     '<tr>' +
        //     '<td>' + obj.data_uploaded.date + '</td>' +
        //     '<td>' + obj.data_uploaded.file_name + '</td>' +
        //     '<td>' + obj.data_uploaded.user + '</td>' +
        //     '<td>' +
        //     '<a href="' + obj.data_uploaded.url + '" class="btn tooltips btn-xs btn-primary">' +
        //     '<i class="fa fa-download"></i>' +
        //     '</a>' +
        //     '</td>' +
        //     '</tr>'
        // );
        //
        // if (obj.status === 200) {
        //     swal({
        //         title: obj.title,
        //         text: obj.message,
        //         type: 'success',
        //         allowOutsideClick: true,
        //         confirmButtonClass: 'btn-success'
        //     );}
        //     $('#loading').hide();
        //     return;
        // }
        //
        // runAlertError(Sucesso, obj.message);

    });

    $("#send-import").on("click", function() {
        myDropzone.processQueue()
    });

})