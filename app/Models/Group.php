<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'name',
        'description'
    ];

    public function scopeList($query, array $search = null)
    {
        if (isset($search['name']) && !empty($search['name'])) {

            $name = mb_strtoupper($search['name'], 'UTF-8');

            $query->where(DB::raw('UPPER(groups.name)'), 'like', "%{$name}%")
                ->orWhere(DB::raw('UPPER(groups.description)'), 'like', "%{$name}%");

        }

        return $query;
    }

    // Selecionar todos grupos que o usuário pertence
    public function scopeByUsers($query, $users){

        $user_ids = [];

        if ($users->count()) {

            foreach ($users as $user) {
                $user_ids[] = $user->id;
            }

        }

        return $query->select('user_group.user_id', 'groups.description')

            ->join('user_group', function ($join) use ($user_ids) {
                $join->on('groups.id', '=', 'user_group.group_id')
                     ->whereIn('user_group.user_id', $user_ids);
            });

    }


    public function scopeRemoveAdmin($query){

        return $query->where('groups.name', '<>', config('sys.admin_user'));
    }

    public function scopeByName($query, $name)
    {
        return $query->where('groups.name', '=', $name);
    }

    public function scopeByCompany($query, $company_id)
    {
    	return $query->where('groups.company_id', '=', $company_id);
    }


    public function scopeOrdered($query)
    {
        return $query->orderBy('groups.description', 'ASC');
    }

    // -- ----------------------------------------------------------------
    // -- CONTROLE DE PERMISSÕES
    // -- ----------------------------------------------------------------

    public function permissions(){
        return $this->belongsToMany(Permission::class, 'group_permission', 'group_id', 'permission_id');
    }

    public function users(){
        return $this->belongsToMany(User::class, 'user_group', 'group_id', 'user_id');
    }

    public function solverGroupsGroup(){
        return $this->belongsToMany( SolverGroups::class, 'solver_groups_group','group_id','solver_group_id')->withPivot('is_internal_show','is_external_show','is_create');
    }

    public function isAdmin()
    {
        return ($this->is_admin == 1);
    }

    public function addPermission($permission)
    {
        if (is_string($permission)) {
            return $this->permissions()->save(
                Permission::where('name', '=', $permission)->firstOrFail()
            );
        }

        return $this->permissions()->save(
            Permission::findOrFail($permission->id)
        );
    }

    public function hasPermission($permission)
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }

        if (is_array($permission)) {
            foreach($permission as $r) {
                if ($this->permissions->contains('name', $r)) {
                    return true;
                }
            }
            return false;
        }

        return $permission->intersect($this->permissions)->count();
    }

    public function revokePermission(Permission $permission)
    {
        return $this->permissions()->detach($permission);
    }

    public function scopeNotIsAdmin($query)
    {
        return $query->where('groups.is_admin', '=', 0);
    }
}
