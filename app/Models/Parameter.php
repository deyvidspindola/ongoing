<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'company_id',

        'auth_ad',
        'simultaneous_access',

        'mail_host',
        'mail_port',
        'mail_from',
        'mail_from_name',
        'mail_username',
        'mail_password',

        'zenvia_conta',
        'zenvia_senha',
        'zenvia_grupo_id',

        'campanha_active_job',
        'campanha_active_mail',
        'campanha_active_sms',

        'campanha_job_path',

        'campanha_mail_subject',
        'campanha_mail_message',

        'campanha_sms_subject',
        'campanha_sms_message',

        'link_mail_subject',
        'link_mail_message',

        'link_sms_subject',
        'link_sms_message',

        'cielo_environment',
        'cielo_softDescripton',
        'cielo_maxInstallments',
        'cielo_homologacao_endpoint',
        'cielo_homologacao_merchantId',
        'cielo_homologacao_merchantKey',
        'cielo_producao_endpoint',
        'cielo_producao_merchantId',
        'cielo_producao_merchantKey',

    ];

    public function scopeByCompany($query, $company_id)
    {
        return $query->where('company_id', '=', $company_id);
    }
}
