<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OffenderSynchronism extends Model
{
	use SoftDeletes;

	protected $fillable = ['offender_id', 'ppm', 'type', 'possible_treatment', 'effectively_treated', 'treated_percentage', 'register_date'];

	public function scopeByType($query, $type)
	{
		return $query->where('type', $type);
	}

	//filtra os ofensores
	public function scopeByOffenderID($query, $offender_id)
	{
		return $query->whereOffenderId($offender_id);
	}

	public function scopeByPeriod($query, $date__ini, $date_end)
	{
		$period = [
			Carbon::createFromFormat('d/m/Y', $date__ini)->format('y-m-d'),
			Carbon::createFromFormat('d/m/Y', $date_end)->format('y-m-d')
			];
		return $query->whereBetween('register_date', $period);
	}


	public function offender()
	{
		return $this->belongsTo(Offender::class);
	}

	public function getRegisterDateAttribute($value)
	{
		return date('d/m/Y', strtotime($value));
	}

	public function getCreatedAtAttribute($value)
	{
		return date('d/m/Y', strtotime($value));
	}

	public function getUpdatedAtAttribute($value)
	{
		return date('d/m/Y', strtotime($value));
	}


}
