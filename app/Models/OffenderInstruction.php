<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OffenderInstruction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'filename',
        'file',
        'instruction_id',
        'offender_id',
        'user_id',
        'is_three_days_email_sent',
        'is_five_days_email_sent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_three_days_email_sent' => 'boolean',
        'is_five_days_email_sent' => 'boolean',
    ];

    /**
     * Get the user that owns the phone.
     */
    public function offender()
    {
        return $this->belongsTo(Offender::class);
    }
}
