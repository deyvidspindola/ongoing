<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupPermission extends Model
{
    protected $table = 'group_permission';

    public $timestamps = false;

    protected $fillable = [
        'group_id',
        'permission_id',
    ];
}
