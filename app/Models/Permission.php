<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = false;

    public function groups(){
        return $this->belongsToMany(Group::class, 'group_permission', 'permission_id', 'group_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('description');
    }

}
