<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'ads';

    public $timestamps = true;

    protected $fillable = [
        'company_id',
        'connection',
        'username',
        'password',
        'domain',
        'port',
        'dn_users',
        'dn_groups',
        'fields',
    ];

    public function scopeByCompany($query, $company_id)
    {
        return $query->where('company_id', '=', $company_id);
    }

}
