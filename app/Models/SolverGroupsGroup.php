<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SolverGroupsGroup extends Model
{
    protected $table = 'solver_groups_group';

    public $timestamps = true;

    protected $fillable = [
        'group_id',
        'solver_group_id',
        'is_internal_show',
        'is_external_show',
        'is_create'
    ];
}
