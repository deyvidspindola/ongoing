<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offender extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'offender_id',
        'offender',
        'type',
        'status',
        'origin',
        'retention_level',
        'available',
        'detailed_explanation_offender',
        'answer_for_the_user_in_the_call',
        'preventive_offender',
        'project_pack',
        'classification',
        'predict_definitive',
        'num_patch',
        'patch_identification',
        'unit_c',
        'ocurrence_id',
        'procedure_id',
        'observation',
        'instruction_id',
        'instruction_file',
        'ppm_id',
        'event_id',
        'proceeding_id',
        'scenario_validation',
        'description_and_procedure_id',
        'action_solution_targeting_for_responsible_team',
        'exception',
        'symptom',
        'fact',
        'cause',
        'action',
        'system_legacy',
        'group_identification',
        'solver_group_id',
        'ppm',
        'created_at',
        'sheet',
        'user_id',
    ];

    private function solverGroups($id = false)
    {
        $groups = auth()->user()->groups->pluck('id')->toArray();

        $userGroups = SolverGroups::whereHas('groups', function ($query) use ($groups,$id) {
            if($id) {
                $query->where('solver_groups.id', $id);
            } else {
                $query->whereIn('groups.id', $groups);
            }
        })
            ->pluck('name', 'id')
            ->toArray();

        foreach ($userGroups as $key => $group) {

            $solverGrups[$key] = SolverGroupsGroup::select(['is_internal_show', 'is_external_show'])
                ->whereIn('group_id',$groups)
                ->whereSolverGroupId($key)
                ->first()
                ->toArray();
        }

        return $solverGrups;
    }

    // CONDIÇÕES

    //filtro por tipo e grupo resolvedor
    public function scopeBySolverType($query, array $filter) {

        $sql = '';
        if($filter['solver'] == 'Todos') {
            $solvers = $this->solverGroups();

            foreach ($solvers as $key => $solver) {
                $type = '';
                switch ($filter['type']) {
                    case 'Interno':
                        $type .= ($solver['is_internal_show'] == 'S') ? "'Interno'," : "'',";
                        break;
                    case 'Externo':
                        $type .= ($solver['is_external_show'] == 'S') ? "'Externo'," : "'',";
                        break;
                    default:
                        $type .= ($solver['is_internal_show'] == 'S') ? "'Interno'," : "'',";
                        $type .= ($solver['is_external_show'] == 'S') ? "'Externo'," : "'',";
                        break;
                }
                $type = substr($type,0,-1);
                $sql .= "(solver_group_id = $key AND type IN ($type)) OR ";
            }
            $sql = '('.substr($sql,0,-4).')';
        } else {
            $solvers = $this->solverGroups($filter['solver']);
            foreach ($solvers as $key => $solver) {
                $type = '';
                switch ($filter['type']) {
                    case 'Interno':
                        $type .= ($solver['is_internal_show'] == 'S') ? "'Interno'," : "'',";
                        break;
                    case 'Externo':
                        $type .= ($solver['is_external_show'] == 'S') ? "'Externo'," : "'',";
                        break;
                    default:
                        $type .= ($solver['is_internal_show'] == 'S') ? "'Interno'," : "'',";
                        $type .= ($solver['is_external_show'] == 'S') ? "'Externo'," : "'',";
                        break;
                }
            }
            $type = substr($type,0,-1);
            $sql .= "solver_group_id = {$filter['solver']} AND type IN ($type)";
        }

        return $query->whereRaw($sql);
    }

    //filtra os tipos de ofensores
    public function scopeByType($query, $type)
    {
        return $query->where('type', 'like', "%{$type}%");
    }

    public function scopeByStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    //filtra os ofensores
    public function scopeByOffenderID($query, $offender_id){
        return $query->whereOffenderId($offender_id);
    }

    //filtra os macros
    public function scopeByMacroOffenderID($query, $macro){
        return $query->whereHas('offenderMacroOffender', function ($q) use ($macro) {
            $q->where('macro_offender_id', '=', "{$macro}");
        });
    }

    public function scopeByRetentionLevel($query, $nvl){
        return $query->where('retention_level', 'like', "%{$nvl}%");
    }

    public function scopeByClassification($query, $classificacao){
        return $query->where('classification', 'like', "%{$classificacao}%");
    }

    public function scopeBySolverGroup($query, $group){
        return $query->where('solver_group_id', $group);
    }

    public function scopeByPeriod($query, $date__ini, $date_end)
    {
        $period = [
            Carbon::createFromFormat('d/m/Y', $date__ini)->format('y-m-d'),
            Carbon::createFromFormat('d/m/Y', $date_end)->format('y-m-d')
        ];
        return $query->whereBetween(\DB::raw('DATE(created_at)'), $period);
    }

    // ORDENAÇÕES

    public function scopeOrdered($query)
    {
        return $query->orderBy('offender', 'DESC');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getValidAttribute($value)
    {
        return !is_null($value) ? (int) $value : $value;
    }


    /**
     * Crie aqui os relacionamentos com os ofensores
     * Obg.: Sempre que um novo relacionamento for criado, e houver impeditivo para que se possa deletar o ofensor, deve ser
     * incluido dentro do array de relacionamentos no config(ongoing.offender_relationships)
     */

    public function instructions()
    {
        return $this->hasOne(OffenderInstruction::class);
    }

    public function synchronism()
    {
        return $this->hasMany(OffenderSynchronism::class);
    }

    public function offenderMacroOffender()
    {
        return $this->belongsTo(OffenderMacroOffender::class, 'id', 'offender_id');
    }

}
