<?php


namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Session extends Model
{
//    use SoftDeletes;

    /**
     * Opcional, informar a coluna deleted_at como um Mutator de data
     *
     * @var array
     */
    public $timestamps = true;
    protected $table = 'sessions';
//    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'ip_address',
        'user_agent',
        'payload',
        'last_activity',
        'created_at',
        'updated_at'
    ];

//    public function user()
//    {
//        return $this->belongsTo('Cartalyst\Sentinel\Users\EloquentUser');
//    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeUpdateCurrent(Builder $query)
    {
        $user = Sentinel::check();

        return $query->where('id', Session::getId())->update([
            'user_id' => $user ? $user->id : null
        ]);
    }

    public function getLastActivityAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->format('d/m/Y H:i:s');
    }
}