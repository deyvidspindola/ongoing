<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'modelo',
        'marca',
        'preco',
        'ano',
        'placa',
    ];


    // FILTRO LISTAGEM

    public function scopeFilter($query, $request)
    {

        if($request->filter){

            $query->byOfensor($request->filter);

            $query->orWhere('id', 'like', "%{$request->filter}%");

        }

        return $query;
    }


    // CONDIÇÕES

    public function scopeByAno($query, $ano)
    {
        return $query->where('ano', '=', $ano);
    }

    public function scopeByMarca($query, $marca)
    {
        return $query->where('marca', 'like', "%{$marca}%");
    }

    public function scopeByModelo($query, $modelo)
    {
        return $query->Where('modelo', 'like', "%{$modelo}%");
    }


    // ORDENAÇÕES

    public function scopeOrdered($query)
    {
        return $query->orderBy('id', 'DESC');
    }



}
