<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'company_id',
        'store_id',
        'selected_store',
        'amount',
        'name',
        'username',
        'description',
        'email',
        'password',
        'password_cache',
        'locale',
        'type',
        'phone',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeList($query, array $search = null)
    {
        $query->select('users.id', 'users.name', 'users.username', 'users.email', 'users.phone');

        if (isset($search['name']) && !empty($search['name'])) {
            $query->byName($search['name']);
        }
        return $query;
    }

    public function scopeListOperador($query, array $search = null)
    {
        $query->select('users.id', 'users.name', 'users.username', 'users.email');

        if (isset($search['name']) && !empty($search['name'])) {
            $query->byName($search['name']);
        }
        if (isset($search['store_id']) && !empty($search['store_id'])) {
            $query->byStores($search['store_id']);
        }
        return $query;
    }

    public function scopeListSaldo($query, $q)
    {
        $query->select('name', 'username', 'email', 'phone', 'amount');

        if($q) {

            $q = mb_strtoupper($q, 'UTF-8');

            $query->where(DB::raw('UPPER(users.name)'), 'like', "%{$q}%");

            $query->orWhere(DB::raw('UPPER(users.username)'), 'like', "%{$q}%");

            $query->orWhere(DB::raw('UPPER(users.email)'), 'like', "%{$q}%");

            $query->orWhere('users.phone', 'like', "%{$q}%");

        }

        return $query;
    }



    public function scopeRemoveAdmin($query){

        return $query->where('users.username', '<>', config('sys.parameters.admin_user'));

    }

    public function scopeByName($query, $name)
    {
        $name = mb_strtoupper($name, 'UTF-8');

        return $query->where(DB::raw('UPPER(users.name)'), 'like', "%{$name}%")
                   ->orWhere(DB::raw('UPPER(users.username)'), 'like', "%{$name}%");
    }

    public function scopeByUsername($query, $username)
    {
        $username = mb_strtoupper($username, 'UTF-8');

        return $query->where(DB::raw('UPPER(users.username)'), '=', $username);

    }

    public function scopeById($query, $user_id)
    {
        return $query->where('users.id', '=', $user_id);
    }

    public function scopeByIds($query, $ids = [])
    {
        return $query->whereIn('users.id', $ids);
    }


    public function scopeByStore($query, $id)
    {
        return $query->where('users.store_id', '=', $id);
    }

    public function scopeByStores($query, $id)
    {

        return $query->join('user_store', 'user_store.user_id', '=', 'users.id')
                     ->where('user_store.store_id', '=', $id);
    }

    public function scopeByCompany($query, $company_id)
    {

    	return $query->where('users.company_id', '=', $company_id);

    }

    public function scopeByCpf($query, $cpf){

        return $query->where('users.username', '=', $cpf);

    }

    public function scopeByCampanhaName($query, $campanha){

        return $query->where('users.username', '=', $campanha);

    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('users.name');
    }

    // -- ----------------------------------------------------------------
    // -- TIPOS DE PERFIS
    // -- ----------------------------------------------------------------

    public function scopeByAdmin($query)
    {
        return $query->where('users.type', '=', config('sys.user.admin'));
    }

    public function scopeByOperador($query)
    {
        return $query->where('users.type', '=', config('sys.user.operador'));
    }

    public function scopeByCliente($query)
    {
        return $query->where('users.type', '=', config('sys.user.cliente'));
    }

    public function scopeByCampanha($query)
    {
        return $query->where('users.type', '=', config('sys.user.campanha'));
    }

    // -- ----------------------------------------------------------------
    // -- CONTROLE DE PERMISSÕES
    // -- ----------------------------------------------------------------

    public function groups(){
        return $this->belongsToMany(Group::class, 'user_group', 'user_id', 'group_id');
    }

    public function stores(){
        return $this->belongsToMany(Store::class, 'user_store', 'user_id', 'store_id');
    }

    public function Session()
    {
        return $this->hasOne(Session::class);
    }

    public function Company()
    {
        return $this->belongsTo(Company::class);
    }
    
    public function addGroup($group)
    {
        if (is_string($group)) {
            return $this->groups()->save(
                Group::where('name', '=', $group)->firstOrFail()
            );
        }

        return $this->groups()->save(
            Group::findOrFail($group->id)
        );
    }

    public function revokeGroup(Group $group)
    {
        return $this->groups()->detach($group);
    }

    public function hasGroup($group)
    {
        if (is_string($group)) {
            return $this->groups->contains('name', $group);
        }

        if (is_array($group)) {
            foreach($group as $r) {
                if ($this->groups->contains('name', $r)) {
                    return true;
                }
            }
            return false;
        }

        return $group->intersect($this->groups)->count();
    }

    public function hasAdmin()
    {
        return $this->groups->contains('is_admin', 1);
    }

    public function isAdmin()
    {
        return $this->hasAdmin();
    }



}
