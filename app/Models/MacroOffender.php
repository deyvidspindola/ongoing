<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MacroOffender extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'id',
        'macro_offender_id',
        'macro_offender',
    ];

    // FILTRO LISTAGEM

    public function scopeFilter($query, $request)
    {

        if($request->filter){

            $query->byMacroOffender($request->macro);
            $query->byMacroOffenderId($request->id_macro_offender);

        }

        return $query;
    }

    // CONDIÇÕES
    public function scopeByMacroOffenderId($query, $id_macro_offender){
        if ($id_macro_offender != '') {
            return $query->where('id', 'like', "%{$id_macro_offender}%");
        }
    }
    public function scopeByMacroOffender($query, $macro, $type){
        if ($macro != '' && $type != 'Interno') {
            return $query->where('macro_offender', 'like', "%{$macro}%");
        }
    }


	public function offenderMacroOffender()
	{
		return $this->belongsToMany(OffenderMacroOffender::class, 'offender_macro_offenders', 'id', 'macro_offender_id');
	}


    // ORDENAÇÕES

    public function scopeOrdered($query)
    {
        return $query->orderBy('macro', 'DESC');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
}
