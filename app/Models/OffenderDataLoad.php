<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OffenderDataLoad extends Model
{
    public $timestamps = true;

    protected $table = 'offenders_data_load';

    protected $fillable = ['filename', 'user_id', 'log_filename'];

    public function getCreatedAtAttribute($createdDate)
    {
        return date('d/m/Y H:i:s', strtotime($createdDate));
    }

    public function getUpdatedAtAttribute($updatedDate)
    {
        return date('d/m/Y H:i:s', strtotime($updatedDate));
    }

    public function scopeFilter($query, $request)
    {
        return $query;
    }


    // ORDENAÇÕES
    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }


	public function user()
	{
		return $this->belongsTo(User::class);
    }

}
