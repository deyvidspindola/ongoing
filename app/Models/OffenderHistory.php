<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OffenderHistory extends Model
{

    protected $fillable = [
    	'snapshot',
	    'modified_fields',
	    'user_id',
	    'offender_id',
	    'operation_type',
	    'observation2',
	    'created_at',
	    'sheet',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'snapshot' => 'array',
    ];

    // CONDIÇÕES
    public function scopeByOffenderId($query, $id_offender_filter){
	    return $query->whereHas('offender', function ($q) use ($id_offender_filter) {
		    $q->where('offender_id', '=', "{$id_offender_filter}");
	    });
    }

    public function scopeByType($query, $operation_type_filter){
    	return $query->whereRaw("json_extract(snapshot, '$.type') like '%{$operation_type_filter}%'");
    }

	public function scopeByPeriod($query, $date_ini, $date_end)
	{
		$period = [
			Carbon::createFromFormat('d/m/Y', $date_ini)->format('y-m-d'),
			Carbon::createFromFormat('d/m/Y', $date_end)->format('y-m-d')
		];
		return $query->whereBetween(\DB::raw('DATE(created_at)'), $period);
	}


	public function scopeByFilterExtractExcel($query, $filters)
	{
		if ($filters['type_filter'] != 'Todos' && $filters['type_filter'] != ''){
			$query->byType($filters['type_filter']);
		}

		if ($filters['id_offender_filter'] != ''){
			$query->byOffenderId($filters['id_offender_filter']);
		}

		if ($filters['date_ini_filter'] != '' && $filters['date_end_filter'] != ''){
			$query->byPeriod($filters['date_ini_filter'], $filters['date_end_filter']);
		}
	}

    // ORDENAÇÕES
    public function scopeOrdered($query)
    {
        return $query->orderBy('offender_id', 'DESC');
    }

    //Relationships
    public function user()
    {
        return $this->belongsTo(User::class);
    }

	public function offender()
	{
		return $this->belongsTo(Offender::class)->withTrashed();
    }

    public function getCreatedAtAttribute($createdDate)
    {
        return date('d/m/Y', strtotime($createdDate));
    }
    public function getUpdatedAtAttribute($updatedDate)
    {
        return date('d/m/Y', strtotime($updatedDate));
    }
}
