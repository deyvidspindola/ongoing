<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SolverGroups extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'id',
        'name',
    ];


    // FILTRO LISTAGEM

    public function scopeFilter($query, $request)
    {

        if($request->filter){

            $query->byName($request->filter);

            $query->orWhere('id', 'like', "%{$request->filter}%");

        }

        return $query;
    }


    // CONDIÇÕES

    public function scopeByName($query, $name)
    {
        return $query->where('name', '=', $name);
    }


    // ORDENAÇÕES

    public function scopeOrdered($query)
    {
        return $query->orderBy('id', 'DESC');
    }


    public function groups(){
        return $this->belongsToMany( 'App\Models\Group', 'solver_groups_group',  'solver_group_id');
    }

}
