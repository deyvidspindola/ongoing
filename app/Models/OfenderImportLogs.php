<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfenderImportLogs extends Model
{

	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = ['id', 'ofensores', 'ofensores_desativados', 'descricao_antiga', 'ofensores_direcionados', 'of_direcionados_desativados'];

	protected $casts = [
		'ofensores' => 'array',
		'ofensores_desativados' => 'array',
		'descricao_antiga' => 'array',
		'ofensores_direcionados' => 'array',
		'of_direcionados_desativados' => 'array',
	];

}
