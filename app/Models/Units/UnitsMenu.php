<?php

namespace App\Models\Units;

use Illuminate\Database\Eloquent\Model;

class UnitsMenu extends Model
{

	protected $fillable = [
		'title',
		'sub_title',
		'slug',
		'permission'
	];

    public function scopeBySlug($query, $slug)
    {
        return $query->where('slug', 'like', '%'.$slug);

    }

}
