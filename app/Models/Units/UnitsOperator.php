<?php



namespace App\Models\Units;

use App\Http\Traits\CompanyTrait;
use Illuminate\Database\Eloquent\Model;

class UnitsOperator extends Model
{
	use CompanyTrait;

    protected $table = 'units_operator';
    protected $primaryKey = 'id_operadora_telefonia';
}