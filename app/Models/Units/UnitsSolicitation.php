<?php

namespace App\Models\Units;

use App\Http\Traits\CompanyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class UnitsSolicitation extends Model
{

	use CompanyTrait;

    protected $table = 'units_solicitations';

    public $timestamps = true;

    protected $fillable = [
        'username',
        'remote_ip',
        'units_service_id',
        'units_operation_id',
        'contract_number',
        'terminal',
        'node',
        'pa',
        'status',
        'process_status',
        'process_message',
        'proposal_number',
        'cnpj',
        'ticket',
        'softx',
        'tags',
        'date_time',
	    'company_id'
    ];

    public function scopeByStatus($query, $status)
    {
        return $query->whereStatus($status);
    }

    public function scopeByPeriod($query, $date_ini, $date_end)
    {
        $period = [
            Carbon::createFromFormat('d/m/Y', $date_ini)->format('y-m-d'),
            Carbon::createFromFormat('d/m/Y', $date_end)->format('y-m-d')
        ];
        return $query->whereBetween(\DB::raw('DATE(created_at)'), $period);
    }

    public function scopeByUsername($query, $username)
    {
        return $query->where('username', $username);
    }

    public function scopeByService($query, $service_id)
    {
        return $query->where('units_service_id', $service_id);
    }

    public function scopeByTerminal($query, $terminal)
    {
        return $query->where('terminal', $terminal);
    }
    public function service()
    {
        return $this->belongsTo(UnitsService::class, 'units_service_id', 'id');
    }
}