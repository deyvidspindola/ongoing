<?php

namespace App\Models\Units;

use App\Http\Traits\CompanyTrait;
use Illuminate\Database\Eloquent\Model;

class UnitsService extends Model
{

	use CompanyTrait;

    protected $table = 'units_services';

    public $timestamps = true;

    protected $fillable = [
        'description',
        'description_short',
        'description_detailed',
        'permission_name',
        'permission',
        'script_file',
        'online_process',
        'menu',
        'additional_fields',
        'flag_fields',
        'send_ebt_service',
        'units_group_id',
        'units_ebt_service_id',
        'status',
        'order'
    ];

	public function scopeByPermissionName($query, $value)
	{
		return $query->wherePermissionName($value);
    }

	public function scopeByMenu($query, $value)
	{
		return $query->whereMenu($value);
    }

	public function scopeBySlug($query, $value)
	{
		return $query->whereSlug($value);
	}


}