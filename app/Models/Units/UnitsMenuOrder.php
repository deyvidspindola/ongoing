<?php

namespace App\Models\Units;

use App\Http\Traits\CompanyTrait;
use Illuminate\Database\Eloquent\Model;

class UnitsMenuOrder extends Model
{

	use CompanyTrait;

    protected $fillable = ['menu_order', 'company_id'];

}
