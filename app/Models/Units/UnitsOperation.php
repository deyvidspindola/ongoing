<?php

namespace App\Models\Units;

use App\Http\Traits\CompanyTrait;
use Illuminate\Database\Eloquent\Model;

class UnitsOperation extends Model
{

	use CompanyTrait;

    protected $table = 'units_operations';

    public $timestamps = true;

    protected $fillable = [
        'base_name',
        'base_code',
        'city_contract',
        'city_name',
        'uf',
	    'company_id'
    ];

    // Ordenar pelo nome da cidade
    public function scopeOrderedByCity($query)
    {
        return $query->orderBy('city_name', 'ASC')->orderBy('uf', 'ASC');
    }

    // Selecionar operadoras diferente da CTV [21]
    public function scopeBaseNotCtv($query){
        return $query->where('base_code', '<>', '21');
    }
}