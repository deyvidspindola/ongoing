<?php

namespace App\Models\Units;

use Illuminate\Database\Eloquent\Model;

class UnitsGroup extends Model
{
    protected $table = 'units_groups';

    public $timestamps = true;

    protected $fillable = [
        'description',
        'link'
    ];

    public function scopeById($query, $id)
    {
        return $query->where('id', '=', $id);
    }
}