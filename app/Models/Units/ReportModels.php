<?php


namespace App\Models\Units;


class ReportModels
{
    const VOD_TYPES = [
        'SVOD_NET' => 'SVOD NET',
        'TVOD_NET' => 'TVOD NET',
        'NW_SVOD' => 'NOWONLINE SVOD NET',
        'NW_NET' => 'NOWONLINE TVOD NET',
        'TVOD_CLARO' => 'TVOD CLARO',
    ];

    const STATUS = [
        'OK' => 'Processado OK',
        'NOK' => 'Processamento Pendente',
    ];
}