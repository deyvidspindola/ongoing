<?php

namespace App\Models\Units;

use Illuminate\Database\Eloquent\Model;

class UnitsEbtService extends Model
{
    protected $table = 'units_ebt_services';

    public $timestamps = true;

    protected $fillable = [ 
        'service_host',
        'service_port',
        'service_username',
        'service_password',
        'service_endpoint'
    ];
}