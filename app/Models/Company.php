<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    protected $table = 'companies';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'domain'
    ];

    public function scopeList($query, array $search = null){

        // Se não for admin listo somente a empresa da session
        if (!Auth::user()->isAdmin()) {
            $query->byId(session('company')['id']);
        }

        // Se esta filtrando por nome
        if (isset($search['name']) && !empty($search['name'])) {
            $query->byName($search['name']);
        }

        return $query;

    }


    public function scopeByName($query, $name)
    {
        $name = mb_strtoupper($name, 'UTF-8');

        return $query->where(DB::raw('UPPER(companies.name)'), 'like', "%{$name}%")
                     ->orWhere(DB::raw('UPPER(companies.description)'), 'like', "%{$name}%")
                     ->orWhere(DB::raw('UPPER(companies.domain)'), 'like', "%{$name}%");
    }


    public function scopeById($query, $id)
    {
        return $query->where('id', '=', $id);
    }

    public function scopeByDomain($query, $domain)
    {
        return $query->where('domain', '=', $domain);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('companies.description', 'asc');
    }

    public function User()
    {
        return $this->hasMany(User::class);
    }
}
