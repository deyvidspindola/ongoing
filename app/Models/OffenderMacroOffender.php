<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OffenderMacroOffender extends Model
{
	protected $table = 'offender_macro_offenders';
	public $timestamps = false;
    protected $fillable = ['offender_id', 'macro_offender_id'];

    public function offender()
    {
    	return $this->hasOne(Offender::class, 'id', 'offender_id');
    }

    public function macro_offender()
    {
    	return $this->hasMany(MacroOffender::class, 'id', 'macro_offender_id');
    }

}
