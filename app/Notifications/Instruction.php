<?php

namespace App\Notifications;

use App\Models\Offender;
use App\Models\OffenderInstruction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Str;

class Instruction extends Notification
{
    use Queueable;
	/**
	 * @var OffenderInstruction
	 */
	private $instruction;

	/**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($instruction)
    {

	    $this->instruction = $instruction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
    	$user = User::find($this->instruction->user_id);
	    $offender = Offender::find($this->instruction->offender_id);
	    $url = url('offender/'.$this->instruction->offender_id.'/edit');
        return (new MailMessage)
	            ->subject('Notificação Portal Ongoing')
		        ->greeting('Ola, '.$user->name.'!')
		        ->line('Identificamos que o ofensor "'.$offender->offender_id.' - '.Str::limit($offender->offender, 100).'", não tem uma instrução de trabalho anexada.')
		        ->line('Por favor, clique no botão abaixo para verificar e anexar a instrução conforme procedimento.')
		        ->action('Anexar uma instrução', $url);

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    	$offender = Offender::find($this->instruction->offender_id);

	    return [
		    'offender_id' => $this->instruction->offender_id,
		    'user_id' => $this->instruction->user_id,
		    'message' => 'Ofensor '.$offender->offender_id.' ainda não possui uma instrução de trabalho.',
	    ];
    }
}
