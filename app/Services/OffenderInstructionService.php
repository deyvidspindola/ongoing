<?php

namespace App\Services;

use App\Models\OffenderInstruction;
use Illuminate\Support\Facades\DB;

class OffenderInstructionService
{

    /**
     * @var OffenderInstruction $offenderInstruction
     */
    private $offenderInstruction;

    /**
     * OffenderInstructionService constructor.
     *
     * @param OffenderInstruction $offenderInstruction
     */
    public function __construct(OffenderInstruction $offenderInstruction)
    {
        $this->offenderInstruction = $offenderInstruction;
    }

    /**
     * Save a new offender instruction
     *
     * @param array $request
     *
     * @return bool
     * @throws \Exception
     */
    public function store(array $request): bool
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {
                $this->offenderInstruction->create($request);
            });

            return true;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Find an offender instruction by id
     * @param $id
     *
     * @return bool
     * @throws \Exception
     */
    public function find($id): bool
    {
        try{
            $offenderInstruction = $this->offenderInstruction->find($id);

            if (!$offenderInstruction ) {
                return false;
            }

            return true;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Update an Offender instruction
     *
     * @param array $request
     * @param       $id
     *
     * @return bool
     * @throws \Exception
     */
    public function update(array $request, $id): bool
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request, $id) {

                // Recuperar o registro
                $offenderInstruction = $this->offenderInstruction->find($id);

                // Caso o registro não exista
                if (!$offenderInstruction) {
                    throw new \Exception('Registro não encontrado.');
                }

                $offenderInstruction->update($request);
            });

            return true;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}