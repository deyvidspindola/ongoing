<?php

namespace App\Services;

use App\Models\MacroOffender;
use Illuminate\Support\Facades\DB;

class MacroOffenderService
{

    /**
     * @var MacroOffender
     */
    private $macroOffender;

    public function __construct(
        MacroOffender $macroOffender
    )
    {
        $this->macroOffender = $macroOffender;
    }

    public function store(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {
                $this->macroOffender->create($request);

            });

            return true;

        }
        catch (\Exception $e) {

            throw new \Exception($e->getMessage());

        }
    }

    public function find($id)
    {
        try{
            $macroOffender = $this->macroOffender->find($id);

            if( !$macroOffender ){
                return false;
            }

            return true;
        }
        catch (\Exception $e) {

            throw new \Exception($e->getMessage());

        }
    }

}