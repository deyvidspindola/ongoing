<?php

namespace App\Services;

use App\Models\SolverGroups;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitsDashboardService {

  /**
	 * @var SolverGroups
	 */
  private $solverGroups;

  /**
   * Construct function
   *
   * @param SolverGroups $solverGroups
   */
  public function __construct(SolverGroups $solverGroups)
  {
    $this->solverGroups = $solverGroups;
  }

    public function getUserList()
    {
        $logged = User::with(['session', 'company'])
            ->whereCompanyId(auth()->user()->company_id)
            ->whereExists(function ($query){

                $nowMinus5Minutes = Carbon::now()->subMinutes(5);
                $usuarioLogado = auth()->user()->username;

                $query->select(DB::raw(1))
                    ->from('sessions')
                    ->whereRaw('sessions.user_id = users.id')
                    ->whereRaw("(FROM_UNIXTIME(sessions.last_activity)  > '{$nowMinus5Minutes}')");
            })
            ->where('users.username', '<>', auth()->user()->username)
            ->get();

        return $logged;
    }


    public function usersSolverGroups()
	{
		$groups = auth()->user()->groups->pluck('id')->toArray();
		$userGroups = $this->solverGroups
			->whereHas('groups', function ($query) use ($groups) {
				$query->whereIn('groups.id', $groups);
			})
			->pluck('name', 'id')
			->toArray();
		return $userGroups;
	}

}