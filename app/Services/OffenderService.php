<?php

namespace App\Services;

use App\Http\Traits\OffenderHistoryTrait;
use App\Models\MacroOffender;
use App\Models\Offender;
use App\Models\OffenderHistory;
use App\Models\OffenderInstruction;
use App\Models\OffenderMacroOffender;
use App\Models\SolverGroups;
use App\Models\SolverGroupsGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use mysql_xdevapi\Exception;
use Yajra\Datatables\Datatables;

class OffenderService
{

	use OffenderHistoryTrait;

	/**
	 * @var Offender
	 */
	private $offender;
	/**
	 * @var MacroOffender
	 */
	private $macroOffender;
	/**
	 * @var OffenderMacroOffender
	 */
	private $offenderMacroOffender;
	/**
	 * @var OffenderHistory
	 */
	private $offenderHistory;
	/**
	 * @var SolverGroups
	 */
	private $solverGroups;
	/**
	 * @var OffenderInstruction
	 */
	private $offenderInstruction;

	public function __construct(
			Offender $offender,
			MacroOffender $macroOffender,
			OffenderMacroOffender $offenderMacroOffender,
			OffenderHistory $offenderHistory,
			SolverGroups $solverGroups,
			OffenderInstruction $offenderInstruction,
            SolverGroupsGroup $solverGroupsGroup
	)
	{
		$this->offender = $offender;
		$this->macroOffender = $macroOffender;
		$this->offenderMacroOffender = $offenderMacroOffender;
		$this->offenderHistory = $offenderHistory;
		$this->solverGroups = $solverGroups;
		$this->offenderInstruction = $offenderInstruction;
        $this->solverGroupsGroupGroup = $solverGroupsGroup;
	}

	/**
	 * @description Cria o relacionamento de um ofensor com um macro ofensor.
	 * @param $macroId
	 * @param $offender_id
	 */
	private function createOffenderMacroOffender($macroId, $offender_id)
	{
		// verifica se existe o relacionamento do ofensor com o macro ofensor
		$relationships = $this->offenderMacroOffender->whereOffenderId($offender_id)->first();

		if (!is_null($relationships)) {
			// deleta o relacionamento
			$this->offenderMacroOffender->whereOffenderId($offender_id)->delete();
		}

		if(!is_null($macroId) && $macroId > 0){
			// cria um novo relacionamento do ofensor com o macro ofensor
			$this->offenderMacroOffender->create([
				'offender_id' => $offender_id,
				'macro_offender_id' => $macroId
			]);
		}
	}

	/**
	 * @description Cria um novo macro ofensor, atravez do formulário ou retorna o id de um já existente.
	 * @param $request do formulário
	 * @return id do macro ofensor criado ou já existente
	 */
	private function createMacroOffender($request)
	{
		if ($request['type'] != 'Interno') {
			//verifica se foi informado um id no formulário. se não for informado um id é o o macro ofensor é um registro novo.
			if (is_null($request['macro_offender_id']) && !is_null($request['macro_offender'])) {
				//verifica se já existe um macro ofensor com o mesmo nome, se tiver usa o id dele.
				$hasMacroName = $this->macroOffender->where('macro_offender', 'like', "%{$request['macro_offender']}%")->first();
				if (empty($hasMacroName)){
					//caso não tenha o macro ofensor na tabela, recupera o ultimo id para incrementa-lo em um novo.
					$lastMacroId = $this->macroOffender->orderby('macro_offender_id', 'desc')->first();
					//cadastra o novo macro ofensor.
					return $this->macroOffender->create([
						'macro_offender_id' => ($lastMacroId['macro_offender_id'] + 1),
						'macro_offender' => $request['macro_offender']
					])->id;
				}else{
					return $hasMacroName['id'];
				}
			} else {
				return (!is_null($request['macro_offender_id']) && !is_null($request['macro_offender'])) ? $request['macro_offender_id'] : 0;
			}
		}

		return 0;
	}

	public function store($request)
	{
	    $hasOffender = $this->offender->whereOffenderId($request['offender_id'])->first();

		// verifica se o ofensor já existe, caso não exista cria um ofensor, se existir atualiza o mesmo.
		if (is_null($hasOffender)) {

			#Cria um novo Ofensor, via tela
			$newOffender = $request->all();
			$newOffender['sheet'] = 'ofensores';
			if ($newOffender['type'] == 'Interno'){
				$newOffender['retention_level'] = 'N1';
			}

            if ($newOffender['retention_level'] === 'N1' && $newOffender['type'] == 'Externo'){
                if(!Str::contains(Str::slug($newOffender['classification'],'_'), ['falha_de_processo', 'falha_na_abertura_do_chamado'])){
                    abort('500', 'Quando o nivel de retenção for N1 a classificação não pode ser diferente de "Falha de Processo" ou "Falha na abertura do chamado"');
                }
            }

            $newOffender['user_id'] = auth()->user()->id;

            if ($newOffender['solver_group_id'] == 4){
                $newOffender['offender_id'] = 'S'.$newOffender['offender_id'];
            }

			$offender = $this->offender->create($newOffender);

			//Gera os nomes para o arquivo
            $filename = $this->filename($request);
            $name = '';
            $file = null;

            //Verifica se existe arquivo e salva no servidor
            if($request->hasFile('instruction_file')){
                $file = $request->instruction_file->storeAs('docs_instructions', $filename['file_name']);
                $name = $filename['slug_name'];
            }
            $data = [
                'offender_id' => $offender->id,
                'filename' => $name,
                'file' => $file,
                'instruction_id' => $request['instructions']['instruction_id'],
                'user_id' => auth()->user()->id,
                'is_three_days_email_sent' => 1,
                'is_five_days_email_sent' => 1,
            ];
            $this->offenderInstruction->create($data);

			//cria um novo macro ofensor ou retorno o ID de um caso ele já exista
			$macroId = $this->createMacroOffender($request->all());

			#Cria um relacionamento de ofensores com macro ofensores
			$this->createOffenderMacroOffender($macroId, $offender->id);

		} else {
			$this->update($request, $hasOffender->id);
		}

	}

	public function update($request, $id)
	{
		$offender = $this->offender->find($id);

        $newOffender = $request->all();

        if ($newOffender['retention_level'] === 'N1' && $newOffender['type'] == 'Externo'){
            if(!Str::contains(Str::slug($newOffender['classification'],'_'), ['falha_de_processo', 'falha_na_abertura_do_chamado'])){
                abort('500', 'Quando o nivel de retenção for N1 a classificação não pode ser diferente de "Falha de Processo" ou "Falha na abertura do chamado"');
            }
        }

        $offender['macro_offender'] = $offender['macro_offender_id'] = null;

		if ($offender->type == 'Externo' && !is_null($offender->offenderMacroOffender))
            $offender['macro_offender'] = $offender['macro_offender_id'] = $offender->offenderMacroOffender->macro_offender_id;

        $history = $this->createHistory('E', $offender->id, $request->except(['_token', '_method']),$offender->sheet, $offender->toArray());

        if($history){
        	if ($request->type == 'Externo' && $request->status != 'Inativo'){
		        $history['sheet'] = 'descricao_antiga';
	        }else{
		        $history['sheet'] = 'ofensores_desativados';
	        }
            OffenderHistory::create($history);
        }
        unset($offender['macro_offender_id'], $offender['macro_offender']);


		if ($newOffender['type'] == 'Interno'){
			$newOffender['retention_level'] = 'N1';
        }

		$newOffender['type'] = $offender->type;
        if($request->status == 'Inativo' && $offender->status != 'Inativo' && $request->type == 'Externo'){
            $newOffender['sheet'] = 'ofensores_desativados';
            $newOffender['observation'] .= ' | OFENSOR DESATIVADO EM -'.Carbon::now()->format('d/m/Y');
        }elseif ($request->status == 'Ativo' && $offender->status != 'Ativo'){
            $newOffender['sheet'] = 'ofensores';
            $newOffender['observation'] .= ' | OFENSOR REATIVADO EM -'.Carbon::now()->format('d/m/Y');
        }
        $newOffender['user_id'] = auth()->user()->id;
        $update = $offender->update($newOffender);

		//Verifica se existe um arquivo
        $data = [
            'offender_id' => $offender->id,
            'user_id' => auth()->user()->id,
            'is_three_days_email_sent' => 1,
            'is_five_days_email_sent' => 1,
        ];

        if ($request->get('instructions') && !is_null($request['instructions']['instruction_id'])) {
            $data['instruction_id'] = $request['instructions']['instruction_id'];
        }

        if($request->hasFile('instruction_file')){
	        $filename = $this->filename($request);
	        $file = $request->instruction_file->storeAs('docs_instructions', $filename['file_name']);
	        $data['filename'] = $filename['file_name'];
	        $data['file'] = $file;
        }

        $hasInstruction = $this->offenderInstruction->whereOffenderId($id)->first();
        if (!empty($hasInstruction)){
            $hasInstruction->update($data);
        }else{
            $this->offenderInstruction->create($data);
        }

		//cria um novo macro ofensor ou retorno o ID de um caso ele já exista
		$macroId = $this->createMacroOffender($request->all());

		#Cria um relacionamento de ofensores com macro ofensores
		if ($update) {
			$this->createOffenderMacroOffender($macroId, $offender->id);
			return true;
		}

		return false;
	}

	public function destroy($id)
	{
		$cont=0;
		$offender = $this->offender->find($id);

		// verifica se existe algum relacionamento com o ofensor em questão
		foreach (config('ongoing.offender_relationships') as $relationship)
		{
			if ($offender->$relationship->count() > 0){
				$cont++;
			}
		}

		// verifica se existe algum relacionamento para o ofensor
		if ($cont == 0){
			if($offender->delete()){
				$hasInstruction = $this->offenderInstruction->whereOffenderId($id)->first();
				if (!empty($hasInstruction)){
				    $hasInstruction->delete();
			    }

				$history = $this->createHistory('R', $offender->offender_id, $offender, $offender->sheet);
				if($history){
					OffenderHistory::create($history);
				}

                return true;
            };
            return false;
		}else{
			return false;
		}
	}

	public function action ($request, $id)
    {
        $status = ($request->action == 'ativar') ? ['Ativo', 'ofensores_desativados'] : ['Inativo', 'ofensores'];

        try{

            $offender = $this->offender->find($id);
            $offender['status'] = $status[0];

            $newOffender = $offender->getAttributes();
            $oldOffender = $offender->getOriginal();

            if($request->action != 'ativar' && $offender->type == 'Externo'){
                $offender['sheet'] = 'ofensores_desativados';
                $offender['observation'] .= ' | OFENSOR DESATIVADO EM - '.Carbon::now()->format('d/m/Y');
            } elseif ($request->action == 'ativar') {
                $offender['sheet'] = 'ofensores';
                $offender['observation'] .= ' | OFENSOR REATIVADO EM - '.Carbon::now()->format('d/m/Y');
            } else {
                return \Exception('Opção inválida');
            }

            $offender->save();
            $history = $this->createHistory('E', $id, $newOffender, $status[1], $oldOffender);

            if($history){
                if ($offender['type'] == 'Externo' && $offender['status'] != 'Inativo'){
                    $history['sheet'] = 'descricao_antiga';
                }else{
                    $history['sheet'] = 'ofensores_desativados';
                }
                $history['operation_type'] = ($offender['status'] == 'Ativo') ? 'A' : 'N';
                OffenderHistory::create($history);
            }

            return true;

        } catch ( \Exception $e) {
            return $e->getMessage();
        }
        //return DB::table('offenders')->where('id',$id)->update(['status' => $status]);
	}

	public function usersSolverGroups($create = false)
	{
		$groups = auth()->user()->groups->pluck('id')->toArray();
		$userGroups = $this->solverGroups
			->whereHas('groups', function ($query) use ($groups, $create) {
				$query->whereIn('groups.id', $groups);
                if($create)
                    $query->where('solver_groups_group.is_create', 'S');
			})
			->pluck('name', 'id')
			->toArray();

		return $userGroups;
	}

	public function usersSolverGroupsGroup()
	{
		$groups = auth()->user()->groups->pluck('id')->toArray();
        $solverGroupsGroup = [
            'group_id' => SolverGroupsGroup::whereIN('group_id',$groups)->get()->pluck('group_id')->toArray(),
            'is_internal_show' => SolverGroupsGroup::whereIN('group_id',$groups)->get()->pluck('is_internal_show')->toArray(),
            'is_external_show' =>SolverGroupsGroup::whereIN('group_id',$groups)->get()->pluck('is_external_show')->toArray(),
            'is_create' => SolverGroupsGroup::whereIN('group_id',$groups)->get()->pluck('is_create')->toArray()
        ];

        return $solverGroupsGroup;
	}

	public function search($search)
	{
		header("Content-Type: application/json; charset=UTF-8");

    	if($search['acao'] == 'macro_offender') {
		    $returns = $this->macroOffender->selectRaw("concat(macro_offender_id, ' - ', macro_offender) as name, id")->distinct()->where($search['acao'], 'like', '%' . $search['query'] . '%')->orWhere('macro_offender_id', $search['query']);
	    }else if($search['acao'] == 'classification'){
		    $returns = $this->offender->select(''.$search['acao'].' as name')->distinct()->where($search['acao'], 'like' ,'%'.$search['query'].'%')->groupby('name');
		}else{
		    $returns = $this->offender->select(''.$search['acao'].' as name', 'offender_id as id')->where('sheet', '<>', 'descricao_antiga')->where($search['acao'], 'like' ,'%'.$search['query'].'%');
	    }
    	$returns = $returns->get()->toArray();
    	return json_encode($returns);
	}

	public function datatable(Request $request)
	{
		$offenders = $this->offender->with('offenderMacroOffender.macro_offender')
			->select(['id', 'offender_id', 'offender', 'retention_level', 'type', 'created_at', 'solver_group_id', 'status'])
			->where('sheet', '<>', 'descricao_antiga')
            ->whereNull('deleted_at');

		if ($request->has('status_filter') && $request->get('status_filter') != 'Todos') {
			$offenders->byStatus($request->get('status_filter'));
		}

		if ($request->has('id_macro_offender_filter') && $request->get('id_macro_offender_filter') != '') {
			$offenders->byMacroOffenderID($request->get('id_macro_offender_filter'));
		}

		if ($request->has('id_offender_filter') && $request->get('id_offender_filter') != '') {
			$offenders->byOffenderID($request->get('id_offender_filter'));
		}

		if ($request->has('retention_level_filter') && $request->get('retention_level_filter') != '') {
			$offenders->byRetentionLevel($request->get('retention_level_filter'));
		}

		if ($request->has('classification_filter') && $request->get('classification_filter') != '') {
			$offenders->byClassification($request->get('classification_filter'));
		}

        if ($request->has('date_ini_filter') && $request->get('date_ini_filter') != '' &&
            $request->has('date_end_filter') && $request->get('date_end_filter') != '') {
            $offenders->byPeriod($request->get('date_ini_filter'), $request->get('date_end_filter'));
        }

        $offenders->bySolverType([
            'solver' => $request->get('solver_group_filter'),
            'type' => $request->get('type_filter')
        ])->orderByRaw('offender_id*1');

		return Datatables::of($offenders)
			->addColumn('macro_offender', function ($offenders) {
				if (!is_null($offenders->offenderMacroOffender)){
					return $offenders->offenderMacroOffender->macro_offender[0]['macro_offender'];
				}else{
					return '';
				}
			})
			->addColumn('action', function ($offenders) {

			    $permissionActive = auth()->user()->can('offender_active');
				$html = "";

				//listar
				if(auth()->user()->can('offender_list')) {
					$html .= '<a class="btn tooltips btn-xs btn-success show-details" href="javascript:void(0);" data-offender_id="'.$offenders->id.'"> <i class="fa fa-search"></i> </a>';
				}

                if($offenders->status == 'Ativo') {
                    $button = 'btn-danger';
                    $icon = 'fa fa-close';
                } else {
                    $button = 'green-meadow';
                    $icon = 'fa fa-check-square';
                }

                $userGroups = $this->usersSolverGroups(true);
                if(array_key_exists($offenders->solver_group_id,$userGroups)) {
                    $html .= view('utils.buttons.edit')->with(['route' => route('offender.edit', $offenders->id)])->render();

                    if ($offenders->status == 'Ativo')
                        $html .= view('utils.buttons.action')->with(['route' => route('offender.action', $offenders->id), 'id' => $offenders->id, 'status' => $offenders->status, 'button' => $button, 'icon' => $icon])->render();
                }

                if ($offenders->status == 'Inativo' && $permissionActive) {
                    $html .= view('utils.buttons.action')->with(['route' => route('offender.action', $offenders->id), 'id' => $offenders->id, 'status' => $offenders->status, 'button' => $button, 'icon' => $icon])->render();
                }

                return $html;
			})
			->make();
	}

	private function filename($file)
    {
        $slug_name = '';
        $file_name = '';
        if ($file->hasfile('instruction_file')) {
            $extensions = ['.docx', '.doc', '.xlsx', '.xls', '.txt', '.pdf', '.pptx', '.ppt', '.zip', '.rar'];
            $filename = $file->instruction_file->getClientOriginalName();
            $filename = str_replace($extensions, '', $filename);
            $extension = $file->instruction_file->getClientOriginalExtension();
            $slug_name = $filename . '.' . $extension;
            $file_name = $filename . '_' . str_pad($file->id, 6,'0',0) . '.' . $extension;
        }

        return [
            'slug_name' => $slug_name,
            'file_name' => $file_name,
        ];
    }


}