<?php

namespace App\Services;

use App\Models\Auto;
use Illuminate\Support\Facades\DB;

class AutoService
{

    /**
     * @var Auto
     */
    private $auto;

    public function __construct(
        Auto $auto
    )
    {
        $this->auto = $auto;
    }

    public function store(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {


                $ano = $request['ano'];

                if($ano < 2019)

                    throw new \Exception('Ano Inválido, somente acima de 2019.');

                $this->auto->create($request);

            });

            return true;

        }
        catch (\Exception $e) {

            throw new \Exception($e->getMessage());

        }
    }

    public function update(array $request, $id)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request, $id) {

                // Recuperar o registro
                $auto = $this->auto->find($id);

                // Caso o registro não exista
                if(!$auto)

                    throw new \Exception('Registro não encontrado.');

                $hasPlaca = $this->auto->where('placa', '=', $request['placa'])->where('id', '!=', $id)->first();

                if($hasPlaca)

                    throw new \Exception('Placa já cadastrada.');

                $auto->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy($id)
    {

        try {

            $auto = $this->auto->find($id);

            // Verifico se o registro existe
            if(!$auto)

                throw new \Exception('Registro não encontrado');

            // Excluo o registro
            $auto->delete();

            return true;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}