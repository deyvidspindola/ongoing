<?php
/**
 * Created by PhpStorm.
 * User: deyvid
 * Date: 30/04/19
 * Time: 09:45
 */

namespace App\Services;


use App\Models\Offender;
use App\Models\OffenderSynchronism;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OffenderSynchronismService
{

	/**
	 * @var OffenderSynchronism
	 */
	private $synchronism;
	/**
	 * @var Offender
	 */
	private $offender;

	public function __construct(
		OffenderSynchronism $synchronism,
		Offender $offender
	)
	{
		$this->synchronism = $synchronism;
		$this->offender = $offender;
	}

	public function store($request)
	{

		$columns = ['type', 'offender_id', 'possible_treatment', 'effectively_treated', 'treated_percentage', 'register_date', 'ppm'];
		$data = [];

		for ($cont=0; $cont<count($request[$columns[0]]); $cont++){
			foreach ($columns as $column){
				$data[$cont][$column] = $request[$column][$cont];
				if ($column == 'register_date'){
					$data[$cont][$column] = Carbon::createFromFormat('d/m/Y', $request[$column][$cont])->format('Y-m-d');
				}
			}
		}

		foreach ($data as $row)
		{
			$this->synchronism->create($row);
		}

	}

	public function update($request, $id)
	{
		$columns = ['type', 'offender_id', 'possible_treatment', 'effectively_treated', 'treated_percentage', 'register_date', 'ppm'];
		$data = [];

		for ($cont=0; $cont<count($request[$columns[0]]); $cont++){
			foreach ($columns as $column){
				$data[$column] = $request[$column][$cont];
				if ($column == 'register_date'){
					$data[$column] = Carbon::createFromFormat('d/m/Y', $request[$column][$cont])->format('Y-m-d');
				}
			}
		}

		$this->synchronism->find($id)->update($data);
	}

	public function destroy($id)
	{
		if($this->synchronism->find($id)->delete()){
			return true;
		}
		return false;
	}

	public function search($data)
	{
		header("Content-Type: application/json; charset=UTF-8");
		$returns = $this->offender
			->selectRaw("concat(offender_id, ' - ', offender) as name, id, ppm_id")
			->where('offender_id', 'like', 'S%')
			->whereRaw("(offender_id like 'S%{$data['query']}' or offender like '%{$data['query']}%')")
			->get()
			->toArray();


		return json_encode($returns);
	}

	public function datatable(Request $request)
	{
		$syncronism = $this->synchronism->with('offender')
			->select(['id', 'offender_id', 'type', 'register_date']);

		if ($request->has('type_filter') && $request->get('type_filter') > 0) {
			$syncronism->byType($request->get('type_filter'));
		}

		if ($request->has('offender_id_filter') && $request->get('offender_id_filter') != '') {
			$syncronism->byOffenderID($request->get('offender_id_filter'));
		}

		if ($request->has('date_ini_filter') && $request->get('date_ini_filter') != '' &&
			$request->has('date_end_filter') && $request->get('date_end_filter') != '') {
			$syncronism->byPeriod($request->get('date_ini_filter'), $request->get('date_end_filter'));
		}

		return Datatables::of($syncronism)

			->addColumn('offender', function ($syncronism) {
				if (!is_null($syncronism->offender))
					return $syncronism->offender->offender_id.' - '.$syncronism->offender->offender;

				return '';
			})
			->addColumn('type', function ($syncronism) {
				return config('ongoing.synchronism_types.'.$syncronism->type);
			})
			->addColumn('action', function ($syncronism) {
				$html = "";

				//listar
				if(auth()->user()->can('synchronism_list')) {
					$html .= '<a class="btn tooltips btn-xs btn-success show-details" href="javascript:void(0);" data-route="'.route('offender.synchronism.show-details', $syncronism->id).'"> <i class="fa fa-search"></i> </a>';
				}

				// editar
				if (auth()->user()->can('synchronism_edit')) {
					$html .= view('utils.buttons.edit')->with('route', route('offender.synchronism.edit', $syncronism->id))->render();
				}

				// excluir
				if (auth()->user()->can('synchronism_del')) {
					$html .= view('utils.buttons.delete')->with(['route' => route('offender.synchronism.destroy', $syncronism->id), 'id' => $syncronism->id])->render();
				}

				return $html;
			})
			->make();

	}

}