<?php

namespace App\Services;

use App\Models\Ad;
use App\Models\Company;
use App\Models\Group;
use App\Models\Parameter;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CompanyService
{

    /**
     * @var Company
     */
    private $company;
    /**
     * @var Parameter
     */
    private $parameter;
    /**
     * @var Ad
     */
    private $ad;

    public function __construct(
        Company $company,
        Parameter $parameter,
        Ad $ad
    )
    {
        $this->company = $company;
        $this->parameter = $parameter;
        $this->ad = $ad;
    }

    public function store(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // -----------------------------------------------------------------
                // EMPRESA
                // -----------------------------------------------------------------

                // Deixo o nome do domínio em minúsculo
                $data_company['domain'] = mb_strtolower($request['domain'], 'UTF-8');

                // Verifico se o domínio já existe cadastrado
                $company = $this->company->byDomain($data_company['domain'])->first();

                if ($company) {
                    throw new \Exception(Lang::get('table.dominio_ja_cadastrado'));
                }
                // Recupero a descrição da empresa
                $data_company['description'] = $request['description'];

                // Gero o uniquename da empresa
                $data_company['name'] = mb_strtolower($request['description'], 'UTF-8');

                $data_company['name'] = str_replace(' ', '_', $data_company['name']);

                // Cadastro a empresa
                $company = $this->company->create($data_company);

                // -----------------------------------------------------------------
                // PARAMETROS
                // -----------------------------------------------------------------

                $data_parameter = [
                    'company_id'    => $company->id,
                    'auth_ad'       => $request['auth_ad']
                ];

                // Cadastro os parametros
                $this->parameter->create($data_parameter);

                // -----------------------------------------------------------------
                // AD
                // -----------------------------------------------------------------

                $data_ad = [
                    'company_id' => $company->id,
                    'connection' => !empty($request['ad_connection'])   ? $request['ad_connection'] : null,
                    'username'   => !empty($request['ad_username'])     ? $request['ad_username']   : null,
                    'password'   => !empty($request['ad_password'])     ? $request['ad_password']   : null,
                    'domain'     => !empty($request['ad_domain'])       ? $request['ad_domain']     : null,
                    'port'       => !empty($request['ad_port'])         ? $request['ad_port']       : null,
                    'dn_users'   => !empty($request['ad_dn_users'])     ? $request['ad_dn_users']   : null,
                    'dn_groups'  => !empty($request['ad_dn_groups'])    ? $request['ad_dn_groups']  : null,
                    'fields'     => !empty($request['ad_fields'])       ? str_replace(' ', '', $request['ad_fields'])    : null,
                ];

                // Cadastro os ad
                $this->ad->create($data_ad);

                // Atualizo as empresas da session
                $companies = $this->company->ordered()->get();
                session(['companies' => $companies]);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $request, $id)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request, $id) {

                // Recupero a empresa
                $company = $this->company->find($id);

                // Verifico se empresa existe
                if (!$company) {
                    throw new \Exception(Lang::get('table.empresa_nao_encontrada'));
                }

                // Se não for admin, a empresa deve ser a mesma da sessão
                if (!Auth::user()->isAdmin() && $company->id != session('company')['id']) {
                    throw new \Exception(Lang::get('sys.msg.permission_denied'));
                }

                // Recupero os parametros
                $parameter = $this->parameter->byCompany($company->id)->first();

                // Recupero as informações do AD
                $ad = $this->ad->byCompany($company->id)->first();

                // Verifico se tem os parametros e ads
                if (!$parameter || !$ad) {
                    throw new \Exception(Lang::get('sys.msg.not_found'));
                }

                // -----------------------------------------------------------------
                // EMPRESA
                // -----------------------------------------------------------------

                // Deixo o nome do domínio em minúsculo
                $data_company['domain'] = mb_strtolower($request['domain'], 'UTF-8');

                // Se o dominio for diferente do atual
                // Verifico se o domínio já existe cadastrado
                if($company->domain != $data_company['domain']) {

                    $company_domain = $this->company->byDomain($data_company['domain'])->first();

                    if ($company_domain) {
                        throw new \Exception(Lang::get('table.dominio_ja_cadastrado'));
                    }
                }

                // Recupero a descrição da empresa
                $data_company['description'] = $request['description'];

                // Gero o uniquename da empresa
                $data_company['name'] = mb_strtolower($request['description'], 'UTF-8');

                $data_company['name'] = str_replace(' ', '_', $data_company['name']);

                // Atualizo a empresa
                $company->update($data_company);

                // -----------------------------------------------------------------
                // PARAMETROS
                // -----------------------------------------------------------------

                $data_parameter = [
                    'auth_ad' => $request['auth_ad'],
                ];

                // Atualizo os parametros
                $parameter->update($data_parameter);

                // -----------------------------------------------------------------
                // AD
                // -----------------------------------------------------------------

                $data_ad = [
                    'connection' => !empty($request['ad_connection'])   ? $request['ad_connection'] : null,
                    'username'   => !empty($request['ad_username'])     ? $request['ad_username']   : null,
                    'password'   => !empty($request['ad_password'])     ? $request['ad_password']   : null,
                    'domain'     => !empty($request['ad_domain'])       ? $request['ad_domain']     : null,
                    'port'       => !empty($request['ad_port'])         ? $request['ad_port']       : null,
                    'dn_users'   => !empty($request['ad_dn_users'])     ? $request['ad_dn_users']   : null,
                    'dn_groups'  => !empty($request['ad_dn_groups'])    ? $request['ad_dn_groups']  : null,
                    'fields'     => !empty($request['ad_fields'])       ? str_replace(' ', '', $request['ad_fields'])    : null,
                ];


                // Atualizo o AD
                $ad->update($data_ad);

                // Atualizo as empresas da session
                $companies = $this->company->ordered()->get();
                session(['companies' => $companies]);

                // Se a empresa da session for a que foi atualizada, altero o name
                if(session('company')['id'] == $id) {

                    session(['company' => [
                        'id'   => $id,
                        'name' => $request['description'],
                    ]]);

                }

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {

            // Se não for admin, não libero o cadastro
            if (!Auth::user()->isAdmin()) {
                throw new \Exception(Lang::get('sys.msg.permission_denied'));
            }

            // Recupero a empresa
            $company = $this->company->find($id);

            // Verifico se empresa existe
            if (!$company) {
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }

            // Verifico se a empresa a ser excluida é a atual da session
            if (session('company')['id'] == $company->id) {
                throw new \Exception(Lang::get('table.autenticado_empresa_para_exclusao'));
            }


            // Verifico se existem usuários vinculados nessa empresa
            $has_bond = User::byCompany($company->id)->count();

            if($has_bond) {
                throw new \Exception(Lang::get('table.usuarios_vinculados_empresa'));
            }

            // Verifico se existem grupos vinculados nessa empresa
            $has_bond = Group::byCompany($company->id)->count();

            if($has_bond) {
                throw new \Exception(Lang::get('table.grupos_vinculados_empresa'));
            }

            // SE NÃO TEM VINCULOS

            // Excluo os Parametros
            $this->parameter->byCompany($company->id)->delete();

            // Excluo o AD
            $this->ad->byCompany($company->id)->delete();

            // Excluo a Empresa
            $company->delete();

            // Atualizo as empresas da session
            $companies = $this->company->ordered()->get();
            session(['companies' => $companies]);

            return true;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}