<?php

namespace App\Services;

use App\Models\Group;
use App\Models\GroupPermission;
use App\Models\SolverGroupsGroup;
use App\Models\UserGroup;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class GroupService
{

    /**
     * @var Group
     */
    private $group;

    public function __construct(
        Group $group
    )
    {
        $this->group = $group;
    }

    public function store(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Validação do name se for admin
                if ($request['name'] == config('sys.admin_user')) {
                    throw new \Exception(Lang::get('table.grupo_ja_existe'));
                }
                // Verifico se o grupo já existe cadastrado
                $group = $this->group
                              ->byName($request['name'])
                              ->byCompany(session('company')['id'])
                              ->first();

                if($group){
                    throw new \Exception(Lang::get('table.grupo_ja_existe'));
                }

                // Recupero a empresa
                $request['company_id']  = session('company')['id'];

                // Cadastro o Grupo
                $group = $this->group->create($request);

                // Verifico se existe as permissões para efetuar a sincronização
                // dos grupos com as permissões
                if(array_key_exists('permissions', $request)){
                    $group->permissions()->sync($request['permissions']);
                }

                // Verifico se existe as usuários para efetuar a sincronização
                // dos grupos com os usuários
                if(array_key_exists('users', $request)) {

                    $group->users()->sync($request['users']);
                }

                // Verifico se existe os grupos resolvedores para efetuar a sincronização
                // dos grupos com os grupos resolvedores

                if (array_key_exists('solverGroup', $request) && in_array('11',$request['permissions'])) {
                    foreach ($request['solverGroup'] as $key => $sl) {
                        $data = [
                            'solver_group_id' => $key,
                            'is_internal_show' => (in_array("int", $sl)) ? 'S' : 'N',
                            'is_external_show' => (in_array("ext", $sl)) ? 'S' : 'N',
                            'is_create' => (in_array("cre", $sl)) ? 'S' : 'N',
                            'created_at' => Carbon::now()
                        ];

                        $group->solverGroupsGroup()->attach($group->id, $data);
                    }
                }
            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $request, $id)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request, $id) {

                // Verifico se o grupo já existe cadastrado
                $group = $this->group->byCompany(session('company')['id'])->find($id);

                if(!$group){
                    throw new \Exception(Lang::get('sys.msg.not_found'));
                }
                // verifico se o (name) esta no array, caso esteja eu retiro
                if(array_key_exists('name', $request)) {
                    unset($request['name']);
                }
                // Atualizo o Grupo
                $group->update($request);

                // Verifico se existe as permissões para efetuar a sincronização
                // dos grupos com as permissões
                if(array_key_exists('permissions', $request)){
                    $group->permissions()->sync($request['permissions']);
                }else{
                    $group->permissions()->sync([]);
                }
                // Verifico se existe os usuários para efetuar a sincronização
                // dos grupos com os usuários
                if(array_key_exists('users', $request)){
                    $group->users()->sync($request['users']);
                }else{
                    $group->users()->sync([]);
                }

                // Verifico se existe os grupos resolvedores para efetuar a sincronização
                // dos grupos com os grupos resolvedores

                SolverGroupsGroup::whereGroupId($id)->delete();

                if (array_key_exists('solverGroup', $request) && in_array('11',$request['permissions'])) {
                    foreach ($request['solverGroup'] as $key => $sl) {
                        $data = [
                            'solver_group_id' => $key,
                            'is_internal_show' => (in_array("int", $sl)) ? 'S' : 'N',
                            'is_external_show' => (in_array("ext", $sl)) ? 'S' : 'N',
                            'is_create' => (in_array("cre", $sl)) ? 'S' : 'N',
                            'created_at' => Carbon::now()
                        ];

                        $group->solverGroupsGroup()->attach($id, $data);
                    }
                }
            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {

            $group = $this->group->byCompany(session('company')['id'])
                                 ->removeAdmin()
                                 ->find($id);

            // Verifico se grupo existe
            if(!$group){
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }

            // Verifico se existe usuário vinculados nesse grupo
            $has_bond = UserGroup::where('group_id', '=', $id)->count();

            if($has_bond){
                throw new \Exception(Lang::get('table.usuarios_vinculados'));
            }
            // Verifico se existem permissões vinculadas nesse grupo
            $has_bond = GroupPermission::where('group_id', '=', $id)->count();

            if($has_bond){
                throw new \Exception(Lang::get('table.permissoes_vinculadas'));
            }
            $group->delete();

            return true;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}