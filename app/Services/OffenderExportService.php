<?php

namespace App\Services;

use App\Models\Offender;
use App\Models\OffenderHistory;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class OffenderExportService
{
    /**
     * @var Offender
     */
    private $offender;

    /**
     * @var OffenderHistory
     */
    private $offenderHistory;

    public function __construct(Offender $offender, OffenderHistory $offenderHistory)
    {
        $this->offender = $offender;
        $this->offenderHistory = $offenderHistory;
    }

    public function export(string $solverGroup)
    {
        return Excel::create('OFENSORES DE PORTABILIDADE NUMÉRICA V10.09_NET', function ($excel) use ($solverGroup) {

            $sheetNames = array('Ofensores', 'Ofensores Desativados', 'Descrição Antiga');

            foreach  ($sheetNames as $key=>$value) {

                $dataArray = $this->getData($key, $solverGroup);

                $excel->sheet($sheetNames[$key], function ($sheet) use ($dataArray, $key) {
                    $sheet->fromArray($dataArray, null, '', true, false);

                    $lastRow = $sheet->getHighestRow();
                    $lastColumn = $sheet->getHighestColumn();

                    $sheet->setFontSize(10);

                    // setting border
                    $styleArray = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );
                    $sheet->getStyle('A4:'.$lastColumn.$lastRow)->applyFromArray($styleArray);

                    // merging cells
                    if ($key == 1) {
                        $sheet->mergeCells('A1:E1');
                    } else {
                        $sheet->mergeCells('A1:C1');
                        $sheet->mergeCells('A2:B2');
                    }

                    $sheet->cell('A1', function($cell) {
                        // formatting
                        $cell->setFontFamily('Arial')
                            ->setFontSize(14)
                            ->setBackground('#17375D')
                            ->setFontColor('#ffffff')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    $sheet->cell('A2', function($cell) {
                        // formatting
                        $cell->setFontFamily('Arial')
                            ->setFontSize(10)
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    // formatting column wraptext
                    //if ($key != 0) {
                        $sheet->getStyle('A4:'.$lastColumn.$lastRow)->getAlignment()->setWrapText(true);
                    //}

                    $sheet->cells('A5:'.$lastColumn.$lastRow, function($cells) {
                            $cells->setAlignment('center')->setValignment('center');
                    });

                    // formatting row 4 - header
                    $sheet->row(4, function($row) {
                        $row->setFontFamily('Arial')
                            ->setFontSize(10)
                            ->setBackground('#17375D')
                            ->setFontColor('#ffffff')
                            ->setFontWeight('bold')
                            ->setValignment('center');
                    });

                    // setting bold column E
                    $sheet->cells('E5:E'.$lastRow, function($cells) {
                        $cells->setFontWeight('bold');
                    });

                    // setting background column A
                    $sheet->cells('A5:A'.$lastRow, function($cells) use($key) {
                        // formatting
                        if ($key == 1) {
                            $cells->setBackground('#FF0000');
                        } else {
                            $cells->setBackground('#FFFF00');
                        }
                    });

                    // set background column J
                    $sheet->cells('J5:J'.$lastRow, function($cells) use($key) {
                        // formatting
                        if ($key == 1) {
                            $cells->setBackground('#FF0000');
                        } else {
                            $cells->setBackground('#FFFF00');
                        }
                    });

                    for ($i = 5; $i <= $lastRow; $i++) {
                        $value = $sheet->getCell('D'.$i)->getValue();

                        $sheet->cell('D'.$i, function($cell) use($value) {

                            if ($value == 'N1') {
                                $cell->setBackground('#FF0000');
                            } else {
                                $cell->setBackground('#00B050');
                            }

                        });
                    }

                    $sheet->setWidth(array(
                        'B'     =>  13.14,
                        'C'     =>  40.43,
                        'D'     =>  11.14,
                        'E'     =>  13.57,
                        'F'     =>  16.86,
                        'G'     =>  15.14,
                        'H'     =>  58.43,
                        'I'     =>  65.43,
                        'K'     =>  15.14,
                        'L'     =>  24.86,
                        'M'     =>  59.71,
                        'N'     =>  19.43,
                        'O'     =>  13.29,
                        'P'     =>  14.71,
                        'Q'     =>  20.43,
                        'R'     =>  36.14,
                        'S'     =>  16.14,
                        'T'     =>  18.71
                    ));

                });

                //$excel->getDefaultStyle()->getAlignment()->setWrapText(true);
            }

        })->download('xls');
    }

    private function getData(int $sheetIndex, string $solverGroup) {

        $dataRow = [];

        if ($sheetIndex == 0) {
            $headerRow = array(
                array('OFENSORES PORTABILIDADE'),
                array('Atualizado em:'),
                array(''),
                array('ID', 'DT. CAD.', 'OFENSOR', 'NIVEL DE RETENÇÃO', 'VÁLIDO', 'CLASSIFICAÇÃO', 'ORIGEM',
                'EXPLICAÇÃO DETALHADA DO OFENSOR', 'RESPOSTA PARA O USUÁRIO NO ATENDIMENTO DO CHAMADO', 'ID',
                'ID_NO_PPM', 'PROJETO_PACOTE', 'PREVENTIVO_OFENSOR', 'PREVISAO_DEFINITIVO', 'NUMERO_PATCH',
                'IDENTIFICACAO_PATCH', 'DT. CAD. OFENSOR', 'UNIT - C', 'ID_OCORRENCIA', 'ID_PROCEDIMENTO')
            );
            $dataRow = $this->offender
                ->selectRaw('id, created_at, offender, retention_level, 
                    (CASE WHEN valid = 0 THEN \'NÃO\' ELSE \'SIM\' END) as valid2,                
                    classification, origin,
                    detailed_explanation_offender, answer_for_the_user_in_the_call, id as id2,
                    ppm_id, project_pack, preventive_offender, predict_definitive, num_patch,
                    patch_identification, DATE_FORMAT(created_at, \'%d/%m/%Y\') as created_at2, 
                    unit_c, event_id, proceeding_id')
                ->where('status', 'Ativo')
                ->where('offender_type', 'Externo')
                ->where('solver_group', $solverGroup)
                ->get()->toArray();

            $dataArray = array_merge($headerRow, $dataRow);

        } else if ($sheetIndex == 1) {
            $headerRow = array(
                array('OFENSORES PORTABILIDADE ( DESATIVADOS )'),
                array(''),
                array(''),
                array('ID', 'DT. CAD.', 'OFENSOR', 'NIVEL DE RETENÇÃO', 'VÁLIDO', 'CLASSIFICAÇÃO', 'ORIGEM',
                    'EXPLICAÇÃO', 'RESPOSTA PARA O USUÁRIO', 'ID', 'ID_NO_PPM', 'PROJETO_PACOTE', 'PREVENTIVO OFENSOR',
                    'PREVISAO_DEFINITIVO', 'NUMERO_PATCH', 'IDENTIFICACAO_PATCH', 'DT. CAD. OFENSOR', 'OBSERVAÇÃO')
            );

            $dataRow = $this->offender
                ->selectRaw('id, created_at, offender, retention_level, 
                    (CASE WHEN valid = 0 THEN \'NÃO\' ELSE \'SIM\' END) as valid2,                
                    classification, origin,
                    detailed_explanation_offender, answer_for_the_user_in_the_call, id as id2,
                    ppm_id, project_pack, preventive_offender, predict_definitive, num_patch,
                    patch_identification, DATE_FORMAT(created_at, \'%d/%m/%Y\') as created_at2')
                ->where('status', 'Inativo')
                ->where('offender_type', 'Externo')
                ->where('solver_group', $solverGroup)
                ->get()->toArray();

            foreach ($dataRow as $key=>$column) {
                // find history by id
                $observation = $this->offenderHistory
                    ->selectRaw('(CASE WHEN operation_type = \'I\' THEN observation2 
                    ELSE CONCAT(\'OFENSOR DESATIVADO EM - \',DATE_FORMAT(created_at, \'%d-%m-%Y\')) END) 
                    as observation2')
                    ->where('offender_id', $column['id'])
                    ->whereIn('operation_type', array('I', 'E'))
                    ->orderBy('operation_type', 'asc')
                    ->orderBy('created_at', 'desc')
                    ->get()->toArray();

                $observationColumn = '';
                foreach ($observation as $obs) {
                    $observationColumn = $observationColumn.$obs['observation2']."\r\n";
                }

                $dataRow[$key]['observation2'] = $observationColumn;
            }

            $dataArray = array_merge($headerRow, $dataRow);
        } else {
            $headerRow = array(
                array('OFENSORES PORTABILIDADE'),
                array('Atualizado em:'),
                array(''),
                array('ID', 'DT. CAD.', 'DESCRIÇÃO ANTIGA', 'NIVEL DE RETENÇÃO', 'VÁLIDO', 'CLASSIFICAÇÃO', 'ORIGEM',
                    'EXPLICAÇÃO DETALHADA DO OFENSOR', 'RESPOSTA PARA O USUÁRIO NO ATENDIMENTO DO CHAMADO', 'ID',
                    'ID_NO_PPM', 'PROJETO_PACOTE', 'PREVENTIVO_OFENSOR', 'PREVISAO_DEFINITIVO', 'NUMERO_PATCH',
                    'IDENTIFICACAO_PATCH', 'DT. CAD. OFENSOR', 'OBSERVAÇÃO')
            );

            $offenderId = $this->offender
                ->selectRaw('id')
                ->where('status', 'Ativo')
                ->where('offender_type', 'Externo')
                ->where('solver_group', $solverGroup)
                ->get()->toArray();

            foreach ($offenderId as $key=>$id) {
                // find history by id
                $histories = $this->offenderHistory
                    ->selectRaw('offender_id, snapshot, modified_fields, created_at, operation_type, observation2')
                    ->where('offender_id', $id['id'])
                    ->whereIn('operation_type', array('I', 'E'))
/*                    ->where(function($query)
                    {
                        $query->where('modified_fields', '<>', 'status')
                            ->orWhereNull('modified_fields');
                    })*/
                    ->orderBy('operation_type', 'asc')
                    ->orderBy('created_at', 'desc')
                    ->get()->toArray();

                if (!empty($histories)) {
                    $histories[0]['snapshot']['created_at'] = date('d/m/Y',strtotime($histories[0]['snapshot']['created_at']));

                    $dataRow[$key]['id'] = $histories[0]['snapshot']['id'].'A';
                    $dataRow[$key]['created_at'] = $histories[0]['snapshot']['created_at'];
                    $dataRow[$key]['offender'] = $histories[0]['snapshot']['offender'];
                    $dataRow[$key]['retention_level'] = $histories[0]['snapshot']['retention_level'];
                    $dataRow[$key]['valid'] = $histories[0]['snapshot']['valid'] == 0 ? 'NÃO' : 'SIM';
                    $dataRow[$key]['classification'] = $histories[0]['snapshot']['classification'];
                    $dataRow[$key]['origin'] = $histories[0]['snapshot']['origin'];
                    $dataRow[$key]['detailed_explanation_offender'] = $histories[0]['snapshot']['detailed_explanation_offender'];
                    $dataRow[$key]['answer_for_the_user_in_the_call'] = $histories[0]['snapshot']['answer_for_the_user_in_the_call'];
                    $dataRow[$key]['id2'] = $histories[0]['snapshot']['id'];
                    $dataRow[$key]['ppm_id'] = $histories[0]['snapshot']['ppm_id'];
                    $dataRow[$key]['project_pack'] = $histories[0]['snapshot']['project_pack'];
                    $dataRow[$key]['preventive_offender'] = $histories[0]['snapshot']['preventive_offender'];
                    $dataRow[$key]['predict_definitive'] = $histories[0]['snapshot']['predict_definitive'];
                    $dataRow[$key]['num_patch'] = $histories[0]['snapshot']['num_patch'];
                    $dataRow[$key]['patch_identification'] = $histories[0]['snapshot']['patch_identification'];
                    $dataRow[$key]['created_at2'] = $histories[0]['snapshot']['created_at'];

                    $observationColumn = '';
                    foreach ($histories as $history) {

                        // do not record if it is a modification on the status field from active to inactive
                        if ($history['modified_fields'] == 'status' && $history['snapshot']['status'] == 'Ativo') {
                            continue;
                        }

                        if ($history['operation_type'] == 'E') {
                            $observationColumn = $observationColumn.'OFENSOR ALTERADO EM '. $history['created_at'].
                                ' – Campos ajustados ('.$this->getHeaderName($history['modified_fields']).').'."\r\n";
                        } else {
                            $observationColumn = $observationColumn.$history['observation2']."\r\n";
                        }
                    }

                    $dataRow[$key]['observation2'] = $observationColumn;
                }

            }

            $dataArray = array_merge($headerRow, $dataRow);

        }

        return $dataArray;
    }

    private function getHeaderName(string $fields)
    {
        $fields = explode(", ", $fields);

        $header['id'] = 'ID';
        $header['created_at'] = 'DT. CAD.';
        $header['observation'] = 'Observação';
        $header['offender'] = 'OFENSOR';
        $header['retention_level'] = 'NIVEL DE RETENÇÃO';
        $header['valid'] = 'VÁLIDO';
        $header['classification'] = 'CLASSIFICAÇÃO';
        $header['origin'] = 'ORIGEM';
        $header['detailed_explanation_offender'] = 'EXPLICAÇÃO DETALHADA DO OFENSOR';
        $header['answer_for_the_user_in_the_call'] = 'RESPOSTA PARA O USUÁRIO NO ATENDIMENTO DO CHAMADO';
        $header['ppm_id'] = 'ID_NO_PPM';
        $header['project_pack'] = 'PROJETO_PACOTE';
        $header['preventive_offender'] = 'PREVENTIVO_OFENSOR';
        $header['predict_definitive'] = 'PREVISAO_DEFINITIVO';
        $header['num_patch'] = 'NUMERO_PATCH';
        $header['patch_identification'] = 'IDENTIFICACAO_PATCH';
        $header['unit_c'] = 'UNIT - C';
        $header['event_id'] = 'ID_OCORRENCIA';
        $header['proceeding_id'] = 'ID_PROCEDIMENTO';
        $header['status'] = 'Reativado';

        $returnText = '';
        foreach ($fields as $key => $field) {
            $returnText = $returnText.$header[$field];

            if ($key <= count($fields) - 3) {
                $returnText = $returnText.', ';
            } else if ($key == count($fields) - 2) {
                $returnText = $returnText.' e ';
            }
        }

        return $returnText;

    }


}