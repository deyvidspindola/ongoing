<?php

namespace App\Services;

use App\Models\Parameter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ParametersService
{

    /**
     * @var Parameter
     */
    private $parameter;

    public function __construct(
        Parameter $parameter
    )
    {
        $this->parameter = $parameter;
    }


    public function cieloStore(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Recupero o registro
                $parameter = $this->parameter->byCompany(Auth::user()->company_id)->first();

                // Formato o softDescripton
                $request['cielo_softDescripton'] = mb_strtoupper($request['cielo_softDescripton'], 'UTF-8');

                // Atualizo o registro
                $parameter->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function zenviaStore(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Recupero o registro
                $parameter = $this->parameter->byCompany(Auth::user()->company_id)->first();

                // Atualizo o registro
                $parameter->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function emailStore(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Recupero o registro
                $parameter = $this->parameter->byCompany(Auth::user()->company_id)->first();

                // Atualizo o registro
                $parameter->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function campanhaStore(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Recupero o registro
                $parameter = $this->parameter->byCompany(Auth::user()->company_id)->first();

                // Atualizo o registro
                $parameter->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function linkStore(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Recupero o registro
                $parameter = $this->parameter->byCompany(Auth::user()->company_id)->first();

                // Atualizo o registro
                $parameter->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}