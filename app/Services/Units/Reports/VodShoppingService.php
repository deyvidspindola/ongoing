<?php
/**
 * Created by PhpStorm.
 * User: drspindola
 * Date: 10/09/19
 * Time: 16:30
 */

namespace App\Services\Units\Reports;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use mysql_xdevapi\Exception;
use Yajra\DataTables\DataTables;

class VodShoppingService
{

    public function datatable(Request $request)
    {
        $list = $this->getSelect($request);
        return Datatables::of($list)
            ->addColumn('valor', function ($list) {
                return number_format($list->valor, 2, ',', '');
            })
            ->addColumn('dt_compra', function ($list) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $list->dt_compra)->format('d/m/Y');
            })
            ->make();
    }

    private function getTable($bat)
    {
        switch ($bat){
            case 'TVOD_CLARO':
                return [
                    'table' => 'now.tb_tvod_consumido_claro',
                    'type' => 'TVOD',
                ];
                break;

            case 'TVOD_NET':
                return [
                    'table' => 'now.tb_tvod_consumido_net',
                    'type' => 'TVOD',
                ];
                break;

            case 'NW_NET':
                return [
                    'table' => 'now.tb_tvod_consumido_net',
                    'type' => 'NOWONLINE%',
                ];
                break;

            case 'SVOD_NET':
                return [
                    'table' => 'now.tb_svod_consumido_net',
                    'type' => 'SVOD',
                ];
                break;

            case 'NW_SVOD':
                return [
                    'table' => 'now.tb_svod_consumido_net',
                    'type' => 'NOWONLINE_SVOD',
                ];
                break;

            default :
                return false;
                break;
        }
    }

    public function getSelect($request)
    {
        $dataTable = $this->getTable($request->get('type'));

        if ($request->get('status') == 'NOK') {
            $status_compra_tvod = "AND status_compra NOT LIKE 'Processado%'";
	        $status_compra_svod = "AND observacao NOT LIKE 'Processado%'";
	    } else if ($request->get('status') == 'OK') {
            $status_compra_tvod = "AND status_compra LIKE 'Processado%'";
	        $status_compra_svod = "AND observacao LIKE 'Processado%'";
	    } else {
            $status_compra_tvod = "";
	        $status_compra_svod = "";
        }

        $dateIni = ($request->get('date_ini')) ? Carbon::createFromFormat('d/m/Y', $request->get('date_ini'))->format('Y-m-d') : null;
        $dateEnd = ($request->get('date_end')) ? Carbon::createFromFormat('d/m/Y', $request->get('date_end'))->format('Y-m-d') : null;

        if (Str::contains($request->get('type'), ['TVOD_CLARO', 'TVOD_NET', 'SVOD_NET', 'NW_NET', 'NW_SVOD'])) {
            $this->inseriDados($request->get('type'), $request->get('date_ini'), $request->get('date_end'));
        }

        if(empty($request->get('shopping_id')) && !is_null($dateIni) && !is_null($dateEnd)) {

            if (Str::contains($request->get('type'), ['TVOD_CLARO', 'TVOD_NET', 'NW_NET', 'NOWONLINE%'])) {
                $where = "dt_execucao BETWEEN '{$dateIni}' AND '{$dateEnd}' AND pri_adicional LIKE '{$dataTable['type']}' {$status_compra_tvod}";
            } elseif(Str::contains($request->get('type'), ['NW_SVOD', 'SVOD_NET'])) {
                $where = "dt_execucao BETWEEN '{$dateIni}' AND '{$dateEnd}' AND pri_adicional LIKE '{$dataTable['type']}' {$status_compra_svod}";
            }

        } elseif (empty($request->get('shopping_id')) && !is_null($dateIni)) {

            if (Str::contains($request->get('type'), ['TVOD_CLARO', 'TVOD_NET', 'NW_NET'])) {
                $where = "dt_execucao = '{$dateIni}' AND pri_adicional LIKE '{$dataTable['type']}' {$status_compra_tvod}";
            } elseif(Str::contains($request->get('type'), ['NW_SVOD', 'SVOD_NET'])) {
                $where = "dt_execucao = '{$dateIni}' AND pri_adicional LIKE '{$dataTable['type']}' {$status_compra_svod}";
            }

        } elseif(!empty($request->get('shopping_id'))) {

            $where = "id_compra = '{$request->get('shopping_id')}'";

        } else {

            $where = null;

        }

        if (!is_null($where) && $dataTable){

            $columns = $this->getColumns($request->get('type'));

            try {
                $sql = DB::connection('pgsql_31')
                    ->table(DB::raw($dataTable['table']))
                    ->selectRaw($columns)
                    ->whereRaw($where)
                    ->get();

                return $sql;
            } catch (\Illuminate\Database\QueryException $e){
                Log::debug($e->getMessage());
            }

        }

        return [];

    }

    private function getColumns($type)
    {
        if (Str::contains($type, ['TVOD_CLARO', 'TVOD_NET', 'NW_NET', 'NOWONLINE%'])) {

            $columns = "id_compra, customer, smartcard, dt_compra, nome_filme  produto, valor_arquivo  valor, arquivo_xml, status_compra  observacao";

        } else if ($type == 'NW_SVOD') {

            $columns = "id_compra, (num_contrato || cod_operadora) customer, smartcard, dt_compra, (id_produto || ' - ' || descricao_prod) produto, valor, SUBSTRING(arquivo_xml, 18, 6) arquivo_xml, observacao";

        } else {

            $columns = "id_compra, (num_contrato || cod_operadora) customer, smartcard, dt_compra, (id_produto || ' - ' || descricao_prod) produto, valor, SUBSTRING(arquivo_xml, 10, 6) arquivo_xml, observacao";

        }

        return $columns;

    }

    private function inseriDados($type, $dateIni, $dateEnd)
    {
//        DB::connection('pgsql_31')->table('now.tb_info_extracao')->insert([
//            'tipo_bat' => $type,
//            'data_ini' => $dateIni,
//            'data_fim' => $dateEnd
//        ]);
    }

}