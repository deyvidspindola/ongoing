<?php
/**
 * Created by PhpStorm.
 * User: drspindola
 * Date: 10/09/19
 * Time: 16:30
 */

namespace App\Services\Units\Reports;


use App\Models\Units\UnitsMenu;
use App\Models\Units\UnitsService;
use App\Models\Units\UnitsSolicitation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class AnalyticalAttendanceService
{

    /**
     * @var UnitsSolicitation
     */
    private $solicitation;
    /**
     * @var UnitsService
     */
    private $unitsService;
    /**
     * @var UnitsMenu
     */
    private $unitsMenu;
    /**
     * @var User
     */
    private $user;

    public function __construct(
        UnitsSolicitation $solicitation,
        UnitsService $unitsService,
        UnitsMenu $unitsMenu,
        User $user
    )
    {
        $this->solicitation = $solicitation;
        $this->unitsService = $unitsService;
        $this->unitsMenu = $unitsMenu;
        $this->user = $user;
    }

    public function datatable(Request $request)
    {
        $list = $this->getData($request);
        return Datatables::of($list)->make();
    }


    public function getData($request)
    {
        $solicitations = $this->solicitation->select('*');

        if ($request->has('status') && $request->get('status') != '') {
            $solicitations->byStatus($request->get('status'));
        }

        if ($request->has('date_ini') && $request->get('date_ini') != '' && $request->has('date_end') && $request->get('date_end') != '') {
            $solicitations->byPeriod($request->get('date_ini'), $request->get('date_end'));
        }
        $solicitations = $solicitations->get();

        $line = [];
        foreach ($solicitations as $sl) {

            $user = $this->user->with('groups')->byUsername($sl->username)->first();
            $service = $this->unitsService->find($sl->units_service_id);
            $menu = $this->unitsMenu->bySlug($service->slug)->first();

            $groups = [];
            foreach ($user->groups as $gp) {
                $groups[] = $gp->description;
            }
            $groups = implode(', ', $groups);

            $message =  Str::after($sl->process_message, 'Error Message :' );
            $message = Str::before($message, '.');

            $line[] = [
                'data'          => Carbon::createFromFormat('Y-m-d H:i:s', $sl->created_at)->format('d/m/Y H:i:s'),
                'username'      => $sl->username,
                'nome'          => $user->name,
                'perfil'        => $groups,
                'ip_remoto'     => $sl->remote_ip,
                'descricao'     => $service->description,
                'menu'          => $menu->title,
                'contrato'      => $sl->contract_number,
                'terminal'      => $sl->terminal,
                'bilhete'       => $sl->ticket,
                'dth_janela'    => '',
                'status'        => ($sl->status == 'F') ? 'Sucesso' : 'Erro',
                'msg_processo'  => $message,
                'id_servico'    => $service->id,
            ];

        }

        return $line;

    }

}