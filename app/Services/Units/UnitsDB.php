<?php

namespace App\Services\Units;

use App\Models\Units\UnitsOperation;
use Illuminate\Support\Facades\DB;

class UnitsDB
{
    /**
     * @param $operation
     * $operation a tabela de operations trás os dados das cidades.
     */
    public function getDB($operadora, $empresa = 'NET')
    {
        $operation = UnitsOperation::where('base_code', $operadora)->first();

        if ($empresa == 'NET') {
            $banco = DB::connection("{$operation->base_name}");
        }

        if ($empresa == 'CLARO') {
            $banco = DB::connection('CTV');
        }

        return $banco ;
    }
}