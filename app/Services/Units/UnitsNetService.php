<?php

namespace App\Services\Units;

use App\Models\Units\UnitsOperation;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use PDOException;

class UnitsNetService {

    public $db;

    public function __construct
    (
        UnitsDB $db
    )
    {
        $this->db = $db;
    }

    /**
   * Retorna o nome do assinante, de acordo com a operadora e com o código do contrato NET
   *
   * @param [type] $operadora
   * @param [type] $contrato
   * @return void
   */
  public function buscaAssinante($operadora = 13, $contrato = 32492910)
  {
    try {
        $data = $this->db->getDB($operadora)
            ->table('prod_jd.sn_contrato contrato')
            ->selectRaw("
                assinante.id_assinante,
                assinante.nome_titular,
                assinante.cpf,
                contrato.num_contrato,
                contrato.cid_contrato
            ")
            ->leftJoin('prod_jd.sn_assinante assinante', 'assinante.id_assinante', '=', 'contrato.id_assinante')
            ->whereRaw("contrato.cid_contrato = (
        SELECT
            cid_contrato
        FROM
            prod_jd.sn_cidade_operadora
        WHERE
            cod_operadora = $operadora)")
            ->whereRaw("contrato.num_contrato = $contrato")
            ->first();

      return $data;

    } catch (QueryException $e) { // Falha na query
        throw new \Exception($e->getMessage());
    } catch (PDOException $e) { // Falha na conexão com o banco de dados
        throw new \Exception($e->getMessage());
    }
  }

  public function  buscaDadosTerminal($operadora, $ddd, $telefone)
  {
      try {
          $db = $this->db->getDB($operadora);

          $operadora = str_pad($operadora,3,'0', STR_PAD_LEFT);

          $data = $db
          ->table('SN_TELEFONE_VOIP')
          ->whereRaw("CID_CONTRATO = (SELECT CID_CONTRATO FROM PROD_JD.SN_CIDADE_OPERADORA WHERE COD_OPERADORA = '{$operadora}')
            AND DDD_TELEFONE_VOIP = '{$ddd}'   AND NUM_TELEFONE_VOIP = '{$telefone}'
            AND DT_FIM = TO_DATE('30/12/2049','DD/MM/RRRR')")
          ->get();
          if (count($data) == 0) {
              $data = $this->db->getDB($operadora)
                  ->table('SN_TELEFONE_VOIP')
                  ->whereRaw("CID_CONTRATO = (SELECT CID_CONTRATO FROM PROD_JD.SN_CIDADE_OPERADORA WHERE COD_OPERADORA = '{$operadora}')
                    AND DDD_TELEFONE_VOIP = '{$ddd}'
                    AND NUM_TELEFONE_VOIP = '{$telefone}'
                    AND DT_FIM = (SELECT MAX(DT_FIM) FROM PROD_JD.SN_TELEFONE_VOIP WHERE CID_CONTRATO = (SELECT CID_CONTRATO FROM PROD_JD.SN_CIDADE_OPERADORA WHERE COD_OPERADORA = '{$operadora}')
                    AND DDD_TELEFONE_VOIP = '{$ddd}'
                    AND NUM_TELEFONE_VOIP = '{$telefone}')
                    AND ROWNUM < 2 ")->get();
          }

        return $data;
    } catch (QueryException $e) { // Falha na query
        throw new \Exception($e->getMessage());
    } catch (PDOException $e) { // Falha na conexão com o banco de dados
        throw new \Exception($e->getMessage());
    }

  }

}