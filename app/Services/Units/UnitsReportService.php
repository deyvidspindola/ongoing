<?php

namespace App\Services\Units;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Services\Units\UnitsDB;
use mysql_xdevapi\Exception;

class UnitsReportService
{
    private $unitsDB;
    private $url = "http://10.127.3.135:9100/CRMService";

    public function __construct
    (
        UnitsDB $unitsDB
    )
    {
        $this->unitsDB = $unitsDB;
    }

    public function getStatusOfACustomerOnTraxis($customer)
    {
        $url = $this->url . "/Customers/" . $customer;
        return $this->getService($url);
    }

    public function getProductsOfACustomerOnTraxis($customer, $operadora, $contrato)
    {
        $url = $this->url . "/Customers/" . $customer . "/SubscriptionProducts/";

        $products = $this->getService($url);
        if (isset($products['ProductId'])) {
            $produto = [];
            foreach ($products['ProductId'] as $product) {

                $query = "SELECT DISTINCT  ('PRODUTO_SVOD: ' || SP.ID_PRODUTO || ' - ' || SP.DESCRICAO) PRODUTO
                FROM PROD_JD.SN_PRODUTO                 SP,
                     PROD_JD.SN_REL_PRODUTO_VOD_PRODUTO VOD,
                     PROD_JD.SN_CIDADE_OPERADORA        OP, 
					 PROD_JD.SN_REL_TIPO_CARAC_PRODUTO  TP
               WHERE OP.COD_OPERADORA = '{$operadora}'
                 AND UPPER(VOD.CD_PRODUTO_VOD) = UPPER('{$product}')
                 AND VOD.ID_PRODUTO = SP.ID_PRODUTO
                 AND VOD.CID_CONTRATO = OP.CID_CONTRATO
				 AND VOD.ID_PRODUTO = TP.ID_PRODUTO
				 AND TP.ID_TIPO_CARACTERISTICA = 3
                 --AND ROWNUM < 2
				 AND EXISTS (SELECT 1 FROM PROD_JD.SN_REL_PONTO_PRODUTO
				                     WHERE NUM_CONTRATO = {$contrato}
									   AND CID_CONTRATO = VOD.CID_CONTRATO
									   AND INSTALADO = 1
									   AND DT_FIM > SYSDATE
									   AND ID_PRODUTO = SP.ID_PRODUTO)
              UNION
              SELECT DISTINCT ('PRODUTO_LINEUP: ' || SP.ID_PRODUTO || ' - ' || SP.DESCRICAO) PRODUTO
                FROM PROD_JD.SN_REL_PONTO_PRODUTO       PP,
                     PROD_JD.SN_REL_TIPO_CARAC_PRODUTO  TP,
                     PROD_JD.SN_PRODUTO                 SP,
                     PROD_JD.SN_REL_PRODUTO_VOD_PRODUTO VOD,
                     PROD_JD.SN_CIDADE_OPERADORA        OP, 
					 PROD_JD.SN_LINEUP_TIPO_PONTO_PROD  LP
               WHERE OP.COD_OPERADORA = '{$operadora}'
                 AND UPPER(VOD.CD_PRODUTO_VOD) = UPPER('{$product}')
				 AND SP.ID_TIPO_PRODUTO = 1
                 AND VOD.ID_PRODUTO = SP.ID_PRODUTO
                 AND VOD.CID_CONTRATO = OP.CID_CONTRATO
                 AND VOD.ID_PRODUTO = PP.ID_PRODUTO
                 AND VOD.CID_CONTRATO = PP.CID_CONTRATO
                 AND PP.NUM_CONTRATO = {$contrato}
                 AND PP.INSTALADO = 1
                 AND PP.DT_FIM > SYSDATE
                 AND PP.ID_PRODUTO = TP.ID_PRODUTO
                 AND TP.ID_TIPO_CARACTERISTICA IN (1, 2)
				 AND VOD.CID_CONTRATO = LP.CID_CONTRATO
				 AND VOD.ID_PRODUTO = LP.ID_PRODUTO";

                $produto[$product] = DB::connection($this->getBase($operadora))->select(DB::raw($query));
            }
        }


        return [
            'produtos' => $products,
            'productsDetails' => (isset($produto)) ? $produto : false
        ];
    }


    public function getCustomerOfADeviceOnTraxis($device)
    {
        $url = $this->url . "/Devices/" . $device . "/Customers/";
        return $this->getService($url);
    }

    public function getDevicesOfCustomerOnTraxis($customer)
    {
        $url = $this->url . "/Customers/" . $customer . "/Devices/";
        return $this->getService($url);
    }

    public function getCustomerTags($customer)
    {
        $url = $this->url . "/Customers/" . $customer . "/CustomerTags/";
        return $this->getService($url);
    }

    public function getService($url)
    {
        try{
            $client = new Client();
            $response = $client->request('GET', $url);

            if ($response->getStatusCode() === 200) {
                // Success!
                $xmlResponse = simplexml_load_string($response->getBody()); // Convert response into object for easier parsing
                return object2array($xmlResponse);
            }
            return false;

        }
        catch (\Exception $e){
            //return false necessário pois a classe client interpreta o erro 404 como exception
            return false;
        }
    }

    public function queryPlatform($customer, $operadora, $contrato, $terminal)
    {
        $getStatusOfACustomerOnTraxis = $this->getStatusOfACustomerOnTraxis($customer);
        $getProductsOfACustomerOnTraxis = $this->getProductsOfACustomerOnTraxis($customer, $operadora, $contrato);
        $getCustomerOfADeviceOnTraxis = false;
        if (!empty($terminal)) {
            $getCustomerOfADeviceOnTraxis = $this->getCustomerOfADeviceOnTraxis($terminal);
        }

        $getDevicesOfCustomerOnTraxis = $this->getDevicesOfCustomerOnTraxis($customer);
        $getCustomerTags = $this->getCustomerTags($customer);

        if ($getStatusOfACustomerOnTraxis) {
            $dataStatusOfACustomerOnTraxis = [
                'id' => $getStatusOfACustomerOnTraxis['@attributes']['id'],
                'IsBarred' => (isset($getStatusOfACustomerOnTraxis['IsBarred']) and $getStatusOfACustomerOnTraxis['IsBarred']) ? true : false,
                'CustomerData' => $getStatusOfACustomerOnTraxis['CustomerData']
            ];

            $dataProductsOfACustomerOnTraxis = [
                'customerId' => (isset($getProductsOfACustomerOnTraxis['produtos']['@attributes']['customerId'])) ? $getProductsOfACustomerOnTraxis['produtos']['@attributes']['customerId'] : false,
                'ProductId' => (isset($getProductsOfACustomerOnTraxis['produtos']['ProductId'])) ? $getProductsOfACustomerOnTraxis['produtos']['ProductId'] : false,
                'productsDetails' => $getProductsOfACustomerOnTraxis['productsDetails']
            ];


            $dataDevicesOfCustomerOnTraxis = [
                'customerId' => ($getDevicesOfCustomerOnTraxis['@attributes']['customerId']) ? $getDevicesOfCustomerOnTraxis['@attributes']['customerId'] : false,
                'DeviceId' => (isset($getDevicesOfCustomerOnTraxis['DeviceId'])) ? $getDevicesOfCustomerOnTraxis['DeviceId'] : false
            ];

            $dataCustomerTags = [
                'customerId' => ($getCustomerTags['@attributes']['customerId']) ? $getCustomerTags['@attributes']['customerId'] : false,
                'CustomerTagId' => (isset($getCustomerTags['CustomerTagId'])) ? $getCustomerTags['CustomerTagId'] : false
            ];
        } else {

            $data = [
                'success' => false,
                'message' => 'Customer não encontrado!'
            ];

            return $data;
        }

        $data = [
            'getStatusOfACustomerOnTraxis' => $dataStatusOfACustomerOnTraxis,
            'getProductsOfACustomerOnTraxis' => $dataProductsOfACustomerOnTraxis,
            'getDevicesOfCustomerOnTraxis' => $dataDevicesOfCustomerOnTraxis,
            'getCustomerTags' => $dataCustomerTags,
            'success' => true,
            'message' => 'Registro encontrado com sucesso!'
        ];

        if ($getCustomerOfADeviceOnTraxis) {
            $dataCustomerOfADeviceOnTraxis = [
                'deviceId' => $getCustomerOfADeviceOnTraxis['@attributes']['deviceId']
            ];

            $data['getCustomerOfADeviceOnTraxis'] = $dataCustomerOfADeviceOnTraxis;

        } else {
            if (!empty($terminal)) {
                $data = [
                    'success' => false,
                    'message' => 'Smartcard não encontrado!'
                ];

                return $data;
            }
        }

        return $data;
    }

    public function getBase($id)
    {
        $operadora_base = [
            # NETABC
            '386' => 'NETABC', '404' => 'NETABC', '409' => 'NETABC', '413' => 'NETABC',
            '423' => 'NETABC', '100' => 'NETABC', '456' => 'NETABC', '474' => 'NETABC',
            '487' => 'NETABC', '573' => 'NETABC', '577' => 'NETABC', '659' => 'NETABC',
            '668' => 'NETABC', '715' => 'NETABC', '746' => 'NETABC', '756' => 'NETABC',
            '791' => 'NETABC', '121' => 'NETABC', '833' => 'NETABC', '856' => 'NETABC',
            '863' => 'NETABC', '865' => 'NETABC', '976' => 'NETABC', '027' => 'NETABC',
            '268' => 'NETABC', '076' => 'NETABC', '103' => 'NETABC', '104' => 'NETABC',
            '129' => 'NETABC', '897' => 'NETABC', '141' => 'NETABC', '143' => 'NETABC',
            '162' => 'NETABC', '187' => 'NETABC', '207' => 'NETABC', '235' => 'NETABC',
            '136' => 'NETABC', '223' => 'NETABC', '269' => 'NETABC', '291' => 'NETABC',

            # NETBH
            '013' => 'NETBH',

            # NETBRASIL
            '015' => 'NETBRASIL', '040' => 'NETBRASIL', '010' => 'NETBRASIL', '038' => 'NETBRASIL',
            '004' => 'NETBRASIL', '228' => 'NETBRASIL',

            # NETISP
            '008' => 'NETISP', '052' => 'NETISP', '011' => 'NETISP', '056' => 'NETISP',
            '054' => 'NETISP', '055' => 'NETISP', '009' => 'NETISP', '005' => 'NETISP',
            '053' => 'NETISP', '006' => 'NETISP', '455' => 'NETISP', '333' => 'NETISP',
            '752' => 'NETISP', '070' => 'NETISP', '133' => 'NETISP', '584' => 'NETISP',
            '389' => 'NETISP', '603' => 'NETISP', '917' => 'NETISP', '753' => 'NETISP',
            '757' => 'NETISP', '154' => 'NETISP', '720' => 'NETISP', '733' => 'NETISP',
            '043' => 'NETISP', '770' => 'NETISP', '066' => 'NETISP', '285' => 'NETISP',
            '541' => 'NETISP', '710' => 'NETISP', '330' => 'NETISP', '895' => 'NETISP',
            '550' => 'NETISP', '750' => 'NETISP', '847' => 'NETISP', '529' => 'NETISP',
            '152' => 'NETISP', '832' => 'NETISP', '758' => 'NETISP', '852' => 'NETISP',
            '930' => 'NETISP', '405' => 'NETISP', '193' => 'NETISP', '126' => 'NETISP',
            '197' => 'NETISP', '275' => 'NETISP', '030' => 'NETISP', '250' => 'NETISP',
            '220' => 'NETISP', '036' => 'NETISP', '363' => 'NETISP', '566' => 'NETISP',
            '742' => 'NETISP', '209' => 'NETISP', '338' => 'NETISP', '803' => 'NETISP',
            '806' => 'NETISP', '336' => 'NETISP', '359' => 'NETISP', '294' => 'NETISP',
            '419' => 'NETISP', '514' => 'NETISP', '437' => 'NETISP', '565' => 'NETISP',
            '312' => 'NETISP', '278' => 'NETISP', '540' => 'NETISP', '954' => 'NETISP',
            '609' => 'NETISP', '067' => 'NETISP', '305' => 'NETISP', '923' => 'NETISP',
            '332' => 'NETISP', '834' => 'NETISP',

            # NETSOROCABA
            '471' => 'NETSOROCABA', '661' => 'NETSOROCABA', '769' => 'NETSOROCABA', '820' => 'NETSOROCABA',
            '195' => 'NETSOROCABA', '272' => 'NETSOROCABA', '794' => 'NETSOROCABA', '641' => 'NETSOROCABA',
            '430' => 'NETSOROCABA', '531' => 'NETSOROCABA', '907' => 'NETSOROCABA', '480' => 'NETSOROCABA',
            '399' => 'NETSOROCABA', '007' => 'NETSOROCABA', '178' => 'NETSOROCABA', '443' => 'NETSOROCABA',
            '194' => 'NETSOROCABA', '105' => 'NETSOROCABA', '530' => 'NETSOROCABA', '533' => 'NETSOROCABA',
            '567' => 'NETSOROCABA', '719' => 'NETSOROCABA', '161' => 'NETSOROCABA', '595' => 'NETSOROCABA',
            '599' => 'NETSOROCABA', '656' => 'NETSOROCABA', '714' => 'NETSOROCABA', '722' => 'NETSOROCABA',
            '765' => 'NETSOROCABA', '239' => 'NETSOROCABA', '882' => 'NETSOROCABA', '095' => 'NETSOROCABA',
            '226' => 'NETSOROCABA', '237' => 'NETSOROCABA', '924' => 'NETSOROCABA', '022' => 'NETSOROCABA',
            '048' => 'NETSOROCABA', '295' => 'NETSOROCABA', '621' => 'NETSOROCABA', '316' => 'NETSOROCABA',
            '096' => 'NETSOROCABA', '216' => 'NETSOROCABA', '063' => 'NETSOROCABA', '276' => 'NETSOROCABA',
            '858' => 'NETSOROCABA', '488' => 'NETSOROCABA', '572' => 'NETSOROCABA', '795' => 'NETSOROCABA',
            '637' => 'NETSOROCABA', '024' => 'NETSOROCABA', '241' => 'NETSOROCABA', '508' => 'NETSOROCABA',
            '507' => 'NETSOROCABA', '097' => 'NETSOROCABA', '042' => 'NETSOROCABA', '230' => 'NETSOROCABA',

            # NETSP
            '003' => 'NETSP',

            # NETSUL
            '093' => 'NETSUL', '690' => 'NETSUL', '688' => 'NETSUL', '700' => 'NETSUL',
            '890' => 'NETSUL', '087' => 'NETSUL', '687' => 'NETSUL', '696' => 'NETSUL',
            '089' => 'NETSUL', '685' => 'NETSUL', '884' => 'NETSUL', '881' => 'NETSUL',
            '695' => 'NETSUL', '083' => 'NETSUL', '088' => 'NETSUL', '086' => 'NETSUL',
            '689' => 'NETSUL', '996' => 'NETSUL', '091' => 'NETSUL', '686' => 'NETSUL',
            '878' => 'NETSUL', '693' => 'NETSUL', '691' => 'NETSUL', '078' => 'NETSUL',
            '880' => 'NETSUL', '692' => 'NETSUL', '694' => 'NETSUL', '075' => 'NETSUL',
            '879' => 'NETSUL', '684' => 'NETSUL', '851' => 'NETSUL', '678' => 'NETSUL',
            '246' => 'NETSUL', '910' => 'NETSUL', '037' => 'NETSUL', '459' => 'NETSUL',
            '279' => 'NETSUL', '282' => 'NETSUL', '493' => 'NETSUL', '821' => 'NETSUL',
            '277' => 'NETSUL', '871' => 'NETSUL', '115' => 'NETSUL', '560' => 'NETSUL',
            '498' => 'NETSUL', '458' => 'NETSUL', '322' => 'NETSUL', '058' => 'NETSUL',
            '739' => 'NETSUL', '203' => 'NETSUL', '798' => 'NETSUL', '379' => 'NETSUL',
            '543' => 'NETSUL', '506' => 'NETSUL', '163' => 'NETSUL', '420' => 'NETSUL'
        ];

        return $operadora_base[$id];
    }
}