<?php

namespace App\Services\Units;

use App\Models\Units\UnitsSolicitation;
use App\Models\Units\UnitsMenu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;

class UnitsAcitivitiesLogService
{

    /**
     * @var UnitsSolicitation
     */
    private $unitsSolicitation;
    private $unitsMenu;

    public function __construct(
        UnitsSolicitation $unitsSolicitation,
        UnitsMenu $unitsMenu
    )
    {
        $this->unitsSolicitation = $unitsSolicitation;
        $this->unitsMenu = $unitsMenu;
    }


    public function datatable(Request $request)
    {
        $solicitations = $this->getData($request);

        return Datatables::of($solicitations)
            ->addColumn('service_description', function ($solicitation) {
                return $solicitation->service->description ?? '';
            })
            ->addColumn('action', function ($solicitation){
                return '<a class="btn tooltips btn-xs btn-success show-details" href="javascript:void(0);" data-log_active_id="'.$solicitation['id'].'"> <i class="fa fa-search"></i> </a>';
            })
            ->make();
    }

    public function getData($request)
    {
        $solicitations = $this->unitsSolicitation
            ->select(['id', 'username', 'status', 'units_service_id', 'created_at', 'terminal'])->with('service');

        if ($request->has('date_ini_filter') && $request->get('date_ini_filter') != '' &&
            $request->has('date_end_filter') && $request->get('date_end_filter') != '') {
            $solicitations->byPeriod($request->get('date_ini_filter'), $request->get('date_end_filter'));
        }

        if ($request->has('user_filter') && $request->get('user_filter') != '') {
            $solicitations->byUsername($request->get('user_filter'));
        }

        if ($request->has('service_filter') && $request->get('service_filter') != '') {
            $solicitations->byService($request->get('service_filter'));
        }

        if ($request->has('terminal_filter') && $request->get('terminal_filter') != '') {
            $solicitations->byTerminal($request->get('terminal_filter'));
        }

        $x = $solicitations->get();

        $line = [];

        foreach ($x as $sl) {

            $menu = $this->unitsMenu->bySlug($sl->service->slug)->first();

            $line[] = [
                'id' => $sl->id,
                'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $sl->created_at)->format('d/m/Y H:i:s'),
                'username' => $sl->username,
                'terminal' => $sl->terminal,
                'units_service_id' => $sl->units_service_id,
                'service' => strtoupper($menu->title . ' - ' . $sl->service->description),
                'status' => $sl->status,
            ];

        }

        return $line;

    }


}