<?php

namespace App\Services\Units;

use App\Models\Units\UnitsMenu;
use App\Models\Units\UnitsMenuOrder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

class UnitsMenuService
{

	/**
	 * @var UnitsMenu
	 */
	private $unitsMenu;
	/**
	 * @var UnitsMenuOrder
	 */
	private $unitsMenuOrder;

	public function __construct(
		UnitsMenu $unitsMenu,
		UnitsMenuOrder $unitsMenuOrder
	)
	{
		$this->unitsMenu = $unitsMenu;
		$this->unitsMenuOrder = $unitsMenuOrder;
	}

	public function makeMenu($type=null)
	{
		$data = $this->unitsMenu->select('id')->get()->toArray();
		$order = $this->unitsMenuOrder->first();

		if ($order){
			$order = json_decode($order->menu_order, true);
			foreach (Arr::flatten($data) as $d){
				if (!in_array($d, Arr::flatten($order))){
					$order = Arr::add($order, count($order), ['id' => $d]);
				}
			}
			$data = $order;
		}

		if ($type == 'Nettable'){
			return $this->buildNettable($data);
		}

		return $this->buildMenu($data);
	}

	public function create($request)
	{
		try {

			$request['company_id'] = auth()->auth()->company_id;
			$this->unitsMenu->create($request);

			return response()->json([
				'title' => 'Sucesso!',
				'message' => 'Registro incluido com sucesso'
			], 200);


		} catch (\Exception $e){

			return response()->json([
				'title' => 'Erro!',
				'message' => 'Ocorreu um erro ao tentar incluir esse registro \n'.$e->getMessage()
			], 400);

		}

	}

	public function update($request)
	{
		try {

			$menus = $this->unitsMenu->find($request['id']);
			$menus->update($request);

			return response()->json([
				'title' => 'Sucesso!',
				'message' => 'Registro atualizado com sucesso'
			], 200);


		} catch (\Exception $e){

			return response()->json([
				'title' => 'Erro!',
				'message' => 'Ocorreu um erro ao tentar atualizar esse registro \n'.$e->getMessage()
			], 400);

		}
	}

	public function updateOrder($request)
	{

		try {

			$data['menu_order'] = $request['menu'];
			$order = $this->unitsMenuOrder->first();

			if ($order){
				$order->update($data);
			}else{
				$data['company_id'] = auth()->user()->company_id;
				$order->create($data);
			}

			return response()->json([
				'title' => 'Sucesso!',
				'message' => 'Registro alterado com sucesso'
			], 200);


		} catch (\Exception $e){

			return response()->json([
				'title' => 'Erro!',
				'message' => 'Ocorreu um erro ao tentar atualizar esse registro \n'.$e->getMessage()
			], 400);

		}
	}


	private function buildNettable($menu)
	{
		$result = null;
		foreach ($menu as $m) {
			$dataMenu = $this->getDataMenu($m['id']);
			if(isset($dataMenu)){
				if (!isset($m['children'])){
					$result .= "
						<li class='dd-item dd3-item' data-id='{$m['id']}'>
		                    <div class='dd-handle dd3-handle'> </div>
		                    <div class='dd3-content edit' data-title='{$dataMenu->title}' data-id='{$dataMenu->id}'> {$dataMenu->title}</div>
						</li>";
				} else {
					$result .= "
						<li class='dd-item dd3-item' data-id='{$m['id']}'>
		                    <div class='dd-handle dd3-handle'> </div>
		                    <div class='dd3-content edit' data-title='{$dataMenu->title}' data-id='{$dataMenu->id}'> {$dataMenu->title}</div>
		                    ".$this->buildNettable($m['children'])."
						</li>";
				}
			}
		}
		return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
	}


	private function buildMenu($menu)
	{
		$result = null;
		foreach ($menu as $m) {

			$dataMenu = $this->getDataMenu($m['id']);
			if(isset($dataMenu)){

				$active = (request()->is($dataMenu->slug)) ? 'active open' : '';

				$url = url($dataMenu->slug);

				if ($dataMenu->slug == 'javascript:void(0);') {
                    $url = "#";
                }

				if (!isset($m['children'])){

					if (Gate::allows($dataMenu['permission'])) {

						$result .= "
					        <li class='nav-item ".$active."'>
					            <a href='$url' class='nav-link nav-toggle'>
					                <span class='title'>{$dataMenu->title}</span>
					            </a>
					        </li>
				        ";
					}

				} else {

					if (Gate::allows($dataMenu['permission'])) {
						$result .= "
					        <li class='nav-item ".$active."'>
					            <a href='$url' class='nav-link nav-toggle'>
					                <span class='title'>{$dataMenu->title}</span>
					                <span class='arrow'></span>
					            </a>
					            ".$this->buildMenu($m['children'])."
					        </li>
					    ";
					}

				}

			}

		}
		return $result ?  "\n<ul class='sub-menu'>\n$result</ul>\n" : null;
	}

	private function getDataMenu($id)
	{
		return $this->unitsMenu->find($id);
	}




}