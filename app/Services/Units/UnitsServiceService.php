<?php

namespace App\Services\Units;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Models\Units\UnitsService;
use App\Http\Resources\UnitsServiceCollection;
use App\Http\Resources\UnitsServiceResource;
use phpDocumentor\Reflection\Types\Array_;

class UnitsServiceService { 

  private $units_service;

  public function __construct(UnitsService $units_service)
  {
    $this->units_service = $units_service;
  }

  /**
   * Retorna um único registro
   *
   * @param [type] $id
   * @return void
   */
  public function find($id)
  {
    $data = UnitsService::find($id);
    if (!$data) {
      throw new ModelNotFoundException('Serviço não encontrado com o ID ' . $id);
    }
    return new UnitsServiceResource($data);
  }

  public function whereIn(array $slugs)
  {
      $data = UnitsService::whereIn('slug', $slugs)->get();
      if (!$data) {
          throw new ModelNotFoundException('Serviço não encontrado');
      }
      return new UnitsServiceResource($data);
  }

}