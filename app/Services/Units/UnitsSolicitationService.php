<?php

namespace App\Services\Units;

use App\Models\Units\UnitsService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\Units\UnitsSolicitation;

class UnitsSolicitationService { 

      private $units_solicitation;
      private $units_services;
      private $now;

    public function __construct(
        UnitsSolicitation $units_solicitation,
        UnitsService $units_services
    )
    {
        $this->units_solicitation = $units_solicitation;
        $this->units_services = $units_services;
        $this->now = Carbon::now();
    }

    public function store(Array $data)
    {
        $create = $this->units_solicitation->create($data);
        if (!$create) {
            return false;
        }
        return true;
    }

    public function getQtdSolicitations($month = false, $year = false)
    {

        (!$month) ? $month = $this->now->month : $month = $month ;
        (!$year) ? $year = $this->now->year : $year = $year ;

        $solicitacoes = $this->units_solicitation->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->where('status', '!=', 'P')
            ->get();
        return $solicitacoes;
    }

    public function whereIn(string $column, array $services, $month = false, $year = false)
    {
        (!$month) ? $month = $this->now->month : $month = $month ;
        (!$year) ? $year = $this->now->year : $year = $year ;

        $data = $this->units_solicitation->whereIn($column, $services)
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->select(\DB::raw('DAY(created_at) as dia'), \DB::raw('count(id) as qtdByDay'))
            ->groupBy('dia')
            ->get();

        if (!$data) {
            throw new ModelNotFoundException('Não há solicitações para estes serviços');
        }
        return $data;
    }

    public function getAnaliticalData(array $subServices, $month = false, $year = false)
    {
        $now = Carbon::now();

        (!$month) ? $month = $now->month : $month = $month ;
        (!$year) ? $year = $now->year : $year = $year ;

        $data = $this->units_solicitation->whereIn('slug', $subServices)
        ->whereMonth('units_solicitations.created_at', $month)
        ->whereYear('units_solicitations.created_at', $year)
        ->join('units_services', 'units_services.id', '=', 'units_solicitations.units_service_id')
        ->select('units_services.slug as slug', \DB::raw('count(units_solicitations.id) as qtd'))
        ->groupBy('slug')
//        ->orderBy('slug', 'asc')
        ->get();

        if (!$data) {
            throw new ModelNotFoundException('Não há solicitações para estes serviços');
        }
        return $data;
    }
}