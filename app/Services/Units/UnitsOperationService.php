<?php

namespace App\Services\Units;

use App\Models\Units\UnitsOperation;
use App\Http\Resources\UnitsOperationCollection;
use App\Http\Resources\UnitsOperationResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * UnitsOperationService class
 */
class UnitsOperationService { 

  /**
   * Undocumented variable
   *
   * @var [type]
   */
  private $units_operation;

  /**
   * Undocumented function
   *
   * @param UnitsOperation $units_operation
   */
  public function __construct(UnitsOperation $units_operation)
  {
    $this->units_operation = $units_operation;
  }

  /**
   * Retorna os registros
   *
   * @return void
   */
  public function all()
  {
    $data = $this->units_operation->orderedByCity()->baseNotCtv()->get();
    if(!$data){
      throw new ModelNotFoundException('Nenhuma operadora encontrada.');
    }
    return new UnitsOperationCollection($data);
  }


  /**
   * Retorna um único registro
   *
   * @param [type] $id
   * @return void
   */
  public function find($id)
  {
    $data = UnitsOperation::find($id);
    if (!$data) {
      throw new ModelNotFoundException('Nenhuma operadora encontrada com o ID ' . $id);
    }
    return new UnitsOperationResource($data);
  }

}