<?php

namespace App\Services\Units;

use App\Models\SolverGroups;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UnitsSiteShortcutService {

  /**
	 * @var SolverGroups
	 */
  private $solverGroups;

  /**
   * Construct function
   *
   * @param SolverGroups $solverGroups
   */
  public function __construct(SolverGroups $solverGroups)
  {
    $this->solverGroups = $solverGroups;
  }

  public function usersSolverGroups()
	{
		$groups = auth()->user()->groups->pluck('id')->toArray();
		$userGroups = $this->solverGroups
			->whereHas('groups', function ($query) use ($groups) {
				$query->whereIn('groups.id', $groups);
			})
			->pluck('name', 'id')
			->toArray();
		return $userGroups;
	}

}