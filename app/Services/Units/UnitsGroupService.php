<?php

namespace App\Services\Units;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Units\UnitsGroup;

class UnitsGroupService { 

  private $units_group;

  public function __construct(UnitsGroup $units_group)
  {
    $this->units_group = $units_group;
  }

}