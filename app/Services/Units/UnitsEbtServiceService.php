<?php

namespace App\Services\Units;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Units\UnitsEbtService;

class UnitsEbtServiceService { 

  private $units_ebt_service;

  public function __construct(UnitsEbtService $units_ebt_service)
  {
    $this->units_ebt_service = $units_ebt_service;
  }

}