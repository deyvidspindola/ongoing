<?php

namespace App\Services;

use App\Models\OffenderHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class OffenderHistoryService
{

    /**
     * @var OffenderHistory
     */
    private $offenderHistory;

    public function __construct(OffenderHistory $offenderHistory)
    {
        $this->offenderHistory = $offenderHistory;
    }

	public function search($search)
	{
		header("Content-Type: application/json; charset=UTF-8");
		$returns = $this->offenderHistory
			->selectRaw("JSON_UNQUOTE(json_extract(snapshot, '$.offender_id')) as offender_id, JSON_UNQUOTE(json_extract(snapshot, '$.offender')) as name, JSON_UNQUOTE(json_extract(snapshot, '$.type')) as type")
			->distinct()
			->whereRaw("UPPER(json_extract(snapshot, '$.offender')) like '%" . strtoupper($search['query']) . "%'")
			->orWhereRaw("json_extract(snapshot, '$.offender_id') = '" . $search['query']  . "'")
			->get()
			->toArray();

		return json_encode($returns);
	}

	public function datatable(Request $request)
	{
		$offenders = $this->offenderHistory
			->select(['id', 'offender_id', 'snapshot', 'operation_type', 'created_at', 'updated_at'])->orderby('id', 'desc');

		if ($request->has('type_filter') && $request->get('type_filter') != 'Todos') {
			$offenders->byType($request->get('type_filter'));
		}

		if ($request->has('id_offender_filter') && $request->get('id_offender_filter') != '') {
			$offenders->byOffenderId($request->get('id_offender_filter'));
		}

		if ($request->has('date_ini_filter') && $request->get('date_ini_filter') != '' &&
			$request->has('date_end_filter') && $request->get('date_end_filter') != '') {
			$offenders->byPeriod($request->get('date_ini_filter'), $request->get('date_end_filter'));
		}

		if($request->get('solver_group_filter') != 'Todos') {
            $offenders->whereRaw("json_extract(snapshot, '$.solver_group_id') = {$request->get('solver_group_filter')}");
        }

		return Datatables::of($offenders)

            ->addColumn('offender_id', function ($offenders) {
                return $offenders->snapshot['offender_id'];
            })

			->addColumn('offender', function ($offenders) {
                $ret = '';
			    if(array_key_exists('offender', $offenders->snapshot)) {
                    $ret = $offenders->snapshot['offender'];
                }

			    return $ret;
			})

			->addColumn('type', function ($offenders) {
				return config('ongoing.history_types.'.$offenders->operation_type);
			})

			->addColumn('action', function ($offenders) {
				$html = "";

				if(auth()->user()->can('offender_list')) {
					$html .= '<a class="btn tooltips btn-xs btn-success show-details" href="javascript:void(0);" data-route="'.route('offender.history.show-details', $offenders->id).'"> <i class="fa fa-search"></i> </a>';
				}

				return $html;
			})
			->make();

	}

    public function usersSolverGroups()
    {
        $groups = auth()->user()->groups->pluck('id')->toArray();
        $userGroups = $this->solverGroups
            ->whereHas('groups', function ($query) use ($groups) {
                $query->whereIn('groups.id', $groups);
            })
            ->pluck('name', 'id')
            ->toArray();
        return $userGroups;
    }

}