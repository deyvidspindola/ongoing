<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class UserService
{
    /**
     * @var User
     */
    private $user;

    public function __construct(
        User $user
    )
    {
        $this->user = $user;
    }

    public function store(array $request)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Validação do username se for admin
                if ($request['username'] == config('sys.parameters.admin_user')) {
                    throw new \Exception(Lang::get('table.usuario_cadastrado'));
                }
                // Validação se já existe o usuário na empresa
                $user = $this->user->byUsername($request['username'])
                                   ->byCompany(session('company')['id'])
                                   ->first();

                // Se já existe o usuário cadastrado
                if($user)

                    throw new \Exception(Lang::get('table.usuario_cadastrado'));

                // Faço a criptografia do password
                $request['password_cache'] = cryptArt($request['password']);
                $request['password']       = bcrypt($request['password']);

                // Recupero a empresa
                $request['company_id'] = session('company')['id'];

                // Cadastro o Usuário
                $this->user->create($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $request, $id)
    {
        try {

            // Cadastro com transação
            DB::transaction(function() use ($request, $id) {

                // Recupero o usuário
                $user = $this->user->byCompany(session('company')['id'])
                                   ->removeAdmin()
                                   ->find($id);

                // Verifico se usuário existe
                if (!$user) {
                    throw new \Exception(Lang::get('sys.msg.not_found'));
                }
                // Verifico se o (username) esta no array, caso esteja eu retiro
                if (array_key_exists('username', $request)) {
                    unset($request['username']);
                }
                // Verifico se está alterando o password
                if(!empty($request['password'])) {

                    $request['password_cache'] = cryptArt($request['password']);
                    $request['password']       = bcrypt($request['password']);

                }
                else{
                    unset($request['password']);
                }
                // Atualizo o Usuário
                $user->update($request);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }


    public function updatePassword($request)
    {

        try {

            // Cadastro com transação
            DB::transaction(function() use ($request) {

                // Verifico se as senhas são iguais
                if ($request->new_password != $request->confirm_new_password) {
                    throw new \Exception( Lang::get('table.as_senhas_nao_correspondem'));
                }
                // Recupero os dados do usuário logado
                $user = $this->user->find(Auth::user()->id);

                if (!$user) {
                    throw new \Exception(config(Lang::get('sys.msg.not_found')));
                }
                // Verifico se a senha atual é valida
                if (!Auth::attempt(['username' => $user->username, 'password' => $request->password])) {
                    throw new \Exception(Lang::get('table.senha_atual_invalida'));
                }
                // Atualizo a senha
                $user->update([
                    'password' => bcrypt($request->new_password)
                ]);

            });

            return true;

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy($id)
    {

        try {

            // Recupero o usuário
            $user = $this->user->byCompany(session('company')['id'])
                               ->removeAdmin()
                               ->find($id);

            // Verifico se usuário existe
            if (!$user) {
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }
            // Excluo os grupos vinculados caso exista
            UserGroup::where('user_id', '=', $id)->delete();

            // Excluo o usuário
            $user->delete();

            return true;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}