<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

	    View::composer(
		    'administrador.menu_units.nav', 'App\Http\Controllers\ViewComposers\MenuComposer'
	    );
//	    View::composer(
//	        'layouts.inc.breadcrumb', 'App\Http\Controllers\ViewComposers\BreadcrumbComposer'
//        );

    }
}
