<?php

if (!function_exists('formatGroupsLdap')) {

    function formatGroupsLdap($groups)
    {

        $result = [];

        foreach ($groups as $group) {

            $tmp = explode(',', $group);

            if (count($tmp) > 1) {

                $tmp_row = explode('=', $tmp[0]);

                $result[] = $tmp_row[1];

            }

        }

        return $result;

    }

}

if (!function_exists('in_array_groups')) {

    function in_array_groups($groups, $groups_ad)
    {

        // verifico se existe elementos iguais nos arrays
        $intersect = array_intersect($groups, $groups_ad);

        return (count($intersect)) ? true : false;

    }

}

// -- --------------------------------------------------------------
// -- BUSCA DE USUÁRIOS E GRUPOS NO AD
// -- --------------------------------------------------------------

if (!function_exists('getLdap')) {

    // $connection
    // $username
    // $domain
    // $password
    // $dn          - Base DN
    // $tipo        - Tipo de busca (Group ou User)
    // $order       - Se será ordenado (true / false)

    function getLdap($connection, $username, $domain, $password, $dn, $order = false){

        $connect = ldap_connect($connection);

        // Definir algumas opções do LDAP para falar com AD
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        // Essa é a conta LDAP administrador com acesso
        $username  = $username.'@'.$domain;

        // Bind como um administrador de domínio
        $ldap_bind = @ldap_bind($connect, $username, $password);

        // Se não foi possivel fazer a conexão
        if(!$ldap_bind) return [];

        // Defino o tipo de busca, somente ativos
        $search = '(&(distinguishedname=*)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))';

        // caminho para a busca
        $search = ldap_search($connect, $dn, $search, array('cn', 'samaccountname'));

        // comando get
        $get_entries = ldap_get_entries($connect, $search);

        // Encerro a conexão
        ldap_unbind($connect);

        // Formato os grupos do AD
        $result_ad = formatResultLdap($get_entries, $order);

        return $result_ad;

    }

}

// Função para retornar o resultado formatado
if (!function_exists('formatResultLdap')) {

    function formatResultLdap($valores, $ordem = false){

        if(!is_array($valores)) return $valores;

        $resultado = [];

        foreach($valores as $key => $valor){

            if(is_numeric($key) && array_key_exists('samaccountname', $valor) && array_key_exists('cn', $valor))

                $resultado[$valor['samaccountname'][0]] = $valor['cn'][0];

        }

        if($ordem) asort($resultado);

        return $resultado;

    }

}