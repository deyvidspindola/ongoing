<?php

use Illuminate\Support\Arr;
use Carbon\Carbon;

if (!function_exists('modified_fields')) {

	function modified_fields($array, $array_except)
	{
		$return = Arr::except(explode(', ', $array), $array_except);
		return implode(', ', $return);
	}

}

if (!function_exists('object2array')) {
    function object2array($object) { return @json_decode(@json_encode($object),1); }
}

if (!function_exists('formatReferenciaBySmsCampanha')) {

    function formatReferenciaBySmsCampanha($referencia)
    {

        $array = explode(' a ', $referencia);

        if(count($array) > 1){

            $arrayData = explode('/', $array[1]);

            return ucfirst(strtolower(intToMesAbreviado($arrayData[1]))).'/'.$arrayData[2];

        }

        return '';

    }

}

if (!function_exists('permiteEstorno')) {

    function permiteEstorno($dtTransacao, $valorTransacao, $clienteSaldo, $userType)
    {
        // Valor da transação superior ao crédito atual
        if($valorTransacao > $clienteSaldo)

            return false;

        // Limite de dias de acordo com o perfil

        $date1 = date_create($dtTransacao);
        $date2 = date_create(date('Y-m-d H:i:s'));
        $diffDate = date_diff($date1, $date2);

        $dias = intval($diffDate->format("%d"));

        $horas = intval($diffDate->format("%h"));

        $minutos = ($horas * 60) + intval($diffDate->format("%i"));

        // Operador 300 dias após transação
        if($userType == 3) {

            if ($dias > 300) return false;

        }

        // Cliente 15 minutos após transação
        elseif($userType == 4){

            if ($minutos > 15) return false;

        }
        else

            return false;


        return true;

    }

}

if (!function_exists('rmZerosLeftCnpj')) {

    function rmZerosLeftCnpj($string){

        $string = intval($string);

        // CPF completo
        if(strlen($string) == 11) {

            // verifico se de fato é um cpf
            // se não for completo com zero para o cnpj
            if(!validaCPF($string))

                $string = str_pad($string, 14, "0", STR_PAD_LEFT);

        }

        // CPF incompleto
        elseif(strlen($string) < 11) {

            $string = str_pad($string, 11, "0", STR_PAD_LEFT);

            // verifico se de fato é um cpf
            // se não for completo com zero para o cnpj
            if(!validaCPF($string))

                $string = str_pad($string, 14, "0", STR_PAD_LEFT);


        }
        // CNPJ incompleto
        elseif(strlen($string) >= 11 && strlen($string) < 14)

            $string = str_pad($string, 14, "0", STR_PAD_LEFT);


        return $string;

    }

}

if (!function_exists('gerarSenha')) {

    function gerarSenha($tamanho,  $simbolos = true, $maiusculas = true, $minusculas = true, $numeros = true){
        $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiúsculas
        $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
        $nu = "0123456789"; // $nu contem os números
        $si = "!@#$%¨&*()_+="; // $si contem os símbolos

        $senha = '';

        if ($maiusculas){
            // se $maiusculas for "true", a variável $ma é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($ma);
        }

        if ($minusculas){
            // se $minusculas for "true", a variável $mi é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($mi);
        }

        if ($numeros){
            // se $numeros for "true", a variável $nu é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($nu);
        }

        if ($simbolos){
            // se $simbolos for "true", a variável $si é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($si);
        }

        // retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variável $tamanho
        return substr(str_shuffle(str_shuffle(str_shuffle($senha))),0, $tamanho);
    }

}

if (!function_exists('dateFormat')) {

    function dateFormat($date, $type = "pt-BR")
    {

        if($type == "pt-BR")

            $date = date('d/m/Y H:i:s', strtotime($date));

        return $date;

    }

}

if (!function_exists('maskPhone')) {

    function maskPhone($number)
    {

        if(empty($number)) return $number;

        return (strlen($number) == 11)

            ?  mask($number, '(##) #####-####')

            :  mask($number, '(##) ####-####');



    }

}

if (!function_exists('isMobile')) {

    function isMobile()
    {

        $user_agents = ["iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric"];

        foreach($user_agents as $user_agent){

            if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE)

                return true;

        }

        return false;

    }

}


if (!function_exists('formatarNomeCompleto')) {

    function formatarNomeCompleto($string)
    {
        $array = explode(' ', $string);

        $array = array_filter($array);

        $desconsiderar = ['de', 'da', 'e', 'dos', 'do'];

        // Percorro cada elemento do array
        foreach($array as $keyName => $valueName){

            $valueName = mb_strtolower($valueName, 'UTF-8');

            $array[$keyName] = (in_array($valueName, $desconsiderar)) ? $valueName : ucfirst($valueName);

        }

        return implode(' ', $array);

    }

}

// Retorna o mes com zero a direita caso seja menor do que 10
if (!function_exists('formatMesComZero')) {

    function formatMesComZero($mes)
    {

        $mes = intval($mes);

        return ($mes < 10 ? "0{$mes}" : $mes);

    }

}

// De acordo com o periodo (01/05/2018 00:00 - 29/05/2018 23:59) separo
// o dia, mes e ano inicial e final
if (!function_exists('getSeparatePeriod')) {

    function getSeparatePeriod($period)
    {

        $array = explode(' - ', $period);

        $arrayPeriodoInicial = explode(' ', $array[0]);
        $arrayPeriodoFinal   = explode(' ', $array[1]);

        $arryDataInicial = explode('/', $arrayPeriodoInicial[0]);
        $arryDataFinal   = explode('/', $arrayPeriodoFinal[0]);

        return (object)[
            'inicial' => (object)[
                'dia' => intval($arryDataInicial[0]),
                'mes' => intval($arryDataInicial[1]),
                'ano' => intval($arryDataInicial[2]),
            ],
            'final' => (object)[
                'dia' => intval($arryDataFinal[0]),
                'mes' => intval($arryDataFinal[1]),
                'ano' => intval($arryDataFinal[2]),
            ]
        ];

    }

}

// Função que retorna um array com o numero de parcelas
if (!function_exists('getEnvironment')) {

    function getEnvironment()
    {

        $isValid = ['producao', 'homologacao'];

        $environment = env('APP_ENV', 'homologacao');

        $environment = (in_array($environment, $isValid) ? $environment : 'homologacao');

        return $environment;

    }

}

// Função que retorna um array com o numero de parcelas
if (!function_exists('selectCliente')) {

    function selectCliente($clientes)
    {

        if(empty($clientes)) return [];

        $array = [];

        foreach ($clientes as $cliente)

            $array[$cliente['id']] = maskCpfCnpj($cliente['username']).' - '.$cliente['name'];

        return $array;

    }

}

// Função que retorna um array com o numero de parcelas
if (!function_exists('getParcelas')) {

    function getParcelas($max = 12)
    {

        $installments = [];

        for ($i = 1; $i <= $max; $i++) {

            if($i == 1)

                $txt = '01';//'À  Vista';

            else{

                $txt = ($i < 10) ? '0'.$i : $i;

            }

            $installments[$i] = $txt;

        }

        return $installments;
    }

}

// Função que retorna um array com o numero de parcelas
if (!function_exists('getParcelasWithAmount')) {

    function getParcelasWithAmount($max = 12, $amount = null)
    {

        $installments = [];

        for ($i = 1; $i <= $max; $i++) {

            if($i == 1)

                $txt = '01';

            else{

                $txt = ($i < 10) ? '0'.$i : $i;

            }

            if($amount)

                $txt .= ' x R$ '.number_format(($amount / $i), 2, ',', '.');


            $installments[$i] = $txt;

        }

        return $installments;
    }

}


// LAYOUT XML NFe
// Função para formatar a string de xml dos produtos

if (!function_exists('formatXML_NFe')) {

    function formatXML_NFe($result)
    {

        // Caso não tenha resultado
        if(!$result) return [];

        // Recupero a string do XML para objeto
        $result = simplexml_load_string($result->xmlEnvio);

        $data = [];

        foreach($result->infNFe->det as $row){

            $row = (array) $row->prod;

            // Recupero a quantidade
            $quantidade = intval($row['qCom']);

            if($quantidade < 10) $quantidade = '0'.$quantidade;

            // Recupero o código do produto
            $codigo = $row['cProd'];

            // Recupero os valores
            $valorBruto = floatval($row['vProd']);

            $desconto = (array_key_exists('vDesc', $row)) ? floatval($row['vDesc']) : 0;

            $valorLiquido = $valorBruto - $desconto;

            $data[] = [
                'codigo'        => $codigo,
                'descricao'     => formatSpaceDescription($row['xProd']),
                'unidade'       => str_replace('.','',$row['uCom']),
                'quantidade'    => $quantidade,
                'valor_produto' => number_format(floatval($row['vUnCom']), 2, ',', '.'),
                'valor_bruto'   => number_format($valorBruto, 2, ',', '.'),
                'desconto'      => number_format($desconto, 2, ',', '.'),
                'valor_liquido' => number_format($valorLiquido, 2, ',', '.'),
            ];

        }

        return [
            'data'     => $data,
            'desconto' => number_format(floatval($result->infNFe->total->ICMSTot->vDesc), 2, ',', '.'),
            'total'    => number_format(floatval($result->infNFe->total->ICMSTot->vNF), 2, ',', '.'),
        ];

    }

    function formatSpaceDescription($string){

        // Quebro a string pelos pontos
        $string = explode('.', $string);

        // Retiro os elemtos vazios e nulos
        $string = array_filter($string);

        // Deixo a string separada por espaços
        return implode(' ', $string);

    }

}

// retorno select multiple formatar array request
if (!function_exists('requestSelectMultiple')) {

    function requestSelectMultiple($array)
    {

        if(empty($array))

            return null;

        foreach($array as $key => $value)

            $array[$key] = intval($value);

        return $array;
    }

}

// Mascaras

// $cnpj = "11222333000199";
// $cpf = "00100200300";
// $cep = "08665110";
// $data = "10102010";
//
// echo mask($cnpj,'##.###.###/####-##');
// echo mask($cpf,'###.###.###-##');
// echo mask($cep,'#####-###');
// echo mask($data,'##/##/####');

if (!function_exists('mask')) {
    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}

// Mascara de CPF ou CNPJ

if (!function_exists('maskCpfCnpj')) {
    function maskCpfCnpj($string)
    {

        // CPF
        if(strlen($string) == 11)

            return mask($string, '###.###.###-##');

        // CNPJ
        elseif(strlen($string) == 14)

            return mask($string, '##.###.###/####-##');

    }
}

if (!function_exists('unmaskCnpj')) {
    function unmaskCnpj($string)
    {

        $string = str_replace('.','', $string);
        $string = str_replace('/','', $string);
        $string = str_replace('-','', $string);

        return $string;

    }
}

// Validar CPF ou CNPJ
// somente valores numérios
if (!function_exists('validaCpfCnpj')) {

    function validaCpfCnpj($string){

        // CPF
        if(strlen($string) == 11)

            return validaCPF($string);

        // CNPJ
        elseif(strlen($string) == 14)

            return validaCNPJ($string);

        return false;

    }
}

// Validar CPF
if (!function_exists('validaCPF')) {

    function validaCPF($cpf = null)
    {

        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Verifico se é numero o campo
        if(!is_numeric($cpf)){
            return false;
        }

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

}

if (!function_exists('validaCNPJ')) {

    function validaCNPJ($cnpj)
    {

        // Verifica se um número foi informado
        if (empty($cnpj)) {
            return false;
        }

        // Verifico se é numero o campo
        if(!is_numeric($cnpj)){
            return false;
        }

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cnpj) != 14) {
            return false;
        }


        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000' ||
            $cnpj == '11111111111' ||
            $cnpj == '22222222222' ||
            $cnpj == '33333333333' ||
            $cnpj == '44444444444' ||
            $cnpj == '55555555555' ||
            $cnpj == '66666666666' ||
            $cnpj == '77777777777' ||
            $cnpj == '88888888888' ||
            $cnpj == '99999999999'
        ) {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CNPJ é válido
        } else {

            // Valida primeiro dígito verificador
            for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
                $soma += $cnpj{$i} * $j;
                $j = ($j == 2) ? 9 : $j - 1;
            }

            $resto = $soma % 11;

            if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
                return false;

            // Valida segundo dígito verificador
            for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
                $soma += $cnpj{$i} * $j;
                $j = ($j == 2) ? 9 : $j - 1;
            }

            $resto = $soma % 11;

            return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);

        }
    }

}


// Formatar valor para banco de dados
// Input  -> R$ 1.500,35
// Output -> 1500.35

if (!function_exists('formatMoneyForDb')) {

    function formatMoneyForDb($value)
    {

        $value = str_replace('R$', '', $value);
        $value = str_replace(' ', '', $value);
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);

        return number_format(floatval($value),2,'.', '');
    }

}

if (!function_exists('formatCardForDb')) {

    function formatCardForDb($value)
    {

        $value = str_replace(' ', '.', $value);

        $value = explode('.', $value);

        $value[1] = '****';
        $value[2] = '****';

        return implode('.', $value);

    }

}

if (!function_exists('formatMoneyForCielo')) {

    function formatMoneyForCielo($value, $formatarValor = true)
    {

        if($formatarValor)

            $value = formatMoneyForDb($value);

        // Sepero o real do centavo
        $value = explode('.', $value);

        // O valor enviado para cielo deve ser em centavos
        // multiplo a parte real por 100, para passar para a unidade de centavos
        $real    = intval($value[0]) * 100;
        $centavo = count($value) > 1 ? intval($value[1]) : 0;

        return $real + $centavo;

    }

}

if (!function_exists('formatCardForCielo')) {

    function formatCardForCielo($value)
    {
        return  str_replace(['.', ' '], ['', ''], $value);

    }

}

if (!function_exists('formatExpirationForCielo')) {

    function formatExpirationForCielo($data)
    {
        // formato (05/2026)

        $data = str_replace(' / ', '/20', $data);

        return trim($data);

    }

}

if (!function_exists('formatNameForCielo')) {

    function formatNameForCielo($value)
    {
        return  mb_strtoupper($value, 'UTF-8');
    }

}

if (!function_exists('getStates')) {


    function getStates()
    {
        return [
            'AC' => "Acre",
            'AL' => "Alagoas",
            'AM' => "Amazonas",
            'AP' => "Amapá",
            'BA' => "Bahia",
            'CE' => "Ceará",
            'DF' => "Distrito Federal",
            'ES' => "Espírito Santo",
            'GO' => "Goiás",
            'MA' => "Maranhão",
            'MT' => "Mato Grosso",
            'MS' => "Mato Grosso do Sul",
            'MG' => "Minas Gerais",
            'PA' => "Pará",
            'PB' => "Paraíba",
            'PR' => "Paraná",
            'PE' => "Pernambuco",
            'PI' => "Piauí",
            'RJ' => "Rio de Janeiro",
            'RN' => "Rio Grande do Norte",
            'RO' => "Rondônia",
            'RS' => "Rio Grande do Sul",
            'RR' => "Roraima",
            'SC' => "Santa Catarina",
            'SE' => "Sergipe",
            'SP' => "São Paulo",
            'TO' => "Tocantins"
        ];

    }
}

if (!function_exists('validateDate')) {

    function validateDate($dat)
    {
        $data = explode("/", "$dat"); // fatia a string $dat em pedados, usando / como referência
        $d = $data[0];
        $m = $data[1];
        $y = $data[2];

        // verifica se a data é válida!
        // 1 = true (válida)
        // 0 = false (inválida)

        return (checkdate($m, $d, $y)) ? true : false;

    }

}

if (!function_exists('soapResponseToArray')) {

    function soapResponseToArray($response, $tag)
    {

        $result = [];

        if(!$response) return $result;

        $response = str_ireplace('><', ">\n<", $response);

        $response = explode(PHP_EOL, $response);

        if(!is_array($response)) return $result;

        $tag_open = 0;

        $key = 0;

        foreach ($response as $row) {

            if ($tag_open) {

                if ($row != "</{$tag}>")
                    $result[$key][] = (strip_tags($row)) ?: null;
                else {
                    $tag_open = 0;
                    $key++;
                }

            } elseif ($row == "<{$tag}>")

                $tag_open = 1;

        }


        return $result;

    }

}

// Exemplo: (11022016, '##/##/####') -> 11/02/2016

if (!function_exists('mask_string')) {

    function maskString($string, $mask)
    {

        if(substr_count($mask, '#') != strlen($string))
            return $string;


        $string = str_replace(' ', '', $string);
        for ($i = 0; $i < strlen($string); $i++)
            $mask[strpos($mask, '#')] = $string[$i];

        return $mask;

    }
}

// Conversor de unidades

if (!function_exists('converterBytes')) {

    function converterBytes($valor, $de, $para)
    {

        switch($de){

            case 'KB' :

                switch ($para){

                    case 'B'  : $valor *= pow(2,10); break;
                    case 'KB' : break;
                    case 'MB' : $valor /= pow(2,10); break;
                    case 'GB' : $valor /= pow(2,20); break;
                    case 'TB' : $valor /= pow(2,30); break;
                    case 'PB' : $valor /= pow(2,40); break;
                    case 'EB' : $valor /= pow(2,50); break;

                }

                break;

            case 'MB' :

                switch ($para){

                    case 'B'  : $valor *= pow(2,20); break;
                    case 'KB' : $valor *= pow(2,10); break;
                    case 'MB' : break;
                    case 'GB' : $valor /= pow(2,10); break;
                    case 'TB' : $valor /= pow(2,20); break;
                    case 'PB' : $valor /= pow(2,30); break;
                    case 'EB' : $valor /= pow(2,40); break;

                }

                break;

            case 'GB' :

                switch ($para){

                    case 'B'  : $valor *= pow(2,30); break;
                    case 'KB' : $valor *= pow(2,20); break;
                    case 'MB' : $valor *= pow(2,10); break;
                    case 'GB' : break;
                    case 'TB' : $valor /= pow(2,10); break;
                    case 'PB' : $valor /= pow(2,20); break;
                    case 'EB' : $valor /= pow(2,30); break;

                }

                break;
        }

        return $valor;

    }

}

// -- --------------------------------------------------------------
// -- LISTAR DIRETORIOS
// -- --------------------------------------------------------------

// Formato do plugin dynatree

if (!function_exists('list_dir_dynatree')) {

    function list_dir_dynatree($dir_base)
    {

        if (!file_exists($dir_base)) return false;

        $dir = new \DirectoryIterator($dir_base);

        $diretorios = null;

        $node = 0;

        foreach ($dir as $file) {

            if (!$file->isDot() && $file->isDir()) {

                $diretorios[$node] = ["title" => $file->getFilename(), "key" => $file->getRealPath(), "isFolder" => true];

                $dir_base2 = $dir_base . '/' . $file->getFilename();

                $subDir = list_dir_dynatree($dir_base2);

                if ($subDir)

                    $diretorios[$node]["children"] = $subDir;

                $node++;

            }
        }

        return $diretorios;

    }

}

// array

if (!function_exists('list_dir')) {

    function list_dir($dir_base)
    {

        if (!file_exists($dir_base)) return false;

        $dir = new \DirectoryIterator($dir_base);

        $diretorios = null;

        foreach ($dir as $file) {

            if (!$file->isDot() && $file->isDir()) {

                $diretorios[$file->getRealPath()] = $file->getRealPath();


                $dir_base2 = $dir_base . '/' . $file->getFilename();

                $subDir = list_dir($dir_base2);


                if ($subDir)

                    $diretorios = array_merge($diretorios, $subDir);


            }
        }

        if($diretorios)

            $diretorios = array_unique($diretorios);


        return $diretorios;

    }

}


// Funções para controlar dia da semana

if (!function_exists('getDiasSemana')) {

    function getDiasSemana($abreviado = false){

        return ($abreviado)

            ? ['Dom.', 'Seg.', 'Ter.', 'Qua.', 'Qui.', 'Sex.', 'Sab.']

            : ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];

    }

}

if (!function_exists('dateToIntDiaSemana')) {

    function dateToDiaSemana($date, $tipo = 'inteiro'){

        return ($tipo == 'inteiro')
            ? date('w', strtotime($date))
            : getDiasSemana()[date('w', strtotime($date))];


    }

}

// Retorna os nomes dos meses
if(!function_exists('getMonthNames')){
    function getMonthNames($abbreviate = false){
        if(!$abbreviate){
            return [
                '01' => 'Janeiro',
                '02' => 'Fevereiro',
                '03' => 'Março',
                '04' => 'Abril',
                '05' => 'Maio',
                '06' => 'Junho',
                '07' => 'Julho',
                '08' => 'Agosto',
                '09' => 'Setembro',
                '10' => 'Outubro',
                '11' => 'Novembro',
                '12' => 'Dezembro'
            ];
        } else {
            return [
                '01' => 'Jan',
                '02' => 'Fev',
                '03' => 'Ma',
                '04' => 'Abr',
                '05' => 'Mai',
                '06' => 'Jun',
                '07' => 'Jul',
                '08' => 'Ago',
                '09' => 'Set',
                '10' => 'Out',
                '11' => 'Nov',
                '12' => 'Dez'
            ];
        }
    } 
}

/**
 * Retorna uma quantidade de anos definida, do atual, para os anteriores
 * se nenhum valor for adicionado, pega os 3 últimos anos
 */
if (!function_exists('getYears')) 
{
    function getYears($n_years = 3)
    {
      $now = Carbon::now();
      $this_year = $now->year; // ano_atual  
      $years_list = [];
      for($i = 0; $n_years > $i; $i++){
        $years_list[($this_year - $i)] = ($this_year - $i);
      }
      return $years_list;
    }
}

// recupera porta e host de uma string
if (!function_exists('getHostPort')) {

    function getHostPort($address){

        $tmp = explode('&', $address);

        $data['host'] = explode('=',$tmp[0])[1];
        $data['port'] = explode('=',$tmp[1])[1];

        return $data;

    }

}

// Verifica se existe vm online
if (!function_exists('isOnline')) {

    function isOnline($key, $array){

        return array_key_exists($key, $array) ? 1 : 0;

    }

}

// calcula a quantidade de clones a partir de uma master
if (!function_exists('getQtdClones')) {

    function getQtdClones($uniquename, $array){

        $count = 0;

        foreach($array as $key => $value){

            $result = explode('_' , $key);

            // Se for um clone e o prefixo do uniquename for igual ao da master clone
            if(count($result) > 1 && $result[0] == $uniquename)

                $count++;

        }

        return $count;

    }

}

// Criptografar
if (!function_exists('cryptArt')) {

    function cryptArt($senha = null){

        if(empty($senha)) return null;

        $senha = str_split($senha);

        $senha = array_reverse($senha);

        foreach($senha as $key => $value)

            $senha[$key] = base64_encode(rand(100,999).$value);

        $senha = json_encode($senha, JSON_FORCE_OBJECT);

        return base64_encode($senha);

    }

}

// Descriptografar
if (!function_exists('decryptArt')) 
{
    function decryptArt($senha = null)
    {
        if(empty($senha)) return null;

        $senha = base64_decode($senha);
        $senha = json_decode($senha, JSON_FORCE_OBJECT);

        foreach($senha as $key => $value)
            $senha[$key] = substr(base64_decode($value), 3, strlen($value));
        $tmp = array_reverse($senha);
        $senha = '';
        foreach($tmp as $key => $value)  $senha .= $value;
        return $senha;
    }
}

// Calcular tempo online

if (!function_exists('calcTimeOnline')) {

    function calcTimeOnline($ano, $mes, $dia, $hora, $minuto, $segundo){

        // Tranformo em inteiro os resultados
        $ano = intval($ano);
        $mes = intval($mes);
        $dia = intval($dia);
        $hora = intval($hora);
        $minuto = intval($minuto);
        $segundo = intval($segundo);

        $texto = '';

        if ($ano)
            $texto .= $ano . (($ano > 1) ? ' anos ' : ' ano ');

        if ($mes)
            $texto .= $mes . (($mes > 1) ? ' meses ' : ' mes ');

        if ($dia)
            $texto .= $dia . (($dia > 1) ? ' dias ' : ' dia ');

        if ($hora) {

            if ($dia) $texto .= ', ';

            $texto .= $hora . (($hora > 1) ? ' horas ' : ' hora ');
        }

        if ($minuto) {

            if ($hora) $texto .= 'e ';

            $texto .= $minuto . (($minuto > 1) ? ' minutos ' : ' minuto ');

        }

        if ($texto == '' && $segundo)
            $texto .= $segundo . (($segundo > 1) ? ' segundos ' : ' segundo ');

        if(!$ano && !$mes && !$dia && !$hora && $minuto && $segundo)
            $texto .= 'e '.$segundo . (($segundo > 1) ? ' segundos ' : ' segundo ');

        return $texto;

    }

}




if (!function_exists('intToMes')) {

    function intToMes($mes){

        $mes = intval($mes);

        switch ($mes){

            case 1: return 'Janeiro';
            case 2: return 'Fevereiro';
            case 3: return 'Março';
            case 4: return 'Abril';
            case 5: return 'Maio';
            case 6: return 'Junho';
            case 7: return 'Julho';
            case 8: return 'Agosto';
            case 9: return 'Setembro';
            case 10: return 'Outubro';
            case 11: return 'Novembro';
            case 12: return 'Dezembro';
            default: return 'Janeiro';

        }

    }

}

if (!function_exists('intToMesAbreviado')) {

    function intToMesAbreviado($mes){

        $mes = intval($mes);

        switch ($mes){

            case 1:  return Lang::get('table.JAN');
            case 2:  return Lang::get('table.FEV');
            case 3:  return Lang::get('table.MAR');
            case 4:  return Lang::get('table.ABR');
            case 5:  return Lang::get('table.MAI');
            case 6:  return Lang::get('table.JUN');
            case 7:  return Lang::get('table.JUL');
            case 8:  return Lang::get('table.AGO');
            case 9:  return Lang::get('table.SET');
            case 10: return Lang::get('table.OUT');
            case 11: return Lang::get('table.NOV');
            case 12: return Lang::get('table.DEZ');
            default: return Lang::get('table.JAN');

        }

    }

}

if (!function_exists('intToHora')) {

    function intToHora($hora){

        return ($hora < 10 ? "0{$hora}" : $hora) . ':00';

    }

}

// Retorna as colunas do BVE de acordo com um START
if (!function_exists('getColumnsBve')) {

    function getColumnsBve($start){

        return (object) [
            'id'       => $start->bve_id,
            'name'     => $start->bve_name,
            'protocol' => $start->bve_protocol,
            'host'     => $start->bve_host,
            'ip'       => $start->bve_ip,
            'port_api' => $start->bve_port_api,
        ];

    }

}



// -- --------------------------------------------------------
// -- LOGS REDE
// -- --------------------------------------------------------

// Formata o filtro
if (!function_exists('tcpdumpFormatFilter')) {

    function tcpdumpFormatFilter($tags){

        $tags = explode(',', $tags);

        foreach ($tags as $tag) $filter[] = "-e '{$tag}'";

        return implode(' ',$filter);
    }

}

if (!function_exists('tcpdumpFormat')) {

    function tcpdumpFormat($data, $tags){

        // caso não tenha conteúdo
        if(empty($data)) return [];

        // Recupero as tags
        $tags = explode(',',$tags);

        // Defino as palavras na cor vermelha
        foreach($tags as $tag)

            $tagsReplace[] = "<b style='color: red'>".strtoupper($tag)."</b>";

        // Realizo a troca do conteudo (case insensitive)
        $data = str_ireplace($tags, $tagsReplace, $data);

        // Gero array do resultado pela quebra de linha
        $data = explode(PHP_EOL, $data);

        // retiro as linhas vazias e nulas
        $data = array_filter($data);

        // Variavel de resultado
        $result = [];

        // Quantidade limite de indices no resultado
        $resultLimit = count($data); // Total de Registros
        $resultLimit = 2000;         // Limite definido

        // A cada 100 caracteres dou um espaço
        for($key = 0; $key < $resultLimit; $key++){

            // Finalizo se a chave não existe no conteudo
            if(!array_key_exists($key, $data)) break;

            // Recupero a linha
            $result[] = wordwrap($data[$key], 50, " ", 1);

        }

        return $result;

    }

}


// -- --------------------------------------------------------
// -- LOGS TECLADO
// -- --------------------------------------------------------

// Formata a data do log
if (!function_exists('keyloggerFormatDate')) {

    function keyloggerFormatDate($date){

        $array = explode('/', $date);

        $date = [];

        foreach($array as $value)

            $date[] = (intval($value) < 10) ? '0'.intval($value) : intval($value);

        return implode('/', $date);

    }

}

// Formata o texto de entrada
if (!function_exists('keyloggerFormat')) {

    function keyloggerFormat($key, $tipo, $isDetalhes = 0){

        // Formato o valor, retirando as tags '<>'

        $key = trim(str_replace('>', ']', str_replace('<', '[', $key)));

        // Caso seja para os detalhes, retorno o resultado

        if($isDetalhes) return $key;

        // Caso for enter troco por <br>

        //$key = str_replace('[ENTER]', '<br>', $key);

        // Defino as teclas de troca

        $keys = [
            '[ENTER]',
            '[SPACE]',
            '[TAB]',
            '[ESC]',
            '[BACKSPACE]',
            '[L-SHIFT]',
            '[R-SHIFT]',
        ];

        $keys1 = [
            '<br>',
            '&nbsp;',
            '&nbsp;&nbsp;&nbsp;&nbsp;',
            '',
            '',
            '',
            '',
        ];

        $keys2 = [
            '<span class="text-primary">[ENTER]</span>',
            '<span class="text-primary">[SPACE]</span>',
            '<span class="text-primary">[TAB]</span>',
            '<span class="text-primary">[ESC]</span>',
            '<span class="text-primary">[BACKSPACE]</span>',
            '<span class="text-primary">[L-SHIFT]</span>',
            '<span class="text-primary">[R-SHIFT]</span>',
        ];

        // Verifico se o tipo não é para conter os caracteres especiais
        // 0 - sem caracter especial
        // 1 - com caracter especial

        $key = (!$tipo)

            ? str_replace($keys, $keys1, $key)

            : str_replace($keys, $keys2, $key);

        // Retorno o resultado
        return $key;

    }

}

// Formata o texto de entrada
if (!function_exists('keyloggerFormat2')) {

    function keyloggerFormat2($key, $tipo, $isDetalhes = 0){

        // Formato o valor, retirando as tags '<>'

        $key = trim(str_replace('>', ']', str_replace('<', '[', $key)));

        // Caso seja para os detalhes, retorno o resultado

        if($isDetalhes) return $key;

        // Defino as teclas de troca

        $keys = [
            'Lcontrol',
            'Lmenu',
            'Delete',
            'Return',
            'Space',
            'Tab',
            '[ESC]',
            '[BACKSPACE]',
            'Lshift',
            'Rshift',

            'Home',
            'Right',
            'End',

            'Oem_1','Oem_2','Oem_3','Oem_4','Oem_5','Oem_6','Oem_7','Oem_8','Oem_9','Oem_10',

            'Back',

            'Oem_Comma',

            'Rcontrol',
            'Escape',

        ];

        $keys1 = [
            '',
            '',
            '',
            '<br>',
            '&nbsp;',
            '&nbsp;&nbsp;&nbsp;&nbsp;',
            '',
            '',
            '',
            '',

            '',
            '',
            '',

            '','','','','','','','','','',

            '',

            '',
            '',
        ];

        $keys2 = [
            spanKeys('[L-CONTROL]'),
            spanKeys('[LMENU]'),
            spanKeys('[DELETE]'),
            spanKeys('[ENTER]'),
            spanKeys('[SPACE]'),
            spanKeys('[TAB]'),
            spanKeys('[ESC]'),
            spanKeys('[BACKSPACE]'),
            spanKeys('[L-SHIFT]'),
            spanKeys('[R-SHIFT]'),
            spanKeys('[HOME]'),
            spanKeys('[RIGHT]'),
            spanKeys('[END]'),

            spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),spanKeys('[ACENTO]'),

            spanKeys('[BACKSPACE]'),

            spanKeys('[R-CONTROL]'),
            spanKeys('[ESC]'),
        ];

        // Verifico se o tipo não é para conter os caracteres especiais
        // 0 - sem caracter especial
        // 1 - com caracter especial

        $key = (!$tipo)

            ? str_replace($keys, $keys1, $key)

            : str_replace($keys, $keys2, $key);

        // Retorno o resultado
        return $key;

    }

    function spanKeys($key){
        return '<span class="text-primary">'.$key.'</span>';
    }

}

// Formata os detalhes
if (!function_exists('keyloggerFormatDetalhes')) {

    function keyloggerFormatDetalhes($detalhes){

        // Caso não tenha resultado
        if(empty($detalhes)) return null;

        return '<p>'.implode('</p><p>', $detalhes).'</p>';

    }

}

// Formata o conteudo, agrupando por janelas
if (!function_exists('keyloggerFormatConteudo')) {

    function keyloggerFormatConteudo($conteudo){

        if(empty($conteudo)) return null;

        // Percorro o conteudo agrupando as informações por janela

        $array = [];

        foreach($conteudo as $row){

            // Caso seja a primeira iteração
            if(empty($array)){

                $array[] = "<b>{$row['window']}</b><br><br>{$row['key']}";

                // Salvo o nome da janela atual
                $windowCurrent = $row['window'];

            }

            // Caso não seja mais a primeira iteração
            else{

                // Se estiver ainda estiver na mesma janela
                if($row['window'] == $windowCurrent){

                    // Recupero o indice do array da ultima janela
                    $key = (count($array) - 1);

                    // Concateno com a tecla
                    $array[$key] .= $row['key'];

                }

                // caso tenha mudado de Janela
                else{

                    $array[] = "<hr><b>{$row['window']}</b><br><br>{$row['key']}";

                    // Salvo o nome da janela atual
                    $windowCurrent = $row['window'];

                }

            }

        }

        // Retorno cada linha das janelas como um parágrafo
        return '<p>'.implode('</p><p>', $array).'</p>';

    }

}

// Formata o conteudo, agrupando por janelas
if (!function_exists('keyloggerFormatConteudoWindow')) {

    function keyloggerFormatConteudoWindow($conteudo){

        if(empty($conteudo)) return null;

        // Percorro o conteudo agrupando as informações por janela

        $window = [];

        foreach($conteudo as $row){

            // Caso seja a primeira iteração
            if(empty($window)){

                $window[] = [
                    'title'   => $row['window'],
                    'content' => $row['key']
                ];

                // Salvo o nome da janela atual
                $windowCurrent = $row['window'];

            }

            // Caso não seja mais a primeira iteração
            else{

                // Se estiver ainda estiver na mesma janela
                if($row['window'] == $windowCurrent){

                    // Recupero o indice do array da ultima janela
                    $key = (count($window) - 1);

                    // Concateno com a tecla
                    $window[$key]['content'] .= $row['key'];

                }

                // caso tenha mudado de Janela
                else{

                    $window[] = [
                        'title'   => $row['window'],
                        'content' => $row['key']
                    ];

                    // Salvo o nome da janela atual
                    $windowCurrent = $row['window'];

                }

            }

        }

        $html = '';

        // Percorro cada janela agrupada, formatando o HTML
        foreach ($window as $row) {

            $html .= '<div class="col-sm-12" >';
            $html .=    '<div class="panel panel-default">';
            $html .=        '<div class="panel-heading" >';
            $html .=            '<i class="fa fa-desktop"></i >';
            $html .=            "<b>{$row['title']}</b>";
            $html .=        '</div>';

            $html .=        '<div class="panel-body">';
            $html .=            $row['content'];
            $html .=        '</div>';
            $html .=    '</div>';
            $html .= '</div>';
        }

        return $html;

    }

}

// Formata o conteudo, agrupando por janelas
if (!function_exists('getPeriodo')) {

    function getPeriodo($request){

        // Caso esteja setado o período
        if(!empty($request->period))

            return $request->period;

        // Recupero o período do mês atual
        $mes = date('m');
        $ano = date('Y');
        $dia = date('d');

        return ("01/{$mes}/{$ano} 00:00 - {$dia}/{$mes}/{$ano} 23:59");

    }

}

if (!function_exists('formatPeriodo')) {

    function formatPeriodo($data){

        $data = explode(' ', $data);

        return implode('-',array_reverse(explode('/', $data[0]))).' '.$data[1];
    }

}

if (!function_exists('runLog')) {

    function runLog($message, $prefixo = 'api'){

        if(env('API_LOG')) {

            $message = '[ ' . date('Y-m-d H:i:s') . ' ] ' . $_SERVER['REMOTE_ADDR'] . ' :: ' . $message;

            $file = storage_path('logs/'.$prefixo.'_'. date('Y-m-d').'.txt');

            $command = "echo '{$message}' >> {$file}";

            shell_exec($command);

        }

    }

}

if (!function_exists('runLogRequest')) {

    function runLogRequest($method, $request){

        if(env('API_LOG')) {

            // Recupero o prefixo
            $prefixo = 'api';

            // Defino  data e Hora da inserção
            $messageTime = '[ ' . date('Y-m-d H:i:s') . ' ] ' . $_SERVER['REMOTE_ADDR'] . ' :: ';

            // Recupero o metodo
            $method = $messageTime . strtoupper($method);

            // Recupero o request em jason
            $request = $messageTime. 'INPUT > ' . str_replace(['{', '}', '[]'], ['', '', ''], json_encode($request->all()));

            // Rebra de linha
            $eol = PHP_EOL;

            // Defino nome do arquivo a ser salvo
            $file = storage_path('logs/'.$prefixo.'_'. date('Y-m-d').'.txt');

            // Defino a mensagem do metodo
            $command = "echo '{$method}' >> {$file}";
            shell_exec($command);

            $command = "echo '{$request}{$eol}' >> {$file}";
            shell_exec($command);

        }

    }

}

if (!function_exists('formatSelectTags')) {

    function formatSelectTags($data){

        $array = [];

        foreach($data as $row)

            $array[$row] = $row;

        return $array;
    }

}

if (!function_exists('formatSelectGrouping')) {

    function formatSelectGrouping($colunas){

        $array = [];

        foreach($colunas as $coluna) {

            // somente se for para exibir essa coluna
            if($coluna->show_grouping)

                $array[$coluna->column] = $coluna->description;

        }

        return $array;

    }

}

if (!function_exists('getUri')) {

    function getUri($request){

        $request = explode('?',$request->getRequestUri());

        return '?'.$request[1];

    }

}

if (!function_exists('validateDatePrint')) {

    function validateDatePrint($date){

        $date = explode('-', $date);

        // Verifico se o dia está na posição do mês
        return (intval($date[1]) > 12)

            ? "{$date[0]}-{$date[2]}-{$date[1]}"

            : "{$date[0]}-{$date[1]}-{$date[2]}";

    }

}

if (!function_exists('getProtocol')) {

    function getProtocol(){

        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

    }

}

if (!function_exists('getWs')) {

    function getWs(){

        return (getProtocol() == 'http') ? 'ws' : 'wss';

    }

}


if (!function_exists('validateHttps')) {

    function validateHttps($request){

        // Uri's que se forem HTTP, ficarão HTTP
        $uriLiberadas = [
            '/workspace',
            '/vms/check_online',
        ];

        $requestUri = explode('?', $request->getRequestUri())[0];

        if(config('sys.https') && !in_array($requestUri, $uriLiberadas) && getProtocol() == 'http'){

            $uri = str_replace('http', 'https', $request->getUri());

            return (object) [
                'redirect' => 1,
                'uri'      => $uri
            ];

        }

        return (object) ['redirect' => 0];

    }

}

if (!function_exists('formatValueExtrato')) {

    function formatValueExtrato($value){

        return ($value < 0)

            ?'<span class="text-danger">'.number_format($value,2,',','.').'</span>'

            : number_format($value,2,',','.');

    }

}

if (!function_exists('brandCard')) {
    function brandCard ($cardNumber) {
        $brand = false;

        // BANDEIRAS ACEITAS NA DOCUMENTAÇÃO CIELO
        switch ($cardNumber) {
            case (bool) preg_match('/^(636368|438935|504175|451416|636297)/', $cardNumber) :
                $brand = 'Elo';
                break;

            case (bool) preg_match('/^(606282)/', $cardNumber) :
                $brand = 'Hipercard';
                break;

            case (bool) preg_match('/^(5067|4576|4011)/', $cardNumber) :
                $brand = 'Elo';
                break;

            case (bool) preg_match('/^(3841)/', $cardNumber) :
                $brand = 'Hipercard';
                break;

            case (bool) preg_match('/^(6011)/', $cardNumber) :
                $brand = 'Discover';
                break;

            case (bool) preg_match('/^(622)/', $cardNumber) :
                $brand = 'Discover';
                break;

            case (bool) preg_match('/^(301|305)/', $cardNumber) :
                $brand = 'Diners';
                break;

            case (bool) preg_match('/^(34|37)/', $cardNumber) :
                $brand = 'Amex';
                break;

            case (bool) preg_match('/^(36,38)/', $cardNumber) :
                $brand = 'Diners';
                break;

            case (bool) preg_match('/^(64,65)/', $cardNumber) :
                $brand = 'Discover';
                break;

            case (bool) preg_match('/^(50)/', $cardNumber) :
                $brand = 'Aura';
                break;

            case (bool) preg_match('/^(35)/', $cardNumber) :
                $brand = 'JCB';
                break;

            case (bool) preg_match('/^(60)/', $cardNumber) :
                $brand = 'Hipercard';
                break;

            case (bool) preg_match('/^(4)/', $cardNumber) :
                $brand = 'Visa';
                break;

            case (bool) preg_match('/^(5)/', $cardNumber) :
                $brand = 'Master';
                break;
        }

        return $brand;
    }
}

