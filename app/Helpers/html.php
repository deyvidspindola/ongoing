<?php

if (!function_exists('changeViewPass')) {

    function changeViewPass($input_id)
    {

        $html  = '<span input_id="'.$input_id.'" type="show" class="change-view-pass input-group-addon">';
        $html .= '    <i class="fa fa-eye"></i>';
        $html .= '</span>';

        return $html;

    }

}

if (!function_exists('formatContentEmailCampanha')) {

    function formatContentEmailCampanha($data, $values)
    {

        $data = str_replace(["\r", "\n"], ['', '<br>'], $data);

        $dataArray = explode('<tabela>', $data);

        if(count($dataArray) > 1)

            $data = '<span style="color: #74787E; font-size: 16px; line-height: 1.5em;">'.implode('</span><tabela><span style="color: #74787E; font-size: 16px; line-height: 1.5em;">', $dataArray).'</span>';

        // Recupero o html da tabela
        $tabela = json_decode($values->tabela, true);

        $html = '';

        $html .= '<div>';
        $html .= '<table border="1" style="border: 1px solid #DFDFDF; color: #74787E !important; width: 100% !important; border-collapse: collapse;">';
        $html .= '    <thead>';
        $html .= '        <tr>';
        $html .= '            <th  style="width: 50px; font-size: 13px; text-align: center; padding: 5px"> Ln </th>';
        $html .= '            <th  style=" text-align: center; font-size: 13px; padding: 5px"> Data Lançamento </th>';
        $html .= '            <th  style=" text-align: center; font-size: 13px; padding: 5px"> N° Documento </th>';
        $html .= '            <th style="padding-right: 18px; font-size: 13px; text-align: right;"> QLanc </th>';
        $html .= '        </tr>';
        $html .= '    </thead>';
        $html .= '    <tbody>';

        foreach($tabela as $row) {

        $html .= '        <tr>';
        $html .= '            <td style="text-align: center; padding: 3px; font-size: 13px;"> '.$row['id'].' </td>';
        $html .= '            <td style="text-align: center; font-size: 13px;"> '.$row['data_lancamento'].' </td>';
        $html .= '            <td style="text-align: center; font-size: 13px;"> '.$row['nmr_documento'].' </td>';
        $html .= '            <td style="text-align: right;  padding-right: 18px; font-size: 13px;"> '.number_format($row['valor'],2,',','.'). '</td>';
        $html .= '        </tr>';

        }

        $html .= '    </tbody>';
        $html .= '    <tfoot>';
        $html .= '        <tr>';
        $html .= '            <th style="padding: 5px 5px 5px 18px; font-size: 13px; text-align: left;" colspan="3"> TOTAL </th>';
        $html .= '            <th style="padding-right: 18px; text-align: right; font-size: 13px;">'.number_format($values->valor,2,',','.').'</th>';
        $html .= '        </tr>';
        $html .= '    </tfoot>';
        $html .= '</table>';
        $html .= '</div>';

        $de = [
            '<cliente>',
            '<descricao>',
            '<valor>',
            '<referencia>',
            '<campanha>',
            '<tabela>',
            '<cpf_cnpj>',
        ];

        $para = [
            $values->name,
            $values->descricao,
            'R$ '.number_format($values->valor,2,',','.'),
            $values->referencia,
            $values->campanha,
            $html,
            maskCpfCnpj($values->cpf)
        ];

        $data = str_replace($de, $para, $data);

        return $data;

    }

}

if (!function_exists('tagJobCampanhaStatus')) {

    function tagJobCampanhaStatus($status)
    {
        switch($status){

            case 1 : return '<span style="width: 150px;" class="btn btn-xs yellow-gold  no-hover cursor-default text-center"> EM PROCESSAMENTO </span>';

            case 2 : return '<span style="width: 150px;" class="btn btn-xs btn-danger   no-hover cursor-default text-center"> ERRO PROCESSAMENTO </span>';

            case 3 : return '<span style="width: 150px;" class="btn btn-xs green-meadow no-hover cursor-default text-center"> PROCESSADO </span>';

        }

    }

}

if (!function_exists('tagCreateUpdate')) {

    function tagCreateUpdate($status)
    {
        return (mb_strtoupper($status, 'UTF-8') == 'CREATE')

            ? '<span style="width: 58px;" class="btn btn-xs green-meadow no-hover cursor-default"> CREATE </span>'

            : '<span style="width: 58px;" class="btn btn-xs yellow-gold no-hover cursor-default"> UPDATE </span>';

    }

}

if (!function_exists('tagSimNao')) {

    function tagSimNao($status)
    {
        return ($status)

            ? '<span class="btn btn-xs green-meadow no-hover cursor-default"> SIM </span>'

            : '<span class="btn btn-xs btn-danger no-hover cursor-default"> NÃO </span>';

    }

}

if (!function_exists('timelineCalendar')) {

    function timelineCalendar($mesesComTransacao, $anoSelecionado, $mesSelecionado)
    {

        // Caso não tenha meses com transação

        // Comentei esse trecho para exibir a timeline, mesmo sem ter transações do ano em questão
        //if(empty($mesesComTransacao)) return "";

        $html  = '<div class="tiles" style="padding-right: 10px;">';
        $html .= '    <table style="width: 100%;">';
        $html .= '        <tr>';

        // Percorro cada mês do ano
        for($mes = 1; $mes <= 12; $mes++){

            // Recupero o ultimo dia do mes em questão
            $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$anoSelecionado)); // Mágica, plim!

            // Recupero o perído Inicial e final do mês em questão
            $period = sprintf("01/%s/%s 00:00 - %s/%s/%s 23:59", formatMesComZero($mes), $anoSelecionado, $ultimo_dia, formatMesComZero($mes), $anoSelecionado);

            // Defino o estilo da box do mes
            $bg = 'bg-grey-cascade';
            $icon = 'fa-calendar-o';

            // Caso tenha transação
            if(in_array($mes, $mesesComTransacao)){

                $bg = 'bg-green-meadow';
                $icon = 'fa-calendar';

            }

            // Verifico se é o mes atual selecionado
            $selected = ($mesSelecionado == $mes ? 'selected' : '');

            // Recupero o nome do mês
            $txtMes = intToMesAbreviado($mes);


            $html .= '<td style="border-right: 1px solid #E0E0E0;" class="timeline-calendar" data-period="'.$period.'">';
            $html .= '   <div class="tile '.$bg.' '.$selected.'" style="width: 100% !important; height: 90px !important; padding-left: 0px !important; padding-bottom: 0px !important; margin-bottom: 0px !important;">';
            $html .= '       <div class="corner"> </div>';
            $html .= '        <div class="check"> </div>';
            $html .= '        <div class="tile-body" style="padding-top: 0px !important;">';
            $html .= '           <i class="fa '.$icon.'" style="font-size: 30px !important; margin-top: 0px !important;"></i>';
            $html .= '       </div>';
            $html .= '        <div class="tile-object text-center" style="padding-top: 5px;">';
            $html .= '            <span style="font-weight: 400;font-size: 13px;"> '.$txtMes.' </span>';
            $html .= '        </div>';
            $html .= '    </div>';
            $html .= '</td>';

        }

        $html .= '        </tr>';
        $html .= '    </table>';
        $html .= '</div>';

        return $html;

    }
}

if (!function_exists('timelineAnteriorProximo')) {

    function timelineAnteriorProximo($mes, $ano)
    {

        // Recupero a data anterior

        $mesAnterior = $mes - 1;
        $anoAnterior = $ano;

        if($mes == 1){
            $mesAnterior = 12;
            $anoAnterior = $ano - 1;
        }

        // Recupero o ultimo dia do mes em questão
        $ultimo_dia = date("t", mktime(0,0,0,$mesAnterior,'01',$anoAnterior));

        // Recupero o perído Inicial e final do mês em questão
        $periodoAnterior = sprintf("01/%s/%s 00:00 - %s/%s/%s 23:59", formatMesComZero($mesAnterior), $anoAnterior, $ultimo_dia, formatMesComZero($mesAnterior), $anoAnterior);


        // Recupero a data proxima

        $mesProximo = $mes + 1;
        $anoProximo = $ano;

        if($mes == 12){
            $mesProximo = 1;
            $anoProximo = $ano + 1;
        }

        // Recupero o ultimo dia do mes em questão
        $ultimo_dia = date("t", mktime(0,0,0,$mesProximo,'01',$anoProximo));

        // Recupero o perído Inicial e final do mês em questão
        $periodoProximo = sprintf("01/%s/%s 00:00 - %s/%s/%s 23:59", formatMesComZero($mesProximo), $anoProximo, $ultimo_dia, formatMesComZero($mesProximo), $anoProximo);

        // Recupero os textos

        $txtAnterior = Lang::get('sys.btn.Anterior');
        $txtProximo  = Lang::get('sys.btn.Proximo');

        // Gero o html

        $html  = '<div class="row">';
        $html .= '    <div class="col-sm-12">';

        $html .= '        <div class="btn-group btn-group-circle pull-right" style="%s">';
        $html .= '            <button style="width: 95px;" type="button" data-period="'.$periodoAnterior.'" class="timeline-calendar btn btn-default"><i class="fa fa-angle-left"></i>&nbsp;&nbsp;'.$txtAnterior.'</button>';
        $html .= '            <button style="width: 95px;" type="button" data-period="'.$periodoProximo.'" class="timeline-calendar btn btn-default">'.$txtProximo.'&nbsp;&nbsp;<i class="fa fa-angle-right"></i></button>';
        $html .= '        </div>';

        $html .= '    </div>';
        $html .= '</div>';

        return [
            'top'    => sprintf($html, 'margin-bottom: 20px; margin-top: 20px;'),
            'button' => sprintf($html, 'padding-bottom: 10px'),
        ];


    }
}


if (!function_exists('timelineAno')) {

    function timelineAno($anoSelecionado)
    {

        $anoInicioFor = ($anoSelecionado > date('Y')) ? $anoSelecionado : date('Y');

        $html = '<div style="margin-bottom: 20px; margin-top: 20px;">';

        $html .= '<select id="timeline_ano" class="form-control search-select">';

        for($ano = $anoInicioFor; $ano >= config('sys.start_year'); $ano--){

            $mes = ($ano == date('Y')) ? intval(date('m')) : 1;

            $selected = ($ano == $anoSelecionado) ? 'selected' : '';

            $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));

            $periodo = sprintf("01/%s/%s 00:00 - %s/%s/%s 23:59", formatMesComZero($mes), $ano, $ultimo_dia, formatMesComZero($mes), $ano);

            $html .= sprintf('<option value="%s" %s>%s</option>', $periodo, $selected, $ano);

        }

        $html .= '</select>';

        $html .= '</div>';

        return $html;


    }
}

if (!function_exists('trSemTransacaoContaCorrente')) {

    function trSemTransacaoContaCorrente()
    {
        $html  = '<tr>';
        $html .= '    <td colspan="9" class="text-center" style="padding: 20px 20px 15px 20px;">';
        $html .= '        <i style="font-size: 70pt; color: #E0E0E0; margin-top: 40px" class=" icon-drawer"></i>';
        $html .= '    </td>';
        $html .= '</tr>';

        return $html;

    }
}

// -- --------------------------------------------------------------------------
// -- ALERTS BOX
// -- --------------------------------------------------------------------------

if (!function_exists('alertSuccess')) {

    function alertSuccess($msg)
    {
        $html  = '<div class="note note-info font-blue-soft">';
        $html .= '<button class="close" data-close="alert"></button>';
        $html .= '<p><i class="fa fa-check-circle"></i>&nbsp;&nbsp; <b>'.\Lang::get('sys.msg.alert-success').':</b> '.$msg.'</p>';
        $html .= '</div>';

        return $html;

        /*$html  = '<div class="note note-success" style="color: #136970;">';
        $html .= '<button class="close" data-close="alert"></button>';
        $html .= '<p><i class="fa fa-check-circle"></i>&nbsp;&nbsp;<b>'.Lang::get('sys.msg.alert-success').':</b> '.$msg.'</p>';
        $html .= '</div>';

        return $html;*/
    }
}

if (!function_exists('alertError')) {

    function alertError($msg)
    {

        $html  = '<div class="note note-danger font-red-intense">';
        $html .= '<button class="close" data-close="alert"></button>';

        if(is_array($msg)){

            foreach($msg as $row)

                $html .= '<p><i class="fa fa-times-circle"></i>&nbsp;&nbsp;<b>' . Lang::get('sys.msg.alert-error') . ':</b> ' . $row . '</p>';

        }else

            $html .= '<p><i class="fa fa-times-circle"></i>&nbsp;&nbsp;<b>'.Lang::get('sys.msg.alert-error').':</b> '.$msg.'</p>';

        $html .= '</div>';

        return $html;
    }

}

if (!function_exists('alertInfo')) {

    function alertInfo($msg)
    {
        $html  = '<div class="note note-info font-blue-soft">';
        $html .= '<button class="close" data-close="alert"></button>';
        $html .= '<p><i class="fa fa-info-circle"></i>&nbsp;&nbsp; <b>'.Lang::get('sys.msg.alert-info').':</b> '.$msg.'</p>';
        $html .= '</div>';

        return $html;
    }
}

if (!function_exists('alertWarning')) {

    function alertWarning($msg)
    {
        $html  = '<div class="note note-warning" style="color: #7f6814;">';
        $html .= '<button class="close" data-close="alert"></button>';
        $html .= '<p><i class="fa fa-warning"></i>&nbsp;&nbsp;<b>'.Lang::get('sys.msg.alert-warning').':</b> '.$msg.'</p>';
        $html .= '</div>';

        return $html;
    }
}

// -- --------------------------------------------------------------------------
// -- BUTTONS ACTIONS
// -- --------------------------------------------------------------------------

if (!function_exists('btnNew')) {

    function btnNew($url, $txt = null){
        $txt = ($txt ? $txt : Lang::get('sys.btn.new'));

        $html  = '<div class="pull-right">';
        $html .= '<a href="'.$url.'" class="btn btn-md green-meadow" style=""><i class="fa fa fa-plus"></i>&nbsp;&nbsp;'.$txt.'</a>';
        $html .= '</div>';

        return $html;
    }
}

if (!function_exists('btnEdit')) {

    function btnEdit($url){
        return '<a href="'.$url.'" data-original-title="'.Lang::get('sys.btn.edit').'" data-placement="top" class="btn tooltips btn-xs btn-primary" target=""><i class="fa fa-pencil"></i></a>';
    }
}

if (!function_exists('btnViewModal')) {

    function btnViewModal($id)
    {
        return '
            <button
                data-offenders_id="' . $id . '"
                class="btn tooltips btn-xs btn-success open-detalhes">
                <i class="fa fa-search"></i>
            </button>
        ';
    }
}

if (!function_exists('btnAddModal')) {

    function btnAddModal()
    {
        return '
            <a class="btn btn-md green-meadow tooltips btn-success open-escolha"> 
                <i class="fa fa-plus"></i> 
                &nbsp;&nbsp;Novo
            </a>
        ';
    }
}

if (!function_exists('btnDelete')) {

    function btnDelete($url)
    {
        return '<span href="'.$url.'" 
                        data-sa-title="'.Lang::get('sys.alert.delete.sa_title').'" 
                        data-sa-message="'.Lang::get('sys.alert.delete.sa_message').'" 
                        data-sa-confirmbuttontext="'.Lang::get('sys.alert.delete.sa_confirmButtonText').'" data-sa-cancelbuttontext="'.Lang::get('sys.alert.delete.sa_cancelButtonText').'" data-sa-popuptitlecancel="'.Lang::get('sys.alert.delete.sa_popupTitleCancel').'" data-original-title="'.Lang::get('sys.btn.delete').'" data-placement="top" class="btn tooltips btn-xs btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i></span>';
    }
}

if (!function_exists('btnCancel')) {

    function btnCancel($url, $isEdit = false){

        $sa_message = $isEdit ? Lang::get('sys.alert.cancel.sa_message_edit') : Lang::get('sys.alert.cancel.sa_message');

        return '<span href="'.$url.'" data-sa-title="'.Lang::get('sys.alert.cancel.sa_title').'" data-sa-message="'.$sa_message.'" data-sa-confirmbuttontext="'.Lang::get('sys.alert.cancel.sa_confirmButtonText').'" data-sa-cancelbuttontext="'.Lang::get('sys.alert.cancel.sa_cancelButtonText').'" data-sa-popuptitlecancel="'.Lang::get('sys.alert.cancel.sa_popupTitleCancel').'" class="btn btn-md btn-danger btn-cancel" style="width: 120px;"><i class="fa fa fa-times"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.cancel').'</span>';
    }
}

if (!function_exists('btnSave')) {

    function btnSave(){
        return '<button type="submit" class="btn btn-md green-meadow btn-save-form" style="width: 120px;"><i class="fa fa-save"></i> &nbsp;&nbsp;'.Lang::get('sys.btn.save').'</button>';
    }
}

if (!function_exists('btnFilter')) {

    function btnFilter($url, $export = false)
    {
        $btn  = '<div class="pull-right box-bnt-filters">';
        $btn .= '<button type="submit"  data-loading-text="Loading..." id="btn-filtrar" class="btn btn-primary"><i class="fa fa-filter"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.filter').'</button>';
        $btn .= '&nbsp;&nbsp;';
        $btn .= '<a href="'.$url.'" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.clean').'</a>';
        if ($export) {
            $btn .= '&nbsp;&nbsp;';
            $btn .= '<a class="btn blue" id="export_filter"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; Exportar Excel</a>';
        }
        $btn .= '</div>';

        return $btn;
    }
}

if (!function_exists('btnFilterExportAjax')) {

    function btnFilterExportAjax($url, $export = false)
    {
        $btn  = '<div class="pull-right box-bnt-filters">';
        $btn .= '<button type="button" id="btn-filtrar" class="btn btn-primary"><i class="fa fa-filter"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.filter').'</button>';
        $btn .= '&nbsp;&nbsp;';
        $btn .= '<a href="'.$url.'" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.clean').'</a>';
        if ($export) {
            $btn .= '&nbsp;&nbsp;';
            $btn .= '<a class="btn blue" id="export_filter"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; Exportar Excel</a>';
        }
        $btn .= '</div>';

        return $btn;
    }
}


if (!function_exists('btnFilterCenter')) {

    function btnFilterCenter($url)
    {
        $btn  = '<div class="box-bnt-filters">';
        $btn .= '<button type="submit" id="btn-filtrar" class="btn btn-primary"><i class="fa fa-filter"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.filter').'</button>';
        $btn .= '&nbsp;&nbsp;';
        $btn .= '<a href="'.$url.'" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;'.Lang::get('sys.btn.clean').'</a>';
        $btn .= '</div>';

        return $btn;
    }
}

if (!function_exists('btnForm')) {

    function btnForm($url, $isEdit = false){
        return btnCancel($url, $isEdit).'&nbsp;&nbsp;'.btnSave();
    }
}



// -- --------------------------------------------------------------------------
// -- OUTROS
// -- --------------------------------------------------------------------------



if (!function_exists('btnLink')) {

    function btnLink($url, $title = '', $typeClass = 'btn-default', $sizeClass = 'btn-xs', $icon = null, $style = null)
    {

        $icon = ($icon) ? "<i class='fa {$icon}'></i>&nbsp;&nbsp;" : null;

        return '<a href="' . $url . '" class="btn '. $sizeClass .' '. $typeClass .'" style="'.$style.'">'. $icon . $title .'</a>';
    }
}

if (!function_exists('btnExport')) {

    function btnExport($route, $id)
    {
        $html = '';

        $html .= '<a href="'.$route.'" id="'.$id.'" class="btn btn-xs btn-success" style="margin-bottom: 50px !important; padding: 3px 10px 3px 10px !important;">';
        $html .= '<i class="clip-file-excel"></i> Exportar dados';
        $html .= '</a>';

        return $html;
    }
}

if (!function_exists('btnIconAction')) {

    /**
     * Gera tag <a> no estino botão com tooltip por tipo
     * @param $url
     * @param string $type
     * @param string $title
     * @param string $typeClass
     * @param string $sizeClass
     * @return string
     */
    function btnIconAction($url, $type = 'edit', $title = '', $typeClass = '', $sizeClass = 'btn-xs', $target = null, $attrs = null)
    {

        $alinhamento = 'top';

        switch ($type) {
            case 'apply':
                $icon = 'fa fa-share';
                $typeClass = ($typeClass) ? $typeClass : 'btn-primary';
                break;
            case 'kill':
                $icon = 'clip-switch';
                $typeClass = ($typeClass) ? $typeClass : 'btn-danger';
                $alinhamento = 'left';
                break;
            case 'play':
                $icon = 'fa fa-play';
                $typeClass = ($typeClass) ? $typeClass : 'btn-success';
                break;
            case 'stop':
                $icon = 'fa fa-stop';
                $typeClass = ($typeClass) ? $typeClass : 'btn-danger';
                break;
            case 'delete':
                $icon = 'glyphicon glyphicon-trash';
                $typeClass = ($typeClass) ? $typeClass : 'btn-danger';
                break;
            case 'eliminar':
                $icon = 'clip-minus-circle-2';
                $typeClass = ($typeClass) ? $typeClass : 'btn-danger';
                break;
            case 'edit':
                $icon = 'fa fa-pencil';
                $typeClass = ($typeClass) ? $typeClass : 'btn-primary';
                break;
            case 'refresh':
                $icon = 'clip-refresh';
                $typeClass = ($typeClass) ? $typeClass : 'btn-primary';
                break;
            case 'new':
                $icon = 'fa fa-plus';
                break;
            case 'print':
                $icon = 'glyphicon glyphicon-print';
                break;
            case 'image':
                $icon = 'fa fa-image';
                break;
            case 'upload':
                $icon = 'fa fa-upload';
                break;
            case 'download':
                $icon = 'fa fa-download';
                break;
            case 'check':
                $icon = 'glyphicon glyphicon-ok';
                break;
            case 'download-bs':
                $icon = 'glyphicon glyphicon-download';
                break;
            case 'search':
                $icon = 'glyphicon glyphicon-search';
                break;
            case 'view':
                $icon = 'fa fa-eye';
                $alinhamento = 'left';
                $typeClass = ($typeClass) ? $typeClass : 'btn-success';
                break;

            case 'images':
                $icon = 'clip-images-2';
                $typeClass = ($typeClass) ? $typeClass : 'btn-success';
                break;
            default:
                $icon = 'glyphicon glyphicon-edit';
        }

        $typeClass = ($typeClass) ? $typeClass : 'btn-default';

        // Verifico se existe atributos
        $attr = null;

        if($attrs){
            foreach($attrs as $key => $value)
                $attr .= $key.'="'.$value.'" ';
        }

        return '<a '.$attr.' href="' . $url . '" data-original-title="'. $title .'" data-placement="'.$alinhamento.'" class="btn tooltips '. $sizeClass .' '. $typeClass .'" target="'. $target .'"><i class="'. $icon .'"></i></a> ';
    }
}


// -- --------------------------------------------------------------
// -- GERAR PAGINAÇÃO LINKS
// -- --------------------------------------------------------------

if (!function_exists('linksPaginacao')) {

    function linksPaginacao($totalRegistros, $porPagina, $pagina, $classe){

        if($totalRegistros && ($totalRegistros > $porPagina)) {

            ?>

            <div class="row">
                <div class="col-sm-12">
                    <ul class="pagination">

                        <?php
                        //calcula o número de páginas arredondando o resultado para cima
                        $numPaginas = ceil($totalRegistros / $porPagina);

                        // Calculo a pagina (anterior)
                        $paginaAnterior = ($pagina == 1) ? 1 : $pagina - 1;

                        // Calculo a pagina (proximo)
                        $paginaProximo = ($pagina == $numPaginas) ? $numPaginas : $pagina + 1;

                        // Calculos para exibição dos links
                        if ($numPaginas < 10) {
                            $paginaInicio = 1;
                            $paginaFim = $numPaginas;
                        } elseif ($pagina < 6) {
                            $paginaInicio = 1;
                            $paginaFim = 9;
                        } elseif ($pagina > ($numPaginas - 4)) {
                            $paginaInicio = $numPaginas - 8;
                            $paginaFim = $numPaginas;
                        } else {
                            $paginaInicio = $pagina - 4;
                            $paginaFim = $pagina + 4;
                        }

                        // Primeiro
                        echo '<li><a href="#" class="' . $classe . '" pagina="1">Primeiro</a></li>';

                        // Imprimo o botão (Anterior)
                        echo ($pagina == 1)
                            ? '<li class="disabled"><span>«</span></li>'
                            : '<li><a href="#" class="' . $classe . '" pagina="' . $paginaAnterior . '" rel="prev">«</a></li>';


                        // Imprimo os links
                        for ($i = $paginaInicio; $i <= $paginaFim; $i++) {

                            if ($i == $pagina)
                                echo '<li class="active"><span>' . $i . '</span></li>';
                            else
                                echo '<li><a href="#" class="' . $classe . '" pagina="' . $i . '">' . $i . '</a></li>';

                        }

                        // Imprimo o botão (Próximo)
                        echo ($pagina == $numPaginas)
                            ? '<li class="disabled"><span>»</span></li>'
                            : '<li><a href="#" class="' . $classe . '" pagina="' . $paginaProximo . '" rel="next">»</a></li>';


                        // Último
                        echo '<li><a href="#" class="' . $classe . '" pagina="' . $numPaginas . '">Último</a></li>';


                        ?>

                    </ul>

                </div>
            </div>

            <?php

        }

    }

}


