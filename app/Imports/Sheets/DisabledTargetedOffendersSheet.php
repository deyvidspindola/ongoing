<?php

namespace App\Imports\Sheets;

use App\Http\Traits\OffenderHistoryTrait;
use App\Http\Traits\OffenderImportTrait;
use App\Models\OfenderImportLogs;
use App\Models\Offender;
use App\Models\OffenderHistory;
use App\Models\OffenderInstruction;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DisabledTargetedOffendersSheet implements ToCollection, WithHeadingRow
{
	use OffenderHistoryTrait, OffenderImportTrait;

	const sheet             = 'of_direcionados_desativados';
	const type              = 'Interno';
	const status            = 'Inativo';
	const key               = 'offender_id';

    private $solverGroup;

    public function __construct(int $solverGroup)
    {
        $this->solverGroup = $solverGroup;
    }

	public function collection(Collection $rows)
	{
		$rowNumber = 2;
		$logMsg = '';
		$logArray = [];

		if ($this->checkLayout($rows->first(), self::sheet)) {
			// Recupera todos os offensores da base;
			$offenders = Offender::whereType(self::type)->whereStatus(self::status)->whereSheet(self::sheet)->get();
			$new = ($offenders->isEmpty()) ? true : false;

			foreach ($rows as $row) {
				try{
					$rowValidate = $this->validColunm(self::sheet, $row);
					if ($rowValidate['status']) {
						//Cria um array dos offensores com os campos no formato da tabela
						$offender = collect($this->createObject($row, self::sheet));

						if ($new) {
							$this->offender($offender, $new);
						} else {
							// Verifica se o ofensor já existe na base;
							if (!empty($offenders->firstWhere(self::key, $offender->get(self::key)))){
								// Verifica se existe alguma diferença no ofensor existente na base com o novo registro vindo da importação.
								if (!empty($offender->diffAssoc($offenders->firstWhere(self::key, $offender->get(self::key))))){
									$this->offender($offender, $new); // Atualiza o ofensor caso ele tenha alguma alteração.
								}
							}else{
								// caso não encontre o ofensor na base, cria o novo ofensor.
								$this->offender($offender, true);
							}
						}
						$logMsg = 'OK';
					} else {
						$logMsg = $rowValidate['mensagem'];
					}
				} catch (\Exception $e) {
					$logMsg = $e->getMessage();
				} finally {
                    if ($logMsg == 'OK') {
                        $logArray[$rowNumber][0] = 'Linha ' . $rowNumber;
                        $logArray[$rowNumber][1] = $logMsg;
                    } else {
                        $logArray[$rowNumber][0] = 'Linha ' . $rowNumber;
                        $logArray[$rowNumber][1] = 'Registro inválido. Verifique os dados informados - ' . $logMsg;
                    }
				}
				$rowNumber++;
			}
		}
		OfenderImportLogs::find(1)->update([self::sheet => $logArray]);
	}

	public function headingRow(): int
	{
		return 1;
	}

	/**
	 * @description Cria ou atualiza um ofensor
	 * @param $offender
	 * @param $new
	 */
	private function offender($offender, $new)
	{
        $offender['type']               = self::type;
        $offender['status']             = self::status;
        $offender['solver_group_id']    = $this->solverGroup;
        $offender['sheet']              = self::sheet;
        $offender['retention_level']    = 'N1';
        $offender['user_id']            = auth()->user()->id;

		if ($new) {
			$offenderId = Offender::create($offender->toArray())->id;

            $data = [
                'offender_id' => $offenderId,
                'filename' => '',
                'file' => '',
                'instruction_id' => '',
                'user_id' => auth()->user()->id,
                'is_three_days_email_sent' => 1,
                'is_five_days_email_sent' => 1,
            ];
            OffenderInstruction::create($data);

			$history = $this->createHistory('I', $offenderId, $offender, self::sheet);
			if($history){
				OffenderHistory::create($history);
			}
		} else {
			$oldOffender = Offender::whereOffenderId($offender->get(self::key))->whereSheet(self::sheet)->first();
			if (count(array_diff($offender->toArray(), $oldOffender->toArray())) > 0){
				$history = $this->createHistory('E', $oldOffender->id, $offender->toArray(), self::sheet, $oldOffender->toArray());
				if($history){
					OffenderHistory::create($history);
				}
			}
			$oldOffender->update($offender->toArray());
		}
	}
}
