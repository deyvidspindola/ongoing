<?php

namespace App\Imports\Sheets;

use App\Http\Traits\OffenderHistoryTrait;
use App\Http\Traits\OffenderImportTrait;
use App\Jobs\ImportOffenders;
use App\Models\MacroOffender;
use App\Models\OfenderImportLogs;
use App\Models\Offender;
use App\Models\OffenderHistory;
use App\Models\OffenderInstruction;
use App\Models\OffenderMacroOffender;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OffendersSheet implements ToCollection, WithHeadingRow
{

	use OffenderHistoryTrait, OffenderImportTrait;


    private $solverGroup;

    public function __construct(int $solverGroup)
    {
        $this->solverGroup = $solverGroup;
    }

    const sheet             = 'ofensores';
    const type              = 'Externo';
    const status            = 'Ativo';
    const key               = 'offender_id';

	/**
	 * @param Collection $rows
	 * @return bool
	 * @throws \Exception
	 */
	public function collection(Collection $rows)
    {
	    $rowNumber = 5;
	    $logMsg = '';
	    $logArray = [];
	    if ($this->checkLayout($rows->first(), self::sheet)) {

		    // Recupera todos os offensores da base;
		    $offenders = Offender::whereType(self::type)->whereStatus(self::status)->whereSheet(self::sheet)->get();
		    $new = ($offenders->isEmpty()) ? true : false;

            //Cria ou recupera um id de macro ofendor
            $macroOffenderID = $this->macroOffender($rows, $new);

            //Atualiza o array com o id do macro ofensor já autalizado com o novo id vindo da planilha
            $rows = $rows->each(function ($item) use ($macroOffenderID) {
                $item->map(function ($value, $key) use ($macroOffenderID, $item) {
                    if($key == 'idmacro_ofensor' && isset($value) && !empty($value)){
                        return $item[$key] = $macroOffenderID[$value];
                    }
                });
            });

            //Cria um array com o id o offensor atual (Planilha) com o id do macro ofensor atual (Planilha)
            $OffenderMacroOffender = $rows->mapWithKeys(function ($item) {
                return [$item['id'] => $item['idmacro_ofensor']];
            });

            foreach ($rows as $row) {
                if(!is_null($row['id'])) {
                    try {
                        $rowValidate = $this->validColunm(self::sheet, $row);
                        if ($rowValidate['status']) {
                            $offender = collect($this->createObject($row, self::sheet));
                            if ($new) {
                                $this->offender($offender, $OffenderMacroOffender, $new);
                            } else {
                                // Verifica se o ofensor já existe na base;
                                if (!empty($offenders->firstWhere(self::key, $offender->get(self::key)))) {
                                    Log::info('Has offender base | Key => '.self::key);
                                    // Verifica se existe alguma diferença no ofensor existente na base com o novo registro vindo da importação.
                                    if (!empty($offender->diffAssoc($offenders->firstWhere(self::key, $offender->get(self::key))))) {
                                        Log::info('Has offender diff | Key => '.self::key);
                                        $this->offender($offender, $OffenderMacroOffender, $new); // Atualiza o ofensor caso ele tenha alguma alteração.
                                    }
                                } else {
                                    // caso não encontre o ofensor na base, cria o novo ofensor.
                                    Log::info('create a offender');
                                    $this->offender($offender, $OffenderMacroOffender, true);
                                }
                            }
                            $logMsg = 'OK';
                        } else {
                            $logMsg = $rowValidate['mensagem'];
                        }
                    } catch (\Exception $e) {
                        $logMsg = $e->getMessage();
                    } finally {
                        if ($logMsg == 'OK') {
                            $logArray[$rowNumber][0] = 'Linha ' . $rowNumber;
                            $logArray[$rowNumber][1] = $logMsg;
                        } else {
                            $logArray[$rowNumber][0] = 'Linha ' . $rowNumber;
                            $logArray[$rowNumber][1] = 'Registro inválido. Verifique os dados informados - ' . $logMsg;
                        }
                    }
                    $rowNumber++;
                }
            }

	    } else {
		    return false;
	    }
	    OfenderImportLogs::create(['id' => 1, self::sheet => $logArray]);
    }

	public function headingRow(): int
	{
		return 4;
	}

    /**
     * @description Cria ou atualiza um ofensor
     * @param $offender
     * @param $OffenderMacroOffender
     * @param $new
     */
    private function offender($offender, $OffenderMacroOffender, $new)
    {

        $offender['type']               = self::type;
        $offender['status']             = self::status;
        $offender['solver_group_id']    = $this->solverGroup;
        $offender['sheet']              = self::sheet;
        $offender['user_id']            = auth()->user()->id;

    	if ($new) {
            $ofender_id = Offender::create($offender->toArray())->id;

            $data = [
                'offender_id' => $ofender_id,
                'filename' => '',
                'file' => '',
                'instruction_id' => '',
                'user_id' => auth()->user()->id,
                'is_three_days_email_sent' => 1,
                'is_five_days_email_sent' => 1,
            ];
            OffenderInstruction::create($data);

			if (!is_null($OffenderMacroOffender[$offender->get(self::key)]) || !empty($OffenderMacroOffender[$offender->get(self::key)])){
				OffenderMacroOffender::create([self::key => $ofender_id, 'macro_offender_id' => $OffenderMacroOffender[$offender->get(self::key)]]);
			}

            $history = $this->createHistory('I', $ofender_id, $offender->toArray(), self::sheet);
	        if($history){
		        OffenderHistory::create($history);
	        }
        } else {
            $getOffender = Offender::whereOffenderId($offender->get(self::key))->whereSheet(self::sheet)->first();
            $newOffender = $offender->toArray();
		    $newOffender['macro_offender_id'] = MacroOffender::find($offender['macro_offender_id'])->macro_offender_id;
		    $history = $this->createHistory('E', $getOffender->id, $newOffender, self::sheet, $getOffender);
		    if($history){
			    OffenderHistory::create($history);
		    }

		    $getOffender->update($offender->toArray());
	        $offender_id = $getOffender->id;

		    if (!is_null($OffenderMacroOffender[$offender->get(self::key)]) || !empty($OffenderMacroOffender[$offender->get(self::key)])){
		        $relationship = OffenderMacroOffender::whereOffenderId($offender_id)->first();
	            if (!empty($relationship)) {
		            OffenderMacroOffender::whereOffenderId($offender_id)->update(['macro_offender_id' => $OffenderMacroOffender[$offender->get(self::key)]]);
	            }else{
	                OffenderMacroOffender::create([self::key => $offender_id, 'macro_offender_id' => $OffenderMacroOffender[$offender->get(self::key)]]);
	            }
            }
        }
    }


	/**
	 * @description Cria um macro ofensor ou retrona o id de um já existente.
	 * @param $rows
	 * @return retorna um array com todos os ids criados ou atualizados
	 */
	private function macroOffender($rows, $new)
	{
		$macros = MacroOffender::all();
		$macro = $macros->mapWithKeys(function ($item) {
			return [$item['macro_offender_id'] => $item['macro_offender']];
		});

		$newMacro = $rows->mapWithKeys(function ($item) {
			return [$item['idmacro_ofensor'] => $item['macro_ofensor']];
		});

		//traz somente as diferenças entre os dois registros;
		$diff = $newMacro->diffAssoc($macro->toArray());

		if ($new) {

			//Cria os macro ofensores na base;
			foreach ($diff->toArray() as $key => $item){
				if (!empty($key) || is_null($key)){
					$macroOffensor[$key] = MacroOffender::create(['macro_offender_id' => $key, 'macro_offender' => $item])->id;
				}
			}

		} else {

			//Atualiza um macro ofensor já existente
			foreach ($newMacro->toArray() as $key => $item){
				if(!empty($macros->firstWhere('macro_offender_id', $key))) {
					MacroOffender::whereMacroOffenderId($key)->update(['macro_offender' => $item]);
					$macroOffensor[$key] = $macros->firstWhere('macro_offender_id', $key)->id;
				} else {
					if (!empty($key) || is_null($key)) {
						$macroOffensor[$key] = MacroOffender::create(['macro_offender_id' => $key, 'macro_offender' => $item])->id;
					}
				}
			}

		}

		if (!isset($macroOffensor)){
			$i = 0;
			foreach ($macro->toArray() as $key => $item){
				$row = $macros->firstWhere('macro_offender_id', $key)->get('id')->toArray();
				$macroOffensor[$key] = $row[$i]['id'];
				$i++;
			}
		}

		return $macroOffensor;
	}

}
