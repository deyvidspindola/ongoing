<?php

namespace App\Imports;

use App\Imports\Sheets\DisabledOffendersSheet;
use App\Imports\Sheets\DisabledTargetedOffendersSheet;
use App\Imports\Sheets\OffendersSheet;
use App\Imports\Sheets\OldDescriptionSheet;
use App\Imports\Sheets\TargetedOffendersSheet;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OffendersImport implements WithMultipleSheets, SkipsUnknownSheets
{

	use Importable;

    private $solverGroup;

    public function __construct(int $solverGroup)
    {
        $this->solverGroup = $solverGroup;
    }

    public function sheets(): array
	{
		return [
			'Ofensores' => new OffendersSheet($this->solverGroup),
			'Ofensores direcionados' => new TargetedOffendersSheet($this->solverGroup),
			'Ofensores Desativados' => new DisabledOffendersSheet($this->solverGroup),
			'Descrição Antiga' => new OldDescriptionSheet($this->solverGroup),
			'Of. direcionados desativado' => new DisabledTargetedOffendersSheet($this->solverGroup),
		];
	}

	/**
	 * @param int|string $sheetName
	 * @description faz a validação das abas da planilha
	 */
	public function onUnknownSheet($sheetName)
	{
		// E.g. you can log that a sheet was not found.
		info("A aba {$sheetName} não foi encontrada");

	}

}