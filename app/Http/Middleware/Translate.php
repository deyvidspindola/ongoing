<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Translate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Caso não setado o locale da session
        if(App::getLocale() && !Session::get('locale'))

            Session::put('locale', App::getLocale());

        // Se estiver setado o locale da session e ela for diferente do default
        // então faço a troca do locale
        elseif(App::getLocale() != Session::get('locale'))

            app()->setLocale(Session::get('locale'));

        // Atualização do locale do usuário
        if(Auth::user()->locale != Session::get('locale')){

            Auth::user()->locale = Session::get('locale');
            Auth::user()->save();

        }

        return $next($request);
    }
}
