<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

trait OffenderHistoryTrait
{

    use OffenderImportTrait;

	/**
	 * @description Cria um array com o historico de um ofensor para ser gravado na tabela.
	 * @param $operationType
	 * @param $offender_id
	 * @param $newOffender
	 * @param null $oldOffender
	 * @return array com o historico do ofensor para gravar na base
	 */
	public function createHistory($operationType, $offender_id, $newOffender, $sheet = null, $oldOffender = null)
	{
        $offenderHistory['sheet'] = $sheet;
        unset($newOffender['macro_offender'],$oldOffender['macro_offender']);

        if(!is_null($sheet) && ($sheet == 'ofensores_desativados' || $sheet == 'descricao_antiga')){
			$offenderHistory['observation2'] = ' | '.$newOffender['observation'];
		} elseif (!is_null($sheet) && $sheet == 'of_direcionados_desativados'){
			$offenderHistory['observation2'] = ' | OFENSOR DESATIVADO EM -'.$newOffender['created_at'];
			$offenderHistory['created_at'] = $newOffender['created_at'];
		}

		#move um ofensor externo para desativado
		if (!is_null($sheet) && $sheet == 'ofensores' && isset($newOffender['status']) && isset($oldOffender['status'])){
			if ($operationType == 'E' && $newOffender['status'] == 'Inativo' && $oldOffender['status'] != 'Inativo'){
				$offenderHistory['observation2'] = ' | OFENSOR DESATIVADO EM -'.Carbon::now()->format('d/m/Y');
				if($newOffender['type'] == 'Externo')
					$offenderHistory['sheet'] = 'ofensores_desativados';
			} elseif ($operationType == 'E' && $newOffender['status'] == 'Ativo' && $oldOffender['status'] != 'Ativo'){
				$offenderHistory['observation2'] = ' | OFENSOR REATIVADO EM -'.Carbon::now()->format('d/m/Y');
				if($newOffender['type'] == 'Externo')
					$offenderHistory['sheet'] = 'ofensores';
			}
		}

        #move um ofensor interno para desativado
        if (!is_null($sheet) && $sheet == 'ofensores_direcionados'){
            if ($operationType == 'E' && $newOffender['status'] == 'Inativo' && $oldOffender['status'] != 'Inativo'){
	            $offenderHistory['observation2'] = ' | OFENSOR DESATIVADO EM -'.Carbon::now()->format('d/m/Y');
                $offenderHistory['created_at'] = Carbon::now()->format('Y-m-d');
	            $offenderHistory['sheet'] = 'ofensores_direcionados';
            }
        }

        if (!is_null($oldOffender) && is_array($oldOffender)) {
            if ($newOffender['type'] == 'Externo')
                $oldOffender['offender_id'] = $oldOffender['offender_id'].'A';
        }

        $offenderHistory['snapshot']        = is_null($oldOffender) ? $newOffender : $oldOffender;
		$offenderHistory['user_id']         = Auth::user()->id;
		$offenderHistory['offender_id']     = $offender_id;
		$offenderHistory['operation_type']  = $operationType;

		$newOffender = $this->treats_altered_fields($newOffender);
        if (!is_null($oldOffender) && is_array($oldOffender))
		    $oldOffender = $this->treats_altered_fields($oldOffender);

		$offenderHistory['modified_fields'] = is_array($oldOffender) ? array_diff_assoc($newOffender, $oldOffender) : null;

		if (!is_null($offenderHistory['modified_fields'])) {
			// In some cases the diff could got changes on created_at field, in this case, we discard it
			$offenderHistory['modified_fields'] = $this->ignoreFilds($offenderHistory['modified_fields'], $newOffender['type']);

			// Extract the indexes of the modified values. The indexes are the fields that were changed.
			// Then we glue the array and change it to string.
			$offenderHistory['modified_fields'] = implode(', ', array_keys($offenderHistory['modified_fields']));

			$modified_fields = explode(', ', $offenderHistory['modified_fields']);
            foreach ($modified_fields as $field){
            	if ($field == 'macro_offender_id')
		            $field = str_replace('macro_offender_id', 'macro_offender', $field);

                $fields[] = ($this->colunms($sheet, $field) == 'descricao_do_problema') ? 'ofensor' : $this->colunms($sheet, $field);
            }
            $offenderHistory['modified_fields'] = implode(', ', $fields);

			if (empty($offenderHistory['modified_fields'])) {
				return false;
			}
		}


		// Save history register
		return $offenderHistory;
	}

	private function ignoreFilds($modifieldFields, $type='Externo')
    {
	    $ignore_fields = ['created_at', 'updated_at', 'offender_macro_offender', 'instructions', 'symptom', 'fact', 'cause', 'action', 'offender_id'];
	    if ($type == 'Interno'){
		    $ignore_fields = ['created_at', 'updated_at', 'offender_macro_offender', 'available', 'instructions', 'macro_offender_id', 'symptom', 'fact', 'cause', 'action'];
	    }
        return Arr::except($modifieldFields, $ignore_fields);
	}

	private function treats_altered_fields($offenderFields)
	{
		$fields = [
			'symptom',
			'fact',
			'cause',
			'action',
			'offender',
			'action_solution_targeting_for_responsible_team',
			'scenario_validation',
			'observation',
			'classification',
			'detailed_explanation_offender',
			'answer_for_the_user_in_the_call',
			'exception',
			'description_and_procedure_id'
		];
		foreach ($fields as $field){
		    if (key_exists($field, $offenderFields))
			    $offenderFields[$field] = Str::slug($offenderFields[$field], '_');
		}
		return $offenderFields;
	}


}