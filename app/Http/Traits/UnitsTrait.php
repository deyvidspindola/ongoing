<?php
/**
 * Created by PhpStorm.
 * User: deyvid
 * Date: 25/07/19
 * Time: 11:36
 */

namespace App\Http\Traits;


trait UnitsTrait
{

	public function data($request)
	{

		$dados['username']              = auth()->user()->username;
		$dados['company_id']            = auth()->user()->company_id;
		$dados['remote_ip']             = request()->ip();
		$dados['units_service_id']      = $request->id;
		$dados['units_operation_id']    = $request->base_code_hide;
		$dados['contract_number']       = $request->contract_code;
		$dados['terminal']              = preg_replace('/\D/', '', $request->terminal_number);
		$dados['node']                  = $request->node;
		$dados['pa']                    = $request->pa;
		$dados['status']                = "P";

		return $dados;
	}

	public function getSlug($slug)
	{
		$slug = explode('/', $slug);
		return last($slug);
	}

}