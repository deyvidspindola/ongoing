<?php

namespace App\Http\Traits;


trait CompanyTrait
{

	public function scopeByCompany($query)
	{
		return $query->whereCompanyId(auth()->user()->company_id);
	}

}