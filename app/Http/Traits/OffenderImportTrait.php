<?php

namespace App\Http\Traits;


use Carbon\Carbon;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Shared\Date;

trait OffenderImportTrait
{

	public function checkLayout($data, $sheet)
	{
		$columns = $this->colunmsFile($sheet);
		foreach ($columns as $colum){
			if (!array_key_exists($colum, $data->toArray())){
				return false;
			}
		}
		return true;
	}

	public function createObject($data, $sheet)
	{
		$columns = $this->colunmsFile($sheet);
		$data = $data->toArray();
		foreach ($columns as $key => $column)
		{
			if ($key == 'created_at'){
				if ($data[$column] != ''){
					$created_at  = Date::excelToDateTimeObject($data[$column])->format('d/m/y');
					$array[$key] = Carbon::createFromFormat('d/m/y', $created_at)->format('Y-m-d');
				} else {
					$array[$key] = Carbon::now()->format('Y-m-d');
				}
			}else{
                $array[$key] = $data[$column];
			    if($sheet == 'descricao_antiga' && $key == 'offender_id'){
                    $array[$key] = $data[$column].'A';
                }
			}
		}
		return $array;
	}

    public function colunms($sheet, $field)
    {
        $colunms = $this->colunmsFile($sheet);
        if (key_exists($field, $colunms)){
            return $colunms[$field];
        }
        return $field;
	}

	public function validColunm($sheet, $row)
	{
		$return['status'] = true;
		$return['mensagem'] = '';
		switch ($sheet) {
			case 'ofensores':
			case 'ofensores_desativados':
			case 'descricao_antiga':
				if ($row['nivel_de_retencao'] === 'N1'){
					if(!Str::contains(Str::slug($row['classificacao'],'_'), ['falha_de_processo', 'falha_na_abertura_do_chamado'])){
						$return['status'] = false;	
						$return['mensagem'] .= ' | Quando o nivel de retenção for N1 a classificação não pode ser diferente de "Falha de Processo" ou "Falha na abertura do chamado"';
					}
				}
			break;
		}

		$collum = $this->colunmsFile($sheet);
		if(is_null($row[$collum['offender_id']]) || empty($row[$collum['offender_id']])){
			$return['status'] = false;
			$return['mensagem'] .= ' | O '.$collum['offender_id'].' está vazio';
		}

		if(is_null($row[$collum['offender']]) || empty($row[$collum['offender']])){
			$return['status'] = false;
			$return['mensagem'] .= ' | O '.$collum['offender'].' está vazio';
		}
		return $return;
	}

	private function colunmsFile($sheet)
	{
		switch ($sheet) {
			case 'ofensores':
				$colunas = [
					'offender_id'                                       => 'id',
					'macro_offender_id'                                 => 'idmacro_ofensor',
					'macro_offender'                                    => 'macro_ofensor',
					'observation'                                       => 'observacao',
					'offender'                                          => 'ofensor',
					'retention_level'                                   => 'nivel_de_retencao',
					'available'                                         => 'valido',
					'classification'                                    => 'classificacao',
					'origin'                                            => 'origem',
					'detailed_explanation_offender'                     => 'explicacao_detalhada_do_ofensor',
					'answer_for_the_user_in_the_call'                   => 'resposta_para_o_usuario_no_atendimento_do_chamado',
					'ppm_id'                                            => 'id_no_ppm',
					'project_pack'                                      => 'projeto_pacote',
					'preventive_offender'                               => 'preventivo_ofensor',
					'predict_definitive'                                => 'previsao_definitivo',
					'num_patch'                                         => 'numero_patch',
					'patch_identification'                              => 'identificacao_patch',
					'created_at'                                        => 'dt_cad_ofensor',
					'unit_c'                                            => 'unit_c',
					'event_id'                                          => 'id_ocorrencia',
					'proceeding_id'                                     => 'id_procedimento'
				];
				break;
			case 'ofensores_direcionados':
				$colunas = [
					'offender_id'                                       => 'id_cenario',
					'offender'                                          => 'descricao_do_problema',
					'scenario_validation'                               => 'validacao_do_cenario',
					'description_and_procedure_id'                      => 'descricao_e_id_procedimento',
					'action_solution_targeting_for_responsible_team'    => 'acao_solucao_direcionamento_para_equipe_responsavel',
					'exception'                                         => 'excecao'
				];
				break;
			case 'ofensores_desativados':
				$colunas = [
					'offender_id'                                       => "id",
					'offender'                                          => "ofensor",
					'retention_level'                                   => "nivel_de_retencao",
					'available'                                         => "valido",
					'classification'                                    => "classificacao",
					'origin'                                            => "origem",
					'detailed_explanation_offender'                     => "explicacao",
					'answer_for_the_user_in_the_call'                   => "resposta_para_o_usuario",
					'ppm_id'                                            => "id_no_ppm",
					'project_pack'                                      => "projeto_pacote",
					'preventive_offender'                               => "preventivo_ofensor",
					'predict_definitive'                                => "previsao_definitivo",
					'num_patch'                                         => "numero_patch",
					'patch_identification'                              => "identificacao_patch",
					'created_at'                                        => "dt_cad",
					'observation'                                       => "observacao"
				];
				break;
			case 'descricao_antiga':
				$colunas = [
					'offender_id'                                       => "id",
					'offender'                                          => "descricao_antiga",
					'retention_level'                                   => "nivel_de_retencao",
					'available'                                         => "valido",
					'classification'                                    => "classificacao",
					'origin'                                            => "origem",
					'detailed_explanation_offender'                     => "explicacao_detalhada_do_ofensor",
					'answer_for_the_user_in_the_call'                   => "resposta_para_o_usuario_no_atendimento_do_chamado",
					'ppm_id'                                            => "id_no_ppm",
					'project_pack'                                      => "projeto_pacote",
					'preventive_offender'                               => "preventivo_ofensor",
					'predict_definitive'                                => "previsao_definitivo",
					'num_patch'                                         => "numero_patch",
					'patch_identification'                              => "identificacao_patch",
					'created_at'                                        => "dt_cad_ofensor",
					'observation'                                       => "observacao",
				];
				break;
			case 'of_direcionados_desativados':
				$colunas = [
					'offender_id'                                       => "id_cenario",
					'offender'                                          => "descricao_do_problema",
					'scenario_validation'                               => "validacao_do_cenario",
					'description_and_procedure_id'                      => "descricao_e_id_procedimento",
					'action_solution_targeting_for_responsible_team'    => "acao_solucao_direcionamento_para_equipe_responsavel",
					'created_at'                                        => "data_de_desativacao"
				];
				break;
		}

		return $colunas;
	}

}