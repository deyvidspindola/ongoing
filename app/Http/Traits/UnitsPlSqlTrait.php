<?php


namespace App\Http\Traits;


trait UnitsPlSqlTrait
{

	public function trataSql($file, $replaces, $exception = false)
	{
		$arquivo = fopen('sql/'.$file, 'r');
		$linha = '';
		//Lê o conteúdo do arquivo
		while(!feof($arquivo)) {
			//Mostra uma linha do arquivo
			$linha .= fgets($arquivo, 1024);
		}

        $replaces['base_code_hide'] = str_pad($replaces['base_code_hide'],3,'0', STR_PAD_LEFT);

		$replaces['terminal_number'] = preg_replace('/\D/', '', $replaces['terminal_number']);
        $linha = str_replace('[CONTRATO]', $replaces['contract_code'], $linha);
        $linha = str_replace('[COD_OPERADORA]', $replaces['base_code_hide'], $linha);

        $linha = str_replace('[TERMINAL]', $replaces['terminal_number'], $linha);
        $linha = str_replace('[OPERADORA]', $replaces['base_code_hide'], $linha);

        if (isset($replaces['date_time'])) {
            $linha = str_replace('[DATA_JANELA]', $replaces['date_time'], $linha);
        }
        if (isset($replaces['ticket_number'])) {
            $linha = str_replace('[BILHETE]', $replaces['ticket_number'], $linha);
        }


        if (isset($replaces['pa'])) {
            $linha = str_replace('[PA]', $replaces['pa'], $linha);
            $linha = str_replace('[pa]', $replaces['pa'], $linha);
        }

        if ($exception) {
            $linha = str_replace('[SOFTX]', "{$replaces['base_code_hide']}", $linha);
        } else {
            if (isset($replaces['softx'])) {
                $linha = str_replace('[SOFTX]', "{$replaces['softx']}", $linha);
            }
        }
		// Fecha arquivo aberto
		fclose($arquivo);
		return $linha;
	}

}