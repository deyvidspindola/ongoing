<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Requests\GroupRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Models\Ad;
use App\Models\Group;
use App\Models\Parameter;
use App\Models\Permission;
use App\Models\SolverGroups;
use App\Models\SolverGroupsGroup;
use App\Models\User;
use App\Services\GroupService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class GroupsController extends Controller
{
    /**
     * @var Group
     */
    private $group;
    /**
     * @var GroupService
     */
    private $groupService;
    /**
     * @var Permission
     */
    private $permission;
    /**
     * @var Ad
     */
    private $ad;
    /**
     * @var User
     */
    private $user;

    public function __construct(
        Group $group,
        GroupService $groupService,
        Permission $permission,
        User $user,
        Ad $ad
    ){

        $this->group = $group;
        $this->groupService = $groupService;
        $this->permission = $permission;
        $this->user = $user;
        $this->ad = $ad;
    }


    public function index(Request $request)
    {
        $this->authorize('group_list');

        $groups = $this->group->list($request->all())
                              ->removeAdmin()
                              ->byCompany(session('company')['id'])
                              ->ordered()
                              ->paginate();

        // Verifico se irei buscar os grupos do AD ou cadastrar um novo
        $parameters = Parameter::byCompany(session('company')['id'])->first();
        $auth_ad = 0;

        // Grupos pelo LDAP
        if($parameters && $parameters->auth_ad){
            $auth_ad = 1;
        }

        return view('administrador.grupos.index', compact('request', 'groups', 'auth_ad'));
    }

    public function create()
    {
        $this->authorize('group_add');

        // Verifico se irei buscar os grupos do AD ou cadastrar um novo
        $parameters = Parameter::byCompany(session('company')['id'])->first();

        // Grupos pelo LDAP
        if($parameters && $parameters->auth_ad){

            // Recupero os grupos do AD, que não estão salvos no sistema
            $groups = ['' => Lang::get('sys.selecione')] + $this->getGroupsLdap();
            $auth_ad = 1;

        }else{

            $groups  = [];
            $auth_ad = 0;

        }

        // Recupero as Permissões
        $permissions = $this->permission->ordered()->pluck('description', 'id')->toArray();

        // Recupero os Usuários
        $users = $this->user
                      ->byCompany(session('company')['id'])
                      ->removeAdmin()
                      ->ordered()
                      ->pluck('name', 'id')
                      ->toArray();

        $solverGroups = SolverGroups::all();
        $arrSolverGroups = SolverGroups::all()->toArray();
        $sgg = [];

        foreach ($arrSolverGroups as $j) {
            if(!array_key_exists($j['id'], $sgg)) {
                $sgg[$j['id']] = [
                    "int" => false,
                    "ext" => false,
                    "cre" => false
                ];
            }
        }

        return view('administrador.grupos.create', compact('groups', 'permissions', 'users', 'auth_ad', 'solverGroups','sgg'));

    }

    public function store(GroupRequest $request)
    {

        $this->authorize('group_add');

        try {

            $this->groupService->store($request->all());

            return redirect()->route('administrador.grupos.index')->with('msg', Lang::get('sys.msg.success.save'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function edit($id)
    {
        $this->authorize('group_edit');

        try {

            // Recupero o grupo
            $group = $this->group
                          ->removeAdmin()
                          ->byCompany(session('company')['id'])
                          ->find($id);

            // Verifico se grupo existe
            if (!$group) {
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }

            // Recupero as Permissões
            $permissions = $this->permission->ordered()->pluck('description', 'id')->toArray();

            // Recupero os Usuários
            $users = $this->user
                ->byCompany(session('company')['id'])
                ->removeAdmin()
                ->ordered()
                ->pluck('name', 'id')
                ->toArray();

            // Recupero as permissões vinculadas
            $linked_permissions = $group->permissions->pluck('id')->toArray();

            // Recupero os usuários vinculados
            $linked_users = $group->users->pluck('id')->toArray();

            $solverGroups = SolverGroups::all();
            $arrSolverGroups = SolverGroups::all()->toArray();

            $solverGroupsGroup = $group->solverGroupsGroup->pluck('pivot')->toArray();
            $sgg = [];

            foreach ($solverGroupsGroup as $k) {
                $sgg[$k['solver_group_id']] = [
                    "int" => ($k['is_internal_show'] == 'S' ? true : false),
                    "ext" => ($k['is_external_show'] == 'S' ? true : false),
                    "cre" => ($k['is_create'] == 'S' ? true : false)
                ];
            }

            foreach ($arrSolverGroups as $j) {
                if(!array_key_exists($j['id'], $sgg)) {
                    $sgg[$j['id']] = [
                        "int" => false,
                        "ext" => false,
                        "cre" => false
                    ];
                }
            }

            // Verifico se irei buscar os grupos do AD ou cadastrar um novo
            $parameters = Parameter::byCompany(session('company')['id'])->first();
            $auth_ad = 0;

            // Grupos pelo LDAP
            if($parameters && $parameters->auth_ad){
               $auth_ad = 1;
            }

            return view('administrador.grupos.edit', compact('group', 'permissions', 'linked_permissions', 'users', 'linked_users', 'solverGroups','sgg', 'auth_ad'));

        }
        catch (\Exception $e) {
            return redirect()->route('administrador.grupos.index')->withErrors($e->getMessage());
        }
    }

    public function update(GroupUpdateRequest $request, $id)
    {


        $this->authorize('group_edit');

        try {

            $this->groupService->update($request->all(), $id);

            return redirect()->route('administrador.grupos.index')->with('msg', Lang::get('sys.msg.success.update'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function destroy($id)
    {
        $this->authorize('group_del');

        try {

            $this->groupService->destroy($id);

            return redirect()->route('administrador.grupos.index')->with('msg', Lang::get('sys.msg.success.delete'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    private function getGroupsLdap(){

        $ad = $this->ad->byCompany(session('company')['id'])->first();

        $groups_ad = getLdap(
            $ad->connection,
            $ad->username,
            $ad->domain,
            $ad->password,
            $ad->dn_groups,
            false
        );


        // Recupero os grupos do sistema que são os grupos do AD
        $groups = Group::whereIn('name', $groups_ad)
                        ->removeAdmin()
                        ->byCompany(session('company')['id'])
                        ->pluck('name', 'id')
                        ->toArray();

        // Removo os que já estão na base de dados
        if(!empty($groups)) {
            foreach ($groups as $group) {
                unset($groups_ad[$group]);
            }
        }

        return ($groups_ad);

    }


}
