<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;

use App\Models\Ad;
use App\Models\Company;
use App\Models\Parameter;
use App\Models\Units\UnitsMenu;
use App\Models\Units\UnitsMenuOrder;
use App\Services\CompanyService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class CompanyController extends Controller
{


    /**
     * @var Company
     */
    private $company;
    /**
     * @var CompanyService
     */
    private $companyService;
    /**
     * @var Parameter
     */
    private $parameter;
    /**
     * @var Ad
     */
    private $ad;

    public function __construct(
        Company $company,
        CompanyService $companyService,
        Parameter $parameter,
        Ad $ad
    )
    {
        $this->company = $company;
        $this->companyService = $companyService;
        $this->parameter = $parameter;
        $this->ad = $ad;
    }

    public function index(Request $request){

        $this->authorize('company_list');

        // Se não tiver liberado na aplicação (multiempresa) ou
        // Se tiver liberado (multiempresa) e Não for admin, então
        // redireciono para a página de edição da empresa do usuário.

        if ( !config('sys.show_company') || (config('sys.show_company') && !Auth::user()->isAdmin()) ) {

            return redirect()->route('administrador.empresa.edit', session('company')['id']);
        }
        // Se for admin, listo as empresas cadastradas
        $empresas = $this->company
                         ->list($request->all())
                         ->ordered()
                         ->paginate();

        return view('administrador.empresa.index', compact('request', 'empresas'));

    }

    public function create()
    {
        // Se não tiver liberado na aplicação (multiempresa) ou
        // Se tiver liberado (multiempresa) e Não for admin, então
        // não liberado o cadastro.

        if ( !config('sys.show_company') || (config('sys.show_company') && !Auth::user()->isAdmin()) ) {
            return redirect()->route('administrador.empresa.edit', session('company')['id'])->withErrors(Lang::get('sys.msg.permission_denied'));
        }
        // Dados default
        $data = [
            'auth_ad' => 0,
            'simultaneous_access' => 1,
            'ad_fields' => 'givenName,sn,mail,samaccountname,memberof',
        ];

        return view('administrador.empresa.create', compact('data'));

    }

    public function store(Request $request)
    {
        // Se não tiver liberado na aplicação (multiempresa) ou
        // Se tiver liberado (multiempresa) e Não for admin, então
        // não liberado o cadastro.

        if ( !config('sys.show_company') || (config('sys.show_company') && !Auth::user()->isAdmin()) ) {
            return redirect()->route('administrador.empresa.edit', session('company')['id'])->withErrors(Lang::get('sys.msg.permission_denied'));
        }
        try {

            $this->companyService->store($request->all());
//            $company = Company::latest()->first();
//
//            $menus_order = UnitsMenuOrder::first();
//
//            $newMenusOrder = [
//                'menu_order' => $menus_order->menu_order,
//                'company_id' => $company->id,
//            ];
//
//            UnitsMenuOrder::create($newMenusOrder);

            return redirect()->route('administrador.empresa.index')->with('msg', Lang::get('sys.msg.success.save'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function edit($id)
    {
        $this->authorize('company_edit');

        try {

            // Recupero a empresa
            $company = $this->company->find($id);

            // Verifico se emprtesa existe
            if (!$company) {
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }
            // Se não for admin, a empresa deve ser a mesma da sessão
            if (!Auth::user()->isAdmin() && $company->id != session('company')['id']) {
                throw new \Exception(Lang::get('sys.msg.permission_denied'));
            }
            // Recupero os parametros
            $parameters = $this->parameter->byCompany($company->id)->first();

            // Recupero as informações do AD
            $ads = $this->ad->byCompany($company->id)->first();

            // Verifico se tem os parametros e ads
            if (!$parameters || !$ads) {
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }
            // Formato os dados
            $data = [
                'id'                  => $company->id,
                'description'         => $company->description,
                'domain'              => $company->domain,
                'auth_ad'             => $parameters->auth_ad,
                'simultaneous_access' => $parameters->simultaneous_access,
                'ad_connection'       => $ads->connection,
                'ad_username'         => $ads->username,
                'ad_password'         => $ads->password,
                'ad_domain'           => $ads->domain,
                'ad_port'             => $ads->port,
                'ad_dn_users'         => $ads->dn_users,
                'ad_dn_groups'        => $ads->dn_groups,
                'ad_fields'           => $ads->fields,
            ];


            return view('administrador.empresa.edit', compact('data'));

        }
        catch (\Exception $e) {

            return (config('sys.show_company') && Auth::user()->isAdmin())

                ? redirect()->route("administrador.empresa.index")->withErrors($e->getMessage())

                : redirect()->route("administrador.empresa.edit", session('company')['id'])->withErrors($e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $this->authorize('company_edit');

        try {

            $this->companyService->update($request->all(), $id);

            return (config('sys.show_company') && Auth::user()->isAdmin())

                ? redirect()->route('administrador.empresa.index')->with('msg', Lang::get('sys.msg.success.update'))

                : redirect()->route("administrador.empresa.edit", session('company')['id'])->with('msg', lang::get('sys.msg.success.update'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function destroy($id)
    {
        try {

            $this->companyService->destroy($id);

            return redirect()->route('administrador.empresa.index')->with('msg', Lang::get('sys.msg.success.delete'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

    }


    // TESTAR CONEXÃO LDAP
    public function testConnection(Request $request){

        $connect = ldap_connect($request->ad_connection);

        // Definir algumas opções do LDAP para falar com AD
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        // Essa é a conta LDAP administrador com acesso
        $username  = $request->ad_username.'@'.$request->ad_domain;

        // Bind como um administrador de domínio
        $ldap_bind = @ldap_bind($connect, $username, $request->ad_password);

        if ($ldap_bind) {
            return response()->json([
                'success' => true,
                'title' => 'Sucesso!',
                'message' => Lang::get('table.conexao_efetuada_ad')
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'title' => 'Erro!',
                'message' => Lang::get('table.nao_foi_possivel_conexao_ad')
            ], 200);
        }

    }

}
