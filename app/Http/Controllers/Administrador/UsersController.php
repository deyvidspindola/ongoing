<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Group;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class UsersController extends Controller
{

    /**
     * @var User
     */
    private $user;
    /**
     * @var Group
     */
    private $group;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(
        User $user,
        Group $group,
        UserService $userService
    ){
        $this->user = $user;
        $this->group = $group;
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $this->authorize('user_list');

        $users = $this->user->list($request->all())
                            ->byCompany(session('company')['id'])
                            ->removeAdmin()
                            ->ordered()
                            ->paginate();

        $groups = $this->group->byUsers($users)
                              ->ordered()
                              ->get()
                              ->toArray();

        // Formato o resultado do grupo
        $groups = $this->agruparGrupos($groups);

        return view('administrador.usuarios.index', compact('request', 'users', 'groups'));
    }

    public function create()
    {
        $this->authorize('user_add');

        return view('administrador.usuarios.create');

    }

    public function store(UserRequest $request)
    {

        $this->authorize('user_add');

        try {

            $this->userService->store($request->all());

            return redirect()->route('administrador.usuarios.index')->with('msg', Lang::get('sys.msg.success.save'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function edit($id)
    {
        $this->authorize('user_edit');

        try {

            // Recupero o usuário
            // Pela empresa na session e desconsiderando admin geral
            $user = $this->user->byCompany(session('company')['id'])
                               ->removeAdmin()
                               ->find($id);

            // Verifico se usuário existe
            if (!$user) {
                throw new \Exception(Lang::get('sys.msg.not_found'));
            }

            return view('administrador.usuarios.edit', compact('user'));

        }
        catch (\Exception $e) {
            return redirect()->route('administrador.usuarios.index')->withErrors($e->getMessage());
        }
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $this->authorize('user_edit');

        try {

            $this->userService->update($request->all(), $id);

            return redirect()->route('administrador.usuarios.index')->with('msg', Lang::get('sys.msg.success.update'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function destroy($id)
    {
        $this->authorize('user_del');

        try {

            $this->userService->destroy($id);

            return redirect()->route('administrador.usuarios.index')->with('msg', Lang::get('sys.msg.success.delete'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }


    // FUNÇÕES AUXILIARES

    private function agruparGrupos($grupos){

        if (empty($grupos))  {
            return [];
        }
        foreach ($grupos as $grupo) {
            $tmp[$grupo['user_id']][] = $grupo['description'];
        }
        foreach ($tmp as $key => $array) {
            $tmp[$key] = implode(', ', $array);
        }

        return $tmp;

    }
}
