<?php

namespace App\Http\Controllers\Administrador;

use App\Models\Units\UnitsMenu;
use App\Models\Units\UnitsMenuOrder;
use App\Services\Units\UnitsMenuService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuUnitsController extends Controller
{

	/**
	 * @var UnitsMenu
	 */
	private $unitsMenu;
	/**
	 * @var UnitsMenuOrder
	 */
	private $unitsMenuOrder;
	/**
	 * @var UnitsMenuService
	 */
	private $unitsMenuService;

	public function __construct(
		UnitsMenu $unitsMenu,
		UnitsMenuOrder $unitsMenuOrder,
		UnitsMenuService $unitsMenuService
	)
	{
		$this->unitsMenu = $unitsMenu;
		$this->unitsMenuOrder = $unitsMenuOrder;
		$this->unitsMenuService = $unitsMenuService;
	}

	public function index()
	{
		$menu = $this->unitsMenuService->makeMenu('Nettable');
		return view('administrador.menu_units.index', compact('menu'));
	}

	public function create(Request $request)
	{
		return $this->unitsMenuService->create($request->all());
	}

	public function show($id)
    {
        $data = $this->unitsMenu->find($id);
        return $data;
    }

	public function update(Request $request)
	{
		return $this->unitsMenuService->update($request->all());
	}

	public function updateOrder(Request $request)
	{
		return $this->unitsMenuService->updateOrder($request->all());
	}
}
