<?php

namespace App\Http\Controllers\ViewComposers;

use App\Models\Units\UnitsMenuOrder;
use App\Services\Units\UnitsMenuService;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class MenuComposer extends Controller
{

	/**
	 * @var UnitsMenuService
	 */
	private $unitsMenuOrder;

	public function __construct(
		UnitsMenuOrder $unitsMenuOrder,
        UnitsMenuService $unitsMenuService
	)
	{
		$this->unitsMenuOrder = $unitsMenuOrder;
		$this->unitsMenuService = $unitsMenuService;
	}

	public function compose(View $view)
	{
		$menu = $this->unitsMenuService->makeMenu();
		$view->with('dinamic_menu', $menu);
	}

}
