<?php

namespace App\Http\Controllers\ViewComposers;

use App\Http\Controllers\Controller;
use App\Services\Units\UnitsMenuService;
use Illuminate\View\View;

class BreadcrumbComposer extends Controller
{
    /**
     * @var UnitsMenuService
     */
    private $request;

    public function __construct()
    {

    }

    public function compose(View $view)
    {
        $bread = explode('/', \Request::url());
        //Acho que precisamos Refatorar
//        dd($bread);
        if (count($bread) <= 4) {
            $breadcrumb = ['Units', ucfirst(last($bread))];
        } else {

            $penultimo = $bread[4];

            $ultimo = $bread[count($bread) - 2];

            $breadcrumb = ['Units', ucwords(str_replace('-', ' ', $penultimo)), ucwords(str_replace('-', ' ', $ultimo))];
        }
        $view->with('dinamic_breadcrump', $breadcrumb);
    }

}
