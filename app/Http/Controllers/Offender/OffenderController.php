<?php

namespace App\Http\Controllers\Offender;

use App\Http\Requests\OffenderRequest;
use App\Models\MacroOffender;
use App\Models\Offender;
use App\Models\OffenderInstruction;
use App\Models\User;
use App\Notifications\Instruction;
use App\Services\OffenderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class OffenderController extends Controller
{

	/**
	 * @var Offender
	 */
	private $offender;
	/**
	 * @var OffenderService
	 */
	private $offenderService;
	/**
	 * @var MacroOffender
	 */
	private $macroOffender;
	/**
	 * @var OffenderInstruction
	 */
	private $offenderInstruction;

	/**
	 * Construct function
	 *
	 * @param Offender $offender
	 * @param OffenderService $offenderService
	 * @param MacroOffender $macroOffender
	 * @param OffenderInstruction $offenderInstruction
	 */
	public function __construct(
		Offender $offender,
		OffenderService $offenderService,
		MacroOffender $macroOffender,
		OffenderInstruction $offenderInstruction
	)
	{
		$this->offender = $offender;
		$this->offenderService = $offenderService;
		$this->macroOffender = $macroOffender;
		$this->offenderInstruction = $offenderInstruction;
	}

	private function offenderCreate() {
        $offenderCreate = $this->offenderService->usersSolverGroupsGroup();
        $createPermission = false;

        if(in_array('S',$offenderCreate['is_create'])) {
            $createPermission = true;
        }

        return $createPermission;
    }

//    private function offenderActive() {
//        $permissionActive = auth()->user()->can('offender_active');
//        return $permissionActive;
//    }

	public function index()
	{
		$this->authorize('offender_list');
		$userSolverGroups = $this->offenderService->usersSolverGroups();

        if(count($userSolverGroups) > 0) {

            $userSolverGroups['Todos'] = 'Todos';
            $type = ['Todos' => 'Todos', 'Externo'=>'Externo', 'Interno'=>'Interno'];

            $createPermission = $this->offenderCreate();

        } else {
            $userSolverGroups[] = 'Sem Permissão';
            $type[] = 'Sem Permissão';
            $createPermission = false;
        }

        return view('offender.index', compact('userSolverGroups', 'type', 'createPermission'));
	}

	public function create()
	{
		if (!$this->offenderCreate())
		    abort(403);

		$resolverGroup = $this->offenderService->usersSolverGroups(true);
		$retention_level = ['N1'=>'N1', 'N2'=>'N2'];
		return view('offender.create', compact('resolverGroup', 'retention_level'));
	}

	public function store(OffenderRequest $request)
	{
        if (!$this->offenderCreate())
            abort(403);
		try {
			$this->offenderService->store($request);
			return redirect()->route('offender.index')->with('msg', 'Registro inserido com sucesso!');
		} catch (\Exception $e) {
			return redirect()->back()->withInput()->withErrors($e->getMessage());
		}
	}

	public function show_details($id)
	{
		$offender = $this->offender->with('offenderMacroOffender.macro_offender')->where('id', $id)->first();
		if (!is_null($offender->offenderMacroOffender)) {
			$offender['macro_offender_id'] = $offender->offenderMacroOffender->macro_offender->first()->id;
			$offender['macro_offender'] = $offender->offenderMacroOffender->macro_offender->first()->macro_offender_id.' - '.$offender->offenderMacroOffender->macro_offender->first()->macro_offender;
		}
		$retention_level = ($offender->type == 'Interno') ? ['N1'=>'N1'] : ['N1'=>'N1', 'N2'=>'N2'];
		$resolverGroup = $this->offenderService->usersSolverGroups();
		return view('offender.modal', compact('offender','resolverGroup', 'retention_level'))->render();
	}

	public function edit($id)
	{
        if (!$this->offenderCreate())
            abort(403);

		$offender = $this->offender->with('offenderMacroOffender.macro_offender','instructions')->find($id);
		if (!is_null($offender->offenderMacroOffender)) {
			$offender['macro_offender_id'] = $offender->offenderMacroOffender->macro_offender->first()->id;
			$offender['macro_offender'] = $offender->offenderMacroOffender->macro_offender->first()->macro_offender_id.' - '.$offender->offenderMacroOffender->macro_offender->first()->macro_offender;
		}

		$activePermission = '';
		if ($offender->status == 'Inativo' && !auth()->user()->can('offender_active')) {
            $activePermission = 'disabled';
        }

		$resolverGroup = $this->offenderService->usersSolverGroups(true);
		$interno = ($offender->type == 'Interno') ? 'displayNone' : '';
        $externo = ($offender->type == 'Externo') ? 'displayNone' : '';
		$retention_level = ($offender->type == 'Interno') ? ['N1'=>'N1'] : ['N1'=>'N1', 'N2'=>'N2'];
		return view('offender.edit', compact('offender','resolverGroup', 'interno', 'retention_level', 'externo', 'activePermission'));
	}

	public function update(OffenderRequest $request, $id)
	{
        if (!$this->offenderCreate())
            abort(403);

		try {
			$this->offenderService->update($request, $id);
			return redirect()->route('offender.index')->with('msg', 'Registro atualizado com sucesso!');
		} catch (\Exception $e) {
			return redirect()->back()->withInput()->withErrors($e->getMessage());
		}
	}

    public function action(Request $request, $id)
    {
        $actionMsg = ($request->action == 'ativar') ? ['ativado','ativar'] : ['inativado','inativar'];

        $this->authorize('offender_active');
        try {
            $return = $this->offenderService->action($request, $id);

            if ($return) {
                return response()->json([
                    'title' => 'Sucesso!',
                    'message' => "Registro $actionMsg[0] com sucesso"
                ], 200);
            }

        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Erro!',
                'message' => "Ocorreu um erro ao tentar $actionMsg[1] esse registro \n".$e->getMessage()
            ], 400);
        }
    }

	public function search(Request $request)
	{
		echo $this->offenderService->search($request->all());
	}

	public function datatable(Request $request)
	{
		if ($request->ajax()) {
			return $this->offenderService->datatable($request);
		}
		return redirect()->route('offender.index');
	}

	public function download($file_id)
    {
        $instruction = $this->offenderInstruction->find($file_id);
        return Storage::download($instruction->file, $instruction->filename);
    }

    public function remove_file($id)
    {
    	if($this->offenderInstruction->find($id)){
    		$this->offenderInstruction->find($id)->delete();
	    }
    }

	public function offender_id_exists($offender_id)
	{
		return is_null($this->offender->whereOffenderId($offender_id)->first()) ? 1 : 0;
    }
}
