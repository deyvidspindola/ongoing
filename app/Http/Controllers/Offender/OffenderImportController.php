<?php

namespace App\Http\Controllers\Offender;

use App\Exports\LogOffendersImport;
use App\Imports\OffendersImport;
use App\Models\OfenderImportLogs;
use App\Models\OffenderDataLoad;
use App\Services\OffenderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class OffenderImportController extends Controller
{

	/**
	 * @var OffenderDataLoad
	 */
	private $offenderDataLoad;
    /**
     * @var OffenderService
     */
    private $offenderService;

    public function __construct(
	    OffenderDataLoad $offenderDataLoad,
        OffenderService $offenderService
    )
	{
		$this->offenderDataLoad = $offenderDataLoad;
        $this->offenderService = $offenderService;
    }

	public function index(Request $request)
	{
		$this->authorize('offender_list');
		$offenderDataLoad = $this->offenderDataLoad->filter($request)->ordered()->paginate();
        $userSolverGroups = $this->offenderService->usersSolverGroups();
		return view('offender_import.index', compact('request', 'offenderDataLoad', 'userSolverGroups'));
	}

	public function import(Request $request)
	{
		$offenderImport = new OffendersImport($request->get('solver_group_filter'));
		try {
			$offenderImport->import($request->file('file'));
		} catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
			$failures = $e->failures();
			return $failures;
		}

		$filename =  $request->file('file')->getClientOriginalName();
		$filenameLog = explode('.', $filename);
		$filenameLog = \Str::slug($filenameLog[0].'-log-data-'.date('y-m-d H:i:m:s')).'.xlsx';
		(new LogOffendersImport)->store($filenameLog);
		OfenderImportLogs::find(1)->delete();
		OffenderDataLoad::create(['filename' => $filename, 'user_id' => auth()->user()->id, 'log_filename' => $filenameLog]);
	}

	public function log_download($id)
	{
		$fileLog = OffenderDataLoad::find($id);
		return Storage::download($fileLog->log_filename);
	}

}
