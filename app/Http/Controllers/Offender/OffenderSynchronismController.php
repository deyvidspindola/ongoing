<?php

namespace App\Http\Controllers\Offender;

use App\Exports\SynchronismExport;
use App\Http\Controllers\Controller;
use App\Models\Offender;
use App\Models\OffenderSynchronism;
use App\Services\OffenderSynchronismService;
use Illuminate\Http\Request;

class OffenderSynchronismController extends Controller
{


    /**
     * @var Offender
     */
    private $offender;
    /**
     * @var OffenderSynchronismService
     */
    private $offenderSynchronismService;
    /**
     * @var OffenderSynchronism
     */
    private $synchronism;


    public function __construct(
        OffenderSynchronism $synchronism,
        Offender $offender,
        OffenderSynchronismService $offenderSynchronismService
    )
    {
        $this->offender = $offender;
        $this->offenderSynchronismService = $offenderSynchronismService;
        $this->synchronism = $synchronism;
    }

    public function index()
    {
        $this->authorize('synchronism_list');
        $types = config('ongoing.synchronism_types');
        $types[0] = 'Todos';
        return view('offender_synchronism.index', compact('types'));
    }

    public function create()
    {
        $this->authorize('synchronism_add');
        return view('offender_synchronism.create');
    }

    public function store(Request $request)
    {
        $this->authorize('synchronism_add');
        try {
            $this->offenderSynchronismService->store($request->except('_token'));
            return redirect()->route('offender.synchronism.index')->with('msg', 'Registro inserido com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function show_details($id)
    {
        $this->authorize('synchronism_edit');
        $synchronism = $this->synchronism->withTrashed()->with('offender')->find($id);
        $synchronism['offender_id'] = $synchronism->offender->id;
        $synchronism['offender'] = $synchronism->offender->offender_id.' - '.$synchronism->offender->offender;
        return view('offender_synchronism.modal', compact('synchronism'))->render();
    }

    public function edit($id)
    {
        $this->authorize('synchronism_edit');
        $synchronism = $this->synchronism->find($id);
        $synchronism['offender_id'] = $synchronism->offender->id;
        $synchronism['offender'] = $synchronism->offender->offender_id.' - '.$synchronism->offender->offender;
        $rowId = $id;
        return view('offender_synchronism.edit', compact('synchronism', 'rowId'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('synchronism_edit');
        try {
            $this->offenderSynchronismService->update($request->except('_token', '_method'), $id);
            return redirect()->route('offender.synchronism.index')->with('msg', 'Registro atualizado com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function destroy($id)
    {
        $this->authorize('synchronism_del');
        try {

            if ($this->offenderSynchronismService->destroy($id)) {
                return response()->json([
                    'title' => 'Sucesso!',
                    'message' => 'Registro Exluido com sucesso'
                ], 200);
            }

            return response()->json([
                'title' => 'Erro!',
                'message' => 'Não é possivel excluir esse registro, existem relacionamentos com ele'
            ], 400);

        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Erro!',
                'message' => 'Ocorreu um erro ao tentar exluir esse registro'
            ], 400);
        }
    }

    public function search(Request $request)
    {
        echo $this->offenderSynchronismService->search($request->all());
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            return $this->offenderSynchronismService->datatable($request);
        }
        return redirect()->route('offender.index');
    }

    public function get_row()
    {
        $rowId = (string) \Str::uuid();
        return view('offender_synchronism.inc.form', compact('rowId'))->render();
    }

    public function export(Request $request)
    {
        return (new SynchronismExport($request->all()))->download('HISTORICO DE OFENSORES.xlsx');
    }
}
