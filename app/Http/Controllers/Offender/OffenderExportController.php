<?php

namespace App\Http\Controllers\Offender;

use App\Exports\OffendersExport;
use App\Services\OffenderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OffenderExportController extends Controller
{

	/**
	 * @var OffenderService
	 */
	private $offenderService;

	public function __construct(OffenderService $offenderService)
	{
		$this->offenderService = $offenderService;
	}

	public function index()
	{
		$userSolverGroups = $this->offenderService->usersSolverGroups();
		return view('offender_export.index', compact('userSolverGroups'));
	}

	public function export(Request $request)
	{
        return (new OffendersExport($request->solver_group_filter))->download('OFENSORES DE PORTABILIDADE NUMÉRICA V10.09_NET.xlsx');
	}

}
