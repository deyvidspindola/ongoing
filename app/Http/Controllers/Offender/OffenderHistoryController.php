<?php

namespace App\Http\Controllers\Offender;

use App\Exports\OffendersExportHystory;
use App\Http\Traits\OffenderImportTrait;
use App\Models\MacroOffender;
use App\Models\OffenderHistory;
use App\Models\OffenderMacroOffender;
use App\Services\OffenderHistoryService;
use App\Services\OffenderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OffenderHistoryController extends Controller
{

    use OffenderImportTrait;

	/**
	 * @var OffenderHistoryService
	 */
	private $historyService;
	/**
	 * @var OffenderHistory
	 */
	private $offenderHistory;
	/**
	 * @var OffenderService
	 */
	private $offenderService;
	/**
	 * @var MacroOffender
	 */
	private $macroOffender;
	/**
	 * @var OffenderMacroOffender
	 */
	private $offenderMacroOffender;

	public function __construct(
		OffenderHistoryService $historyService,
		OffenderHistory $offenderHistory,
		OffenderService $offenderService,
		MacroOffender $macroOffender,
		OffenderMacroOffender $offenderMacroOffender
	)
	{
		$this->historyService = $historyService;
		$this->offenderHistory = $offenderHistory;
		$this->offenderService = $offenderService;
		$this->macroOffender = $macroOffender;
		$this->offenderMacroOffender = $offenderMacroOffender;
	}

	public function index()
	{
		$this->authorize('offender_list');
		$userSolverGroups = $this->offenderService->usersSolverGroups();
        $userSolverGroups['Todos'] = 'Todos';

        return view('offender_history.index',compact('userSolverGroups'));
	}

	public function show_details($id)
	{
		$this->authorize('offender_list');
		$offenderHistory = $this->offenderHistory->find($id);
		$offender = $offenderHistory->snapshot;
		$offender['user'] = $offenderHistory->user->name;
		$offender['modified_fields'] = $offenderHistory->modified_fields;
		$offender['operation_type'] = config('ongoing.history_types.'.$offenderHistory->operation_type);
		if (!empty($offenderHistory->observation2)){
            $offender['observation'] .= $offenderHistory->observation2;
        }
		$offender['created_at'] = $offenderHistory->created_at;
		if (isset($offender['macro_offender_id']) && $offender['macro_offender_id'] > 0){
			$offender['macro_offender'] = $this->macroOffender->find($offender['macro_offender_id'])->macro_offender_id.' - '.$this->macroOffender->find($offender['macro_offender_id'])->macro_offender;
		}

		$resolverGroup = $this->offenderService->usersSolverGroups();
		return view('offender_history.modal', compact('offender','resolverGroup'))->render();
	}

	public function search(Request $request)
	{
		echo $this->historyService->search($request->all());
	}

	public function datatable(Request $request)
	{
		if ($request->ajax()) {
			return $this->historyService->datatable($request);
		}
		return redirect()->route('offender.history.index');
	}

	public function export(Request $request)
	{
		return (new OffendersExportHystory($request->all()))->download('HISTORICO DE OFENSORES.xlsx');
	}

}
