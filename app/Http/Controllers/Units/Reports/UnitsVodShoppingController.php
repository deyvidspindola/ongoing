<?php


namespace App\Http\Controllers\Units\Reports;

use App\Exports\Units\VodShoppingExport;
use App\Services\Units\Reports\VodShoppingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitsVodShoppingController
{

    /**
     * @var VodShoppingService
     */
    private $vodShoppingService;

    public function __construct(VodShoppingService $vodShoppingService)
    {
        $this->vodShoppingService = $vodShoppingService;
    }

    public function comprasVod()
    {

        $types =  [
            'SVOD_NET'      => 'SVOD NET',
            'TVOD_NET'      => 'TVOD NET',
            'NW_SVOD'       => 'NOWONLINE SVOD NET',
            'NW_NET'        => 'NOWONLINE TVOD NET',
            'TVOD_CLARO'    => 'TVOD CLARO',
        ];

        $status = [
            ''      => 'TODOS',
            'OK'    => 'Processado OK',
            'NOK'   => 'Processamento Pendente',
        ];

        return view('units.reports.vod_shopping.index', compact('types', 'status'));

    }


    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            return $this->vodShoppingService->datatable($request);
        }
        return redirect()->route('units.relatorios.compras_vod ');
    }

    public function export(Request $request)
    {
        $list = $this->vodShoppingService->getSelect($request);
        return (new VodShoppingExport($list))->download('Compras_VOD.xlsx');
    }

}