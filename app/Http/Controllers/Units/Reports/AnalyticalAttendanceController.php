<?php

namespace App\Http\Controllers\Units\Reports;

use App\Exports\Units\AnalyticalAttendanceExport;
use App\Services\Units\Reports\AnalyticalAttendanceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnalyticalAttendanceController extends Controller
{

    /**
     * @var AnalyticalAttendanceService
     */
    private $attendanceService;

    public function __construct(AnalyticalAttendanceService $attendanceService)
    {
        $this->attendanceService = $attendanceService;
    }

    public function index()
    {

        $status = [
            ''      => 'TODOS',
            'F'    => 'Sucesso',
            'E'   => 'Erro',
        ];

        return view('units.reports.analytical_attendance.index', compact( 'status'));

    }


    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            return $this->attendanceService->datatable($request);
        }
        return redirect()->route('units.relatorios.atendimentos-units ');
    }

    public function export(Request $request)
    {
        $list = (object) $this->attendanceService->getData($request);
        return (new AnalyticalAttendanceExport($list))->download('Relatório_Analitico_.xlsx');
    }

}
