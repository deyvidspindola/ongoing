<?php

namespace App\Http\Controllers\Units;

use App\Http\Controllers\Controller;
use App\Services\Units\UnitsReportService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;


class UnitsReportController extends Controller
{
    private $reportService;

    public function __construct
    (
        UnitsReportService $reportService
    )
    {
        $this->reportService = $reportService;
    }

    public function index(Request  $request)
    {
        return view('units.units_query_platform.index');
    }

    public function search(Request $request)
    {

        $customer = $request->customer;
        $operadora = substr($request->customer, -3, 3);
        $contrato = substr($request->customer, 0, (strlen($request->customer) - 3));
        $terminal = $request->smartcard;

        $data = $this->reportService->queryPlatform($customer, $operadora, $contrato, $terminal);

        return response()->json([
            $data
        ], 200);

    }


}

