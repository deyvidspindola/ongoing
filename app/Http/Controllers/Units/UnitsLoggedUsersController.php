<?php

namespace App\Http\Controllers\Units;

use App\Models\Session;
use App\Models\User;
use App\Services\UnitsDashboardService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UnitsLoggedUsersController extends Controller
{
    public $dashboardService;

    public function __construct
    (
        UnitsDashboardService $dashboardService
    )
    {
        $this->dashboardService = $dashboardService;
    }

    public function index()
    {
        $logged = $this->dashboardService->getUserList();
        return view('units.units_logged_users.index')->with(compact('logged'));

    }

    public function logout(Request $request)
    {
        $session = Session::whereUserId($request->id)->first();

        if (\Auth::user()->id == $session->user_id) {
            \Auth::logout();
//            $session->delete();
            return redirect('/login');
        }
        $session->delete();
        return redirect()->back();
    }

}
