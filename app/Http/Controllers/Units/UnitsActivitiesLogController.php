<?php

namespace App\Http\Controllers\Units;

use App\Models\Units\UnitsSolicitation;
use App\Services\Units\UnitsAcitivitiesLogService;
use App\Models\Units\UnitsService;
use App\Models\Units\UnitsMenu;
use App\Models\User;
use App\Services\Units\UnitsSolicitationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnitsActivitiesLogController extends Controller
{

    private $unitsActivitiesLogService;
    private $unitsService;
    private $unitsMenu;
    private $user;

    public function __construct(
        UnitsAcitivitiesLogService $unitsActivitiesLogService,
        UnitsService $unitsService,
        UnitsMenu $unitsMenu,
        User $user
    )
    {
        $this->unitsActivitiesLogService = $unitsActivitiesLogService;
        $this->unitsService = $unitsService;
        $this->unitsMenu = $unitsMenu;
        $this->user = $user;
    }

    public function index()
    {
       $this->authorize('units_activities_log_list');
       $unitsServices = $this->unitsService->get();
       $users = $this->user->orderBy('username')->pluck('username','username')->toArray();

       foreach ($unitsServices as $service) {

           if(!empty($service->slug)){
               $menu = $this->unitsMenu->bySlug($service->slug)->first();
               if($menu){
                   $services[ $service->id ] = strtoupper($menu['title'] . ' - ' . $service->description);
               }
           }else{
               $services[ $service->id ] = strtoupper($service->description);
           }
       }

       asort($services);

       return view('units.units_log_activities.index', compact('services','users'));
    }

    public function search(Request $request)
    {
        echo $this->unitsActivitiesLogService->search($request->all());
    }

    public function show_details($id)
    {
        $log_activities = UnitsSolicitation::find($id);
        if ($log_activities)
            return view('units.units_log_activities.modal', compact('log_activities'))->render();

        return false;
    }


    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            return $this->unitsActivitiesLogService->datatable($request);
        }
        return redirect()->route('units.index');
    }

}
