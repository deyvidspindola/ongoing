<?php

namespace App\Http\Controllers\Units;

use App\Http\Controllers\Controller;
use App\Http\Traits\UnitsPlSqlTrait;
use App\Http\Traits\UnitsRouteTrait;
use App\Http\Traits\UnitsTrait;
use App\Models\Units\UnitsMenu;
use App\Models\Units\UnitsOperation;
use App\Models\Units\UnitsOperator;
use App\Models\Units\UnitsService;
use App\Models\Units\UnitsSolicitation;
use App\Services\Units\UnitsDB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UnitsController extends Controller {

	use UnitsPlSqlTrait, UnitsTrait;

	/**
	 * @var UnitsService
	 */
	private $unitsService;
	/**
	 * @var UnitsOperation
	 */
	private $unitsOperation;
	/**
	 * @var UnitsSolicitation
	 */
	private $unitsSolicitation;
	private $unitsOperator;
    private $unitsDB;

	public function __construct(
		UnitsService $unitsService,
		UnitsOperation $unitsOperation,
		UnitsSolicitation $unitsSolicitation,
        UnitsOperator $unitsOperator,
        UnitsDB $unitsDB
	)
	{
		$this->unitsService = $unitsService;
		$this->unitsOperation = $unitsOperation;
		$this->unitsSolicitation = $unitsSolicitation;
		$this->unitsOperator = $unitsOperator;
		$this->unitsDB = $unitsDB;
	}

	public function index($slug)
	{
		$configs = $this->getConfigs($slug);
		$this->authorize($configs['permission']);
		$layout = $this->layout();
		$route = url("units/solicitacoes/form/$slug/");
		$data = $this->unitsService->bySlug($configs['slug'])->whereStatus('1')->orderBy('order')->get();
		return view('units.default.index', compact('layout', 'data', 'route'));
	}

	public function form($slug, $id)
	{
        $id = (is_numeric($id)) ? $id : $this->getSlug($slug);
        $configs = $this->getConfigs($id);
        $this->authorize($configs['permission']);
		$layout = $this->layout();
		$route = 'units.solicitacoes.store';
		$routeBack = substr('units/solicitacoes/index/'.$slug, 0, -2);

        $service = $this->unitsService->byCompany()->find($id);
		$operation = $this->unitsOperation->byCompany()->orderedByCity()->baseNotCtv()->get()->toArray();

		$data = [
			'service' => $service,
			'operations' => $operation,
            'operator' => $this->unitsOperator->byCompany()->get()->toArray(),
            'route_back' => $routeBack
		];

		return view('units.default.form', compact('layout', 'data', 'route'));
	}

	public function store(Request $request)
	{
//		$this->authorize('internal_portability_exec');
		$service = $this->unitsService->byCompany()->find($request->id);
//        dd($service);
		if ($service->id == 24) {
            $plSql = $this->trataSql($service->script_file, $request->all(), true);
        } else {
            $plSql = $this->trataSql($service->script_file, $request->all());
        }

        $data = $this->data($request);
        $solicitation = $this->unitsSolicitation->create($data);

        /*
         * Data: 20/08/2019
         * O motivo pelo qual fizemos da forma abaixo, é porque todos os scripts executados tratam dentro dele e retornam as
         * mensagens. Entendido isso, observamos que para todos os casos, ele devolve a mensagem em um Exception
         * A unica saida que encontramos, foi de não alterar o PL-SQL para não ocasionar nenhum tipo de problema na execução.
         * Portanto, a orientação que temos é de não alterar o script abaixo exceto em último caso!
         */


		try {

		    if ($service->units_group_id == 1 and $service->permission_name != 'units_servico_pc_incluir_area_local_ctv') {
		        $empresa = 'NET';
            } else if ($service->units_group_id == 1 and $service->permission_name == 'units_servico_pc_incluir_area_local_ctv') {
                $empresa = 'CLARO';
            } else if ($service->units_group_id == 4) {
		        $empresa = 'CLARO';
            }

			$run = $this->unitsDB->getDB($request->base_code_hide, $empresa)->select(DB::raw($plSql));

			$message = 'Script executado com sucesso!';

            $this->update($solicitation->id, 'F', $message);
			return redirect()->back()->with('msg', $message);
		} catch (\Illuminate\Database\QueryException $e){
            $message =  Str::after($e->getMessage(), 'Error Message :' );
            $message = Str::before($message, '.');

		    if ($e->getCode() == '20010') {
                $this->update($solicitation->id, 'F', $message);
                return redirect()->back()->with('msg', 'Script executado com sucesso!');
            } else {
		        $this->update($solicitation->id, 'E', $message);
                return redirect()->back()->withErrors($message);
            }
        }
	}


	public function update($id_solicitation, $status, $msg)
    {
        $solicitation = $this->unitsSolicitation->where('id', $id_solicitation)->first();
        $solicitation->status = $status;
        $solicitation->process_message = $msg;
        $solicitation->save();
        return true;
    }

	private function layout()
	{
        $current = url()->current();
	    $slug = explode('/', $current);

        $page_title = config('ongoing.breadcrumb.'.Arr::last($slug));

        if (!isset($page_title)) {
            $page_title = str_replace('-', ' ', Arr::last($slug));
            $page_title = Str::title($page_title);
        }

        $filtered = Arr::except($slug, [0,1,2,3,4,5]);

        $arr = ['Units'];
        foreach ($filtered as $v)
        {
            $name = config('ongoing.breadcrumb.'.$v);
            $l = str_replace('-', ' ', $v);
            $l = Str::title($l);
            if (!is_numeric($l)){
                $arr[] = (isset($name)) ? $name : $l;
            }
        }

        return [
			'breadcrumb'        => $arr,
			'page_title'        => $page_title,
			'page_title_small'  => 'Solicitações'
		];
	}

	private function getConfigs($slug)
	{
		$permission = UnitsMenu::whereSlug('units/solicitacoes/index/'.$slug)->select('permission')->first();
        if (is_null($permission)){
            $permission = UnitsService::whereId($slug)->select('permission_name as permission')->first();
        }

		$slug = $this->getSlug($slug);

		$data = [
			'permission' => $permission->permission,
			'slug' => $slug
		];

		return $data;

	}
	
}