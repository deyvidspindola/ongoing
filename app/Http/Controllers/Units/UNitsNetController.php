<?php

namespace App\Http\Controllers\Units;

use App\Http\Controllers\Controller;
use App\Services\Units\UnitsNetService;
use Illuminate\Http\Request;

class UnitsNetController extends Controller {

    private $unitsNetService;

    public function __construct(UnitsNetService $unitsNetService)
    {
        $this->unitsNetService = $unitsNetService;
    }

    public function getCustomer(Request $request)
    {
        try{

            $input = $request->all();
            if ($input['operadora'] == '21') {
                $base_claro = True;
            } else {
                $base_claro = False;
            }

            $data = $this->unitsNetService->buscaAssinante($input['operadora'], $input['contrato'], $base_claro);

            if(!$data){
                return response()->json(['message' => 'Contrato inválido ou não pertence a cidade informada.'], 400);
            }

            return response()->json(['success'=>'Ok', 'data' => $data]);
        } catch(\Exception $e){
            // TODO: Qual mensagem exibir para o usuário em caso de falha de conexão com a base de dados da net/claro?
            return response()->json(['error' => $e->getMessage(), 'message' => 'Erro de conexão com o banco de dados.']);

        }
    }

    public function getTerminal(Request $request)
    {
        try {
            $dados = $request->all();
            $dados['ddd'] = substr($dados["terminal"], 0, 2);
            $dados['telefone'] = substr($dados["terminal"], 2, 9);

            $data = $this->unitsNetService->buscaDadosTerminal($dados['operadora'],$dados['ddd'],$dados['telefone']);

            if (count($data) == 0 || !$data) {
                return response()->json(['message' => 'Telefone não encontrado'], 400);
            }

            return response()->json(['success' => 'Ok', 'data' => $data]);

        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'message' => 'Erro de conexão com o banco de dados.']);
        }
    }
}