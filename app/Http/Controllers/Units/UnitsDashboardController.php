<?php

namespace App\Http\Controllers\Units;

use App\Http\Controllers\Controller;
use App\Models\Units\UnitsService;
use App\Services\Units\UnitsSolicitationService;
use App\Services\UnitsDashboardService;
use App\Charts\Dashboard1;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

class UnitsDashboardController extends Controller {

  private $unitsDashboardService;
  private $unitsSolicitationService;
  private $unitsService;

  public function __construct(
      UnitsDashboardService $unitsDashboardService,
      UnitsSolicitationService $unitsSolicitationService,
      UnitsService $unitsService

  )
  {
    $this->unitsDashboardService = $unitsDashboardService;
    $this->unitsSolicitationService = $unitsSolicitationService;
    $this->unitsService = $unitsService;
  }

  /**
   *
   * @param Request $request
   * @return void
   */
    public function index(Request $request)
    {
//        $this->authorize('units_dashboard');
	    $userSolverGroups = $this->unitsDashboardService->usersSolverGroups();
        $userSolverGroups['Todos'] = 'Todos';

        $years = getYears(); // listar anos
        $months = getMonthNames(); // listar meses

        $data = [
            'portabilidade' => [
                2,4,6,7,9,10,16,17,18,19
            ],
            'servicosSlug' => [
                8 => 'portabilidade-interna',
                7 => 'portabilidade-intrinseca',
                1 => 'status-contrato',
                6 => 'incluir-area-local-ctv',
                4 =>'inserir-area-local',
                5 => 'troca-fqdn',
                2 => 'retirada-ponto-opcao',
                0 => 'bilhete-portabilidade',
                3 => 'mudanca-endereco'
            ],
        ];
        ksort($data['servicosSlug']);

        $year = false;
        $month = false;
        if (isset($request->year_filter)) {
            $year = $request->year_filter;
        }
        if (isset($request->month_filter)) {
            $month = $request->month_filter;
        }

        $solicitacoes = $this->getQtdSolicitations($month, $year);

        $getPercent = $this->getPercentBySolicitationStatus($solicitacoes);

        $solicitationsCount = $solicitacoes->count();

        if($solicitationsCount > 0) {
            $percent = [
                'error' => round(($getPercent['error'] * 100) / $solicitationsCount, 1),
                'success' => round(($getPercent['success'] * 100) / $solicitationsCount, 1),
                //            'pending'   => round(($getPercent['pending'] * 100) / $solicitationsCount, 1),
            ];
        } else {
            $percent = [
                'error' => 0,
                'success' => 0
            ];
        }

        $chartfirst = $this->getFirstChart($this->getCharts($data['portabilidade'],$month, $year));
        $chartSecondary = $this->getSecondtChart($data['servicosSlug'], $month, $year);

        $logged = $this->unitsDashboardService->getUserList();

        return view('units.units_dashboard.index', compact('userSolverGroups', 'years', 'months', 'chartfirst', 'chartSecondary', 'getPercent', 'percent', 'logged'));
    }

    public function getPercentBySolicitationStatus($solicitations)
    {
        $data = [
            'error' => 0,
            'success' => 0,
            'pending' => 0
        ];
        foreach ($solicitations as $solicitation) {
            switch ($solicitation->status) {
                case 'E':
                    $data['error'] += 1;
                    break;
                case 'F':
                    $data['success'] +=1;
                    break;
//                default:
//                    $data['pending'] +=1;
//                    break;
            }
        }
        return $data;
    }

    public function getQtdSolicitations($month, $year)
    {

        $oy = $this->unitsSolicitationService->getQtdSolicitations($month, $year);
        return $oy;
    }

    public function getCharts(array $services, $month = false, $year = false)
    {
        return $this->unitsSolicitationService->whereIn('units_service_id', $services, $month, $year);
    }

    public function getFirstChart($servicos)
    {
        $chartfirst = new Dashboard1;
        for ($i =1; $i<= date('t'); $i++) {
            $dias[] = $i;
        }

        $chartfirst->labels(array_values($dias));

        $info = [];
        foreach ($servicos as $key => $value) {
            if (!array_key_exists($key,$dias)) {
                $info[$value->dia] = 0;
            } else {
                $info[$value->dia] = $value->qtdByDay;
            }
        }
        foreach ($dias as $dia) {
            if (!isset($info[$dia])) {
                $info[$dia] = 0;
            }
        }
        ksort($info);

        $chartfirst->dataset('Portabilidade', 'line', array_values($info));
        $chartfirst->dataset('Now', 'line', array_values($info));

        return $chartfirst;
    }

    public function getSecondtChart($slugs, $month = false, $year = false)
    {
        $chart = new Dashboard1;

        $teste = $this->unitsSolicitationService->getAnaliticalData(array_values($slugs), $month, $year);
        $labels = [
            7 => 'Portabilidade Intrínseca',
            8 => 'Portabilidade Interna',
            6 => 'Portabilidade CTV',
            5 => 'Troca FQDN',
            4 => 'Incidentes/Inserir área local',
            3 => 'Incidentes/Mudança de endereço',
            2 => 'Incidentes/Retirada de Pto. Desc. por opção',
            1 => 'Incidentes/Status Contrato',
            0 => 'Incidentes/Bilhete Portabilidade'
        ];
        $dataset = [];
        foreach ($teste as $item) {
            $dataset[$item->slug] = $item->qtd;
        }

        foreach ($slugs as $slug) {
            if (!array_key_exists($slug, $dataset)) {
                $dataset[$slug] = 0;
            }
        }
        ksort($dataset);
        asort($labels);

        $chart->labels(array_values($labels));
        $chart->dataset('Serviço', 'pie', array_values($dataset))->options([
            'color' => ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                '#492970', '#f28f43', '#77a1e5', '#c42525']
        ]);

        return $chart;
    }
}