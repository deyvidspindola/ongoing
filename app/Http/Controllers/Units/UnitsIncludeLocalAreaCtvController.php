<?php

namespace App\Http\Controllers\Units;

//use App\Services\Units\UnitsOperatorService;
use App\Services\Units\UnitsOperationService;
use App\Services\Units\UnitsServiceService;
use App\Services\Units\UnitsSolicitationService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnitsIncludeLocalAreaCtvController extends Controller
{
    public function index()
    {
        //$this->authorize('units_site_shortcuts');
        //$userSolverGroups = $this->unitsSiteShortcutService->usersSolverGroups();
        //$userSolverGroups['Todos'] = 'Todos';
        //return view('units.units_site_shortcuts.index', compact('userSolverGroups'));

        // Seleciona as operadoras ( AS CIDADES )
        $units_operation_data = [];
        try{
//            $units_operation_data = $this->unitsOperationService->all();
        } catch(ModelNotFoundException $e){
            $units_operation_data = [];
        }

        // Seleciona as operadoras
        $units_service_data = [];
        try {
            $units_operator_data = $this->unitsOperatorService->all();
        } catch (ModelNotFoundException $e) {
            $units_operator_data = [];
        }

        // Seleciona o servi�o
        $units_service_data = [];
        try{
            $units_service_data = $this->unitsServiceService->find(2);
        } catch(ModelNotFoundException $e){
            $units_service_data = [];
        }

        return view('units.units_include_local_area_ctv.index')->with(compact('units_operation_data', 'units_service_data', 'units_operator_data'));
    }

    private $unitsOperationService;
    private $unitsServiceService;
    private $unitsSolicitationService;
    private $unitsOperatorService;

    public function __construct
    (
        UnitsOperationService $unitsOperationService,
        UnitsServiceService $unitsServiceService,
        UnitsSolicitationService $unitsSolicitationService
//        UnitsOperatorService $unitsOperatorService

    )
    {
        $this->unitsOperationService = $unitsOperationService;
        $this->unitsServiceService = $unitsServiceService;
        $this->unitsSolicitationService = $unitsSolicitationService;
//        $this->unitsOperatorService = $unitsOperatorService;
    }

    public function form()
    {
        // Seleciona as operadoras ( AS CIDADES )
        $units_operation_data = [];
        try{
//            $units_operation_data = $this->unitsOperationService->all();
        } catch(ModelNotFoundException $e){
            $units_operation_data = [];
        }

        // Seleciona as operadoras
        $units_service_data = [];
        try {
            $units_operator_data = $this->unitsOperatorService->byCompany()->get();
        } catch (ModelNotFoundException $e) {
            $units_operator_data = [];
        }

        // Seleciona o servi�o
        $units_service_data = [];
        try{
            $units_service_data = $this->unitsServiceService->byCompany()->find(2);
        } catch(ModelNotFoundException $e){
            $units_service_data = [];
        }

        return view('units.units_include_local_area_ctv.form')->with(compact('units_operation_data', 'units_service_data', 'units_operator_data'));
    }

    public function store(Request $request)
    {
        $service = $this->unitsServiceService->byCompany()->find($request->units_service_id);

        $arquivo = fopen($service->script_file, 'r');
        $linha = '';
        //L� o conte�do do arquivo
        while(!feof($arquivo)) {
            //Mostra uma linha do arquivo
            $linha .= fgets($arquivo, 1024);
        }

        $request->terminal_number = preg_replace('/\D/', '', $request->terminal_number);
        $linha = str_replace('[CONTRATO]', "{$request->contract_code}", $linha);
        $linha = str_replace('[TERMINAL]', "{$request->terminal_number}", $linha);
        $linha = str_replace('[OPERADORA]', "{$request->base_code_hide}", $linha);
        $linha = str_replace('[SOFTX]', "{$request->city_contract_hide}", $linha);
        $linha = str_replace('[COD_OPERADORA]', $request->operadora, $linha);


dd($linha);
        // Fecha arquivo aberto
        fclose($arquivo);

        //1� Passo!
        //Salvar o registro do procedimento;

        $dados['username']              = auth()->user()->username;
        $dados['remote_ip']             = request()->ip();
        $dados['units_service_id']      = $request->units_service_id;
        $dados['units_operation_id']    = $request->base_code_hide;
        $dados['contract_number']       = $request->contract_code;
        $dados['terminal']              = preg_replace('/\D/', '', $request->terminal_number);
        $dados['node']                  = $request->node;
        $dados['pa']                    = $request->pa;
        $dados['status']                = $request->status;
        $dados['softx']                 = $request->city_contract_hide;

        $this->unitsSolicitationService->store($dados);
        $run = \DB::connection('ora_base_claro_dev')
            ->select(\DB::raw($linha));

        if (!$run) {
            return redirect()->back();
        }
        return redirect('/',200,'','' );
    }
}

