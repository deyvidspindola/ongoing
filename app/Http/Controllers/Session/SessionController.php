<?php

namespace App\Http\Controllers\Session;

use App\Models\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SessionController extends Controller
{

    // TROCAR UMA EMPRESA NA SESSION

    public function companyChoice(Request $request)
    {

        $id = $request->id;

        if (!$id || !Auth::user()->isAdmin()) {
            return redirect()->back();
        }
        $company = Company::find(intval($id));

        if(!$company){
            return redirect()->back();
        }
        // Altero o valor da sessão

        session(['company' => [
            'id'   => $company->id,
            'name' => $company->description,
        ]]);

        return redirect()->back();

    }

    // TROCAR UMA LINGUA NA SESSION

    public function languageChoice(Request $request)
    {

        // Realizo a troco da lingua somente se estiver liberado no
        // arquivo de configuração
        if(config('sys.show_locale')){
            session()->put('locale', $request->id);
        }
        return redirect()->back();

    }

}
