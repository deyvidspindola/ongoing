<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\Ad;
use App\Models\Company;
use App\Models\Group;
use App\Models\Parameter;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller Personalizado
    |--------------------------------------------------------------------------
    |
    | A partir dessa linha a autenticação é personalizada para a aplicação.
    |
    */

    public function username()
    {
        return 'username';
    }

    public function login(Request $request)
    {

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {

            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // Recupero as credenciais
        $credentials = $this->credentials($request);

        // Mensagem de erro
        $messageError = Lang::get('auth.failed');

        // ADMIN GERAL
        if( ($credentials[$this->username()] === config('sys.parameters.admin_user')) && env('ADMIN_ACCESS', false) && ($this->local($credentials)) ) {

            return $this->sendLoginResponse($request);

        }

        // OUTROS USUÁRIOS
        elseif($credentials[$this->username()] != config('sys.parameters.admin_user')) {

            // Recupero a empresa pelo usuário logado
            $credential = array_filter(explode('@', $credentials[$this->username()]));

            // Recupero o usuário
            $credentials[$this->username()] = $credential[0];
            // Defino a empresa
            $company = null;

            // RECUPERO A EMPRESA
            // ---------------------------------------------------------------

            // Caso tenha informado a empresa por parametro
            if (count($credential) > 1) {

                // Recupero o dominio
                $domain = $credential[1];

                // Recupero a empresa selecionada
                $company = Company::byDomain($domain)->first();
            } // Caso não tenha informado a empresa, e tenha somente uma empresa cadastrada
            elseif (Company::count() == 1) {

                // Recupero a empresa
                $company = Company::first();

            }

            // Caso não tenha informado uma empresa, e exista mais que uma empresa cadastrada
            else{
                    $messageError = Lang::get('sys.auth.company');
            }
            // EMPRESA RECUPERADA
            // ---------------------------------------------------------------

            // Continuo a autenticação somente se existe a empresa
            if($company) {

                // incluo nas credenciais a empresa
                $credentials['company_id'] = $company->id;

                // Recupero os parametros do sistema pela empresa selecionada
                $parameters = Parameter::byCompany($credentials['company_id'])->first();


                // Realizo a autenticação, somente se a empresa tiver parametros
                if ($parameters) {

                    if(// AUTENTICAÇÃO LOCAL
                        (!$parameters->auth_ad && $this->local($credentials)) ||
                        // AUTENTICAÇÃO LDAP
                        ($parameters->auth_ad && $this->ldap($credentials))) {

                        return $this->sendLoginResponse($request);
                    }
                } else{
                    $messageError = Lang::get('auth.company');
                }

            }

            // Caso não tenha encontrado a empresa e não mudou a mensagem de erro padrão
            elseif($messageError == Lang::get('auth.failed')){
                $messageError = Lang::get('auth.company');
            }
        }

        return $this->sendFailedLoginResponse($request, $messageError);

    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        // Registro a empresa autenticada
        if(!session()->exists('company')){

            // Recupero a empresa do usuário
            $company = Company::find(Auth::user()->company_id);

            // Seto na sessão
            session(['company' => [
                'id'   => $company->id,
                'name' => $company->description
            ]]);

        }

        // Recupero as empresas cadastradas
        if(!session()->exists('companies')) {

            $companies = Company::ordered()->get();

            // Seto na sessão
            session(['companies' => $companies]);
        }

        // Defino o locale do usuário
        if(!session()->exists('locale')){

            $locale = (Auth::user()->locale) ? Auth::user()->locale : App::getLocale();

            session(['locale' => $locale]);

        }

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }


    protected function sendFailedLoginResponse(Request $request, $message = null)
    {

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.

        $this->incrementLoginAttempts($request);

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => $message,
            ]);
    }

    /*
    |--------------------------------------------------------------------------
    | TIPOS DE AUTENTICAÇÕES
    |--------------------------------------------------------------------------
    */


    /**
     * Autenticação Local (DB)
     */
    protected function local($credentials)
    {
        $credentials = [
            $this->username() => $credentials[$this->username()],
            'password' => $credentials['password'],
        ];

        return ($this->guard()->attempt($credentials)) ? true : false;
    }

    /**
     * Autenticação LDAP
     */
    protected function ldap($credentials)
    {

        // Recupero as informações de conexão com o AD de acordo com a empresa
        $ad = Ad::byCompany($credentials['company_id'])->first();

        // Se tem os parametros no ad, e acesso liberado
        if($ad) {

            $connect = ldap_connect($ad->connection);

            // Conexão LDAP
            ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

            // Defino o nome do usuário no LDAP
            $userPrincipalName = $credentials[$this->username()] . '@' . $ad->domain;

            // Autenticação LDAP
            if (@ldap_bind($connect, $userPrincipalName, $credentials['password'])) {

                // Realizo a busca do das informações do usuario no LDAP
                $ldap_search = ldap_search(
                    $connect, $ad->dn_users,
                    "(userPrincipalName=$userPrincipalName)",
                    explode(',', $ad->fields)
                );

                // Recupero os dados de entrada do usuário
                $ldap_get_entries = ldap_get_entries($connect, $ldap_search);

                // Encerro a conexão
                ldap_unbind($connect);

                // Recupero os grupos cadastrados no sistema
                $groups = Group::byCompany($credentials['company_id'])->where('name', '<>', 'admin')->pluck('name', 'id')->toArray();

                // Recupero os grupos que o usuário faz parte no AD
                $groups_ad = (isset($ldap_get_entries[0]['memberof'])) ? formatGroupsLdap($ldap_get_entries[0]['memberof']) : [];
                // Validação dos grupos

                // 1) Verifico se existe grupos cadastrados no sistema
                // 2) Verifico se existe grupos do usuário no AD
                // 3) Verifico se o usuário pertence aos grupos do sistema


                if (empty($groups) || empty($groups_ad) || !in_array_groups($groups, $groups_ad)) {
                    $messageError = Lang::get('auth.not_in_a_group');
                    return false;
                }
                // Recupero os dados do usuário na DB, da empresa selecionada

                $user = User::byUsername($credentials[$this->username()])->byCompany($credentials['company_id'])->first();

                // FORMATO A INFORMAÇÕES DO LDAP PARA O CADASTRO DO USUÁRIO

                // Recupero o id da empresa
                $company_id = $credentials['company_id'];

                // Recupero o primeiro nome
                $nome = isset($ldap_get_entries[0]['givenname'][0]) ? $ldap_get_entries[0]['givenname'][0] : '';

                // Recupero o sobrenome, mas valido se está setado
                $sobrenome = isset($ldap_get_entries[0]['sn'][0]) ? $ldap_get_entries[0]['sn'][0] : '';

                // Formato o name (nome + sobrenome)
                $name = trim($nome.' '.$sobrenome);

                // Recupero o username
                $username = $credentials[$this->username()];

                // Recupero a senha
                $password = bcrypt($credentials['password']);

                // Recupero o email
                $email = ( isset($ldap_get_entries[0]['mail']) && !empty($ldap_get_entries[0]['mail']) ) ? $ldap_get_entries[0]['mail'][0] : null;


                // CADASTRO O USUÁRIO, CASO NÃO EXISTA

                if (!$user) {

                    $user = new User;

                    $user->company_id = $company_id;
                    $user->name       = $name;
                    $user->username   = $username;
                    $user->password   = $password;
                    $user->email      = $email;

                    $user->save();

                }

                // ATUALIZO O CADASTRO, CASO O USUÁRIO EXISTA

                else{

                    $user_update['name']     = $name;
                    $user_update['password'] = $password;

                    if (($email && !$user->email) || ($email && $user->email && ($email != $user->email))) {
                        $user_update['email'] = $email;
                    }
                    $user->update($user_update);

                }

                // ATUALIZAÇÃO DOS GRUPOS DO USUÁRIO

                // Recupero quais grupos do usuário no AD existem no sistema
                // Obtendo os ids dos grupos
                $groups_in_system = array_keys(array_intersect($groups, $groups_ad));

                // Realizo a sincronização dos grupos do usuário
                $user->groups()->sync($groups_in_system);


                // Se o acesso está liberado, finalizo a autenticação
                // Finalizo a autenticação do usuário
                Auth::login($user);

                return true;

            }

        }

        // Caso tenha ocorrido algum erro na autenticação
        return false;

    }
}
