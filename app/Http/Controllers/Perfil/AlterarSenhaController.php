<?php

namespace App\Http\Controllers\Perfil;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;


class AlterarSenhaController extends Controller
{

    /**
     * @var User
     */
    private $user;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(
        User $user,
        UserService $userService
    ){
        $this->user = $user;
        $this->userService = $userService;
    }

    public function index()
    {
        return view('perfil.alterar_senha.index');
    }

    public function update(Request $request)
    {

        try {

            $this->userService->updatePassword($request);

            return redirect()->route('perfil.alterar_senha.index')->with('msg', Lang::get('sys.msg.success.update_password'));

        }
        catch(\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

    }


}



