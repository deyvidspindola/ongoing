<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UnitsOperationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'base_name' => $this->uppercase($this->base_name),
        'base_code' => $this->base_code,
        'city_contract' => $this->city_contract,
        'city_name' => strtoupper($this->city_name),
        'uf' => strtoupper($this->uf)
      ];
    }

    private function uppercase($string){
        return strtoupper($string);
    }

}