<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UnitsServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'description' => $this->description,
        'description_short' => $this->description_short,
        'description_detailed' => $this->description_detailed,
        'permission_name' => $this->permission_name,
        'permission' => $this->permission,
        'script_file' => strtolower($this->script_file),
        'online_process' => $this->online_process,
        'additional_fields' => $this->additional_fields,
        'flag_fields' => $this->flag_fields,
        'send_ebt_service' => $this->send_ebt_service,
        'units_group_id' => $this->units_group_id,
        'units_ebt_service_id' => $this->units_ebt_service_id
      ];

    }

}
