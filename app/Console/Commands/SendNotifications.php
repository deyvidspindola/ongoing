<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Notifications\Instruction;
use App\Notifications\InstructionId;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to user for instruction offender file is null';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $instructions = DB::table('offender_instructions')
            ->join('offenders', function ($join) {
                $join->on('offenders.id', '=', 'offender_instructions.offender_id')
                    ->where('offenders.type', '=', 'Externo')
                    ->where('offenders.status', '=', 'Ativo')
                    ->where('offenders.retention_level', '=', 'N1')
                    ->whereNull('offenders.deleted_at')
                    ->whereRaw("REPLACE(LOWER(offenders.classification), ' ', '_') in ('falha_de_processo', 'falha_na_abertura_do_chamado')");
            })
            ->whereNull('offender_instructions.file')
            ->select('offender_instructions.*', 'offenders.user_id')
            ->get();

		$count = 0;
		$today = Carbon::now();
//		$today = Carbon::now()->addDay(15);

		//envia as notificações para os ofensores que não possui anexo
	    foreach ($instructions as $instruction){

            $days = Carbon::parse($instruction->created_at)->diffInDays($today);

		    if ($days >= 3){

			    User::find($instruction->user_id)->notify(new Instruction($instruction));
			    sleep(5);
				$count++;
		    }

	    }

        $instructions = DB::table('offender_instructions')
            ->join('offenders', function ($join) {
                $join->on('offenders.id', '=', 'offender_instructions.offender_id')
                    ->where('offenders.type', '=', 'Externo')
                    ->where('offenders.status', '=', 'Ativo')
                    ->where('offenders.retention_level', '=', 'N1')
                    ->whereNull('offenders.deleted_at')
                    ->whereRaw("REPLACE(LOWER(offenders.classification), ' ', '_') in ('falha_de_processo', 'falha_na_abertura_do_chamado')");
            })
            ->whereNotNull('offender_instructions.file')
            ->whereNull('offender_instructions.instruction_id')
            ->select('offender_instructions.*', 'offenders.user_id')
            ->get();

	    foreach ($instructions as $inst) {

            $days = Carbon::parse($inst->created_at)->diffInDays($today);

            if ($days >= 5){

                User::find($inst->user_id)->notify(new InstructionId($inst));
                sleep(5);
                $count++;
            }

        }

	    if($count > 0){
		    $this->info('Notificações enviadas.');
	    }else{
		    $this->warn('Não há notificações para serem enviadas.');
	    }

    }
}
