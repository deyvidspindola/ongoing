<?php

namespace App\Services;

/**
 * Class CAIntegration
 *
 * This class has the Soap client in charge of execute soap calls and return
 * their results to CAIntegration.
 *
 * @package App\CAIntegration
 */
class CAIntegration
{

    /**
     * @var SoapClient $client
     */
    private $client;

    /**
     * @var int $sessionID
     */
    private $sessionID;

    /**
     * @var array $criteria
     */
    private $criteria = [];

    /**
     * Limit of rows a request should retrieve
     */
    const MAX_ROWS = 250;

    /**
     * CAIntegration constructor.
     *
     */
    public function __construct()
    {
        ini_set("max_execution_time", 600);
        ini_set("default_socket_timeout", 600);

        $this->client = new \SoapClient(config('caservice.wsdl'), array("trace" => 1, "exception" => 0));

        $this->login();
    }

    /**
     * Log in to the webservice
     *
     * @return string $session_id
     */
    public function login()
    {
        $options = [
            'login' => [
                'username' => config('caservice.username'),
                'password' => config('caservice.password'),
            ]
        ];

        // Get the session id
        if (is_null($this->sessionID)) {
            $this->sessionID = $this->client->__soapCall("login", $options)->loginReturn;
        }
    }

    /**
     * Retrieve list handle for retrieving CA info
     */
    protected function findListHandle()
    {
        return $this->doQueryCall($this->criteria);
    }

    /**
     * Execute an select operation in the web-service
     *
     * @param array $options
     *
     * @return string XML
     */
    public function doSelectSOAPCall(array $criteria)
    {
        // Set session id
        $criteria['sid'] = $this->sessionID;

        if (!isset($criteria['attributes'])) {
            $criteria['attributes'] = [];
        }

        $options = [
            'doSelect' => $criteria
        ];

        return $this->client->__soapCall("doSelect", $options)->doSelectReturn;
    }

    /**
     * Execute an do query operation in the webservice
     * returning list handle and list length
     *
     * @param array $criteria
     *
     * @return string XML
     */
    public function doQueryCall(array $criteria): \stdClass
    {
        // Set session id
        $criteria['sid'] = $this->sessionID;

        $options = [
            'doQuery' => $criteria
        ];

        return $this->client->__soapCall("doQuery", $options)->doQueryReturn;
    }

    /**
     * Execute an get list value operation in the webservice
     * returning the list items from a determined start point
     *
     * @param array $criteria
     *
     * @return string XML
     */
    public function doGetListValuesCall(array $criteria)
    {
        // Set session id
        $criteria['sid'] = $this->sessionID;

        // If we want to return some specific fields, we should put them here
        if (!isset($criteria['attributeNames'])) {
            $criteria['attributeNames'] = [];
        }

        $options = [
            'getListValues' => $criteria
        ];

        return $this->client->__soapCall("getListValues", $options)->getListValuesReturn;
    }

}