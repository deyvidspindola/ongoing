<?php


namespace App\CAIntegration\Services;


use App\CAIntegration\Utils\ParserXMLReturn;
use App\Services\CAIntegration;
use http\Exception\InvalidArgumentException;

class AnalystCAService extends CAIntegration
{

    /**
     * Analyst id
     *
     * @var string $analystId
     */
    private $analystId;

    /**
     * @var array $analyst
     */
    private $analyst = [];

    /**
     * Where clause format
     *
     * @var string $format
     */
    private $format = "id = U'%s'";

    /**
     * @var array $fieldMap
     */
    private $fieldMap = [
        "last_name"     => "last_name",
        "email_address" => "email"
    ];

    public function run()
    {

        $criteria["maxRows"]     = 1;
        $criteria['objectType']  = 'cnt';
        $criteria['whereClause'] = sprintf($this->format, $this->analystId);

        $this->analyst = $this->doSelectSOAPCall($criteria);

        $this->analyst = ParserXMLReturn::parseXML($this->analyst, $this->fieldMap, false);

    }

    /**
     *
     * @param string $analystId
     *
     * @return AnalystCAService
     */
    public function withAnalystId(string $analystId): AnalystCAService
    {
        $this->analystId = $analystId;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnalyst(): array
    {
        return $this->analyst;
    }

}