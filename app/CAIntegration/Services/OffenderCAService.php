<?php


namespace App\CAIntegration\Services;


use App\CAIntegration\Utils\ParserXMLReturn;
use App\Models\Offender;
use App\Models\OffenderHistory;
use App\Models\SolverGroups;
use App\Services\CAIntegration;

class OffenderCAService extends CAIntegration
{

    /**
     * @var AnalystCAService $analystCAService
     */
    private $analystCAService;

    /**
     * @var Offender $offender
     */
    private $offenderModel;

    /**
     * @var array $offenderFieldMap
     */
    private $offenderFieldMap = [
        'zsym'            => "offender",
        'zreso_user'      => "answer_for_the_user_in_the_call",
        'z_id_ofensor'    => 'id',
        'zClassificacao'  => 'classification',
        'zexplicacao_ofe' => 'detailed_explanation_offender',
        'zsintoma'        => 'symptom',
        'zfato'           => 'fact',
        'zcausa'          => 'cause',
        'zAcao'           => 'action'
    ];

    /**
     * @var string solver group identification hash used for retrieving offenders
     *
     * @see caservice config file
     */
    private $group;

    /**
     * @var string solver group name to find id
     *
     * @see caservice config file
     */
    private $groupName;

    /**
     * Contains the id of the offender's solve group
     *
     * @var int $groupId
     */
    private $groupId;

    /**
     * OffenderCAService constructor.
     */
    public function __construct(AnalystCAService $analystCAService, Offender $offenderModel)
    {
        parent::__construct();

        $this->offenderModel = $offenderModel;

        $this->analystCAService = $analystCAService;
    }

    /**
     * Starts the process
     */
    public function run()
    {

        // Group id of the offenders
        $this->groupId = SolverGroups::select('id')->where('name', $this->groupName)
                                                   ->first()
                                                   ->toArray()['id'];

        $offenders = [];

        $listHandler  = $this->findListHandler();

        $offendersIds = $this->findOffendersId($listHandler);

        $criteria['maxRows']    = 1;
        $criteria['objectType'] = 'z_resolution_code';
        $criteria['attributes'] = ['zsym', 'zreso_user','last_mod_by', 'zstatus_ppm', 'z_id_ofensor', 'zClassificacao',
                                    'zexplicacao_ofe', 'zsintoma', 'zfato', 'zcausa', 'zAcao'];

        foreach ($offendersIds as $offenderId) {
            $criteria["whereClause"] = sprintf("id = %s", $offenderId['offender_id']);

            $offender = $this->doSelectSOAPCall($criteria);

            $offender = ParserXMLReturn::parseXML($offender, $this->offenderFieldMap, false);

            $offenders[] = $offender;

        }

        $this->saveOrUpdateOffenders($offenders);

    }

    /**
     * Method responsible for save or update an DB offender register based on two criterias
     * 1 - If the offender retrieved in CA webservice doesn't exist locally, we save it.
     * 2 - If the offender retrieved in CA is different of its version in our DB, we update it.
     *
     * We iterate throughout the offenders Collection applying the two criterias above. By the end of
     * foreach loop we will keep in the collection just the offender which satisfies the two criterias
     * Any other offender that fails will be unsetted.
     *
     * @param array $offenders
     *
     */
    private function saveOrUpdateOffenders(array $offenders)
    {

        // Iterate throughout the collection applying the two criterias.
        // Note: refer to the method description
        foreach ($offenders as $offender) {

            // In some cases the offender returned by CA webservice doesn't has a valid ID, in this case
            // we go to the next
            if (!isset($offender['id']) || $offender['id'] === '0' || strlen($offender['id']) === 0) {
                continue;
            }

            // Tries to find offender in our DB
            $offenderDB = Offender::select('offender', 'detailed_explanation_offender', 'classification', 'ppm', 'id',
                'answer_for_the_user_in_the_call')
                ->where('id', $offender['id'])
                ->first();

            // If the current offender doesn't exists in DB, we stored it
            if (is_null($offenderDB)) {
                $offender['offender_type'] = "Externo";
                $offender['solver_group']  = $this->groupId;
                Offender::updateOrCreate(['id' => $offender['id']], $offender);
                continue;
            }

            // Convert it to array to do a array_diff with the array returned by the webservice
            $offenderDBArray = $offenderDB->toArray();

            $offenderDiff = array_diff($offender, $offenderDBArray);

            // If there are no differences we jump it
            if (count($offenderDiff) == 0) {
                continue;
            }

            // If the execution arrives here means that we have to create or update the offender
            Offender::updateOrCreate(['id' => $offender['id']], $offender);

            // Saves the change in offender history
            $offenderHistory = [
                'operation_type'  => 'E',
                'offender_id'     => $offender['id'],
                'snapshot'        => $offenderDB->toJson(),
                'observation_2'   => "Operação executada pelo CAService.",
                'modified_fields' => implode(", ", array_keys($offenderDiff)),
                'user_id'         => 1 // Let's consider that the ADMIN did this operation
            ];

            OffenderHistory::create($offenderHistory);
        }
    }

    /**
     * Receive the listHandler and iterate over the list in CA webservice
     * using pagination
     *
     * @param \stdClass $listHandler
     *
     * @return array
     */
    public function findOffendersId(\stdClass $listHandler)
    {
        $offendersIds           = [];

        // Length of the list. -1 due the last index
        $length                 = $listHandler->listLength -1;

        // List handle identification
        $criteria['listHandle'] = $listHandler->listHandle;

        // Fields we want to retrieve
        $fieldMap               = ["zresolution" => "offender_id"];

        // Limit of the list, i.e. last element to be retrieved
        $limit  = ($length <= self::MAX_ROWS) ? $length : self::MAX_ROWS;

        for ($startIndex = 0, $endIndex = $limit;
             $startIndex < $length;
             $startIndex += self::MAX_ROWS, $endIndex = ($length - $endIndex) + self::MAX_ROWS
        ) {
            // start the pagination
            $criteria['startIndex'] = $startIndex;

            // Last element in pagination
            $criteria['endIndex']   = $endIndex;

            // Retrieve the elements in window start and end index
            $listValuesXML   = $this->doGetListValuesCall($criteria);

            // Parse the offenders by using a $fieldMap
            $parsedOffenders = ParserXMLReturn::parseXML($listValuesXML, $fieldMap);

            // Merge the current array with the new one parsed in ParserXMLReturn
            $offendersIds    = array_merge($offendersIds, $parsedOffenders);
        }

        return $offendersIds;
    }

    /**
     * Get List handler
     */
    private function findListHandler()
    {
        $criteria                = [];
        $criteria['whereClause'] = sprintf("z_group = U'%s'", $this->group);
        $criteria['objectType']  = 'z_resolution_group';

        return $this->doQueryCall($criteria);
    }

    /**
     * @param string $group
     *
     * @return OffenderCAService
     */
    public function withGroup(string $group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @param string $groupName
     *
     * @return OffenderCAService
     */
    public function withGroupName(string $groupName)
    {
        $this->groupName = $groupName;
        return $this;
    }

}