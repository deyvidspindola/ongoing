<?php

namespace App\CAIntegration\Utils;

/**
 * Class ParserXMLReturn
 *
 * Parse XML provided by the webservice and return
 * an array with the elements
 *
 * This class uses protected variable fieldsMap to parse XML
 *
 * @package App\CAIntegration\Utils
 */
class ParserXMLReturn
{

    /**
     * Parse XML returned by CA SOAP web-service and return array with items.
     *
     * @param string $itemsXML
     * @param array  $fieldMap
     * @param bool   $shouldReturnArray
     *
     * @return array
     */
    public static function parseXML(string $itemsXML, array $fieldMap, bool $shouldReturnArray = true): array
    {
        $itemsXML = (new \SimpleXMLElement($itemsXML));

        $items = [];

        foreach ($itemsXML->UDSObject as $UDSObject) {

            foreach ($UDSObject->Attributes as $attributes) {

                $item = [];

                foreach ($attributes as $attribute) {

                    $attributeName  = (string) $attribute->AttrName;
                    $attributeValue = (string) $attribute->AttrValue;

                    if (!array_key_exists($attributeName, $fieldMap)) {
                        continue;
                    }

                     $columnName = $fieldMap[$attributeName];
                    // In some cases the webservice returns the value with an "\n" in the end, we need to remove it
                    // before set the value in the $item
                    $item[$columnName] = trim($attributeValue);

                }

                $items[] = $item;
            }
        }

        // If we should not return an array containing several items, we return just position 0.
        // If we'll parse just one element, then, the position 0 holds the only element available
        // that's why we are returning $items[0]
        return !$shouldReturnArray ? $items[0] : $items;
    }
}