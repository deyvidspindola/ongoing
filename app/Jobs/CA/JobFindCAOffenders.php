<?php

namespace App\Jobs\CA;

use App\CAIntegration\Services\OffenderCAService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobFindCAOffenders implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * @var OffenderCAService $offender
     */
    private $offenderCAService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OffenderCAService $offenderCAService)
    {
        $this->offenderCAService = $offenderCAService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groups = config('caservice.groups');

        foreach ($groups as $group) {
            $this->offenderCAService->withGroup($group['id'])
                                    ->withGroupName($group['name'])
                                    ->run();
        }
    }
}
