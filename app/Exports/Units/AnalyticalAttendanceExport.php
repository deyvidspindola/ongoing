<?php

namespace App\Exports\Units;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class AnalyticalAttendanceExport implements FromView
{
    use Exportable;

    /**
     * @var array
     */
    private $request;

    public function __construct(object $request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        return view('units.reports.analytical_attendance.sheets.analytical_attendance_sheet', [
            'list' => $this->request
        ]);

    }


}
