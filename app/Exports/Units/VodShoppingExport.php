<?php

namespace App\Exports\Units;

ini_set('memory_limit', '1000M');
ini_set('max_execution_time', '0');

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class VodShoppingExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    use Exportable;

    /**
     * @var array
     */
    private $request;

    public function __construct(object $request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        return view('units.reports.vod_shopping.sheets.vod_shopping_sheet', [
           'list' => $this->request
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
