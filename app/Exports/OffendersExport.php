<?php

namespace App\Exports;

use App\Exports\Sheets\DisabledOffendersSheet;
use App\Exports\Sheets\OffendersSheet;
use App\Exports\Sheets\OldDescriptionSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OffendersExport implements WithMultipleSheets
{
	use Exportable;

	protected $type;

	public function __construct(int $type)
    {
        $this->type = $type;
    }


    public function sheets(): array
	{
		return [
			'Ofensores' => new OffendersSheet($this->type),
			'Ofensores Desativados' => new DisabledOffendersSheet($this->type),
			'Descrição Antiga' => new OldDescriptionSheet($this->type)
		];
	}

}
