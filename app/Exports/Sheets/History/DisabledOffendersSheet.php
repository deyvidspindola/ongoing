<?php

namespace App\Exports\Sheets\History;

use App\Models\OffenderHistory;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class DisabledOffendersSheet implements FromView, WithTitle, WithEvents
{

	private $filters;

	public function __construct(array $filters)
	{
		$this->filters = $filters;
	}

    public function view(): View
    {
        return view('offender_history.sheets.disabled_offenders_sheet', [
            'offenders' => OffenderHistory::byFilterExtractExcel($this->filters)->whereSheet('ofensores_desativados')->get()
        ]);
    }


    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A4:'.$event->sheet->getHighestColumn().$event->sheet->getHighestRow();
                $array = [
                    'font' => [
                        'name' => 'Arial',
                        'bold' => false,
                        'italic' => false,
                        'underline' => false,
                        'strikethrough' => false,
                        'color' => [
                            'rgb' => '000000'
                        ]
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => [
                                'rgb' => '000000'
                            ]
                        ]
                    ]
                ];
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($array);

                $cellRange = 'A5:'.$event->sheet->getHighestColumn().$event->sheet->getHighestRow();
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                    ],
                ]);

                $A1_style = [
                    'font' => [
                        'name' => 'Arial',
                        'bold' => true,
                        'size' => 14,
                        'color' => [
                            'rgb' => 'FFFFFF'
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('A1')->applyFromArray($A1_style);

                $cellRange = 'A4:'.$event->sheet->getHighestColumn().'4';
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'name' => 'Arial',
                        'bold' => true,
                        'size' => 10,
                        'color' => [
                            'rgb' => 'FFFFFF'
                        ],
                    ],
                ]);

                $cells_width = [
                    'A' => 7.75,
                    'B' => 13.14,
                    'C' => 40.43,
                    'D' => 11.14,
                    'E' => 13.57,
                    'F' => 16.86,
                    'G' => 15.14,
                    'H' => 58.43,
                    'I' => 65.43,
                    'K' => 15.14,
                    'L' => 24.86,
                    'M' => 59.71,
                    'N' => 19.43,
                    'O' => 13.29,
                    'P' => 14.71,
                    'Q' => 20.43,
                    'R' => 36.14,
                    'S' => 16.14,
                    'T' => 18.71
                ];

                foreach ($cells_width as $cell => $width){
                    $event->sheet->getDelegate()->getColumnDimension($cell)->setWidth($width);
                }


            },
        ];
    }

	public function title(): string
	{
		return 'Ofensores Desativados';
	}

}
