<?php

namespace App\Exports\Sheets\Log;

use App\Models\OfenderImportLogs;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class LogDisabledOffendersSheet implements FromView, WithTitle, ShouldAutoSize
{

	public function view(): View
	{
		return view('offender_import.log_sheet.log_view', [
			'offenders' => OfenderImportLogs::select(\DB::Raw('ofensores_desativados as ofensores'))->get()->toArray()
		]);
	}

	public function title(): string
	{
		return 'Ofensores Desativados';
	}

}
