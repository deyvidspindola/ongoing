<?php

namespace App\Exports\Sheets\Synchronism;

use App\Http\Traits\SynchronismExportTrait;
use App\Models\OffenderSynchronism;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class SynchronismSheet implements FromView, WithTitle
{

	use SynchronismExportTrait;

	public function view(): View
	{
		$offenders = $this->createData(OffenderSynchronism::with('offender')->whereType(1)->get());
		return view('offender_synchronism.sheet.sheet_view', [
			'offenders' => $offenders
		]);
	}

	public function title(): string
	{
		return 'Sincronismo';
	}

}
