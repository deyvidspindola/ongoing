<?php

namespace App\Exports\Sheets\Synchronism;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class UntrainedSheet implements FromView, WithTitle
{
	public function view(): View
	{
		return view('offender_synchronism.sheet.sheet_view');
	}

	public function title(): string
	{
		return 'Sem tratativa';
	}
}
