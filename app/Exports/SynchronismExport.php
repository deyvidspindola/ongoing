<?php

namespace App\Exports;

use App\Exports\Sheets\Synchronism\OngoingSheet;
use App\Exports\Sheets\Synchronism\SynchronismSheet;
use App\Exports\Sheets\Synchronism\UntrainedSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SynchronismExport implements WithMultipleSheets
{
	use Exportable;

	public function sheets(): array
	{
		return [
			'Sincronismo' => new SynchronismSheet(),
			'Ongoing' => new OngoingSheet(),
			'Sem Tratativa' => new UntrainedSheet()
		];
	}
}
