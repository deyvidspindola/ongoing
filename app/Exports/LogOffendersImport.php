<?php

namespace App\Exports;

use App\Exports\Sheets\Log\LogDisabledOffendersSheet;
use App\Exports\Sheets\Log\LogDisabledTargetedOffendersSheet;
use App\Exports\Sheets\Log\LogOffendersSheet;
use App\Exports\Sheets\Log\LogOldDescriptionSheet;
use App\Exports\Sheets\Log\LogTargetedOffendersSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class LogOffendersImport implements WithMultipleSheets
{
	use Exportable;

	public function sheets(): array
	{
		return [
			'Ofensores' => new LogOffendersSheet(),
			'Ofensores direcionados' => new LogTargetedOffendersSheet(),
			'Ofensores Desativados' => new LogDisabledOffendersSheet(),
			'Descrição Antiga' => new LogOldDescriptionSheet(),
			'Of. direcionados desativdo' => new LogDisabledTargetedOffendersSheet(),
		];
	}
}
