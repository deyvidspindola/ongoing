<?php

namespace App\Exports;

use App\Exports\Sheets\History\DisabledOffendersSheet;
use App\Exports\Sheets\History\OffenderInternalSheet;
use App\Exports\Sheets\History\OldDescriptionSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OffendersExportHystory implements WithMultipleSheets
{
	use Exportable;

	protected $filters;

	public function __construct(array $filters)
	{
		$this->filters = $filters;
	}

	public function sheets(): array
	{
		if ($this->filters['type_filter'] == 'Externo'){
			return [
				'Ofensores Desativados' => new DisabledOffendersSheet($this->filters),
				'Descrição Antiga' => new OldDescriptionSheet($this->filters)
			];
		} else {
			return [
				'Ofensores Interno' => new OffenderInternalSheet($this->filters)
			];
		}

	}
}
