<style>
    .displayNone{
        display: none;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Detalhe da atividade</h4>
</div>
<div class="modal-body">
    {!! Form::model($log_activities, ['class'=>'form-horizontal', 'id' => 'form-save']) !!}
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('terminal', 'Terminal', ['class'=>'control-label']) !!}
                    {!! Form::text('terminal', $log_activities->terminal, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('units_operation_id', 'Operadora', ['class'=>'control-label']) !!}
                    {!! Form::text('units_operation_id', $log_activities->units_operation_id, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('contract_number', 'Contrato', ['class'=>'control-label']) !!}
                    {!! Form::text('contract_number', $log_activities->contract_number, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('proposal_number', 'Proposta', ['class'=>'control-label']) !!}
                    {!! Form::text('proposal_number', $log_activities->proposal_number, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('cnpj', 'CPF/CNPJ', ['class'=>'control-label']) !!}
                    {!! Form::text('cnpj', $log_activities->cnpj, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('ticket', 'Bilhete', ['class'=>'control-label']) !!}
                    {!! Form::text('ticket', $log_activities->ticket, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('process_message', 'Detalhes', ['class'=>'control-label']) !!}
                    {!! Form::textarea('process_message', $log_activities->process_message, ['class' => 'form-control', 'readonly', 'rows' => 10, 'style' => 'resize:none']) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-outline dark">Fechar</button>
</div>