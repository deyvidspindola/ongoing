@extends('layouts.app', [
    'breadcrumb' => ['Dashboard', 'Log de Atividades'],
    'page_title' => 'Log de Atividades',
    'page_title_small' => ''
])

@push('css')
  {{-- autocomplete --}}
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  {{--Date picker--}}
  <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

  {{--boostrap-table--}}
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

  {{--modal--}}
  <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('page_title_actions')

@endpush

@section('content')

  @include('utils.messages')

  {{--Filtros--}}
  {!! Form::open(['id' => 'search-form']) !!}
  <div class="portlet">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-filter"></i>Filtros </div>
      <div class="tools">
        <a href="javascript:;" class="collapse"> </a>
      </div>
    </div>
    <div class="portlet-body well well-sm">
      <div class="row" style="margin-top: 15px">
        <div class="col-md-4">
          {!! Form::label('create_filter', 'Periodo') !!}
          <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
            {!! Form::text('date_ini_filter', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off', 'id' => 'date_ini_filter', 'readonly']) !!}
            <span class="input-group-addon"> até </span>
            {!! Form::text('date_end_filter', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off', 'id' => 'date_end_filter', 'readonly']) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="user_filter">Usuário</label>
            <select name="user_filter" id="user_filter" class="form-control bs-select" data-live-search="true" data-size="8" tabindex="-98">
              <option value="">Selecione um Usuário</option>
              @foreach($users as $user)
                <option value="{{ $user }}">{{ $user }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-4">
          {!! Form::label('terminal_filter', 'Terminal') !!}
          {!! Form::text('terminal_filter', null, ['class' => 'form-control', 'id' => 'terminal_filter']) !!}
        </div>
      </div>
      <div class="row" style="margin-top: 15px">
        <div class="col-md-6">
          <div class="form-group">
            <label for="service_filter">Serviço</label>
            <select name="service_filter" id="service_filter" class="form-control bs-select" data-live-search="true" data-size="8" tabindex="-98">
              <option value="">Selecione um serviço</option>
              @foreach($services as $k => $service)
                <option value="{{ $k }}">{{ $service }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-right">
          {!! btnFilter(route('units.activities_log.index')) !!}
        </div>
      </div>
    </div>
  </div>
  {!! Form::close() !!}

  {{--Listagem--}}
  <table class="table table-striped table-hover" id="activities-log-table">
    <thead>
    <tr>
      <th>Id</th>
      <th>Data/Hora</th>
      <th>Username</th>
      <th>Terminal</th>
      <th>Serviço</th>
      <th>Status</th>
      <th> </th>
    </tr>
    </thead>
  </table>

  {{--modal de visualização do ofensor--}}
  <div id="modal-details" class="modal container fade" tabindex="-1"></div>
@endsection

@push('js')

  {{--@include('utils.messages-js')--}}

  {{--autocomplete--}}
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  {{--boostrap-table--}}
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

  {{--Maskinput--}}
  <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

  {{--modal--}}
  <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>

  {{--date picker--}}
  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

  {{--scripts para a pagina--}}
  <script src="{{ asset('js/units/activities_log.js') }}"></script>

@endpush
