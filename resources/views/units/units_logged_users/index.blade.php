@extends('layouts.app', [
    'breadcrumb' => ['Units', 'Segurança', 'Usuários Logados'],
    'page_title' => 'Segurança',
    'page_title_small' => 'Usuários Logados'
])


@push('css')
  {{-- autocomplete --}}
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  {{--Date picker--}}
  <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

  {{--boostrap-table--}}
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

  {{--modal--}}
  <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

  <div class="row">
    <div class="col-sm-12">
      <p>Usuários logados</p>


      {{--Listagem--}}
      <table class="table table-striped table-hover" id="logged-table">
        <thead>
        <tr>
          <th>Usuário</th>
          <th>Empresa</th>
          <th>IP</th>
          <th>Navegador</th>
          <th>Última ação</th>
          <th class="text-center" style="width: 85px;">Ações</th>
        </tr>
        </thead>
        <tbody>
{{--        {{dd($logged[0]->session)}}--}}
          @foreach($logged as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->company->description }}</td>
              <td>{{ $user->session->ip_address }}</td>
              <td>{{ $user->session->user_agent }}</td>
              <td>{{ $user->session->last_activity }}</td>
              <td>
                <button class="btn btn-success loggout" data-id="{{ $user->id }}">Deslogar</button>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      @endsection

      @push('js')
        <script>
          $(".loggout").on('click', function() {
            $.ajax({
              type: 'POST',
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url:'/units/seguranca/logout',
              data:{'id': $(this).attr('data-id')},
              timeout: 15000,
              beforeSend: function (xhr, settings) {
                $(".loading-fs").show();
              },
              success:function(response){
                window.location.href = "{{ route('units.security.logged_users.index') }}";
              },
              complete: function () {
                $(".loading-fs").hide();
              },
              error: function(response, timeout){
                $(".loading-fs").hide();
              }
            });
          });
        </script>
        {{--autocomplete--}}
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        {{--boostrap-table--}}
        <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

        {{--modal--}}
        <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>

        {{--Maskinput--}}
        <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

        {{--date picker--}}
        <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

        {{--scripts para a pagina--}}
        <script>
          $(function() {
            $("#logged-table").DataTable({
              "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
                },
                "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
                }
              }
            });
          });

        </script>

      @endpush

    </div>
  </div>

@push('js')
@endpush

