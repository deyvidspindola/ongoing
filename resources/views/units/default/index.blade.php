@extends('layouts.app', $layout)

@push('css')
  {{ @$css }}
@endpush

@section('content')

<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-list"></i> Tipos de Solicitações
    </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body">
      @include('units.utils.buttons', [
        'data'  => $data,
        'route' => $route
      ])
    </div>
  </div>
</div>

@endsection

@push('js')
  {{ @$js }}
@endpush