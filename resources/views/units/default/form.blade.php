@extends('layouts.app', $layout)

@push('css')
  {{--datepicker--}}
  <link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker-eonasdan/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />

  {{ @$css }}
@endpush

@push('page_title_actions')

  <a href="{{ url($data['route_back']) }}" class="btn btn-default"> <i class="fa fa-arrow-left"></i> Voltar</a>

@endpush

@section('content')

  <div class="row">
    <div class="col-sm-12">
      @include('utils.messages')
    </div>
  </div>

  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-list"></i> Dados da Solicitação
      </div>
    </div>
    <div class="portlet-body form">
      {!! Form::open(['route' => $route, 'id' => 'search-form', 'class' => 'form-horizontal', 'role' => 'form']) !!}
      <input type="hidden" name="id" value="{{ $data['service']['id'] }}">
      @include('units.utils.form', $data)
      {!! Form::close() !!}
    </div>
  </div>

@endsection

@push('js')
  {{--datetimepiker--}}
  <script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker-eonasdan/build/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
  {{--mask input--}}
  <script src="{{ asset('js/units/jquery.mask.min.js') }}" type="text/javascript"></script>
  {{--Maskinput--}}
  <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

  {{ @$js }}

  <script src="{{ asset('js/units/form.js') }}"></script>
@endpush