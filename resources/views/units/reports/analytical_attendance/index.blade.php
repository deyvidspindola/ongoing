@extends('layouts.app', [
    'breadcrumb' => ['Units', 'Relatório Analítico - Atendimento UNITS'],
    'page_title' => 'Relatório Analítico - Atendimento UNITS',
    'page_title_small' => ''
])
@push('css')

    {{--Date picker--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

    {{--boostrap-table--}}
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@endpush

@section('content')

    {{--Filtros--}}
    {!! Form::open(['id' => 'search-form']) !!}
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>Filtros </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body well well-sm">
            <div class="row">
                <div class="col-sm-3">
                    {!! Form::label('status', 'Status') !!}
                    {!! Form::select('status', $status, '', ['class' => 'bs-select form-control', 'id' => 'status_filter']) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('create_filter', 'Periodo') !!}
                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                        {!! Form::text('date_ini_filter', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off', 'id' => 'date_ini_filter', 'readonly']) !!}
                        <span class="input-group-addon"> até </span>
                        {!! Form::text('date_end_filter', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off', 'id' => 'date_end_filter', 'readonly']) !!}
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <input type="hidden" value="{{ route('units.relatorios.atendimentos-units.export') }}" id="route-export">
                    <input type="hidden" value="no" id="export_file">
                    {!! btnFilter(route('units.relatorios.atendimentos-units'), true) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <table class="table table-condensed table-hover" id="analytical_attendance">
        <thead>
        <tr>
            <th>Data</th>
            <th>Username</th>
            <th>Nome</th>
            <th>Perfil</th>
            <th>IP Remoto</th>
            <th>Id do Serviço</th>
            <th>Descrição Serviço</th>
            <th>Menu</th>
            <th>Contrato</th>
            <th>Terminal</th>
            <th>Bilhete</th>
            <th>DTH Janela</th>
            <th>Status</th>
            <th>Msg. Processo</th>
        </tr>
        </thead>
    </table>

@endsection

@push('js')

    {{--Maskinput--}}
    <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

    {{--boostrap-table--}}
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

    {{--date picker--}}
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/units/reports/analytical_attendance.js') }}"></script>

@endpush
