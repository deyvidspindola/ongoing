<table>
    <thead>
    <tr>
        <th colspan="3">Relatório Analítico - Atendimento UNITS</th>
    </tr>
    <tr><th></th></tr>
    <tr><th></th></tr>
    </thead>
    <tbody>
    {{--headers da tabela--}}
    <tr>
        <td>Data</td>
        <td>Username</td>
        <td>Nome</td>
        <td>Perfil</td>
        <td>IP Remoto</td>
        <td>ID Serviço</td>
        <td>Descrição Serviço</td>
        <td>Menu</td>
        <td>Contrato</td>
        <td>Terminal</td>
        <td>Bilhete</td>
        <td>DTH Janela</td>
        <td>Status</td>
        <td>Msg Processo</td>
    </tr>

    {{--listagem dos campos--}}
    @foreach($list as $li)
        <tr>
            <td>{{ $li['data'] }}</td>
            <td>{{ $li['username'] }}</td>
            <td>{{ $li['nome'] }}</td>
            <td>{{ $li['perfil'] }}</td>
            <td>{{ $li['ip_remoto'] }}</td>
            <td>{{ $li['id_servico'] }}</td>
            <td>{{ $li['descricao'] }}</td>
            <td>{{ $li['menu'] }}</td>
            <td>{{ $li['contrato'] }}</td>
            <td>{{ $li['terminal'] }}</td>
            <td>{{ $li['bilhete'] }}</td>
            <td>{{ $li['dth_janela'] }}</td>
            <td>{{ $li['status'] }}</td>
            <td>{{ $li['msg_processo'] }}</td>
        </tr>
    @endforeach

    </tbody>
</table>