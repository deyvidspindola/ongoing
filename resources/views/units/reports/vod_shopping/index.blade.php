@extends('layouts.app', [
    'breadcrumb' => ['Units', 'NET NOW', 'Relatorios', 'Compras VOD'],
    'page_title' => 'Compras VOD',
    'page_title_small' => 'Relatórios'
])
@push('css')

    {{--Date picker--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

    {{--boostrap-table--}}
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@endpush

@section('content')

    {{--Filtros--}}
    {!! Form::open(['id' => 'search-form']) !!}
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>Filtros </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body well well-sm">
            <div class="row">
                <div class="col-sm-3">
                    {!! Form::label('type', 'Tipo VOD') !!}
                    {!! Form::select('type', $types, '', ['class' => 'bs-select form-control', 'id' => 'type_filter']) !!}
                </div>
                <div class="col-sm-3">
                    {!! Form::label('status', 'Status') !!}
                    {!! Form::select('status', $status, '', ['class' => 'bs-select form-control', 'id' => 'status_filter']) !!}
                </div>
                <div class="col-sm-2">
                    {!! Form::label('shopping_id', 'Compra ID', ['id'=> 'shopping_id']) !!}
                    {!! Form::text('shopping_id', null, ['class' => 'form-control', 'placeholder' => 'Compra ID', 'id' => 'shopping_id_filter']) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('create_filter', 'Periodo') !!}
                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                        {!! Form::text('date_ini', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off', 'id' => 'date_ini_filter', 'readonly']) !!}
                        <span class="input-group-addon"> até </span>
                        {!! Form::text('date_end', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off', 'id' => 'date_end_filter', 'readonly']) !!}
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <input type="hidden" value="{{ route('units.relatorios.compras_vod.export') }}" id="route-export">
                    <input type="hidden" value="no" id="export_file">
                    {!! btnFilter(route('units.relatorios.compras_vod'), true) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <table class="table table-condensed table-hover" id="vod_shopping">
        <thead>
        <tr>
            <th>ID Compra</th>
            <th>Customer</th>
            <th>Smartcard</th>
            <th>Data Compra</th>
            <th>Produto</th>
            <th>Valor</th>
            <th>XML</th>
            <th>Status</th>
        </tr>
        </thead>
    </table>

@endsection

@push('js')

    {{--Maskinput--}}
    <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

    {{--boostrap-table--}}
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

    {{--date picker--}}
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/units/reports/vod_shopping.js') }}"></script>

@endpush
