<table>
    <thead>
    <tr>
        <th colspan="3">Compras VOD</th>
    </tr>
    <tr><th></th></tr>
    <tr><th></th></tr>
    </thead>
    <tbody>
    {{--headers da tabela--}}
    <tr>
        <td>ID COMPRA</td>
        <td>CUSTOMER</td>
        <td>SMARTCARD</td>
        <td>DATA COMPRA</td>
        <td>PRODUTO</td>
        <td>VALOR</td>
        <td>XML</td>
        <td>STATUS</td>
    </tr>

    {{--listagem dos campos--}}
    @foreach($list as $li)
        <tr>
            <td>{{ $li->id_compra }}</td>
            <td>{{ $li->customer }}</td>
            <td>{{ $li->smartcard }}</td>
            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $li->dt_compra)->format('d/m/Y') }}</td>
            <td>{{ $li->produto }}</td>
            <td>{{ $li->valor }}</td>
            <td>{{ $li->arquivo_xml }}</td>
            <td>{{ $li->observacao }}</td>
        </tr>
    @endforeach

    </tbody>
</table>