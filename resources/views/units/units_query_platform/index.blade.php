@extends('layouts.app', [
    'breadcrumb' => ['Units', 'Net Now', 'Consulta', 'Consulta Plataforma'],
    'page_title' => 'Net Now',
    'page_title_small' => 'Consulta Plataforma'
])

@push('css')
    <style>
        .input-group{

            width: 100%;
        }
    </style>

@endpush

@section('content')

    <div class="loading-fs" style="display: none">
        <img src="{{ asset('assets/global/img/loading.gif')}}" alt="loading">
    </div>

    <div class="row">
        <div class="col-sm-12">
            {{--Filtros--}}
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-filter"></i>Filtros
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body well well-sm">
                    <div class="form-group col-md-5">
                        <label>Customer</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="customer" placeholder="Customer">
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label>Smartcard</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="smartcard" placeholder="Smartcard">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <a id="btn-consultar" class="btn-block btn btn-primary">Consultar</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row resultadosConsultaPlataforma" style="display: none;">
        <div class="col-md-12">
            <div class="panel panel-default no-margin">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    Resultado da Consulta
                </div>
                <div class="panel-body no-padding">
                    <div class="panel-heading">
                        <i class="fa fa-list"></i>
                        STATUS CUSTOMER PLATAFORMA
                    </div>

                    <p id="status_customer_platform"></p>
                </div>
                <div class="panel-body no-padding">
                    <div class="panel-heading">
                        <i class="fa fa-list"></i>
                        SMARTCARD(s) PLATAFORMA
                    </div>

                    <table class="table table-striped">
                        <tbody id="smartcards_platform">

                        </tbody>
                    </table>
                </div>

                <div class="panel-body no-padding">
                    <div class="panel-heading">
                        <i class="fa fa-list"></i>
                        SMARTCARD X CUSTOMER
                    </div>

                    <table class="table table-striped">
                        <tbody id="smartcards_x_customer">

                        </tbody>
                    </table>
                </div>
                <div class="panel-body no-padding">
                    <div class="panel-heading">
                        <i class="fa fa-list"></i>
                        TAG(s) PLATAFORMA
                    </div>

                    <table class="table table-striped">
                        <tbody id="tags_platform">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    {{ @$js }}
    <script>
        $(document).ready(function () {

            $("#btn-consultar").on('click', function (e) {
                e.preventDefault();

                $(".resultadosConsultaPlataforma").hide('');

                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/units/relatorios/consulta-plataforma',
                    data: {
                        customer: $("#customer").val(),
                        smartcard: $("#smartcard").val()
                    },
                    beforeSend: function (xhr, settings) {
                        $(".loading-fs").show();
                    },
                    success: function (response) {
                        if (response[0].success) {
                            $(".resultadosConsultaPlataforma").show();

                            let barrado = response[0].getStatusOfACustomerOnTraxis.IsBarred

                            if (response[0].getStatusOfACustomerOnTraxis.CustomerData != '' || response[0].getStatusOfACustomerOnTraxis.CustomerData != undefined) {
                                var nome = response[0].getStatusOfACustomerOnTraxis.CustomerData;
                            } else {
                                var nome = "SEM NOME";
                            }

                            if (barrado) {
                                var msg = ' ESTÁ BLOQUEADO NA PLATAFORMA'
                                $("#status_customer_platform").html("ASSINANTE: " + nome + msg);
                            } else {
                                var msg = ' ESTÁ CADASTRADO E HABILITADO NA PLATAFORMA'
                                $("#status_customer_platform").html("ASSINANTE: " + nome + " COD_CUSTOMER: " + response[0].getStatusOfACustomerOnTraxis.id + msg);
                            }

                            $("#smartcards_x_customer").html('');
                            if (response[0].getProductsOfACustomerOnTraxis.ProductId.length >= 1) {

                                $.each(response[0].getProductsOfACustomerOnTraxis.ProductId, function (index, value) {

                                    let hash = response[0].getProductsOfACustomerOnTraxis.ProductId[index];

                                    if (response[0].getProductsOfACustomerOnTraxis.productsDetails[hash].length >= 1) {
                                        var produto = "PROD_VOD " + response[0].getProductsOfACustomerOnTraxis.productsDetails[hash][0].produto;
                                    } else {
                                        var produto = "";
                                    }

                                    $("#smartcards_x_customer").append("<tr>" +
                                        `<td> HASH: ${response[0].getProductsOfACustomerOnTraxis.ProductId[index]}, ${produto} </td>`
                                        + "<td></tr>");
                                });
                            } else {
                                $("#smartcards_x_customer").append("<tr><td>NÃO HÁ PRODUTOS</td></tr>");
                            }

                            if (response[0].getCustomerTags.CustomerTagId.length >= 1) {

                                $("#tags_platform").append("<tr>"
                                    + "<td>" + response[0].getCustomerTags.CustomerTagId + "</td>" +
                                    "</tr>");
                            }
                            ;
                            $("#smartcards_platform").html('');
                            if (response[0].getDevicesOfCustomerOnTraxis.DeviceId.length >= 1) {
                                $("#smartcards_platform").append("<tr>"
                                    + "<td>" + response[0].getDevicesOfCustomerOnTraxis.DeviceId + "</td>" +
                                    "</tr>");
                            } else {
                                $("#smartcards_platform").html('');
                                $("#smartcards_platform").append("<tr>" +
                                    "<td>NÃO HÁ PRODUTOS</td>"
                                    + "</tr>");
                            }
                        } else {
                            swal('Erro!', response[0].message, 'error');
                        }
                    },
                    complete: function () {
                        $(".loading-fs").hide();
                    },
                    error: function (response, timeout) {
                        alert('Não foi possível se conectar ao servidor para realizar a consulta, tente novamente!');
                    }
                });

            });
        });
    </script>
@endpush