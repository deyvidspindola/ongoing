@push('css')
@endpush

{{-- Gráfico 2 --}}
<div class="panel panel-default no-margin">
  <div class="panel-heading">
      <i class="fa fa-list"></i>
      {{'Solicitações'}}
  </div>
  <div class="panel-body panel-padding-custom">
    <div class="col-md-12">
      <div class="row">
        {{--Grafico 4--}}
        <div class="col-md-6">
          <div class="easy-pie-chart">
            <span class="easy-pie-chart-success number" data-percent="{{ $percent['success'] }}">
              <span class="percent">{{ $percent['success'] }}%</span>
            </span>
            <div class="clearfix"></div>
            <p class="label-chart text-center">Processadas Sucesso</p>
          </div>
        </div>
        {{--Grafico 5--}}
        <div class="col-md-6">
          <div class="easy-pie-chart">
            <span class="easy-pie-chart-error number" data-percent="{{ $percent['error'] }}">
              <span class="percent">{{ $percent['error'] }}%</span>
            </span>
            <div class="clearfix"></div>
            <p class="label-chart text-center">Processadas Erro</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- Gráfico 3 --}}
<div class="panel panel-default no-margin" style="margin-top:20px; max-height: ;">
  <div class="panel-heading">
      <i class="fa fa-list"></i>
      {{'Usuários on-line'}}
  </div>
  <div class="panel-body panel-padding-custom">
    <table class="table table-striped">
      <thead>
        <tr>
          <td class="text-black-50">Usuário</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            {{ auth()->user()->name }}
          </td>
        </tr>
        @foreach ($logged as $user)
          <tr>
            <td>
              {{ $user->name  }}
            </td>
          </tr>
        @endforeach
        <tr>
          <td class="text-dark pull-right">
            Total de Usuários Logados: {{ count($logged)+1 }}
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

@push('js')
  <script src="{{ asset('assets/global/plugins/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
  <script>
    // Gráficos easy-pie-chart
    easyPieChart();

    function easyPieChart(){
      $('.easy-pie-chart-success').easyPieChart({ animate: 1000, lineWidth: 3, barColor: '#195a09', size: 70 });
      $('.easy-pie-chart-error').easyPieChart({ animate: 1000, barColor: "#af1c1c", size: 70 });
    }
  </script>
@endpush