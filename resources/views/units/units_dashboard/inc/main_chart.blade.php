@push('css')
@endpush

<div class="panel panel-default no-margin">
    <div class="panel-heading">
        <i class="fa fa-list"></i>
        {{'Solicitações'}}
    </div>
    <div class="panel-body panel-padding-custom">
        {!! $chartfirst->container() !!}
    </div>
    <div class="panel-body panel-padding-custom">
        {!! $chartSecondary->container() !!}
    </div>
</div>

@push('js')
    {!! $chartfirst->script() !!}
    {!! $chartSecondary->script() !!}
@endpush