@push('css')
  <style>
    .mini-stats .values{
      margin: 5px 0px;
    }
    .mini-stats .values strong {
      display: block;
      font-size: 18px;
      margin-bottom: 2px;
    }
    .mini-stats .description{
      font-size: 14px;
    }

    /* Easy pie */
    .easy-pie-chart .number {
      position: relative;
      display: inline-block;
      width: 70px;
      height: 70px;
      text-align: center;
    }

    .label-chart {
      color: #333333;
      font-size: 16px;
      font-weight: 300;
      display: inline;
      line-height: 1;
      padding: 0.2em 0.6em 0.3em;
      text-align: center;
      vertical-align: baseline;
      white-space: nowrap;
  }
  </style>
@endpush


{{--Grafico 1--}}
{{--<div class="row" style="margin:30px 0px;">--}}
{{--    <div class="col-md-7">--}}
{{--      <div class="row">--}}
{{--        --}}{{--Grafico 1--}}
{{--        <div class="col-md-4">--}}
{{--          <div class="sparkline-chart mini-stats">--}}
{{--            <div class="number" id="sparkline_bar_green"></div>--}}
{{--            <p class="values"><strong>{{ $getPercent['error'] + $getPercent['success'] + $getPercent['pending'] }}</strong></p>--}}
{{--            <div class="label-chart">Solicitações</div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--        --}}{{--Grafico 2--}}
{{--        <div class="col-md-4">--}}
{{--          <div class="sparkline-chart mini-stats">--}}
{{--            <div class="number" id="sparkline_bar_grey"></div>--}}
{{--            <p class="values"><strong>{{ $getPercent['error'] + $getPercent['success'] }}</strong></p>--}}
{{--            <div class="label-chart">Processadas</div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--        --}}{{--Grafico 3--}}
{{--        <div class="col-md-4">--}}
{{--          <div class="sparkline-chart mini-stats">--}}
{{--            <div class="number" id="sparkline_bar_red"></div>--}}
{{--            <p class="values"><strong>{{ $getPercent['pending'] }}</strong></p>--}}
{{--            <div class="label-chart">Pendentes</div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--    </div>--}}

{{--  </div>--}}

@push('js')
  <script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>

  <script type="text/javascript">
    // Gráficos sparkline
    var values = [3,5,8,9,13,14,17];
    var container_bar_width = "50";
    var container_bar_height = "25";

    sparklineChart(values, container_bar_width, container_bar_height);
    
    function sparklineChart(values, _width, _height){
      $('#sparkline_bar_green').sparkline(values, {type: "bar", barColor: "green", width: _width, height: _height});
      $('#sparkline_bar_grey').sparkline(values, {type: "bar", barColor: "grey", width: _width, height: _height});
      $('#sparkline_bar_red').sparkline(values, {type: "bar", barColor: "red", width: _width, height: _height});
    }
  </script>
@endpush