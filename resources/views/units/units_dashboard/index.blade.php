@extends('layouts.app', [
    'breadcrumb' => ['Units', 'Dashboard'],
    'page_title' => 'Dashboard',
    'page_title_small' => ''
])

@push('css')

@endpush

@section('content')
  {{--Filtros--}}
  {!! Form::open(['id' => 'search-form', 'route' => 'units.dashboard.index', 'method' => 'get']) !!}
  <div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i>Filtros </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
      </div>
      <div class="portlet-body well well-sm">
        <div class="row">
          <div class="col-sm-4">
            {!! Form::label('year_filter', 'Ano') !!}
              @if(isset($_GET['year_filter']))
                  {!! Form::select('year_filter', $years, $_GET['year_filter'], ['class' => 'bs-select form-control']) !!}
              @else
                  {!! Form::select('year_filter', $years, date('Y'), ['class' => 'bs-select form-control']) !!}
              @endif
          </div>
          <div class="col-sm-4">
            {!! Form::label('month_filter', 'Mês') !!}
              @if(isset($_GET['month_filter']))
                  {!! Form::select('month_filter', $months, $_GET['month_filter'], ['class' => 'bs-select form-control']) !!}
              @else
                  {!! Form::select('month_filter', $months, date('m'), ['class' => 'bs-select form-control']) !!}
              @endif
          </div>
          <div class="col-sm-4 text-right">
            {!! btnFilter(route('units.dashboard.index')) !!}
          </div>
        </div>
      </div>
  </div>
  {!! Form::close() !!}

  <div class="row">
    <div class="col-md-12">
        <div class="col-md-7">
          {{--Grafico 1 - Gráfico Principal (main_chart)--}}
          @include('units.units_dashboard.inc.main_chart')
        </div>
        <div class="col-md-5">
          {{--Gráfico 2 e 3 --}}
          @include('units.units_dashboard.inc.side_charts')
        </div>
    </div>
  </div>

  {{-- Gráficos sparkline e easy charts --}}
  @include('units.units_dashboard.inc.lower_charts')
  
@endsection

@push('js')
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js" charset="utf-8"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/npm/frappe-charts@1.1.0/dist/frappe-charts.min.iife.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.7.0/d3.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.7/c3.min.js"></script>--}}
@endpush
