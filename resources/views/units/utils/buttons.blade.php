@php($count = 1)
@foreach($data as $item)
    @can($item->permission_name)
        <div class="col-md-3" {!! ($count >= 5) ? 'style="margin-top: 20px;"' : '' !!}>
            <button
                    type="button"
                    class="btn blue btn-outline units-btn-link-big show_modal btn-block"
                    style="white-space: normal; min-height: 86px"
                    data-title="{{ $item->description }}"
                    data-description="{{ $item->description_detailed }}"
                    data-route={{ $route.'/'.$item->id.'/'.\Str::slug($item->description, '-') }}
                    data-route-back={{ url()->current() }}>
                {{ $item->description }}
            </button>
        </div>
        @php($count ++)
    @endcan
@endforeach
<div class="clearfix"></div>

@include('units.utils.modal')