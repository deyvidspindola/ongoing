<div class="loading-fs" style="display: none">
    <img src="{{ asset('assets/global/img/loading.gif')}}" alt="loading">
</div>

<div class="form-body">

    {{--Cidade--}}
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="city_contract">Cidade</label>
                <select name="city_contract" id="city_contract" class="form-control bs-select" data-live-search="true" data-size="8" tabindex="-98" required>
                    <option value="">Selecione uma cidade</option>
                    @foreach($data['operations'] as $operation)
                        <option value="{{ $operation['city_contract'] }}" data-base_code="{{ $operation['base_code'] }}">{{ $operation['city_name'] .' / '. $operation['uf'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="base_code">Dados da Cidade</label>
                <input type="text" class="form-control" name="base_code" id="base_code" disabled>
                <input type="hidden" id="base_code_hide" name="base_code_hide">
            </div>
        </div>
    </div>

    {{--Contato--}}
    @if($data['service']['show_contract'] == 'S')
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="contract_code">Contrato</label>
                <input type="text" class="form-control" name="contract_code" id="contract_code" required disabled>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="customer_name">Assinante</label>
                <input type="text" class="form-control" name="customer_name" id="customer_name" disabled>
            </div>
        </div>
    </div>
    @endif

    {{--Telefone--}}
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="terminal_number">Telefone</label>
                <input type="text" maxlength="10" class="form-control" name="terminal_number" id="terminal_number" required disabled>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="terminal_data">Dados do Telefone</label>
                <input type="text" class="form-control" name="terminal_data" id="terminal_data" disabled>
            </div>
        </div>
    </div>

    {{--Campos Adicionais--}}
    @if($data['service']['additional_fields'] == 'S')
        <div class="row">
            <input type="hidden" value="{{$data['service']['flag_fields']}}" id="flag_fields">
        {{--bilhere e data--}}
        @if($data['service']['flag_fields'] == 1)
            <div class="col-md-4">
                <div class="form-group">
                    <label for="ticket_number">Bilhete</label>
                    <input type="text" class="form-control" name="ticket_number" id="ticket_number" required disabled>
                </div>
            </div>
            <div class="col-md-4">
                <label for="date_time">Data Hora</label>
                <div class="input-group date" id="datetimepicker">
                    <input type="text" data-mask="00/00/0000 00:00" class="form-control" autocomplete="off" name="date_time" id="date_time" required>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        @endif
        {{--softx--}}
        @if($data['service']['flag_fields'] == 2)
            <div class="col-md-4">
                <div class="form-group">
                    <label for="softx">Soft Switch</label>
                    <input type="text" class="form-control" name="softx" id="softx" required>
                </div>
            </div>
        @endif
        {{-- operator --}}
        @if($data['service']['flag_fields'] == 3)
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="operator">Operadora</label>
                    <select id="operator" class="bs-select form-control" name="operadora" data-size="10" required>
                        <option value="">Selecione uma Operadora</option>
                        @foreach($data['operator'] as $operator)
                            <option value="{{$operator['id_operadora_telefonia']}}" data- ="{{$operator['fc_produto_ebt']}}">{{ $operator['nm_operadora_telefonia'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif
        </div>
    @endif

</div>
<div class="form-actions">
    <button type="button" id="tratar" class="btn blue">Tratar</button>
    <button type="button" id="reset" class="btn red">Limpar Dados</button>
</div>
@push('js')

    <script>
    $(document).ready(function(){
        $(document).on("input", "#terminal_number", function () {

            var limite = 10;
            var caracteresDigitados = $(this).val().length;
            var caracteresRestantes = limite - caracteresDigitados;

            if (caracteresDigitados < limite) {
                $("#terminal_data").val("Faltam " + caracteresRestantes + " Caracteres");
            } else if (caracteresDigitados > limite) {
                $("#terminal_data").val("Número Inválido");
            } else {
                $("#terminal_data").val('');
            }
        });
    });



</script>
@endpush