@push('css')
  {{--datepicker--}}
  <link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker-eonasdan/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />

  <style>
    .row{
      margin:16px 0px;
    }
  </style>
@endpush

<div class="loading-fs" style="display: none">
  <img src="{{ asset('assets/global/img/loading.gif')}}" alt="loading">
</div>

<div class="panel-body panel-padding-custom">
  {{-- Token --}}
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}" />
  {{-- Serviço --}}  
  <input type="hidden" id="units_service_id" value="{{ $service_data['id'] }}" name="units_service_id" />

  {{-- Cidade --}}
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        {!! Form::label('city_contract', 'Cidade') !!}
        <select id="city_contract" class="bs-select form-control" name="city_contract" data-live-search="true" data-size="8" tabindex="-98">
          @foreach($operations_data as $op)
            <option value="{{$op->city_contract}}" data-base-code="{{$op->base_code}}">{{$op->city_name}} / {{$op->uf}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        {!! Form::label('base_code', 'Dados da Cidade') !!}
        {!! Form::text('base_code', null,
          ['id' => 'base_code', 'class' => 'form-control', 'placeholder' => 'Código da operadora', 'autocomplete' => 'off', 'disabled' => 'disabled'])
        !!}


      </div>
      <input type="hidden" id="base_code_hide" value="" name="base_code_hide" />
    </div>
  </div>
    
  {{-- Contrato --}}
  @if($service_data['show_contract'] == 'S')
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          {!! Form::label('contract_code', 'Contrato') !!}
          {!! Form::text('contract_code', null,
            ['id' => 'contract_code', 'class' => 'form-control only-number', 'placeholder' => 'Contrato', 'maxLength' => 9, 'disabled' => 'disabled'])
          !!}
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          {!! Form::label('customer_name', 'Assinante') !!}
          {!! Form::text('customer_name', null,
            ['class' => 'form-control', 'id' => 'customer_name', 'placeholder' => 'Assinante', 'disabled' => 'disabled'])
          !!}
        </div>
      </div>
    </div>
  @endif
  
  {{-- Telefone --}}
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        {!! Form::label('terminal_number', 'Telefone') !!}
        {!! Form::text('terminal_number', null,
          ['class' => 'form-control mask-phone-number', 'id' => 'terminal_number', 'placeholder' => 'DDD + Número', 'disabled' => 'disabled'])
        !!}
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        {!! Form::label('terminal_data', 'Dados do Telefone') !!}
        {!! Form::text('terminal_data', null,
          ['class' => 'form-control', 'id' => 'terminal_data', 'placeholder' => 'Dados do Telefone', 'disabled' => 'disabled'])
        !!}
      </div>
    </div>
  </div>

  {{-- // TODO - Tratar os campos BILHETE, DATAHORA, SOFTX nos proximos RFs --}}

  {{-- Campo adicional Bilhete e Data --}}
  @if($service_data['additional_fields'] == 'S' && $service_data['flag_fields'] == 1)
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          {!! Form::label('ticket_number', 'Bilhete') !!}
          {!! Form::text('ticket_number', null,
            ['class' => 'form-control only-number', 'id' => 'ticket_number', 'placeholder' => 'Bilhete', 'required' => 'required'])
          !!}
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          {!! Form::label('date_time', 'Data hora') !!}
          <div class="input-group date" id="datetimepicker">
            {!! Form::text('date_time', '' , ['placeholder' => 'Data hora' , 'class' => 'form-control', 'autocomplete' => 'off'] ) !!}
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
          </div>
        </div>
      </div>
    </div>
  @endif

  {{-- Campo adicional Softx --}}
  @if($service_data['additional_fields'] == 'S' && $service_data['flag_fields'] == 2)
  <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          {!! Form::label('softx', 'Soft Switch') !!}
          {!! Form::text('softx', null,
            ['class' => 'form-control only-number', 'placeholder' => 'Soft Switch'])
          !!}
        </div>
      </div>
  </div>
  @endif

</div>
<div class="panel-footer">
  <div class="row">
    <div class="col-sm-4">
      <button type="submit" class="btn btn-primary" id="submit_button">
        {{Lang::get('sys.btn.treat')}}
      </button>
      <a href="{{ route('units.fqdn_change.form') }}" class="btn red-mint">
        {{Lang::get('sys.btn.clean_data')}}
      </a>
    </div>
  </div>
</div>

@push('js')
  {{--datetimepiker--}}
  <script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker-eonasdan/build/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

  {{--mask input--}}
  <script src="{{ asset('js/units/jquery.mask.min.js') }}" type="text/javascript"></script>

  {{--Maskinput--}}
  <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

  {{-- Scripts da página --}}
  <script src="{{ asset('js/units/search_net_form.js') }}" type="text/javascript"></script>

  <script>


    $('#terminal_number').on('focus', function() {
      $('#submit_button').prop('disabled', true);
      //var terminal_number = $(".mask-phone-number").unmask().val(); // tirar a mascara do number
      //console.log($(".mask-phone-number").unmask().val());

      // TODO, mandar o valor sem mascara para outro campo hidden, e pegar este valor
    
      /*
      @if($service_data['show_contract'] == 'S')
        consistTerminal(terminal_number);
      @else
        console.log("Sem o numero do contrato. Fica para o proximo RF");
      @endif
      */
      
    });
    
  </script>

@endpush