<!-- Modal -->
<div class="modal fade" id="modalInformative" tabindex="-1" role="dialog" aria-labelledby="modalInformativeTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalInformativeTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="modal-description"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
            {{Lang::get('sys.btn.cancel')}}
        </button>
        <a class="btn btn-primary" id="go">
          {{Lang::get('sys.btn.continue')}}
        </a>
      </div>
    </div>
  </div>
</div>