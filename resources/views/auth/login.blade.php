@extends('layouts.auth')

@section('logo')
    {{--<img src="{{ asset('logo.png') }}" />--}}

    <div class="text-center">
        <h1 class="font-blue-steel" style="font-weight: bold; font-size: 40pt">OnGoing</h1>
    </div>

@endsection

@section('content')

    <div class="text-center" style="margin-bottom: 40px;">
        <h3>Bem-vindo ao Portal OnGoing</h3>
        <p>Digite seu usuário e senha de rede para efetuar login</p>
    </div>

    @if (session()->has('message'))
        <div class="alert alert-danger alert-dismissible">
            {{ session('message') }}
        </div>
    @endif

    <form id="form-login" class="form-login" role="form" method="POST" action="{{ url('/login') }}">

        {{ csrf_field() }}

        {{--@if(getEnvironment() == 'homologacao')

            <div class="note note-danger">
                <p style="color: #FC768A !important;" class="block">Você está em um ambiente de <b>HOMOLOGAÇÃO</b>.</p>
            </div>

        @endif--}}

        @if ($errors->has('username'))
            <div class="note note-danger" >
                <button class="close" data-close="alert"></button>
                <p style="color: #e85f68 !important;"><b>ERRO</b>: {{ $errors->first('username') }}</p>
            </div>
        @endif

        <fieldset>

            <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">{{Lang::get('sys.auth.username')}}</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control enterSubmit remove-spaces" type="text"  value="{{ old('username') }}" placeholder="{{Lang::get('sys.auth.username')}}" name="username" id="username" autofocus required />
                </div>
            </div>

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">{{Lang::get('sys.auth.password')}}</label>

                <div class="input-group">
                    <div class="input-icon">
                        <i id="icon-password" class="fa fa-lock" style="z-index: 1000;"></i>
                        <input class="form-control enterSubmit" type="password" placeholder="{{Lang::get('sys.auth.password')}}" name="password" id="password" />
                    </div>
                    {!! changeViewPass('password') !!}
                </div>
            </div>

            <div class="form-actions" style="margin-bottom: 40px;">
                <button type="submit" class="btn blue-steel pull-right" style="width: 100%; height: 50px;">
                    {{Lang::get('sys.auth.login')}}&nbsp;&nbsp;
                </button>
            </div>


        </fieldset>

    </form>

@endsection

@section('scripts')

    @if(App::isLocale('pt-br'))
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>
    @endif

    <script>
        $(function() {

            // Form validate
            runValidate('#form-login');

            // Enter inputs for submit
            runEnterSubmit('#form-login');

            // background slide images
            runbackstretch([
                "{{ asset('assets/pages/media/bg/1.jpg') }}"
            ]);

            changeViewPass();

        });
    </script>
@endsection