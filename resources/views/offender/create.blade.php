@extends('layouts.app', [
    'breadcrumb' => ['Dashboard', 'Ofensores', 'Novo'],
    'page_title' => 'Novo Ofensor',
    'page_title_small' => ''
])

@push('css')
    {{-- autocomplete --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {{--Fileimput--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @include('utils.messages')
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 form">
            <!-- BEGIN FORM-->
            {!! Form::open(['route' => 'offender.store', 'files'=> true, 'class'=>'form-horizontal', 'id' => 'form-save']) !!}
                <div class="form-body">
                    @include('offender.inc.form')
                </div>
                <div class="form-actions right">
                    {!! btnForm(route('offender.index')) !!}
                </div>
            {!! Form::close() !!}
            <!-- END FORM-->
        </div>
    </div>
@endsection

@push('js')
    {{--autocomplete--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    {{--Fileimput--}}
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>

    {{-- Validate --}}
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/offender/create.js') }}"></script>

@endpush
