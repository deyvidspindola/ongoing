@extends('layouts.app', [
    'breadcrumb' => ['Dashboard', 'Ofensores', 'Carga de Dados'],
    'page_title' => 'Carga de Dados',
    'page_title_small' => ''
])

@push('css')

    <link href="{{ asset('assets/global/plugins/dropzone/basic.min.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        form.dropzone-file-area {
            border: 1px solid #848484;
            background: #F6F6F6;
        }

        .dropzone div.dz-preview {
            width: 200px;
        }

        div.dz-details div.dz-size, .dropzone .dz-preview .dz-progress {
            display: none;
        }

        div.dz-details div.dz-filename {
            display: block;
        }

    </style>

@endpush

@section('content')

    @include('utils.messages')

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('offender.import.import') }}" class="dropzone dropzone-file-area" id="my-dropzone">
                {{ csrf_field() }}

                <h3 class="sbold">
                    <b>Arraste</b> ou <b>Clique</b> para importar
                </h3>
                <p>
                    * É necessário que o documento esteja no padrão correto,
                    {{--<a href="{{ url('offender/offender_import/files/ofensores_import_example.xlsx')  }}">--}}
                    {{--clique aqui para baixar o exemplo.--}}
                    {{--</a>--}}
                </p>
                <div id="loading" style="display: none">
                    <img src="https://thumbs.gfycat.com/UnitedSmartBinturong-max-1mb.gif" style="width: 40px" alt="">
                </div>
            </form>
        </div>
        <div class="col-md-12">
            {!! Form::label('solver_group_filter', 'Grupo Resolvedor') !!}
            {!! Form::select('solver_group_filter', $userSolverGroups, 'Todos', ['class' => 'bs-select form-control']) !!}
        </div>
        <div class="col-md-12">
            <button type="submit" id="send-import" class="btn btn-block green-meadow btn-save-form" style="margin-top: 20px;" disabled form="my-dropzone">
                <i class="fa fa-upload"></i> Enviar Carga
            </button>
        </div>
    </div>


    {{-- -----------------------------------------------------
      -- LISTAGEM
      -- ----------------------------------------------------}}

    <div class="panel panel-default no-margin" style="margin-top: 30px">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
        </div>
        <div class="panel-body no-padding">

            <table class="table table-hover table-striped no-margin" id="myTable">
                <thead>
                <tr>
                    <th>Data da Carga</th>
                    <th style="width: 400px;">Nome do arquivo</th>
                    <th>Usuario</th>
                    <th>Log</th>
                </tr>
                </thead>
                <tbody>
                @forelse($offenderDataLoad as $offen)

                    <tr>

                        <td>{{ $offen->created_at }}</td>
                        <td>{{ $offen->filename }}</td>
                        <td>{{ $offen->user->name }}</td>
                        <td>
                            <a href=" {{ route('offender.import.log-download', $offen->id) }}"
                               class="btn tooltips btn-xs btn-primary">
                                <i class="fa fa-download"></i>
                            </a>
                        </td>
                    </tr>
                @empty

                @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('js')

    @include('utils.messages-js')

    <script src="{{ asset('assets/global/plugins/dropzone/dropzone.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/form-dropzone.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/offender_import/index.js') }}"></script>

@endpush

