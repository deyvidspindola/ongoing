<table>
    @if(is_array($offenders) && !is_null($offenders))
        @foreach ($offenders as $offender)
            @foreach ($offender['ofensores'] as $item)
                @if (is_array($item))
                    <tr>
                        <td>{{ $item[0] }}</td>
                        <td>{{ $item[1] }}</td>
                    </tr>
                @endif
            @endforeach
        @endforeach
    @endif
</table>