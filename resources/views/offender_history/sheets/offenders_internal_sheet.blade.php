<table>
    <thead>
    <tr>
        <th colspan="3" style="background-color: #17375D;">OFFENSORES INTERNOS</th>
    </tr>
    <tr><th></th></tr>
    <tr><th></th></tr>
    </thead>
    <tbody>
    {{--headers da tabela--}}
    <tr>
        <td style="background-color: #17375D;" width="5%">ID CENÁRIO</td>
        <td style="background-color: #17375D;">DESCRIÇÃO DO PROBLEMA</td>
        <td style="background-color: #17375D;">VALIDAÇÃO DO CENÁRIO</td>
        <td style="background-color: #17375D;">DESCRIÇÃO E ID PROCEDIMENTO</td>
        <td style="background-color: #17375D;">AÇÃO / SOLUÇÃO / DIRECIONAMENTO PARA EQUIPE RESPONSÁVEL</td>
        <td style="background-color: #17375D;">EXCEÇÃO</td>
        <td style="background-color: #17375D;">OBSERVAÇÃO</td>
    </tr>

    {{--listagem dos campos--}}
    @foreach($offenders as $offender)
        @php
            $obs = $offender->observation2;
            if ($offender->operation_type == 'E'){
                $obs .= ' - Campos alterados '.modified_fields($offender->modified_fields, ['macro_ofensor', 'id']);
            }
        @endphp
        <tr>
            <td style="background-color: #FFFF00">{{ $offender->snapshot['offender_id'] }}</td>
            <td>{{ $offender->snapshot['offender'] }}</td>
            <td>{{ $offender->snapshot['scenario_validation'] }}</td>
            <td>{{ $offender->snapshot['description_and_procedure_id'] }}</td>
            <td>{{ $offender->snapshot['action_solution_targeting_for_responsible_team'] }}</td>
            <td>{{ isset($offender->snapshot['exception']) ? $offender->snapshot['exception'] : '' }}</td>
            <td>{{ $obs }}</td>
        </tr>
    @endforeach

    </tbody>
</table>