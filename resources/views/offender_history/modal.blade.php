<style>
    .displayNone{
        display: none;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{ $offender['offender_id'] }} - {{ \Str::limit($offender['offender'], 100) }}</h4>
</div>
<div class="modal-body">

    <?php
        if ($offender['type'] == 'Interno'){
            $interno = 'displayNone';
        }else{
        	$externo = 'displayNone';
        }
    ?>

    {!! Form::model($offender, ['class'=>'form-horizontal', 'id' => 'form-save']) !!}
    <div class="form-body">
        @include('offender_history.inc.form', ['disabled' => 'disabled'])
    </div>
    {!! Form::close() !!}

</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-outline dark">Fechar</button>
</div>