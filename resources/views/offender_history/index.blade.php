@extends('layouts.app', [
    'breadcrumb' => ['Ofensores', 'Histórico'],
    'page_title' => 'Histórico',
    'page_title_small' => ''
])

@push('css')
    {{-- autocomplete --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {{--datepicker--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

    {{--boostrap-table--}}
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    {{--modal--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

    @include('utils.messages')

    {{--Filtros--}}
    {!! Form::open(['id' => 'search-form']) !!}
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>Filtros </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body well well-sm">
            <div class="row">
                <div class="col-sm-2">
                    {!! Form::label('type_filter', 'Tipo do Ofensor') !!}
                    {!! Form::select('type_filter', ['Todos'=>'Todos', 'Externo'=>'Externo', 'Interno'=>'Interno'], '', ['class' => 'bs-select form-control']) !!}
                </div>
                <div class="col-sm-3">
                    {!! Form::label('solver_group_filter', 'Grupo Resolvedor') !!}
                    {!! Form::select('solver_group_filter', $userSolverGroups, 'Todos', ['class' => 'bs-select form-control']) !!}
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div>
                    <div class="col-sm-2">
                        {!! Form::label('id_offender_filter', 'Id Ofensor') !!}
                        {!! Form::text('id_offender_filter', null, ['class' => 'form-control', 'placeholder' => 'Id Ofensor']) !!}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::label('offender_filter', 'Ofensor') !!}
                        {!! Form::text('offender_filter', null, ['class' => 'form-control', 'id' => 'offender_filter', 'placeholder' => 'Ofensor']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('create_filter', 'Periodo') !!}
                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                            {!! Form::text('date_ini_filter', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off']) !!}
                            <span class="input-group-addon"> até </span>
                            {!! Form::text('date_end_filter', null, ['class' => 'form-control mask_date date-picker', 'autocomplete' => 'off']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <input type="hidden" value="{{ route('offender.history.export') }}" id="route-export">
                    <input type="hidden" value="no" id="export_file">
                    {!! btnFilter(route('offender.history.index'), true, ['excel']) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    {{--Listagem--}}
    <table class="table table-hover" id="offenders-history-table">
        <thead>
        <tr>
            <th>ID Ofensor</th>
            <th style="width: 400px;">Ofensor</th>
            <th>Tipo da Operação</th>
            <th>Criado</th>
            <th>Alterado</th>
            <th class="text-center" style="width: 85px;">Ações</th>
        </tr>
        </thead>
    </table>

    {{--modal de visualização do ofensor--}}
    <div id="modal-details" class="modal container fade" tabindex="-1"></div>
@endsection

@push('js')

    {{--autocomplete--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    {{--boostrap-table--}}
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>--}}

    {{--Maskinput--}}
    <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

    {{--datepiker--}}
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

    {{--modal--}}
    <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/offender_history/index.js') }}"></script>

@endpush
