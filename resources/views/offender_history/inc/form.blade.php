<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            {!! Form::label('type', 'Tipo Ofensor', ['class'=>'control-label']) !!}
            {!! Form::select('type', ['Externo'=>'Externo', 'Interno'=>'Interno'], null, ['class' => 'bs-select form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! Form::label('status', 'Status', ['class'=>'control-label']) !!}
            {!! Form::select('status', ['Ativo'=>'Ativo', 'Inativo'=>'Inativo'], null, ['class' => 'bs-select form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-4 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('macro_offender', 'Macro Ofensor', ['class'=>'control-label']) !!}
            {!! Form::text('macro_offender', null, ['class' => 'form-control autocomplete', 'data-action' => 'macro_offender', 'data-next_imput_id' => 'macro_offender_id', @$disabled]) !!}
            {!! Form::hidden('macro_offender_id', null, ['id' => 'macro_offender_id']) !!}
        </div>
    </div>
    <div class="col-md-4 external">
        <div class="form-group">
            {!! Form::label('solver_group_id', 'Grupo Resolvedor', ['class'=>'control-label']) !!}
            {!! Form::select('solver_group_id', $resolverGroup, null, ['class' => 'bs-select form-control', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <div class="form-group ">
            {!! Form::label('offender_id', 'ID Ofensor', ['class'=>'control-label']) !!}
            {!! Form::text('offender_id', null, ['class' => 'form-control', @$disabled, (isset($offender) ? 'readonly' : '')]) !!}
        </div>
    </div>
    <div class="col-md-2 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('origin', 'Origem', ['class'=>'control-label']) !!}
            {!! Form::text('origin', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-4 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('classification', 'Classificação', ['class'=>'control-label']) !!}
            {!! Form::text('classification', null, ['class' => 'form-control autocomplete', 'data-action' => 'classification', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! Form::label('retention_level', 'Nivel de Retenção', ['class'=>'control-label']) !!}
            {!! Form::select('retention_level', ['N1'=>'N1', 'N2'=>'N2'], null, ['class' => 'bs-select form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-2 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('available', 'Válido', ['class'=>'control-label']) !!}
            {!! Form::select('available', ['SIM'=>'Sim', 'NÃO'=>'Não', 'INVÁLIDO'=>'Inválido'], null, ['class' => 'bs-select form-control', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('offender', 'Ofensor', ['class'=>'control-label']) !!}
            {!! Form::textarea('offender', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row internal {{ @$externo }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description_and_procedure_id', 'Descrição e ID Procedimento', ['class'=>'control-label']) !!}
            {!! Form::textarea('description_and_procedure_id', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row internal {{ @$externo }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('action_solution_targeting_for_responsible_team', 'Ação / Solução / Direcionamento para Equipe Responsável', ['class'=>'control-label']) !!}
            {!! Form::textarea('action_solution_targeting_for_responsible_team', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row internal {{ @$externo }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('exception', 'Exceção', ['class'=>'control-label']) !!}
            {!! Form::textarea('exception', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row internal {{ @$externo }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('scenario_validation', 'Validação do cenário', ['class'=>'control-label']) !!}
            {!! Form::textarea('scenario_validation', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('detailed_explanation_offender', 'Explicação Detalhada do Ofensor', ['class'=>'control-label']) !!}
            {!! Form::textarea('detailed_explanation_offender', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('answer_for_the_user_in_the_call', 'Resposta do usuário no atendimento do chamado', ['class'=>'control-label']) !!}
            {!! Form::textarea('answer_for_the_user_in_the_call', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('preventive_offender', 'Preventivo Ofensor', ['class'=>'control-label']) !!}
            {!! Form::textarea('preventive_offender', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('symptom', 'Sintoma', ['class'=>'control-label']) !!}
            {!! Form::textarea('symptom', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('fact', 'Fato', ['class'=>'control-label']) !!}
            {!! Form::textarea('fact', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('cause', 'Causa', ['class'=>'control-label']) !!}
            {!! Form::textarea('cause', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('action', 'Ação', ['class'=>'control-label']) !!}
            {!! Form::textarea('action', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            {!! Form::label('ppm_id', 'ID PPM', ['class'=>'control-label']) !!}
            {!! Form::text('ppm_id', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-6 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('project_pack', 'Projeto Pacote', ['class'=>'control-label']) !!}
            {!! Form::text('project_pack', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-2 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('predict_definitive', 'Previsão Definitivo', ['class'=>'control-label']) !!}
            {!! Form::text('predict_definitive', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-2 external {{ @$interno }}">
        <div class="form-group">
            {!! Form::label('num_patch', 'Número Patch', ['class'=>'control-label']) !!}
            {!! Form::text('num_patch', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('patch_identification', 'Identificação Patch', ['class'=>'control-label']) !!}
            {!! Form::text('patch_identification', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('unit_c', 'UNIT - C', ['class'=>'control-label']) !!}
            {!! Form::text('unit_c', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('event_id', 'ID Ocorrência', ['class'=>'control-label']) !!}
            {!! Form::text('event_id', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('proceeding_id', 'ID Procedimento', ['class'=>'control-label']) !!}
            {!! Form::text('proceeding_id', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row external {{ @$interno }}">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('observation', 'Observação', ['class'=>'control-label']) !!}
            {!! Form::textarea('observation', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('modified_fields', 'Campos Alterados', ['class'=>'control-label']) !!}
            {!! Form::textarea('modified_fields', null, ['class' => 'form-control', 'rows' => '5', @$disabled]) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('user', 'Alterado por', ['class'=>'control-label']) !!}
            {!! Form::text('user', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('created_at', 'Data da alteração', ['class'=>'control-label']) !!}
            {!! Form::text('created_at', (isset($offender)) ? null : date('d/m/Y'), ['class' => 'form-control', 'disabled']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('operation_type', 'Tipo da Operação', ['class'=>'control-label']) !!}
            {!! Form::text('operation_type', null, ['class' => 'form-control', @$disabled]) !!}
        </div>
    </div>
</div>