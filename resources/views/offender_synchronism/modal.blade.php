<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{ \Str::limit($synchronism->offender, 100) }}</h4>
</div>
<div class="modal-body">
    {!! Form::model($synchronism, ['class'=>'form-horizontal', 'id' => 'form-save']) !!}
    <div class="form-body">
        @include('offender_synchronism.inc.form', ['disabled' => 'disabled', 'edit' => true])
    </div>
    {!! Form::close() !!}

</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-outline dark">Fechar</button>
</div>