@extends('layouts.app', [
    'breadcrumb' => ['Dashboard', 'Ofensores', 'Sincronismo', 'Novo'],
    'page_title' => 'Novo Sincronismo',
    'page_title_small' => ''
])

@push('css')
    {{-- autocomplete --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {{--Date picker--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @include('utils.messages')
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 form">
            <!-- BEGIN FORM-->
            {!! Form::open(['route' => 'offender.synchronism.index', 'files'=> true, 'class'=>'form-horizontal', 'id' => 'form-save']) !!}
                <div class="form-body">
                    {{--onde relamente esta o form--}}
                    @include('offender_synchronism.inc.form', ['new' => true, 'rowId' => 1])
                    {{--onde são feitos os clones--}}
                    <div id="clones"></div>
                </div>
                <div class="form-actions right">
                    {!! btnForm(route('offender.synchronism.index')) !!}
                </div>
            {!! Form::close() !!}
            <!-- END FORM-->
        </div>
    </div>
@endsection

@push('js')
    {{--autocomplete--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    {{--MaskMoney--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>

    {{--Maskinput--}}
    <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

    {{--date picker--}}
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

    {{-- Validate --}}
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>

    <script src="{{ asset('js/offender_synchronism/form.js') }}"></script>
@endpush
