@extends('layouts.app', [
    'breadcrumb' => ['Dashboard', 'Ofensores', 'Sincronismo'],
    'page_title' => 'Sincronismo',
    'page_title_small' => ''
])

@push('css')
    {{-- autocomplete --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {{--Date picker--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

    {{--boostrap-table--}}
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    {{--modal--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('page_title_actions')

    @can('synchronism_add')
        {!! btnNew(route('offender.synchronism.create')) !!}
    @endcan

@endpush

@section('content')

    @include('utils.messages')

    {{--Filtros--}}
    {!! Form::open(['id' => 'search-form']) !!}
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>Filtros </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body well well-sm">
            <div class="row" style="margin-top: 15px">
                <div id='externo'>
                    <div class="col-sm-2">
                        {!! Form::label('type_filter', 'Tipo de Sincronismo') !!}
                        {!! Form::select('type_filter', $types, 0, ['class' => 'bs-select form-control']) !!}

                    </div>
                    <div class="col-sm-6">
                        {!! Form::label('offender_filter', 'Ofensor') !!}
                        {!! Form::text('offender_filter', null, ['class' => 'form-control', 'id' => 'offender_filter', 'placeholder' => 'Ofensor', 'data-url_search' => route('offender.synchronism.search')]) !!}
                        {!! Form::hidden('offender_id_filter', null, ['id' => 'offender_id_filter']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('create_filter', 'Data Cadastro') !!}
                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                            {!! Form::text('date_ini_filter', null, ['class' => 'form-control mask_date', 'autocomplete' => 'off']) !!}
                            <span class="input-group-addon"> até </span>
                            {!! Form::text('date_end_filter', null, ['class' => 'form-control mask_date', 'autocomplete' => 'off']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    {!! btnFilter(route('offender.synchronism.index')) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    {{--Listagem--}}
    <table class="table table-striped table-hover" id="synchronism-table">
        <thead>
        <tr>
            <th>Cadastrado</th>
            <th>Ofensor</th>
            <th>Tipo</th>
            <th class="text-center" style="width: 85px;">Ações</th>
        </tr>
        </thead>
    </table>

    {{--modal de visualização do ofensor--}}
    <div id="modal-details" class="modal container fade" tabindex="-1"></div>

@endsection

@push('js')

    {{--@include('utils.messages-js')--}}

    {{--autocomplete--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    {{--date picker--}}
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

    {{--boostrap-table--}}
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

    {{--Maskinput--}}
    <script src="{{ asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/offender_synchronism/index.js') }}"></script>

@endpush
