<div id="{{@$rowId}}">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('type', 'Tipo Ofensor', ['class'=>'control-label']) !!}
                {!! Form::select('type[]', config('ongoing.synchronism_types'), null, ['class' => 'bs-select form-control', @$disabled]) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('register_date', 'Data Cadastro', ['class'=>'control-label']) !!}
                <div class="input-group input-icon right">
                    {!! Form::text('register_date[]', null, ['class' => 'form-control mask_date', 'data-date-format' => 'dd/mm/yyyy', 'autocomplete' => 'off', @$disabled, 'required', 'data-date-end-date' => '-0d', 'readonly']) !!}
                    <span class="input-group-addon click_date">
                        <i class="fa fa-calendar font-blue"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="{{ (!isset($edit)) ? 'col-md-7' : 'col-md-8' }}">
            <div class="form-group">
                {!! Form::label('offender', 'Ofensor', ['class'=>'control-label']) !!}
                {!! Form::text('offender', null, ['class' => 'form-control offender_id', 'data-rowid' => @$rowId, 'data-url_search' => route('offender.synchronism.search', @$disabled), 'required']) !!}
                {!! Form::hidden('offender_id[]', null, ['id' => 'offender_id_'.@$rowId]) !!}
            </div>
        </div>
        @if(!isset($edit))
            <div class="col-md-1">
                <div class="form-group">
                    @if(isset($new) && $new)
                        <button type="button" style="margin-top: 26px" class="btn btn-md blue add"><i class="fa fa-plus"></i></button>
                    @else
                        <button type="button" style="margin-top: 26px" data-rowId="{{@$rowId}}" class="btn btn-md red remove"><i class="fa fa-trash"></i></button>
                    @endif
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('possible_treatment', 'Possivel Tratamento', ['class'=>'control-label']) !!}
                {!! Form::text('possible_treatment[]', null, ['class' => 'form-control maskMoney', @$disabled]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('effectively_treated', 'Efetivamente Tratado', ['class'=>'control-label']) !!}
                {!! Form::text('effectively_treated[]', null, ['class' => 'form-control maskMoney', @$disabled]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('treated_percentage', 'Percentual Tratado', ['class'=>'control-label']) !!}
                {!! Form::text('treated_percentage[]', null, ['class' => 'form-control maskMoney', @$disabled]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('ppm', 'PPM', ['class'=>'control-label']) !!}
                {!! Form::text('ppm[]', null, ['id' => 'ppm_id_'.@$rowId,'class' => 'form-control', 'readonly']) !!}
            </div>
        </div>
    </div>
</div>