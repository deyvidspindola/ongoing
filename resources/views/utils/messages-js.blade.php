{{-- Mensagem de successo --}}

<script>
    $(function() {

        @if(session('msg'))

            var sa_title = '{{session('msg')}}';
            var sa_type = 'success';
            var sa_allowOutsideClick = true;
            var sa_confirmButtonClass = 'btn-dark';

            swal({
                title: sa_title,
                type: sa_type,
                allowOutsideClick: sa_allowOutsideClick,
                confirmButtonClass: sa_confirmButtonClass
            });

        @endif


        @if(session('msg-error'))

            var sa_title = '{{session('msg-error')}}';
            var sa_type = 'error';
            var sa_allowOutsideClick = true;
            var sa_confirmButtonClass = 'btn-dark';

            swal({
                title: sa_title,
                type: sa_type,
                allowOutsideClick: sa_allowOutsideClick,
                confirmButtonClass: sa_confirmButtonClass
            });

        @endif

        @if(session('msg-debito'))

            var sa_title = '{{Lang::get('table.transacao_efetuada_successo')}}';
            var sa_message = '{{Lang::get('table.deseja_visualizar_detalhes_dessa_transacao')}}';
            var sa_confirmButtonText = '{{Lang::get('table.sim')}}';
            var sa_cancelButtonText = '{{Lang::get('table.nao')}}';

            var sa_type = 'success';
            var sa_confirmButtonClass = 'green-meadow';
            var sa_cancelButtonClass = 'btn-danger';
            var sa_allowOutsideClick = false;
            var sa_showConfirmButton = true;
            var sa_showCancelButton = true;
            var sa_closeOnConfirm = false;
            var sa_closeOnCancel = true;

            swal({
                title: sa_title,
                text: sa_message,
                type: sa_type,
                allowOutsideClick: sa_allowOutsideClick,
                showConfirmButton: sa_showConfirmButton,
                showCancelButton: sa_showCancelButton,
                confirmButtonClass: sa_confirmButtonClass,
                cancelButtonClass: sa_cancelButtonClass,
                closeOnConfirm: sa_closeOnConfirm,
                closeOnCancel: sa_closeOnCancel,
                confirmButtonText: sa_confirmButtonText,
                cancelButtonText: sa_cancelButtonText
            },
            function(isConfirm){
                if (isConfirm){

                    postJS('{{route('pdf.detalhes_debito')}}', {conta_corrente_id: '{{session('msg-debito')['conta_corrente_id']}}', _token : '{{csrf_token()}}'});

                    swal.close();

                }
            });

        @endif



    });
</script>
