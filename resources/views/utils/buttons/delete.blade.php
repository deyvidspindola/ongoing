<a href="javascript:void(0);" data-id="{{ $id }}" data-route="{{ $route }}" class="btn btn-xs btn-danger delete">
    <i class="icon-trash"></i>
</a>

<script>
    $(function () {

        $('body')
            .on('click', '.delete', function () {

                var id    = $(this).attr('data-id');
                var route = $(this).attr('data-route');

                swal({
                    title: 'Deseja realmente excluir?',
                    text: "Isso será irreversivel!",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Sim, Excluir!'
                },function(){
                    destroy(id, route);
                });

            });

    });

    var destroy = function (id, route) {

        $.ajax({
            url: route,
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "application/json; charset=utf-8",
            data: {
                id: id
            }
        })
        .done(function(data) {
            setTimeout(function () {
                swal({
                    title: data.title,
                    text: data.message,
                    type: 'success'
                }, function () {
                    location.reload();
                });
            }, 1000);
        })
        .fail(function(data) {
            setTimeout(function () {
                swal(data.responseJSON.title, data.responseJSON.message, 'error');
            }, 1000);
        });

    }

</script>