<a href="{{ $route }}"
   data-original-title="{{ Lang::get('sys.btn.edit') }}"
   data-placement="top"
   class="btn tooltips btn-xs btn-primary"
   target=""
>
    <i class="fa fa-pencil"></i>
</a>