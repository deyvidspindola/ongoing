<a href="javascript:void(0);" data-id="{{ $id }}" data-route="{{ $route }}" data-status={{$status}} class="btn btn-xs {{$button}} delete">
    <i class="{{$icon}}"></i>
</a>

<script>
    $(function () {

        $('body')
            .on('click', '.delete', function () {

                let id    = $(this).attr('data-id');
                let route = $(this).attr('data-route');
                let status = $(this).attr('data-status');
                let params = '';
                let action = '';

                if(status == 'Ativo') {
                    params = ['Deseja realmente inativar?','Inativar','btn-danger','warning'];
                    action = 'inativar';
                } else {
                    params = ['Deseja realmente ativar?','Ativar','btn-info','info'];
                    action = 'ativar';
                }

                swal({
                    title: params[0],
                    text: '',
                    type: params[3],
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: params[2],
                    confirmButtonText: 'Sim, ' + params[1]
                },function(){
                    destroy(id, route, action);
                });

            });

    });

    var destroy = function (id, route, action) {

        $.ajax({
            url: route,
            type: 'get',
            data: {
                action: action
            }
        })
        .success(function(data) {
            setTimeout(function () {
                swal({
                    title: data.title,
                    text: data.message,
                    type: 'success'
                }, function () {
                    location.reload();
                });
            }, 1000);
        })
        .error(function(data) {
            setTimeout(function () {
                swal(data.responseJSON.title, data.responseJSON.message, 'error');
            }, 1000);
        });

    }

</script>