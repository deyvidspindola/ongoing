{{-- MENSAGEM DE ERRO --}}

@if($errors->any())

    {!! alertError($errors->all()) !!}


@endif

@if (session('msg'))

    {!! alertSuccess(session('msg')) !!}

@endif

