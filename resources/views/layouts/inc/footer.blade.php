<div class="page-footer">
    <div class="page-footer-inner">
        {{ date('Y') }} &copy; ART IT - <i>Intelligent Technology.</i>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>