<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
  <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
  <li class="sidebar-toggler-wrapper hide">
    <div class="sidebar-toggler">
        <span></span>
    </div>
  </li>
  <!-- END SIDEBAR TOGGLER BUTTON -->
  {{--Home--}}
  <li class="nav-item start {{ (request()->is('home*')) ? 'active open' : '' }}">
    <a href="{{ route('home') }}" class="nav-link nav-toggle">
      <i class="icon-home"></i>
      <span class="title">Dashboard</span>
    </a>
  </li>

  @if(Gate::allows('company_list') || Gate::allows('company_edit') || Gate::allows('user_list') || Gate::allows('group_list'))
    <li class="nav-item start {{ (request()->is('administrador*')) ? 'active open' : '' }}">
      <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-lock"></i>
        <span class="title">{{Lang::get('sys.menu.administrador.administrador')}}</span>
        <span class="selected"></span>
        <span class="arrow {{ (request()->is('administrador*')) ? 'active open' : '' }}"></span>
      </a>

      <ul class="sub-menu">
        @if(Gate::allows('company_list') || Gate::allows('company_edit'))
          <li class="nav-item {{ (request()->is('administrador/empresa*')) ? 'active open' : '' }}">
            <a href="{{route('administrador.empresa.index')}}">
              <span class="title">{{Lang::get('sys.menu.administrador.empresas')}}</span>
            </a>
          </li>
        @endif

        @can('user_list')
          <li class="nav-item {{ (request()->is('administrador/usuarios*')) ? 'active open' : '' }}">
            <a href="{{route('administrador.usuarios.index')}}">
              <span class="title">{{Lang::get('sys.menu.administrador.usuarios')}}</span>
            </a>
          </li>
        @endcan

        @can('group_list')
          <li class="nav-item {{ (request()->is('administrador/grupos*')) ? 'active open' : '' }}">
            <a href="{{route('administrador.grupos.index')}}">
              <span class="title">{{Lang::get('sys.menu.administrador.grupos')}}</span>
            </a>
          </li>
        @endcan

        @can('group_list')
          <li class="nav-item {{ (request()->is('administrador/menus-units*')) ? 'active open' : '' }}">
            <a href="{{route('administrador.menus-units.index')}}">
              <span class="title">{{Lang::get('sys.menu.administrador.menus_units')}}</span>
            </a>
          </li>
        @endcan
      </ul>
    </li>
  @endif

  {{--Ofensores--}}
  @if(Gate::allows('offender_list'))
  <li class="nav-item {{ (request()->is('offender*')) ? 'active open' : '' }}">
    <a href="javascript:;" class="nav-link nav-toggle">
      <i class="icon-feed"></i>
      <span class="title">{{Lang::get('sys.menu.ofensores.ofensores')}}</span>
      <span class="arrow"></span>
    </a>
    <ul class="sub-menu">

      {{--Ofensores--}}
      @can('offender_list')
      <li class="nav-item {{ (request()->is('offender')) ? 'active open' : '' }}">
        <a href="{{ route('offender.index') }}" class="nav-link ">
          <span class="title">{{Lang::get('sys.menu.ofensores.ofensores')}}</span>
        </a>
      </li>
      @endcan

      {{--Sincronismo--}}
      @can('synchronism_list')
        <li class="nav-item {{ (request()->is('offender/synchronism')) ? 'active open' : '' }}">
          <a href="{{ route('offender.synchronism.index') }}" class="nav-link ">
            <span class="title">{{Lang::get('sys.menu.ofensores.sincronismo')}}</span>
          </a>
        </li>
      @endcan

      {{--Extração de Dados--}}
      @can('offender_list')
      <li class="nav-item {{ (request()->is('offender/export')) ? 'active open' : '' }}">
        <a href="{{ route('offender.export.index') }}" class="nav-link ">
          <span class="title">{{Lang::get('sys.menu.ofensores.extracao_dados')}}</span>
        </a>
      </li>
      @endcan

      {{--Carga de Dados--}}
      @can('offender_import')
      <li class="nav-item {{ (request()->is('offender/import')) ? 'active open' : '' }}">
        <a href="{{ route('offender.import.index') }}" class="nav-link ">
          <span class="title">{{Lang::get('sys.menu.ofensores.carga_dados')}}</span>
        </a>
      </li>
      @endcan

      {{--Histórico--}}
      @can('offender_list')
      <li class="nav-item  {{ (request()->is('offender/history')) ? 'active open' : '' }}">
        <a href="{{ route('offender.history.index') }}" class="nav-link ">
          <span class="title">{{Lang::get('sys.menu.ofensores.historico')}}</span>
        </a>
      </li>
      @endcan
    </ul>
  </li>
  @endif

  {{--Units--}}
  @if(Gate::allows('units_dashboard'))

    @include('administrador.menu_units.nav')

  @endif 
</ul>