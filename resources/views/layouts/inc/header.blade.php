<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('home') }}">
                <div class="caption logo-default">
                    <span class="caption-subject font-blue-steel bold uppercase"> {!! env('APP_NAME') !!}</span>
                </div>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
            @php
                $totalNotifications = auth()->user()->unreadNotifications->count();
            @endphp
            @if($totalNotifications)
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-default"> {{$totalNotifications}} </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <span class="bold">Notificações</span> pendentes
                            </h3>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                @foreach(auth()->user()->unreadNotifications as $notification)
                                <li>
                                    <a href="{{ route('offender.edit', $notification->data['offender_id']) }}" data-id="{{ $notification->id }}" class="read_notification">
                                        <span class="details">
                                            <span class="label label-sm label-icon label-warning">
                                                <i class="fa fa-bell-o"></i>
                                            </span> {{ $notification->data['message'] }}
                                        </span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </li>
            @endif
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN COMPANY BAR -->

                @if(config('sys.show_company'))

                    <li class="dropdown dropdown-language">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="fa fa-bank"></i>
                            <span class="langname"><small>{{Lang::get('sys.header.company')}}:</small> <b>{{session('company')['name']}}</b> </span>

                            @if(Auth::user()->isAdmin() && session('companies')->count() > 1)
                                <i class="fa fa-angle-down"></i>
                            @else
                                &nbsp;&nbsp;
                            @endif

                        </a>

                        @if(Auth::user()->isAdmin() && session('companies')->count() > 1)

                            <ul class="dropdown-menu dropdown-menu-default">

                                @foreach(session('companies') as $company)

                                    @if($company->id != session('company')['id'])

                                        <li>
                                            <a href="javascript:;" onclick="event.preventDefault(); document.getElementById('choice-form-{{$company->id}}').submit();">
                                                {{$company->description}}
                                            </a>

                                            {!! Form::open(['route' => 'session.company.choice', 'method' => 'post', 'id'=> 'choice-form-'.$company->id, 'style'=>'display: none;']) !!}

                                            {!! Form::hidden('id', $company->id) !!}

                                            {!! Form::close() !!}
                                        </li>

                                    @endif

                                @endforeach

                            </ul>

                        @endif

                    </li>

            @endif

            <!-- END COMPANY BAR -->

                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="fa fa-user"></i>
                        <span class="langname"><small>{{Lang::get('sys.header.user')}}:</small> <b>{{ Auth::user()->name}}</b></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>

                            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="icon-logout"></i>&nbsp;&nbsp;&nbsp;{{Lang::get('sys.header.logout')}}
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>