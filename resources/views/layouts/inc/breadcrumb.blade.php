@if(isset($breadcrumb) && is_array($breadcrumb))

    <div class="page-bar">
        <ul class="page-breadcrumb">
            {!! '<li><span>'.implode('</span><i class="fa fa-circle"></i></li><li><span>', $breadcrumb).'</i></li>' !!}
        </ul>
    </div>


@endif