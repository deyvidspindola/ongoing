@extends('layouts.app', [
    'breadcrumb' => ['Administrador', 'Empresa', 'Listagem'],
    'page_title' => 'Empresa',
    'page_title_small' => '',
])

@push('css')
@endpush

@push('page_title_actions')
    @if(Auth::user()->isAdmin())
        {!! btnNew(route('administrador.empresa.create')) !!}
    @endif
@endpush

@section('content')

    @include('utils.messages')

    {!! Form::open(['route' => 'administrador.empresa.index', 'method' => 'get']) !!}

    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-5">
                {!! Form::label('name', 'Empresa') !!}
                {!! Form::text('name', $request->name, ['class' => 'form-control', 'placeholder' => 'Todas']) !!}
            </div>
            <div class="col-sm-7 text-right">
                {!! btnFilter(route('administrador.empresa.index')) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="panel panel-default no-margin">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{'Listagem'}}
        </div>
        <div class="panel-body no-padding">

            <table class="table table-hover table-striped no-margin">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Domínio para autenticação</th>
                    <th width="80" class="text-center">Ações</th>
                </tr>
                </thead>
                <tbody>
                @forelse($empresas as $empresa)
                    <tr>
                        <td>{{ $empresa->description }}</td>
                        <td><span class="text-primary">usuario<b style="font-size: 11pt">{{ '@'.$empresa->domain }}</b></span></td>

                        <td class="text-center">

                            @can('company_edit')
                                {!! btnEdit(route('administrador.empresa.edit', $empresa->id)) !!}
                            @endcan

                            @if(Auth::user()->isAdmin() && session('company')['id'] != $empresa->id)
                                {!! btnDelete(route('administrador.empresa.destroy', $empresa->id)) !!}
                            @endif

                        </td>

                    </tr>
                @empty
                    <tr>
                        <td colspan="3">
                            {{Lang::get('sys.msg.not_found_all')}}
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            {!! $empresas->appends($request->all())->links() !!}
        </div>
    </div>

@endsection

@push('js')

    @include('utils.messages-js')
    <script>
        $(function() {
            runConfirmDelete();
        });
    </script>

@endpush