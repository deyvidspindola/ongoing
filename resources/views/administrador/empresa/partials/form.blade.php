<div class="row">


    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('description', Lang::get('table.nome_empresa')) !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '255']) !!}
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('domain', Lang::get('table.dominio')) !!}

            <div class="input-icon left">
                <i class="fa fa-info-circle popovers" data-content="{{Lang::get('table.dominio_info')}}" data-placement="right" data-trigger="hover" data-container="body"></i>
                {!! Form::text('domain', null, ['class' => 'form-control remove-spaces-with-point text-lowercase', 'required' => 'required', 'maxlength' => '255']) !!}
            </div>

        </div>

    </div>

</div>

<br>

<div class="row">
    <div class="col-sm-12">

        <!-- tabs -->
        <div class="tabbable-custom">
            <ul id="myTab4" class="nav nav-tabs">
                <li id="tab_parametros" class="active">
                    <a href="#panel_parametros" data-toggle="tab" aria-expanded="true">{{Lang::get('table.parametros')}}</a>
                </li>
                <li id="tab_ad" class="">
                    <a href="#panel_ad" data-toggle="tab" aria-expanded="false">{{Lang::get('table.ad')}}</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- permissoes -->
                <div class="tab-pane active" id="panel_parametros">

                    <br>

                    <div class="row">

                        <div class="col-sm-11 col-sm-offset-1 form-horizontal">

                            <div class="form-group">
                               {!! Form::label('auth_ad', Lang::get('table.auth_ldap'), ['class'=>'control-label col-sm-3', 'style'=>'margin-top: 7px']) !!}
                               <div class="col-sm-9">


                                   <label class="mt-radio mt-radio-outline">
                                       {!! Form::radio('auth_ad', 1, null, ['id'=> 'auth_ad_1', 'class' => 'square-green']) !!}
                                       &nbsp;&nbsp;{{Lang::get('table.sim_up')}}<span></span>
                                   </label>
                                   &nbsp;&nbsp;&nbsp;
                                   <label class="mt-radio mt-radio-outline">
                                       {!! Form::radio('auth_ad', 0, null, ['id'=> 'auth_ad_0', 'class' => 'square-red']) !!}
                                       &nbsp;&nbsp;{{Lang::get('table.nao_up')}}<span></span>
                                   </label>
                                   &nbsp;&nbsp;
                                   <br><span class="font-blue-steel"><i class="fa fa-info-circle"></i> {{Lang::get('table.auth_ldap_info')}}</span>
                               </div>
                            </div>


                        </div>

                    </div>

                </div>
                <!-- /permissoes -->

                <!-- usuários -->
                <div class="tab-pane" id="panel_ad">

                    <br>

                    <div class="row">

                        <div class="col-sm-8 col-sm-offset-1 form-horizontal">

                            <div class="form-group">
                                {!! Form::label('ad_connection', Lang::get('table.conexao'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('ad_connection', null, ['id'=>'ad_connection','class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: 10.230.230.1"]) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_username', Lang::get('table.usuario'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('ad_username', null, ['id'=>'ad_username','class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: xpto"]) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_password', Lang::get('table.senha'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::input('password', 'ad_password', (isset($data['ad_password'])?$data['ad_password']:null), ['id'=>'ad_password','class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: 132456"]) !!}
                                    {!! Form::input('password', 'password_fake', null, ["style"=>"display:none"]) !!}

                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_domain', Lang::get('table.dominio'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('ad_domain', null, ['id'=>'ad_domain','class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: xpto.local"]) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_port', Lang::get('table.porta'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::input('number', 'ad_port', null, ['id'=>'ad_port', 'class' => 'form-control type-number', 'placeholder'=>"Ex.: 3268"]) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_dn_users', Lang::get('table.dn_usuarios'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('ad_dn_users', null, ['id'=>'ad_dn_users','class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: OU=Usuarios,OU=Xpto,DC=xpto,DC=local"]) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_dn_groups', Lang::get('table.dn_grupos'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('ad_dn_groups', null, ['id'=>'ad_dn_groups','class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: OU=Grupos,OU=Xpto,DC=xpto,DC=local"]) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('ad_fields', Lang::get('table.campos'), ['class'=>'control-label col-sm-4']) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('ad_fields', null, ['id'=>'ad_fields', 'class' => 'form-control', "style"=>"width: 100%", 'placeholder'=>"Ex.: givenName,sn,mail,samaccountname,memberof"]) !!}
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-8 col-sm-offset-1 text-right">

                            <span id="test_connection" class="btn btn-primary" style="width: 150px">
                                <i class="fa fa-dashboard"></i> {{Lang::get('table.testar_conexao')}}
                            </span>

                        </div>

                    </div>

                </div>
                <!-- /usuários -->
            </div>
        </div>
        <!-- /tabs -->


    </div>
</div>