@extends('layouts.app', [
    'breadcrumb' => ['Administrador', 'Empresa', Lang::get('table.editar')],
    'page_title' => 'Empresa',
    'page_title_small' => '',
])

@push('css')
@endpush


@push('page_title_actions')

    <div class="row">
        <div class="col-sm-12 text-right">
            {!! btnForm(route('administrador.empresa.index'), true) !!}
        </div>
    </div>

@endpush

@section('content')

    @include('utils.messages')

    {!! Form::model($data, ['route' => ['administrador.empresa.update', $data['id']], 'id' => 'form', 'method' => 'post']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{Lang::get('table.editar')}}
        </div>
        <div class="panel-body">

            @include('administrador.empresa.partials.form')

        </div>
    </div>

    {!! Form::close() !!}

@endsection

@push('js')

    @include('utils.messages-js')

    {{-- Validate --}}
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>

    {{-- Locale --}}
    @if(App::isLocale('pt-br'))
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>
    @endif

    <script>
        $(function() {

            runValidate('#form');

            runBtnSaveForm('#form');

            runConfirmCancel();

        });

        var runBtnSaveForm = function(element){

            $( ".btn-save-form" ).click(function() {

                $(element).submit();

            });

        };

        // VALIDAÇÕES AO SUBMETER O FORMULARIO
        $("#form").on('submit', function( event ) {

            // Recupero o valor da autenticação no AD
            var auth_ad = parseInt($('input:radio[name=auth_ad]:checked').val());

            // Se não for para autenticar no LDAP
            if(auth_ad == 0) return true;

            // Se for para autenticar no LDAP, verifico se as informações

            // Recupero os valores dos campos
            var ad_connection   = $.trim($("#ad_connection").val());
            var ad_username     = $.trim($("#ad_username").val());
            var ad_password     = $.trim($("#ad_password").val());
            var ad_domain       = $.trim($("#ad_domain").val());
            var ad_port         = $.trim($("#ad_port").val());
            var ad_dn_users     = $.trim($("#ad_dn_users").val());
            var ad_dn_groups    = $.trim($("#ad_dn_groups").val());
            var ad_fields       = $.trim($("#ad_fields").val());

            // Se não estiver valido algum campo, exibo mensagem
            if(!ad_connection || !ad_username || !ad_password || !ad_domain || !ad_port || !ad_dn_users || !ad_dn_groups || !ad_fields) {

                runAlertWarning('{{Lang::get('sys.msg.alert-warning')}}!', '{{Lang::get('table.preencher_informacoes_ad')}}');

                // ALTERO AS ABAS

                // Atualizo as abas
                $('#tab_parametros').removeClass('active');
                $('#tab_ad').addClass('active');

                // Atualizo o conteúdo da aba
                $('#panel_parametros').removeClass('active');
                $('#panel_ad').addClass('active');

            }
            // Se estiver tudo preenchido, faço a validação da conexão
            else{

                var test_connection = 0;

                $.ajax({
                    url: "{{route('administrador.empresa.testConnection')}}",
                    type: "POST",
                    data: $('#form').serialize(),
                    async: false,
                    dataType: 'json',
                    success: function (response) {
                        if(!response.success) {
                            runAlertError(response.message, '');
                            event.preventDefault();
                        }
                    },
                error: function (response) {
                    runAlertError(response.message, '');
                }
                });
            }
        });


        // TESTAR SE A CONEXÃO LDAP É VALIDA
        $("#test_connection").click(function() {

            $.ajax({
                url: "{{route('administrador.empresa.testConnection')}}",
                type: "POST",
                dataType: 'json',
                data: $('#form').serialize(),
                success: function (response) {
                    if(response.success) {
                        runAlertSuccess(response.message, '');
                    } else{
                        runAlertError(response.message, '');
                    }
                },
                error: function (response) {
                    runAlertError(response.message, '');
                }

            });

        });
    </script>

@endpush