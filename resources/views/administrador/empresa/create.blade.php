@extends('layouts.app', [
    'breadcrumb' => ['Administrador', 'Empresa', 'Novo'],
    'page_title' => 'Empresa',
    'page_title_small' => '',
])

@push('css')
@endpush

@push('page_title_actions')

    <div class="row">
        <div class="col-sm-12 text-right">
            {!! btnForm(route('administrador.empresa.index')) !!}
        </div>
    </div>

@endpush

@section('content')

    @include('utils.messages')

    {!! Form::model($data, ['route' => 'administrador.empresa.store', 'id' => 'form', 'method' => 'post']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{'Novo'}}
        </div>
        <div class="panel-body">

            @include('administrador.empresa.partials.form')

        </div>
    </div>

    {!! Form::close() !!}


    <div class="modal fade" id="loading_connect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/comp_3.gif') }}" alt="" width="280">
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

    {{-- Validate --}}
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>

    {{-- Locale --}}
    @if(App::isLocale('pt-br'))
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>
    @endif

    <script>
        $(function() {

            runValidate('#form');

            runBtnSaveForm('#form');

            runConfirmCancel();

        });

        var runBtnSaveForm = function(element){

            $( ".btn-save-form" ).click(function() {

                $(element).submit();

            });

        };

        // VALIDAÇÕES AO SUBMETER O FORMULARIO
        $("#form").submit(function( event ) {

            // Recupero o valor da autenticação no AD
            var auth_ad = parseInt($('input:radio[name=auth_ad]:checked').val());

            // Se não for para autenticar no LDAP
            if(auth_ad == 0) return true;

            // Se for para autenticar no LDAP, verifico se as informações

                // Recupero os valores dos campos
                var ad_connection   = $.trim($("#ad_connection").val());
                var ad_username     = $.trim($("#ad_username").val());
                var ad_password     = $.trim($("#ad_password").val());
                var ad_domain       = $.trim($("#ad_domain").val());
                var ad_port         = $.trim($("#ad_port").val());
                var ad_dn_users     = $.trim($("#ad_dn_users").val());
                var ad_dn_groups    = $.trim($("#ad_dn_groups").val());
                var ad_fields       = $.trim($("#ad_fields").val());

                // Se não estiver valido algum campo, exibo mensagem
                if(!ad_connection || !ad_username || !ad_password || !ad_domain || !ad_port || !ad_dn_users || !ad_dn_groups || !ad_fields) {

                    runAlertWarning('{{Lang::get('sys.msg.alert-warning')}}!', '{{Lang::get('table.preencher_informacoes_ad')}}');

                    // ALTERO AS ABAS

                    // Atualizo as abas
                    $('#tab_parametros').removeClass('active');
                    $('#tab_ad').addClass('active');

                    // Atualizo o conteúdo da aba
                    $('#panel_parametros').removeClass('active');
                    $('#panel_ad').addClass('active');

                }
                // Se estiver tudo preenchido, faço a validação da conexão
                else{

                    var test_connection = 0;

                    $.ajax({
                        url: "{{route('administrador.empresa.testConnection')}}",
                        type: "POST",
                        data: $('#form').serialize(),
                        async: false,
                        success: function (response) {
                            test_connection = parseInt(response);
                        }
                    });

                    // Caso tenha passado no teste de conexão
                    if(test_connection == 1) return true;

                    // Se não deu certo o teste de conexão
                    runAlertError('{{Lang::get('table.nao_foi_possivel_conexao_ad')}}', '{{Lang::get('table.revalide_informacao_ad')}}');

                }


            event.preventDefault();

        });


        // TESTAR SE A CONEXÃO LDAP É VALIDA
        $("#test_connection").click(function() {
            $.ajax({
                url: "{{route('administrador.empresa.testConnection')}}",
                type: 'POST',
                data: $('#form').serialize(),
                beforeSend: function() {
                    $('#loading_connect').modal('show');
                },
                complete: function() {
                    $('#loading_connect').modal('hide');
                },
                success: function() {
                    runAlertSuccess('{{Lang::get('table.conexao_efetuada_ad')}}', '');
                },
                error: function() {
                    runAlertError('{{Lang::get('table.conexao_erro_ad')}}', '');
                }
            });
        });
    </script>

@endpush