@extends('layouts.app', [
    'breadcrumb' => ['Administrador', 'Menus UNI.T-S'],
    'page_title' => 'Menus UNI.T-S',
    'page_title_small' => '',
])

@push('css')
    <link href="{{ asset('assets/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('page_title_actions')

@endpush

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{ Lang::get('table.novo_m') }}
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-offset-3 col-md-6">


                    <input type="hidden" id="url" value="{{ route('administrador.menus-units.update-order') }}">
                    
                    <div class="dd" id="nestable_list_3">

                        {!! $menu !!}

                    </div>

                    <button type="button" class="btn btn-block btn-primary" id="new">Novo Menu</button>
                </div>
            </div>

        </div>
    </div>

    @include('administrador.menu_units.inc.modal')

@endsection

@push('js')

    <script src="{{ asset('assets/global/plugins/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
    <script>

        $('.dd').nestable();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body')
            .on('click', '#new', function () {
                // Abre o modal para cadastrar um novo menu
                $('#title').html('Novo Menu');
                $('.form-control').val('');
                $('#id').val(0);
                $('#responsive').modal('show');

            })
            .on('click', '.edit', function () {
                // exibe as informações de um menu já acadastrado
                var id = $(this).data('id');
                $('#title').html($(this).data('title'));
                $.get('menus-units/show/'+id, function (data) {
                    setData(data);
                    $('#responsive').modal('show');
                });
            })
            .on('click', '#save', function (e) {
                //Cadastra ou Atualiza um menu
                var id = $('#id').val();
                var url = (id == 0) ? 'menus-units/create' : 'menus-units/update';

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        title:      $('#titulo').val(),
                        sub_title:  $('#sub_title').val(),
                        permission: $('#permission').val(),
                        slug:       $('#slug').val(),
                        id:         id
                    },
                    success: function (data) {
                        $('#responsive').modal('hide');
                        swal('Sucesso!', data.message, 'success');
                    },
                    error: function (data) {
                        $('#responsive').modal('hide');
                        swal('Erro!', data.message, 'error');
                    }
                });

            })
            .on('change', '.dd', function (e) {
                //Atualiza o posicionamento dos menus
                var url = $('#url').val();
                var list   = e.length ? e : $(e.target);
                if (window.JSON) {
                    var menuPosition = (window.JSON.stringify(list.nestable('serialize')));

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            menu: menuPosition
                        },
                        success: function (data) {
                            // swal('Sucesso!', data.message, 'success');
                        },
                        error: function (data) {
                            swal('Erro!', data.message, 'error');
                        }
                    });

                }
            });

        var setData = function (data) {

            $("#titulo").val(data.title);
            $("#sub_title").val(data.sub_title);
            $("#permission").val(data.permission);
            $("#slug").val(data.slug);
            $('#id').val(data.id);

        }

    </script>

@endpush