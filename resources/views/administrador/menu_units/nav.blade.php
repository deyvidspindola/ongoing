<li class="nav-item {{ (request()->is('units/*')) ? 'active open' : '' }}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-wrench"></i>
        <span class="title">UNI.T-S</span>
        <span class="arrow"></span>
    </a>
    {!! $dinamic_menu !!}
</li>