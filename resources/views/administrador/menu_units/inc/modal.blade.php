<div class="modal fade" id="responsive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route' => 'administrador.grupos.store', 'id' => 'form-save']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="title"></h4>
                </div>
                <div class="modal-body" id="content">
                    <div class="form-group">
                        {!! Form::label('title', 'Titulo', ['class'=>'control-label']) !!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'titulo']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sub_title', 'Sub Titulo', ['class'=>'control-label']) !!}
                        {!! Form::text('sub_title', null, ['class' => 'form-control', 'id' => 'sub_title']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('permission', 'Permissão', ['class'=>'control-label']) !!}
                        {!! Form::text('permission', null, ['class' => 'form-control', 'id' => 'permission']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('url', 'Url', ['class'=>'control-label']) !!}
                        {!! Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) !!}
                    </div>
                </div>
                <input type="hidden" id="id" value="0">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get('sys.btn.cancel')}}</button>
                    <button class="btn btn-primary" type="button" id="save">Salvar</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>