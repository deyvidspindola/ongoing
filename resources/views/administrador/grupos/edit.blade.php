@extends('layouts.app', [
    'breadcrumb' => ['Administrador', Lang::get('table.grupos_acesso'), Lang::get('table.editar')],
    'page_title' => Lang::get('table.grupos_acesso'),
    'page_title_small' => '',
])

@push('css')

    {{-- Duallistbox--}}
    <link rel="stylesheet" href="{{ asset('assets/global/plugins/bootstrap-duallistbox/src/bootstrap-duallistbox.css') }}">

@endpush

@push('page_title_actions')

    <div class="row">
        <div class="col-sm-12 text-right">
            {!! btnForm(route('administrador.grupos.index'), true) !!}
        </div>
    </div>

@endpush

@section('content')

    @include('utils.messages')

    {!! Form::model($group, ['route' => ['administrador.grupos.update', $group->id], 'id' => 'form-save']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{Lang::get('table.editar')}}
        </div>
        <div class="panel-body">

            @include('administrador.grupos.partials.form')

        </div>
    </div>

    {!! Form::close() !!}

@endsection

@push('js')

    {{-- Validate --}}
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>

    @if(App::isLocale('pt-br'))
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>
    @endif

    {{-- Duallistbox--}}
    <script src="{{ asset('assets/global/plugins/bootstrap-duallistbox/src/jquery.bootstrap-duallistbox.js') }}" type="text/javascript"></script>


    <script>
        $(function() {

            runConfirmCancel();

            runValidate('#form-save');

            runBtnSaveForm('#form-save');

            runSelectMultiple('permissions', '{{Lang::get('table.permissoes')}}', '{{Lang::get('table.permissoes_selecionadas')}}');

            runSelectMultiple('users', '{{Lang::get('table.usuarios')}}', '{{Lang::get('table.usuarios_selecionados')}}');

        });

        var runBtnSaveForm = function(element){

            $( ".btn-save-form" ).click(function() {

                $(element).submit();

            });

        };
    </script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/groups/create.js') }}"></script>
@endpush