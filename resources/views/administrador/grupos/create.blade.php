@extends('layouts.app', [
    'breadcrumb' => ['Administrador', Lang::get('table.grupos_acesso'), Lang::get('table.novo_m')],
    'page_title' => Lang::get('table.grupos_acesso'),
    'page_title_small' => '',
])

@push('css')

    {{-- Duallistbox--}}
    <link rel="stylesheet" href="{{ asset('assets/global/plugins/bootstrap-duallistbox/src/bootstrap-duallistbox.css') }}">

    {{-- Select2 --}}
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

@endpush

@push('page_title_actions')

    <div class="row">
        <div class="col-sm-12 text-right">
            {!! btnForm(route('administrador.grupos.index')) !!}
        </div>
    </div>

@endpush

@section('content')

    @include('utils.messages')

    {!! Form::open(['route' => 'administrador.grupos.store', 'id' => 'form-save']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{ Lang::get('table.novo_m') }}
        </div>
        <div class="panel-body">

            @include('administrador.grupos.partials.form')

        </div>
    </div>

    {!! Form::close() !!}

@endsection

@push('js')

    {{-- Validate --}}
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>

    {{-- Duallistbox--}}
    <script src="{{ asset('assets/global/plugins/bootstrap-duallistbox/src/jquery.bootstrap-duallistbox.js') }}" type="text/javascript"></script>

    {{-- Select2 --}}
    <script src="{{ asset('assets/global/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>

    {{-- Locale --}}
    @if(App::isLocale('pt-br'))
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/select2/js/i18n/pt-BR.js')}}" type="text/javascript"></script>
    @endif

    <script>

        $(function() {

            runConfirmCancel();

            runValidate('#form-save');

            runBtnSaveForm('#form-save');

            runSelect2('#group_name');

            runSelectMultiple('permissions', '{{Lang::get('table.permissoes')}}', '{{Lang::get('table.permissoes_selecionadas')}}');

            runSelectMultiple('users', '{{Lang::get('table.usuarios')}}', '{{Lang::get('table.usuarios_selecionados')}}');
        });


        var runBtnSaveForm = function(element){

            $( ".btn-save-form" ).click(function() {

                $(element).submit();

            });

        };
    </script>

    {{--scripts para a pagina--}}
    <script src="{{ asset('js/groups/create.js') }}"></script>
@endpush