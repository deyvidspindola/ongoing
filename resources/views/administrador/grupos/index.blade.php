@extends('layouts.app', [
    'breadcrumb' => ['Administrador', Lang::get('table.grupos_acesso'), 'Listagem'],
    'page_title' => Lang::get('table.grupos_acesso'),
    'page_title_small' => '',
])

@push('css')

@endpush

@push('page_title_actions')
    @can('group_add')
        {!! btnNew(route('administrador.grupos.create')) !!}
    @endcan
@endpush

@section('content')

    @include('utils.messages')

    {!! Form::open(['route' => 'administrador.grupos.index', 'method' => 'get']) !!}

    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-5">
                {!! Form::label('name', Lang::get('table.grupo')) !!}
                {!! Form::text('name', $request->name, ['class' => 'form-control', 'placeholder' => Lang::get('table.all_m')]) !!}
            </div>
            <div class="col-sm-7 text-right">
                {!! btnFilter(route('administrador.grupos.index')) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="panel panel-default no-margin">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{'Listagem'}}
        </div>
        <div class="panel-body no-padding">

            <table class="table table-hover table-striped no-margin">
                <thead>
                <tr>
                    <th style="width: 250px">{{Lang::get('table.nome')}}</th>
                    <th>{{Lang::get('table.descricao')}}</th>
                    <th width="80" class="text-center">Ações</th>
                </tr>
                </thead>
                <tbody>
                @forelse($groups as $group)
                    <tr>
                        <td>{{ $group->name }}</td>
                        <td>{{ $group->description }}</td>


                        <td class="text-center">

                            @if(!$group->is_admin)

                                @can('group_edit')
                                    {!! btnEdit(route('administrador.grupos.edit', $group->id)) !!}
                                @endcan

                                @can('group_del')
                                    @if(!$auth_ad)
                                        {!! btnDelete(route('administrador.grupos.destroy', $group->id)) !!}
                                    @endif
                                @endcan

                            @endif

                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">
                            {{Lang::get('sys.msg.not_found_all')}}
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            {!! $groups->appends($request->all())->links() !!}
        </div>
    </div>



@endsection

@push('js')

    @include('utils.messages-js')

    <script>
        $(function() {
            runConfirmDelete();
        });
    </script>

@endpush