<div class="row">

    <div class="col-sm-6">
        <div class="form-group">

            {{-- NOVO GRUPO --}}
            @if(isset($groups))

                @if($auth_ad)
                    {!! Form::label('name', Lang::get('table.grupo')) !!}
                    {!! Form::select('name', $groups, null, ['id'=>'group_name', 'class' => 'form-control search-select', 'required' => 'required', 'data-locale'=>(App::isLocale('pt-br') ? 'pt-BR' : App::getLocale())]) !!}
                @else
                    {!! Form::label('name', Lang::get('table.nome_grupo')) !!}
                    {!! Form::text('name', null, ['class' => 'form-control remove-spaces text-lowercase', 'required' => 'required', 'maxlength' => '50', 'autofocus']) !!}
                @endif

            {{-- EDITAR GRUPO --}}
            @else
                {!! Form::label('name', Lang::get('table.grupo')) !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
            @endif
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('description', Lang::get('table.descricao')) !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '100']) !!}
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">

        <div class="tabbable-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#panel_permissoes" data-toggle="tab" aria-expanded="true">{{Lang::get('table.permissoes')}}</a>
                </li>
                @if(!$auth_ad)
                    <li class="">
                        <a href="#panel_usuarios" data-toggle="tab" aria-expanded="false">{{Lang::get('table.usuarios')}}</a>
                    </li>
                @endif
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="panel_permissoes">
                    {!! Form::select('permissions[]', $permissions, (isset($linked_permissions) ? $linked_permissions : null), ['id' => 'permissoes', 'multiple' => true, 'data-filter'=>Lang::get('table.filtrar'), 'onchange' => 'hideSolverGroups()']) !!}


                        <table class="table table-hover table-striped no-margin" id="solver_groups_table" style="display: none;">
                            <thead>
                            <tr>
                                <th style="width: 60%;">{{Lang::get('table.grupos_resolvedores')}}</th>
                                <th class="text-center">{{Lang::get('table.grupo_resolvedor_listar_interno')}}</th>
                                <th class="text-center">{{Lang::get('table.grupo_resolvedor_listar_externo')}}</th>
                                <th class="text-center">{{Lang::get('table.grupo_resolvedor_criar')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($solverGroups as $solverGroup)
                                    <tr>
                                        <td>{!! $solverGroup['name']; !!}</td>
                                        <td class="text-center">{!! Form::checkbox("solverGroup[$solverGroup->id][]", 'int', $sgg[$solverGroup->id]['int'], ['class' => 'solver_'.$solverGroup->id]) !!}</td>
                                        <td class="text-center">{!! Form::checkbox("solverGroup[$solverGroup->id][]", 'ext', $sgg[$solverGroup->id]['ext'], ['class' => 'solver_'.$solverGroup->id]) !!}</td>
                                        <td class="text-center">{!! Form::checkbox("solverGroup[$solverGroup->id][]", 'cre', $sgg[$solverGroup->id]['cre'], ['id' => $solverGroup->id, 'class' => 'create']) !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                </div>
                <div class="tab-pane" id="panel_usuarios">
                    {!! Form::select('users[]', $users, (isset($linked_users) ? $linked_users : null), ['multiple' => true, 'data-filter'=>Lang::get('table.filtrar')]) !!}
                </div>
            </div>
        </div>

    </div>
</div>