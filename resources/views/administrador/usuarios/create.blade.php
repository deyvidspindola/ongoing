@extends('layouts.app', [
    'breadcrumb' => ['Administrador', Lang::get('table.usuarios'), Lang::get('table.novo_m')],
    'page_title' => Lang::get('table.usuarios'),
    'page_title_small' => '',
])

@push('css')
@endpush

@section('content')

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            @include('utils.messages')
        </div>
    </div>

    {!! Form::open(['route' => 'administrador.usuarios.store', 'class'=>'form-horizontal', 'id' => 'form-save']) !!}

    <div class="row">

        <div class="col-sm-8 col-sm-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    {{ Lang::get('table.novo_m') }}
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        {!! Form::label('username', Lang::get('table.usuario'), ['class'=>'control-label col-sm-4']) !!}

                        <div class="col-sm-5">
                            {!! Form::text('username', null, ['class' => 'enterSubmit form-control remove-spaces-with-point', 'required' => 'required', 'autofocus']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('password', Lang::get('table.senha'), ['class'=>'control-label col-sm-4']) !!}

                        <div class="col-sm-5">

                            <div class="input-group">
                                {!! Form::input('password', 'password', null, ['class' => 'enterSubmit form-control', 'required' => 'required']) !!}

                                {!! changeViewPass('password') !!}
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        {!! Form::label('name', Lang::get('table.nome'), ['class'=>'control-label col-sm-4']) !!}
                        <div class="col-sm-8">
                            {!! Form::text('name', null, ['class' => 'enterSubmit form-control', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', Lang::get('table.email'), ['class'=>'control-label col-sm-4']) !!}
                        <div class="col-sm-8">
                            {!! Form::email('email', null, ['class' => 'enterSubmit form-control']) !!}
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 text-right">
                    {!! btnForm(route('administrador.usuarios.index')) !!}
                </div>
            </div>

        </div>

    </div>

    {!! Form::close() !!}

@endsection

@push('js')

    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>

    @if(App::isLocale('pt-br'))
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js') }}"></script>
    @endif

    <script>
        $(function() {
            runValidate('#form-save');

            runEnterSubmit('#form-save');

            runConfirmCancel();
        });
    </script>

@endpush
