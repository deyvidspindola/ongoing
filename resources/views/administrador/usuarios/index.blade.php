@extends('layouts.app', [
    'breadcrumb' => ['Administrador', Lang::get('table.usuarios'), 'Listagem'],
    'page_title' => Lang::get('table.usuarios'),
    'page_title_small' => '',
])

@push('css')
@endpush

@push('page_title_actions')
    @can('user_add')
        {!! btnNew(route('administrador.usuarios.create')) !!}
    @endcan
@endpush

@section('content')

    @include('utils.messages')

    {!! Form::open(['route' => 'administrador.usuarios.index', 'method' => 'get']) !!}

    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-5">
                {!! Form::label('name', Lang::get('table.usuario')) !!}
                {!! Form::text('name', $request->name, ['class' => 'form-control', 'placeholder' => Lang::get('table.all_m')]) !!}
            </div>
            <div class="col-sm-7 text-right">
                {!! btnFilter(route('administrador.usuarios.index')) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="panel panel-default no-margin">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            {{'Listagem'}}
        </div>
        <div class="panel-body no-padding">

            <table class="table table-hover table-striped no-margin">
                <thead>
                    <tr>
                        <th>{{Lang::get('table.usuario')}}</th>
                        <th>{{Lang::get('table.nome')}}</th>
                        <th>{{Lang::get('table.email')}}</th>
                        <th>{{Lang::get('table.grupos_de_acesso')}}</th>
                        <th width="80" class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if(array_key_exists($user->id, $groups))
                                {{ $groups[$user->id] }}
                            @endif
                        </td>

                        <td class="text-center">

                            @if($user->username != config('sys.admin_user'))

                                @can('user_edit')
                                    {!! btnEdit(route('administrador.usuarios.edit', $user->id)) !!}
                                @endcan

                                @can('user_del')
                                    {!! btnDelete(route('administrador.usuarios.destroy', $user->id)) !!}
                                @endcan

                            @endif

                        </td>

                    </tr>
                @empty
                    <tr>
                        <td colspan="5">
                            {{Lang::get('sys.msg.not_found_all')}}
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            {!! $users->appends($request->all())->links() !!}
        </div>
    </div>


@endsection

@push('js')

    @include('utils.messages-js')

    <script>
        $(function() {
            runConfirmDelete();
        });
    </script>

@endpush