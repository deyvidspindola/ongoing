<table>
    <thead>
        <tr>
            <th colspan="3" style="background-color: #17375D;">OFFENSORES PORTABILIDADE</th>
        </tr>
        <tr>
            <th colspan="3" style="font-size: 15px">Atualizado em:</th>
            <th></th>
        </tr>
        <tr><th></th></tr>
    </thead>
    <tbody>
        {{--headers da tabela--}}
        <tr>
            <td style="background-color: #17375D;" width="5%">ID</td>
            <td style="background-color: #17375D;">DT. CAD.</td>
            <td style="background-color: #17375D;">OFENSOR</td>
            <td style="background-color: #17375D;">NIVEL DE RETENÇÃO</td>
            <td style="background-color: #17375D;">VÁLIDO</td>
            <td style="background-color: #17375D;">CLASSIFICAÇÃO</td>
            <td style="background-color: #17375D;">ORIGEM</td>
            <td style="background-color: #17375D;">EXPLICAÇÃO DETALHADA DO OFENSOR</td>
            <td style="background-color: #17375D;">RESPOSTA PARA O USUÁRIO NO ATENDIMENTO DO CHAMADO</td>
            <td style="background-color: #17375D;">ID</td>
            <td style="background-color: #17375D;">ID_NO_PPM</td>
            <td style="background-color: #17375D;">PROJETO_PACOTE</td>
            <td style="background-color: #17375D;">PREVENTIVO_OFENSOR</td>
            <td style="background-color: #17375D;">PREVISAO_DEFINITIVO</td>
            <td style="background-color: #17375D;">NUMERO_PATCH</td>
            <td style="background-color: #17375D;">IDENTIFICACAO_PATCH</td>
            <td style="background-color: #17375D;">DT. CAD. OFENSOR</td>
            <td style="background-color: #17375D;">UNIT - C</td>
            <td style="background-color: #17375D;">ID_OCORRENCIA</td>
            <td style="background-color: #17375D;">ID_PROCEDIMENTO</td>
        </tr>

        {{--listagem dos campos--}}
        @foreach($offenders as $offender)
        <tr>
            <td style="background-color: #FFFF00">{{ $offender->offender_id }}</td>
            <td>{{ $offender->created_at }}</td>
            <td>{{ $offender->offender }}</td>
            <td style="background-color: {{ ($offender->retention_level == 'N1') ? '#FF0000' : '#00B050' }}">{{ $offender->retention_level }}</td>
            <td>{{ $offender->available }}</td>
            <td>{{ $offender->classification }}</td>
            <td>{{ $offender->origin }}</td>
            <td>{{ $offender->detailed_explanation_offender }}</td>
            <td>{{ $offender->answer_for_the_user_in_the_call }}</td>
            <td style="background-color: #FFFF00">{{ $offender->offender_id }}</td>
            <td>{{ $offender->ppm_id }}</td>
            <td>{{ $offender->project_pack }}</td>
            <td>{{ $offender->preventive_offender }}</td>
            <td>{{ $offender->predict_definitive }}</td>
            <td>{{ $offender->num_patch }}</td>
            <td>{{ $offender->patch_identification }}</td>
            <td>{{ $offender->created_at }}</td>
            <td>{{ $offender->unit_c }}</td>
            <td>{{ $offender->ocurrence_id }}</td>
            <td>{{ $offender->procedure_id }}</td>
        </tr>
        @endforeach

    </tbody>
</table>