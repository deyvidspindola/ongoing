<table>
    <thead>
    <tr>
        <th colspan="3" style="background-color: #17375D;">OFFENSORES PORTABILIDADE</th>
    </tr>
    <tr>
        <th colspan="3" style="font-size: 15px">Atualizado em:</th>
        <th></th>
    </tr>
    <tr><th></th></tr>
    </thead>
    <tbody>
    {{--headers da tabela--}}
    <tr>
        <td style="background-color: #17375D;" width="5%">ID</td>
        <td style="background-color: #17375D;">DT. CAD.</td>
        <td style="background-color: #17375D;">OFENSOR</td>
        <td style="background-color: #17375D;">NIVEL DE RETENÇÃO</td>
        <td style="background-color: #17375D;">VÁLIDO</td>
        <td style="background-color: #17375D;">CLASSIFICAÇÃO</td>
        <td style="background-color: #17375D;">ORIGEM</td>
        <td style="background-color: #17375D;">EXPLICAÇÃO</td>
        <td style="background-color: #17375D;">RESPOSTA PARA O USUÁRIO</td>
        <td style="background-color: #17375D;">ID</td>
        <td style="background-color: #17375D;">ID_NO_PPM</td>
        <td style="background-color: #17375D;">PROJETO_PACOTE</td>
        <td style="background-color: #17375D;">PREVENTIVO_OFENSOR</td>
        <td style="background-color: #17375D;">PREVISAO_DEFINITIVO</td>
        <td style="background-color: #17375D;">NUMERO_PATCH</td>
        <td style="background-color: #17375D;">IDENTIFICACAO_PATCH</td>
        <td style="background-color: #17375D;">DT. CAD. OFENSOR</td>
        <td style="background-color: #FFFF00;">OBSERVAÇÃO</td>
    </tr>

    {{--listagem dos campos--}}
    @foreach($offenders as $offender)
        @php
            $obs = $offender->observation2;
            if ($offender->operation_type == 'E'){
                $obs .= ' - Campos alterados '.modified_fields($offender->modified_fields, ['macro_ofensor', 'id']);
            }
        @endphp
        <tr>
            <td style="background-color: #FFFF00">{{ $offender->snapshot['offender_id'] }}</td>
            <td>{{ $offender->snapshot['created_at'] }}</td>
            <td>{{ $offender->snapshot['offender'] }}</td>
            <td style="background-color: {{ ($offender->snapshot['retention_level'] == 'N1') ? '#FF0000' : '#00B050' }}">{{ $offender->snapshot['retention_level'] }}</td>
            <td>{{ (isset($offender->snapshot['available'])) ? $offender->snapshot['available'] : $offender->snapshot['valid'] }}</td>
            <td>{{ $offender->snapshot['classification'] }}</td>
            <td>{{ $offender->snapshot['origin'] }}</td>
            <td>{{ $offender->snapshot['detailed_explanation_offender'] }}</td>
            <td>{{ $offender->snapshot['answer_for_the_user_in_the_call'] }}</td>
            <td style="background-color: #FFFF00">{{ $offender->snapshot['offender_id'] }}</td>
            <td>{{ $offender->snapshot['ppm_id'] }}</td>
            <td>{{ $offender->snapshot['project_pack'] }}</td>
            <td>{{ $offender->snapshot['preventive_offender'] }}</td>
            <td>{{ $offender->snapshot['predict_definitive'] }}</td>
            <td>{{ $offender->snapshot['num_patch'] }}</td>
            <td>{{ $offender->snapshot['patch_identification'] }}</td>
            <td>{{ $offender->snapshot['created_at'] }}</td>
            <td>{{ $obs }}</td>
        </tr>
    @endforeach

    </tbody>
</table>