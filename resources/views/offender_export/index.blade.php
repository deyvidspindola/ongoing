@extends('layouts.app', [
    'breadcrumb' => ['Dashboard', 'Ofensores', 'Exportação de Dados'],
    'page_title' => 'Exportação de Ofensores Externos',
    'page_title_small' => ''
])

@section('styles')
    {{-- Modal --}}
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('page_title_actions')


@endsection

@section('content')

    @include('utils.messages')

    {{--Filtros--}}
    {!! Form::open(['route' => 'offender.export.export', 'id' => 'search-form']) !!}
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>Filtros
            </div>
        </div>
        <div class="portlet-body well well-sm">
            <div class="row">
                <div class="col-sm-4">
                    {!! Form::label('solver_group_filter', 'Grupo Resolvedor') !!}
                    {!! Form::select('solver_group_filter', $userSolverGroups, null, ['class' => 'bs-select form-control']) !!}
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-sm-12 text-right">
                    <button class="btn btn-block red-intense"><i class="fa fa-download"></i> Gerar Extração de Dados</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection


