<?php

return [

    // ------------------------------------------------------------------
    // Menu
    // ------------------------------------------------------------------

    'menu' => [
        'home' => 'Início',
        'administrador' => [
            'administrador' => 'Administrador',
            'grupos'    => 'Grupos de Acesso',
            'usuarios'  => 'Usuários',
            'empresas'  => 'Empresa',
            'menus_units' => 'Menus Uni.T-S'
        ],
        'cadastros'         => 'Cadastros',
        'lojas'             => 'Lojas',
        'loja'              => 'Loja',
        'administradores'   => 'Administradores',
        'operador'          => 'Operador',
        'operadores'        => 'Operadores',
        'clientes'          => 'Clientes',
        'cliente'           => 'Cliente',
        'extrato' => 'Extrato',
        'extratos' => 'Extratos',
        'por_clientes' => 'Por Clientes',
        'por_lojas' => 'Por Lojas',
        'ofensores' => [
            'offender' => 'Ofensores',
            'ofensores' => 'Ofensores',
            'sincronismo' => 'Sincronismo',
            'extracao_dados' => 'Extração de Dados',
            'carga_dados' => 'Carga de Dados',
            'historico' => 'Histórico'
        ],
        'units' => [
            'dashboard' => 'Dashboard',
            'atalhos'   => 'Atalhos',
            'portabilidade' => 'Portabilidade',
            'incidentes' => 'Incidentes',
            'inserir_area_local' => 'Inserir Área Local',
            'abrir_solicitacao' => 'Abrir Solicitação',
            'mudanca_endereco' => 'Mudança de Endereço',
            'retirada_ponto' => 'Retirada de Pto. por opção',
            'status_contrato' => 'Status Contrato',
            'bilhete_portabilidade' => 'Bilhete Portabilidade',
            'portabilidade_intrinseca' => 'Portabilidade Intrínseca',
            'portabilidade_interna' => 'Portabilidade Interna',
            'relatorio_analitico' => 'Relatório Analítico - Atendimento UNITS',
            'portabilidade_ctv' => 'Portabilidade CTV',
            'solicitacoes' => 'Solicitações',
            'incluir_area_local_ctv' => 'Incluir Área Local',
            'troca_fqdn' => 'Troca FQDN',
            'net_now' => 'Net Now',
            'relatorios' => 'Relatórios',
            'compras_vod' => 'Compras VOD',
            'seguranca' => 'Segurança',
            'usuarios_logados' => 'Usuários logados',
            'log_atividades' => 'Lista Log Atividades'
        ]
    ],

    // ------------------------------------------------------------------
    // Autenticação
    // ------------------------------------------------------------------

    'auth' => [

        'title'     => 'Faça login em sua conta',
        'subtitle'  => 'Digite seu usuário e senha para efetuar login.',
        'username'  => 'usuário@domínio.com.br',
        'password'  => 'Senha',
        'login'     => 'Acessar',
        'company'   => 'Empresa não informada.',

    ],

    // ------------------------------------------------------------------
    // Header
    // ------------------------------------------------------------------

    'header' => [

        'logout' => 'Sair',
        'company' => 'Empresa',
        'user'    => 'Usuário',
        'change_password' => 'Alterar senha',

    ],

    // ------------------------------------------------------------------
    // Geral
    // ------------------------------------------------------------------

    'selecione' => 'Selecione',
    'erro' => 'Erro',
    'visualizar' => 'Visualizar',
    'fechar' => 'Fechar',

    // ------------------------------------------------------------------
    // Messages
    // ------------------------------------------------------------------

    'msg' => [

        'alert-error'   => 'ERRO',
        'alert-info'    => 'INFORMAÇÃO',
        'alert-success' => 'SUCESSO',
        'alert-warning' => 'ATENÇÃO',

        'permission_denied' => 'Sem permissão para executar essa ação.',

        'not_found' => 'Registro não encontrado.',

        'not_found_all' => 'Nenhum registro encontrado.',

        'success' => [
            'save'   => 'Registro cadastrado com sucesso.',
            'update' => 'Registro atualizado com sucesso.',
            'delete' => 'Registro excluído com sucesso.',

            'update_password' => 'Senha alterada com sucesso.',
        ],

        'error' => [
            'save'   => 'Não foi possível salvar as informações.',
            'update' => 'Não foi possível atualizar as informações.',
            'delete' => 'Não foi possível excluir as informações.',
        ],

        'extrato' => [

            'nenhuma_movimentacao_encontrada' => 'Nenhuma movimentação encontrada.',
            'selecione_um_cliente' => 'Selecione um cliente.',
            'selecione_uma_loja' => 'Selecione uma Loja.',
        ],

    ],

    // ------------------------------------------------------------------
    // Buttons
    // ------------------------------------------------------------------

    'btn' => [

        'new'    => 'Novo',
        'filter' => 'Filtrar',
        'clean'  => 'Limpar',
        'edit'   => 'Editar',
        'delete' => 'Deletar',
        'cancel' => 'Cancelar',
        'save'   => 'Salvar',
        'Anterior' => 'Anterior',
        'Proximo'  => 'Próximo',
        'treat' => 'Tratar',
        'clean_data' => 'Limpar Dados',
        'continue' => 'Continuar'
    ],

    // ------------------------------------------------------------------
    // Alerts
    // ------------------------------------------------------------------

    'alert' => [

        'delete' => [

            'sa_title'              => 'Tem certeza que deseja excluir este registro?',
            'sa_message'            => 'Essa operação não poderá ser desfeita.',
            'sa_confirmButtonText'  => 'Confirmar',
            'sa_cancelButtonText'   => 'Cancelar',
            'sa_popupTitleCancel'   => 'Operação cancelada com sucesso.',

        ],

        'cancel' => [

            'sa_title'              => 'Tem certeza que deseja cancelar?',
            'sa_message'            => 'O registro não será salvo.',
            'sa_message_edit'       => 'As alterações feitas não serão salvas.',
            'sa_confirmButtonText'  => 'Sim',
            'sa_cancelButtonText'   => 'Não',
            'sa_popupTitleCancel'   => 'Operação cancelada com sucesso.',

        ],

        'fixed_site_shortcuts' => [
            'site A'    => 'www.terra.com.br',
            'Artit'     => 'www.artit.com.br'
        ]
    ],

    // ------------------------------------------------------------------
    // GERAL
    // ------------------------------------------------------------------


];