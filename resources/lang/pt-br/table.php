<?php

return [

    //-- ---------------------------------------------------------
    //-- GERAL
    //-- ---------------------------------------------------------

    'all_m'     => 'Todos',
    'all_f'     => 'Todas',
    'listagem'  => 'Listagem',
    'acoes'     => 'Ações',
    'acao'      => 'Ação',
    'novo_m'    => 'Novo',
    'novo_f'    => 'Nova',
    'editar'    => 'Editar',
    'filtrar'   => 'Filtrar',
    'exportar'  => 'Exportar',
    'cancelar'  => '<u>C</u>ancelar',
    'finalizar' => 'F<u>i</u>nalizar',

    //-- ---------------------------------------------------------
    //-- DASHBOARD
    //-- ---------------------------------------------------------

    'total_transacoes' => 'Total de Transações',
    'total_clientes' => 'Total de Clientes',
    'total_resgate' => 'Saldo total para Resgate',
    'ranking_cliente' => 'Transações por Cliente',
    'ranking_loja' => 'Transações por Loja',
    'ranking_saldo_cliente' => 'Saldo por Cliente',
    'quantidade' => 'Quantidade',
    'visualizar_extrato' => 'Visualizar extrato',

    //-- ---------------------------------------------------------
    //-- APLICAÇÕES
    //-- ---------------------------------------------------------

    // Selecionar Loja do Operador
    'selecione_loja' => 'Selecione a Loja',
    'acessar_aplicacao' => 'Acessar aplicação',

    // Operações
    // -----------------------------------------------------------
    'gerar_venda' => 'Gerar Venda',
    'transacao_efetuada_successo' => 'Transação efetuada com sucesso!',
    'gerar_debito' => 'Gerar Débito',
    'gerar_credito' => 'Gerar Crédito',
    'valor_debito' => 'Valor do Débito',
    'numero_serie' => 'N° Série',
    'valor_ultrapassa_saldo_atual' => 'O Valor do débito ultrapassa o saldo atual.',
    'valor_ultrapassa_saldo_atual_cliente' => 'O Valor do débito ultrapassa o saldo atual do cliente.',
    'cliente_nao_encontrado' => 'Cliente não encontrado.',
    'nota_fiscal_ja_cadastrado' => 'Nº NFC já se encontra cadastrado.',
    'numero_serie_ja_cadastrado' => 'Número de Série já se encontra cadastrado.',
    'nfe_invalida' => 'Operação não realizada. N° NFC e Série não são válidos.',
    'nfe_nao_localizada' => 'Não foi possível localizar a NFC.',

    // Extratos
    // -----------------------------------------------------------

    // Cliente
    'periodo' => 'Período',
    'operacao' => 'Operação',
    'numero_nota_fiscal' => 'Número Nota Fiscal',
    'numero_nf_ou_serie' => 'NFC ou Número de Série',
    'nota_fiscal' => 'Nota Fiscal',
    'nota_fiscal_consumidor' => 'Nota Fiscal do Consumidor',
    'credito' => 'Crédito',
    'debito' => 'Débito',
    'valor' => 'Valor',
    'name_holder' => 'Nome Impresso no Cartão',
    'card_number' => 'Número do Cartão',
    'expiration_date' => 'Validade',
    'security_code' => 'Código de Segurança',
    'installments' => 'Parcelas',
    'type_payment' => 'Tipo de Pagamento?',
    'credit_card' => 'Cartão de Crédito',
    'debit_card' => 'Cartão de Débito',
    'saldo' => 'Saldo',
    'saldo_anterior' => 'Saldo Anterior',
    'saldo_atual' => 'Saldo Atual',
    'data_hora' => 'Data / Hora',
    'operador' => 'Operador',
    'senha_cliente' => 'Senha do Cliente',
    'senha_cliente_incorreta' => 'Operação não realizada. A senha do cliente está incorreta.',

    // Cadastros - Lojas
    // -----------------------------------------------------------

    'loja' => 'Loja',

        // Mensagens
        'cnpj_cadastrado' => 'Esse CNPJ já se encontra cadastrado.',
        'loja_cadastrada' => 'Esse nome para Loja já se encontra cadastrado.',
        'erro_excluir_loja_operador_cadastrado' => 'Erro ao excluir loja, existem operadores vinculados à ela.',

    // Cadastros - Operadores
    // -----------------------------------------------------------

    'lojas' => 'Lojas',
    'lojas_vinculadas' => 'Lojas Vinculadas',

        // Mensagens
        'cadastrar_loja_antes_de_operador' => 'Antes de cadastrar operadores, é necessário cadastrar as Lojas.',
        'vincule_em_uma_loja' => 'Vincule o operador em pelo menos uma Loja.',


    // Cadastros - Clientes
    // -----------------------------------------------------------

        // Mensagens
        'cpfj_cnpj_invalido' => 'CPF / CNPJ inválido.',
        'cnpj_invalido' => 'CNPJ inválido.',


    // Administrador - Usuários
    // -----------------------------------------------------------

    'administrador'      => 'Administrador',
    'usuarios'           => 'Usuários',

    'usuario'            => 'Usuário',
    'nome'               => 'Nome',
    'email'              => 'E-mail',
    'senha'              => 'Senha',
    'grupos_de_acesso'   => 'Grupos de acesso',

        // Mensagens
        'usuario_cadastrado' => 'Usuário já cadastrado.',
        'info_senha' => 'Caso não deseje alterar a senha, basta deixar esse campo sem preencher.',


    // Administrador - Grupos
    // -----------------------------------------------------------

    'grupos_acesso' => 'Grupos de Acesso',

    'grupo' => 'Grupo',
    'descricao' => 'Descrição',
    'quant' => 'QUANT.',
    'nome_grupo' => 'Nome do Grupo (Informe um nome único)',
    'permissoes' => 'Permissões',
    'permissoes_selecionadas' => 'Permissões Selecionadas',
    'usuarios_selecionados' => 'Usuários Selecionados',
    'grupos_resolvedores' => 'Grupos Resolvedores',
    'grupo_resolvedor_listar_interno' => 'Listar Interno',
    'grupo_resolvedor_listar_externo' => 'Listar Externo',
    'grupo_resolvedor_criar' => 'Criar/Editar',

        // Mensagens
        'grupo_ja_existe' => 'O grupo selecionado já existe.',
        'usuarios_vinculados' => 'Existem usuários vinculados a este grupo.',
        'permissoes_vinculadas' => 'Existem permissões vinculadas a este grupo.',


    // Administrador - Empresas
    // -----------------------------------------------------------

    'empresa' => 'Empresa',

    'dominio_para_auth' => 'Domínio para autenticação',
    'usuario_empresa_auth' => 'usuario',
    'nome_empresa' => 'Nome da empresa',
    'dominio' => 'Domínio',
    'dominio_info' => 'Caso o sistema seja multiempresa, este domínio será usado para autenticação dos usuários, por exemplo: (usuario@dominio_empresa).',
    'parametros' => 'Parâmetros',
    'ad' => 'Active Directory',
    'auth_ldap' => 'Autenticação pelo LDAP?',
    'auth_ldap_info' => 'Caso não seja autenticação pelo LDAP, então será feita local (DB).',
    'sim_up' => 'SIM',
    'nao_up' => 'NÃO',
    'conexao' => 'Conexão',
    'porta' => 'Porta',
    'dn_usuarios' => 'DN Usuários',
    'dn_grupos' => 'DN Grupos',
    'campos' => 'Campos',
    'testar_conexao' => 'Testar Conexão',

        // Mensagens
        'dominio_ja_cadastrado' => 'Domínio já cadastrado para outra empresa.',
        'empresa_nao_encontrada' => 'Empresa não encontrada.',
        'autenticado_empresa_para_exclusao' => 'Você está autenticado na empresa que deseja excluir.',
        'usuarios_vinculados_empresa' => 'Existem usuários vinculados a esta empresa.',
        'grupos_vinculados_empresa' => 'Existem grupos de acesso vinculados a esta empresa.',

        'preencher_informacoes_ad' => 'Preencha todas as informações de a conexão do Active Directory.',
        'nao_foi_possivel_conexao_ad' => 'Não foi possível efetuar conexão com o Active Directory.',
        'revalide_informacao_ad' => 'Revalide as informações ou selecione para a autenticação ser local.',

        'conexao_efetuada_ad' => 'Conexão efetuada com o Active Directory realizada com sucesso.',
        'conexao_erro_ad' => 'Não foi possível efetuar conexão.',


    // ALTERAR SENHA
    'Perfil' => 'Perfil',
    'senha_atual' => 'Senha atual',
    'nova_senha' => 'Nova senha',
    'confirme_senha' => 'Confirmar senha',
    'as_senhas_nao_correspondem' => 'As senhas não correspondem.',
    'senha_atual_invalida' => 'Senha atual inválida.',

    // meses abreviado
    'JAN' => 'JAN',
    'FEV' => 'FEV',
    'MAR' => 'MAR',
    'ABR' => 'ABR',
    'MAI' => 'MAI',
    'JUN' => 'JUN',
    'JUL' => 'JUL',
    'AGO' => 'AGO',
    'SET' => 'SET',
    'OUT' => 'OUT',
    'NOV' => 'NOV',
    'DEZ' => 'DEZ',


];