<?php

return [ 

  // Botões link das telas do units
  'botoes_link' => [
    'nome' => [
      'erro_inesperado' => 'Erro Inesperado',
      'portabilidade_andamento' => 'Portabilidade em Andamento',
      'mudanca_endereco' => 'Mudança de Endereço',
      'retirada_ponto' => 'Retirada de Pto. / Desc. Opção',
      'correcao_status_contrato' => 'Correção Status Contrato',
      'nao_possui' => 'Não Possui',
      'troca_numero' => 'Troca de Número',
      'port_intrinseca_reenvio_hit' => 'Reenvio de HIT',
      'port_intrinseca_insere_area_local' => 'Insere Área Local',
      'port_intrinseca_excluir_area_local' => 'Excluir Área Local',
      'port_intrinseca_envia_ativacao' => 'Envia Ativação',
      'port_intrinseca_historico_portabilidade' => 'Histórico Portabilidade',
      'port_interna_incluir_area_local' => 'Incluir Área Local',
      'port_interna_excluir_area_local' => 'Excluir Área Local',
      'port_interna_reenviar_hit' => 'Reenviar HIT',
      'incluir_area_local_ctv' => 'Incluir Área Local CTV',
      'troca_fqdn' => 'Troca FQDN'
    ],
    'tooltip' => [
      'erro_inesperado' => 'Ao validar em área local ocorre o erro inesperado',
      'portabilidade_andamento' => 'Ao validar em área local ocorre a portabilidade deste número em andamento',
      'mudanca_endereco' => 'Contrato com processo de portabilidade em andamento! Portanto, não pode abrir a solicitação de "MUDANÇA DE ENDEREÇO - RETIRAR PONTOS"',
      'retirada_ponto' => 'Contrato com processo de portabilidade em andamento! Portanto, não pode abrir a solicitação de "RETIRAR PONTO NETFONE"',
      'correcao_status_contrato' => 'Solicitação de adesão executada e contrato permaneceu com status pendente em instalação',
      'nao_possui' => 'Solicitar alteração do status do contrato para não possui',
      'troca_numero' => 'Troca de número aberta e não gerou histórico de portabilidade',
      'port_intrinseca_reenvio_hit' => 'Nesta opção o comando de desabilitar número será reenviado',
      'port_intrinseca_insere_area_local' => 'Nesta opção o telefone será incluso em área local',
      'port_intrinseca_excluir_area_local' => 'Nesta opção o telefone será removido da área local',
      'port_intrinseca_envia_ativacao' => 'Nesta opção será enviada a ativação',
      'port_intrinseca_historico_portabilidade' => 'Nesta opção o bilhete intrínseco será atualizado na base NET',
      'port_interna_incluir_area_local' => 'Incluir área local de Portabilidade Interna',
      'port_interna_excluir_area_local' => 'Excluir área local de Portabilidade Interna',
      'port_interna_reenviar_hit' => 'Reenviar comando de troca de número',
      'incluir_area_local_ctv' => 'Erro no NerSales: Portabilidade Interna já Cadastrada ou Solicitada',
      'troca_fqdn' => 'Troca de FQDN'
    ],
    'modal_title' => [
      'erro_inesperado' => 'Erro Inesperado',
      'portabilidade_andamento' => 'Portabilidade em Andamento',
      'mudanca_endereco' => 'Mudança de Endereço',
      'retirada_ponto' => 'Retirada de Pto. / Desc. Opção',
      'correcao_status_contrato' => 'Correção Status Contrato',
      'nao_possui' => 'Não Possui',
      'troca_numero' => 'Troca de Número',
      'port_intrinseca_reenvio_hit' => 'Reenvio de HIT',
      'port_intrinseca_insere_area_local' => 'Insere Área Local',
      'port_intrinseca_excluir_area_local' => 'Excluir Área Local',
      'port_intrinseca_envia_ativacao' => 'Envia Ativação',
      'port_intrinseca_historico_portabilidade' => 'Histórico Portabilidade',
      'port_interna_incluir_area_local' => 'Incluir Área Local',
      'port_interna_excluir_area_local' => 'Excluir Área Local',
      'port_interna_reenviar_hit' => 'Reenviar HIT',
      'incluir_area_local_ctv' => 'Incluir Área Local CTV',
      'troca_fqdn' => 'Troca FQDN'

    ],
    'modal_description' => [
      'erro_inesperado' => 'Esta opção deve ser utilizada somente para telefones que estão em outra operadora, e apresentam a mensagem de ERRO INESPERADO, ou já possuem validação em área local para outra proposta/contrato e não é possível realizar a remoção na ferramenta do NETSALES.',
      'portabilidade_andamento' => 'Esta opção deve ser utilizada somente para telefones que estão em outra operadora e apresentam a mensagem de PORTABILIDADE DESTE NÚMERO EM ANDAMENTO.',
      'mudanca_endereco' => 'Nesta opção é possível solicitar a alteração do status da portabilidade de desconexão pendente para ativo para prosseguir com a solicitação de mudança de endereço para o Contrato.',
      'retirada_ponto' => 'Esta opção deve ser utilizada somente para telefones portados que estão em outra operadora e o status da portabilidade esta como Cancelado.',
      'correcao_status_contrato' => 'Contrato possui solicitação de adesão executada e o status do contrato permaneceu pend. em instalação mesmo sem outras solicitações de adesão pendentes de baixa no contrato.',
      'nao_possui' => 'Esta opção deve ser utilizada para solicitar alteração do status do contrato para não possui. Onde o status do contrato estiver com status "Desconexão por opção/Inadimplência".',
      'troca_numero' => 'Esta opção deve ser utilizada somente para telefones que estão em outra operadora. O bilhete de portabilidade será gerado somente se a troca de número estiver aberta e a solicitação será executada somente após a janela de migração.',
      'port_intrinseca_reenvio_hit' => 'Esta opção deve ser utilizada para solicitar o reenvio do comando de desabilitar número com a flag S de portado para que sua OC possa ser executada.',
      'port_intrinseca_insere_area_local' => 'Esta opção deve ser utilizada para os casos de portabilidade intrínseca, onde o terminal já consta reservado no SNR “Embratel”. Sendo necessário inserir o terminal em área local para finalizar a venda.',
      'port_intrinseca_excluir_area_local' => 'Esta opção deve ser utilizada para efetuar a limpeza do número de área local, somente após o término da criação da solicitação de adesão.',
      'port_intrinseca_envia_ativacao' => 'Esta opção deve ser utilizada para os casos de portabilidade intrínseca, onde o terminal da cidade origem da portabilidade intrínseca consta com status “DESCONEXÃO PENDENTE” sendo necessário ativar o mesmo para que seja gerado o bilhete de portabilidade para cidade destino.',
      'port_intrinseca_historico_portabilidade' => 'Esta opção deve ser utilizada para atualizar o histórico de portabilidade na base NET, somente após a ativação do bilhete intrínseco junto a Embratel.',
      'port_interna_incluir_area_local' => 'Esta opção deve ser utilizada para realizar a inclusão de área local para o cenário de portabilidade interna.',
      'port_interna_excluir_area_local' => 'Esta opção deve ser utilizada para realizar a exclusão de área local para o cenário de portabilidade interna.',
      'port_interna_reenviar_hit' => 'Esta opção deve ser utilizada para realizar o reenvio do comando de troca de número do cenário de portabilidade interna.',
      'incluir_area_local_ctv' => 'Esta opção deve ser utilizada para realizar a inclusão de área local somente para o cenário em que ocorrer o erro no Netsales: Portabilidade Interna já Cadastrada ou Portabilidade Interna já Solicitada.',
      'troca_fqdn' => 'Esta opção deve ser utilizada somente para telefones, onde há necessidade de alterar o FQDN e PORTA para telefones portados e netfones. O(s) terminal(s) deve esta ativo, para que se possível realizar a troca com sucesso.'


    ]
  ]

];