<?php

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();
//, 'auth.unique.user'
Route::middleware(['auth', 'translate'])->group(function () {
//Route::middleware(['translate'])->group(function () {

	#Dashboard
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/read_notification/{id}', 'HomeController@read_notification')->name('read_notification');


	Route::group(['prefix' => 'perfil', 'as' => 'perfil.', 'namespace' => 'Perfil'], function () {

		Route::group(['prefix' => 'alterar_senha', 'as' => 'alterar_senha.'], function () {
			Route::get('', ['as' => 'index', 'uses' => 'AlterarSenhaController@index']);
			Route::post('update', ['as' => 'update', 'uses' => 'AlterarSenhaController@update']);
		});

	});

	#Administrador
	Route::namespace('Administrador')->prefix('administrador')->name('administrador.')->group(function (){

		#Empresa
		Route::group(['prefix' => 'empresa', 'as' => 'empresa.'], function () {
			Route::get('', ['as' => 'index', 'uses' => 'CompanyController@index']);
			Route::get('novo', ['as' => 'create', 'uses' => 'CompanyController@create']);
			Route::post('store', ['as' => 'store', 'uses' => 'CompanyController@store']);
			Route::get('editar/{id}', ['as' => 'edit', 'uses' => 'CompanyController@edit']);
			Route::post('update/{id}', ['as' => 'update', 'uses' => 'CompanyController@update']);
			Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'CompanyController@destroy']);
			Route::post('test_connection', ['as' => 'testConnection', 'uses' => 'CompanyController@testConnection']);
		});

		#Usuários
		Route::group(['prefix' => 'usuarios', 'as' => 'usuarios.'], function () {
			Route::get('', ['as' => 'index', 'uses' => 'UsersController@index']);
			Route::get('novo', ['as' => 'create', 'uses' => 'UsersController@create']);
			Route::post('store', ['as' => 'store', 'uses' => 'UsersController@store']);
			Route::get('editar/{id}', ['as' => 'edit', 'uses' => 'UsersController@edit']);
			Route::post('update/{id}', ['as' => 'update', 'uses' => 'UsersController@update']);
			Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'UsersController@destroy']);
		});

		#Grupos
		Route::group(['prefix' => 'grupos', 'as' => 'grupos.'], function () {
			Route::get('', ['as' => 'index', 'uses' => 'GroupsController@index']);
			Route::get('novo', ['as' => 'create', 'uses' => 'GroupsController@create']);
			Route::post('store', ['as' => 'store', 'uses' => 'GroupsController@store']);
			Route::get('editar/{id}', ['as' => 'edit', 'uses' => 'GroupsController@edit']);
			Route::post('update/{id}', ['as' => 'update', 'uses' => 'GroupsController@update']);
			Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'GroupsController@destroy']);
		});




		#Menus UNI.T-S
		Route::prefix('menus-units')->name('menus-units.')->group(function (){
			Route::get('', 'MenuUnitsController@index')->name('index');
			Route::get('show/{id}', 'MenuUnitsController@show')->name('show');
			Route::post('create', 'MenuUnitsController@create')->name('create');
			Route::post('update', 'MenuUnitsController@update')->name('update');
			Route::post('update-order', 'MenuUnitsController@updateOrder')->name('update-order');
		});

	});

	#Ofensores
	Route::namespace('Offender')->group(function () {

		#Cadastros de Ofensores
		Route::resource('offender', 'OffenderController')->except('show');

		Route::prefix('offender')->name('offender.')->group(function (){

			#Detalhes do ofensor para o modal
			Route::get('show-details/{offender}', 'OffenderController@show_details')->name('show-details');

			#Auto completes dos filtros
			Route::get('search', 'OffenderController@search')->name('search');

			#Datatable
			Route::get('datatable', 'OffenderController@datatable')->name('datatable');

			#Download do arquivo
			Route::get('download/{file_id}', 'OffenderController@download')->name('download');

			#Remove File
			Route::get('remove-file/{file_id}', 'OffenderController@remove_file')->name('remove_file');

			#check offender id exists
			Route::get('offender-id-exists/{offender_id}', 'OffenderController@offender_id_exists')->name('offender_id_exists');

            #ação
            Route::get('action/{offender_id}', 'OffenderController@action')->name('action');
			/* ========================================================================================== */
			#Exportação
			Route::prefix('export')->name('export.')->group(function (){
				Route::get('', 'OffenderExportController@index')->name('index');
				Route::post('', 'OffenderExportController@export')->name('export');
			});

			/* ========================================================================================== */
			#Importação
			Route::prefix('import')->name('import.')->group(function (){
				Route::get('', 'OffenderImportController@index')->name('index');
				Route::get('log-download/{id}', 'OffenderImportController@log_download')->name('log-download');
				Route::post('', 'OffenderImportController@import')->name('import');
			});

			/* ========================================================================================== */
			#Notificações
			Route::get('notifications', 'OffenderController@notifications')->name('notifications');

			/* ========================================================================================== */
			#Historico
			Route::resource('history', 'OffenderHistoryController')->only(['index']);
			Route::prefix('history')->name('history.')->group(function (){

				#Detalhes do ofensor para o modal
				Route::get('show-details/{offender}', 'OffenderHistoryController@show_details')->name('show-details');

				#Datatable
				Route::get('datatable', 'OffenderHistoryController@datatable')->name('datatable');

				#Auto completes dos filtros
				Route::get('search', 'OffenderHistoryController@search')->name('search');

				#Exporta Busca do filtro
				Route::post('export', 'OffenderHistoryController@export')->name('export');
			});

			/* ========================================================================================== */
			#Sincronismo
			Route::resource('synchronism', 'OffenderSynchronismController')->except('show');
			Route::prefix('synchronism')->name('synchronism.')->group(function (){
				#Detalhes do ofensor para o modal
				Route::get('show-details/{offender}/{column?}', 'OffenderSynchronismController@show_details')->name('show-details');

				#Datatable
				Route::get('datatable', 'OffenderSynchronismController@datatable')->name('datatable');

				#Auto completes dos filtros
				Route::get('search', 'OffenderSynchronismController@search')->name('search');

				#getRow
				Route::get('get-row', 'OffenderSynchronismController@get_row')->name('get-row');
			});

		});

	});

	#Ferramentas
	Route::namespace('Session')->prefix('session')->name('session.')->group(function (){

		#Troca de empresa
		Route::any('company/choice', 'SessionController@companyChoice')->name('company.choice');

		#Trocar Lingua da aplicação
		Route::post('language/choice', 'SessionController@languageChoice')->name('language.choice');

	});

	# UNITS
	Route::namespace('Units')->group(function ()
	{
	
		Route::prefix('units')->name('units.')->group(function (){

			# DASHBOARD
			Route::prefix('dashboard')->name('dashboard.')->group(function (){
				Route::any('', 'UnitsDashboardController@index')->name('index');
				//Route::post('/charts', 'UnitsDashboardController@charts')->name('charts'); //teste
			});

			##Serviços
			Route::prefix('solicitacoes')->name('solicitacoes.')->group(function (){
				Route::get('index/{slug}', 'UnitsController@index')->where('slug', '([A-Za-z0-9\-\/]+)');
				Route::get('form/{slug}/{id}/{action?}', 'UnitsController@form')->where('slug', '([A-Za-z0-9\-\/]+)');
				Route::post('store', 'UnitsController@store')->name('store')->where('slug', '([A-Za-z0-9\-\/]+)');
			});

            # Segurança
            Route::prefix('seguranca')->name('security.')->group(function (){
                Route::get('usuarios_logados', 'UnitsLoggedUsersController@index')->name('logged_users.index');
                Route::post('logout', 'UnitsLoggedUsersController@logout')->name('logged_users.logout');
            });

            # Lista log atividades
            Route::get('log-atividades', 'UnitsActivitiesLogController@index')->name('activities_log.index');
            Route::get('log-atividades/datatable', 'UnitsActivitiesLogController@datatable')->name('activities_log.datatable');
            Route::get('log-atividades/search', 'UnitsActivitiesLogController@search')->name('activities_log.search');
            Route::get('log-atividades/show-details/{log_active_id}', 'UnitsActivitiesLogController@show_details')->name('activities_log.show-details');


            # Relatórios
            Route::prefix('relatorios')->name('relatorios.')->group(function (){
                Route::get('consulta-plataforma', 'UnitsReportController@index')->name('consulta-plataforma.index');
                Route::post('consulta-plataforma', 'UnitsReportController@search')->name('consulta-plataforma.search');

                #Relatório Analitico - Atendimentos UNI.T-S
                Route::get('atendimentos-units', 'Reports\AnalyticalAttendanceController@index')->name('atendimentos-units');
                Route::get('atendimentos-units/datatable', 'Reports\AnalyticalAttendanceController@datatable')->name('atendimentos-units.datatable');
                Route::post('atendimentos-units/export', 'Reports\AnalyticalAttendanceController@export')->name('atendimentos-units.export');
                Route::get('atendimentos-units/teste', 'Reports\AnalyticalAttendanceController@teste')->name('atendimentos-units.teste');

                #Compras VOD
                Route::get('compras_vod', 'Reports\UnitsVodShoppingController@comprasVod')->name('compras_vod');
                Route::get('compras_vod/datatable', 'Reports\UnitsVodShoppingController@datatable')->name('compras_vod.datatable');
                Route::post('compras_vod/export', 'Reports\UnitsVodShoppingController@export')->name('compras_vod.export');

            });


            # Busca de dados na base da NET
            Route::post('/net/buscar_assinante', 'UnitsNetController@getCustomer')->name('net.get_customer');
            Route::post('/net/buscar_terminal', 'UnitsNetController@getTerminal')->name('net.get_terminal');
            Route::post('/net/realizaMudança', 'UnitsNetController@change')->name('net.change');

		});
		
	});

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
