<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            DB::table('permissions')->insert([
                ['name' => 'units_servico_erro_inesperado', 'description' => 'Uni.t-S / Portabilidade / Inserir Área Local / Erro Inesperado'],
                ['name' => 'units_servico_portabilidade_em_andamento', 'description' => 'Uni.t-S / Portabilidade / Inserir Área Local / Portabilidade em Andamento'],
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}
