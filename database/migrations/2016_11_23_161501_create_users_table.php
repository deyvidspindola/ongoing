<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->string('name');
            $table->string('username');
            $table->string('password');
            $table->text('password_cache')->nullable();
            $table->string('email')->nullable();
            $table->string('phone', 11)->nullable();
            $table->string('fingerprint')->nullable();
            $table->string('locale',10)->default('pt-br');
            $table->string('token_access')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
