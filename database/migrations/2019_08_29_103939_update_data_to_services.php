<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataToServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units_services', function (Blueprint $table) {
            DB::table('units_services')->where('script_file', 'insere_area_local.sql')->update([
                'slug' => 'portabilidade-intrinseca'
            ]);

            DB::table('units_services')->where('script_file', 'OFENSOR348.sql')->update([
                'slug' => 'inserir-local'
            ]);
            DB::table('units_services')->where('script_file', 'OFENSOR299.sql')->update([
                'slug' => 'inserir-local'
            ]);


            DB::table('units_services')->where('permission_name', 'units_servico_renevio_hit')->update([
                'permission_name' => 'units_servico_pintri_renevio_hit'
            ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            //
        });
    }
}
