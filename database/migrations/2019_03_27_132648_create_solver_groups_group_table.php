<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolverGroupsGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solver_groups_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->integer('solver_group_id')->unsigned();

            $table->index('group_id');
            $table->index('solver_group_id');

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('solver_group_id')->references('id')->on('solver_groups');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solver_groups_group');
    }
}
