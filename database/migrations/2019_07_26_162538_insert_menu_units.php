<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertMenuUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Dashboard', 'dashboard', 'units/dashboard/', 'units_dashboard', null, null, null);");
//        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Atalhos', 'Atalhos', 'units.atalhos', ' ', null, null, null);");
        //Portabilidade
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Portabilidade', 'Solicitações', 'javascript:void(0);', 'units_portabilidade', null, null, null);");
        //Incidentes
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Incidentes', 'Solicitações', 'javascript:void(0);', 'units_incidentes', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Inserir Local', 'Solicitações', 'units/solicitacoes/index/portabilidade/incidentes/inserir-local', 'units_inserir_area_local', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Abrir Solicitação', 'Solicitações', 'javascript:void(0);', 'units_abrir_solicitacao', null, null, null);");
        //Abrir Solicitação
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Mudança de Endereço', 'Solicitações', 'units/solicitacoes/index/portabilidade/incidentes/abrir-solicitacao/mudanca-endereco', 'units_mudanca_de_endereco', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Retirada de Pto. por opção', 'Solicitações', 'units/solicitacoes/index/portabilidade/incidentes/abrir-solicitacao/retirada-ponto-opcao', 'units_retirada_pto_opcao', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Status Contrato', 'Solicitações', 'units/solicitacoes/index/portabilidade/incidentes/abrir-solicitacao/status-contrato', 'units_status_contrato', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Bilhete Portabilidade', 'Solicitações', 'units/solicitacoes/index/portabilidade/incidentes/abrir-solicitacao/bilhete-portabilidade', 'units_bilhete_portabilidade', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Portabilidade Intrínseca', 'Solicitações', 'units/solicitacoes/index/portabilidade/portabilidade-intrinseca', 'units_portabilidade_intrinseca', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Portabilidade Interna', 'Solicitações', 'units/solicitacoes/index/portabilidade/portabilidade-interna', 'units_portabilidade_interna', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Relatório Analítico - Atendimento UNITS', 'Solicitações', 'javascript:void(0);', 'units_relatorio_analitico', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Portabilidade CTV', 'Solicitações', 'javascript:void(0);', 'units_portabilidade_ctv', null, null, null);");
        //Portabilidade CTV
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Solicitações', 'Solicitações', 'javascript:void(0);', 'units_solicitacoes', null, null, null);");
        //Solicitações
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Incluir Área Local', 'Solicitações', 'units/solicitacoes/index/portabilidade-ctv/solicitacoes/incluir-area-local-ctv', 'units_incluir_area_local', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Troca FQDN', 'Solicitações', 'units/solicitacoes/index/troca-fqdn', 'units_troca_fqdn', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('NET NOW', 'Solicitações', 'javascript:void(0);', 'units_net_now', null, null, null);");
        //NET NOW
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Relátorios', 'Relatório - Compras VOD', 'javascript:void(0);', 'units_relatorios', null, null, null);");
        //Relatórios
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Compras VOD', 'Relatório - Compras VOD', 'javascript:void(0);', 'units_compras_vod', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Segurança', '', 'javascript:void(0);', 'units_seguranca', null, null, null);");
        //Segurança
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Usuários Logados', 'Segurança', 'units/seguranca/usuarios_logados', 'units_usuarios_logados', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Lista Log Atividades', 'Logs', 'javascript:void(0);', 'units_lista_log_atividades', null, null, null);");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::select("TRUNCATE units_menus");
    }
}
