<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCompaniyToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    DB::table('companies')->insert([
		    'name' => 'art_it',
		    'description' => 'ART IT Intelligent Technology',
		    'domain' => 'artit.com.br',
		    'created_at' => Carbon::now(),
	    ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::select("TRUNCATE companies");
    }
}
