<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUnitsServiceIdTypeOnUnitsSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('units_solicitations', function (Blueprint $table) {
            $table->bigInteger('units_service_id')->change();
            $table->unsignedBigInteger('units_service_id')->change();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units_solicitations', function (Blueprint $table) {
            $table->integer('units_service_id')->unsigned()->change();
        });
    }
}
