<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertUrlsOnUnitsMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Relatorio Analitico
        DB::table('units_menus')->where('id', 12)->update(['slug' => 'units/relatorios/atendimentos-units']);

        //Compras VOD
        DB::table('units_menus')->where('id', 19)->update(['slug' => 'units/relatorios/compras_vod']);

        //Lista Log Atividades
        DB::table('units_menus')->where('id', 22)->update(['slug' => 'units/log-atividades']);

        //Consulta Plataforma
        DB::table('units_menus')->where('id', 24)->update(['slug' => 'units/relatorios/consulta-plataforma']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Relatorio Analitico
        DB::table('units_menus')->where('id', 12)->update(['slug' => 'units/relatorios/atendimentos-units']);

        //Compras VOD
        DB::table('units_menus')->where('id', 19)->update(['slug' => 'units/relatorios/compras_vod']);

        //Lista Log Atividades
        DB::table('units_menus')->where('id', 22)->update(['slug' => 'units/log-atividades']);

        //Consulta Plataforma
        DB::table('units_menus')->where('id', 24)->update(['slug' => 'units/relatorios/consulta-plataforma']);
    }
}
