<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataToMenuOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_orders', function (Blueprint $table) {
            \DB::insert('insert into units_menu_orders (id, menu_order, company_id, created_at, updated_at) value (1, \'[{"id":1},{"id":2,"children":[{"id":3,"children":[{"id":4},{"id":5,"children":[{"id":7},{"id":6}]}]},{"id":8},{"id":9}]},{"id":10},{"id":11},{"id":12},{"id":13},{"id":14,"children":[{"id":15}]},{"id":16},{"id":17,"children":[{"id":18,"children":[{"id":19}], "id": 24}]},{"id":20,"children":[{"id":21}]},{"id":22}]\', 1, now(), now())');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units_menu_orders', function (Blueprint $table) {
	        \DB::select("TRUNCATE units_menu_orders");
        });
    }
}
