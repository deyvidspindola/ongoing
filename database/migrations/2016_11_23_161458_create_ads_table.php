<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable()->unsigned();
            $table->string('connection')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('domain')->nullable();
            $table->integer('port')->nullable()->unsigned();
            $table->string('dn_users')->nullable();
            $table->string('dn_groups')->nullable();
            $table->string('fields')->nullable();

            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
