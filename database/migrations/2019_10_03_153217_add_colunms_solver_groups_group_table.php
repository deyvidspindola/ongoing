<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunmsSolverGroupsGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solver_groups_group', function (Blueprint $table) {
            $table->char('is_internal_show', 1)->after('solver_group_id')->default('N');
            $table->char('is_external_show', 1)->after('is_internal_show')->default('N');
            $table->char('is_create', 1)->after('is_external_show')->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solver_groups_group', function (Blueprint $table) {
            $table->dropColumn('is_internal_show');
            $table->dropColumn('is_external_show');
            $table->dropColumn('is_create');
        });
    }
}
