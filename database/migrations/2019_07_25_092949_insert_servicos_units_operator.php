<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertServicosUnitsOperator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (105, 'TPA TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (112, 'CTBC', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (113, 'FONAR', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (114, 'BRASIL TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (115, 'TELEFONICA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (117, 'TRANSIT BRASIL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (119, 'EPSILON TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (121, 'EMBRATEL', 'S', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (123, 'INTELIG', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (124, 'DIALDATA TELECOMUNICACOES', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (125, 'GVT', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (126, 'IDT BRASIL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (127, 'AEROTECH', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (128, 'VOXBRAS', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (129, 'T-LESTE TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (131, 'TELEMAR', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (132, 'CONVERGIA TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (134, 'ETML', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (135, 'EASYTONE TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (136, 'DSLI VOX', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (137, 'GOLDEN LINE TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (140, 'HOJE TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (141, 'TIM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (142, 'GT GROUP', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (143, 'SERCOMTEL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (144, 'SUL INTERNET', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (145, 'LEVEL 3', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (146, 'AMIGO TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (147, 'BT', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (148, 'NETSERV', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (149, 'BBS OPTIONS', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (151, '51 BRASIL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (152, 'CABO TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (153, 'OSTARA TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (154, 'G30 TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (156, 'ENGEVOX TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (157, 'BR GROUP', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (158, 'VOITEL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (159, 'LOCAL TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (161, 'VONEX', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (162, 'MUNDIVOX', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (163, 'HELLO BRAZIL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (164, 'REMOTA TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (165, 'RN BRASIL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (167, 'SIGNALLINK', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (168, 'SUPORTE TECNOLOGIA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (169, 'REDEVOX', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (170, 'LOCAWEB', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (171, 'DOLLARPHONE', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (172, 'TELECOMDADOS', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (173, 'BRASTEL', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (174, 'TELEFREE', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (175, 'TMAIS', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (176, 'VOX', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (178, 'NORTELPA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (181, 'DATORA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (183, 'PONTO TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (184, 'BBT', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (188, 'TVN', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (189, 'CONECTA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (191, 'FALKLAND', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (192, 'VIACOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (193, 'VIPWAY TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (194, '76 TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (196, 'IVATI INOVACAO E INTERATIVIDADE S/A', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (199, 'ENSITE BRASIL TELECOMUNICAÇÕES LTDA ME', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (200, 'PORTO VELHO TELECOMUNICAÇÕES LTDA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (206, 'DESKTOP', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (208, 'PONTO TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (215, 'VIVO FIXO', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (242, 'TCHETURBO INTERNET PROVIDER', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (254, 'BRASREDE TELECOM', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (294, 'IVELOZ TELECOM SERVIÇOS EM TELECOMUNICAÇÕES LTDA', 'N', 1)");
        \DB::select("INSERT INTO units_operator (id_operadora_telefonia, nm_operadora_telefonia, fc_produto_ebt, company_id) VALUES (302, 'PORTO CONECTA', 'N', 1)");
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::select("TRUNCATE units_operator");
    }
}
