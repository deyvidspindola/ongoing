<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMacroOffendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('macro_offenders', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('macro_offender_id')->nullable();
            $table->string('macro_offender', 200)->nullable();
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('macro_offenders');
    }
}
