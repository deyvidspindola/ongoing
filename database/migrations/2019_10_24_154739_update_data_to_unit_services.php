<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataToUnitServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('units_services')->where('id', '22')->update([
            'description_detailed' => 'Esta opção deve ser utilizada para realizar a exclusão de área local para o cenário de portabilidade interna.'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_services', function (Blueprint $table) {
            //
        });
    }
}
