<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenderSynchronismsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offender_synchronisms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offender_id')->unsigned();
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->string('ppm', 50)->nullable();
            $table->char('type');
            $table->date('register_date');
            $table->decimal('possible_treatment', 10, 2)->nullable();
            $table->decimal('effectively_treated', 10, 2)->nullable();
            $table->decimal('treated_percentage', 10, 2)->nullable();
            $table->timestamps();
	        $table->softDeletes();
	        $table->foreign('offender_id')->references('id')->on('offenders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offender_synchronisms');
    }
}
