<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_operations', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('base_name')->nullable()->comment('Antigo campo - base');
            $table->string('base_code')->nullable()->comment('Antigo campo - cod_operadora');
            $table->string('city_contract')->nullable()->comment('Antigo campo - cid_contrato');
            $table->string('city_name')->nullable()->comment('Antigo campo - nm_cidade');
            $table->string('uf')->nullable()->comment('Antigo campo - sigla_estado');
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_operations');
    }
}
