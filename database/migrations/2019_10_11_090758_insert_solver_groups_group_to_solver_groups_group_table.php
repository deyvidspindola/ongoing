<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertSolverGroupsGroupToSolverGroupsGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solver_groups', function (Blueprint $table) {
            @DB::table('solver_groups')->insertOrIgnore([

                ['id' => 1, 'name' => 'NETSMS - PORTABILIDADE', 'created_at' => Carbon::now()],
                ['id' => 2, 'name' => 'ONGOING NOW', 'created_at' => Carbon::now()],
                ['id' => 3, 'name' => 'ONGOING BI ARTIT', 'created_at' => Carbon::now()],
                ['id' => 4, 'name' => 'SINCRONISMO', 'created_at' => Carbon::now()],

            ]);
        });

        Schema::table('solver_groups_group', function (Blueprint $table) {

            DB::select("TRUNCATE solver_groups_group");

            DB::table('solver_groups_group')->insert([

                ['group_id' => 2, 'solver_group_id' => '1', 'is_internal_show' => 'S' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],
                ['group_id' => 2, 'solver_group_id' => '2', 'is_internal_show' => 'S' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],
                ['group_id' => 2, 'solver_group_id' => '3', 'is_internal_show' => 'S' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],
                ['group_id' => 2, 'solver_group_id' => '4', 'is_internal_show' => 'S' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],
                ['group_id' => 5, 'solver_group_id' => '1', 'is_internal_show' => 'N' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],
                ['group_id' => 3, 'solver_group_id' => '2', 'is_internal_show' => 'N' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],
                ['group_id' => 4, 'solver_group_id' => '3', 'is_internal_show' => 'N' , 'is_external_show' => 'S', 'is_create' => 'S', 'created_at' => Carbon::now()],

            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solver_groups_group', function (Blueprint $table) {
            DB::select("TRUNCATE solver_groups_group");
        });
    }
}
