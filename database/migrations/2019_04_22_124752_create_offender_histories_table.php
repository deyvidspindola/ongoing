<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offender_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->text('snapshot');
            $table->text('modified_fields')->nullable();
            $table->string('offender_id', 100)->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('operation_type', 1);
            $table->text('observation2')->nullable();
            $table->string('sheet', 255)->nullable();
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();

		    $table->foreign('user_id')->references('id')->on('users');
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offender_histories');
    }
}
