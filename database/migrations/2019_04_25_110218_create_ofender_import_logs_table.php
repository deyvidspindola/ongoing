<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfenderImportLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofender_import_logs', function (Blueprint $table) {
            $table->integer('id');
        	$table->text('ofensores')->nullable();
            $table->text('ofensores_desativados')->nullable();
            $table->text('descricao_antiga')->nullable();
            $table->text('ofensores_direcionados')->nullable();
            $table->text('of_direcionados_desativados')->nullable();
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofender_import_logs');
    }
}
