<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsOperatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_operator', function (Blueprint $table) {
            $table->bigIncrements('id_operadora_telefonia');
            $table->string('nm_operadora_telefonia');
            $table->string('fc_produto_ebt');
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_operator');
    }
}
