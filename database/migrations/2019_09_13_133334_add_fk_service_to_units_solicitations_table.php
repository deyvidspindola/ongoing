<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkServiceToUnitsSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units_solicitations', function (Blueprint $table) {
            $table->foreign('units_service_id')->references('id')->on('units_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units_solicitations', function (Blueprint $table) {
            $table->dropForeign(['units_services_id_foreign']);
        });
    }
}
