<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertConsultaPlataformaOnPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {

            DB::table('permissions')->insert([
                ['name' => 'units_netnow_consultas' , 'description' => 'Units / Net Now / Consultas'],
                ['name' => 'units_netnow_consulta_plataforma'  , 'description' => 'Units / Net Now / Consultas / Consulta Plataforma'],
            ]);
        });
        Schema::table('group_permission', function (Blueprint $table) {
            DB::table('group_permission')->insert([
                ['group_id' => 1, 'permission_id' => 61],
                ['group_id' => 2, 'permission_id' => 61],
                ['group_id' => 3, 'permission_id' => 61],
                ['group_id' => 4, 'permission_id' => 61],

                ['group_id' => 1, 'permission_id' => 62],
                ['group_id' => 2, 'permission_id' => 62],
                ['group_id' => 3, 'permission_id' => 62],
                ['group_id' => 4, 'permission_id' => 62],
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
