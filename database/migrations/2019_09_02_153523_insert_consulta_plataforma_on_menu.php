<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertConsultaPlataformaOnMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Consultas', 'consultas', 'javascript:void(0);', 'units_netnow_consultas', null, null, null);");
        \DB::select("INSERT INTO units_menus (title, sub_title, slug, permission, created_at, updated_at, deleted_at) VALUES ('Consulta Plataforma', 'Consulta Plataforma', 'units/relatorios/consulta-plataforma', 'units_netnow_consulta_plataforma', null, null, null);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
