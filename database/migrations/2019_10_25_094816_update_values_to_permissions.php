<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateValuesToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('group_permission')->whereIn('permission_id',[12,13,14,16])->delete();
        DB::table('permissions')->whereIn('id',[12,13,14,16])->delete();
        DB::insert("INSERT INTO permissions (name,description,created_at) VALUES ('offender_active', 'Ofensores / Ativar', NOW())");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
