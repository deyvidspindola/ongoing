<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsSolicitations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_solicitations', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('username')->nullable()->comment('Antigo campo - username');
            $table->string('remote_ip')->nullable()->comment('Antigo campo - ip_remoto');
            $table->integer('units_service_id')->unsigned()->comment('Antigo campo - id_servico');
            $table->integer('units_operation_id')->unsigned()->comment('Antigo campo - operadora');
            $table->string('contract_number')->nullable()->comment('Antigo campo - contrato');
            $table->string('terminal')->nullable()->comment('Antigo campo - terminal');
            $table->string('node')->nullable()->comment('Antigo campo - node');
            $table->string('pa')->nullable()->comment('Antigo campo - pa');
            $table->string('status')->nullable()->comment('Antigo campo - status');
            $table->string('process_status')->nullable()->comment('Antigo campo - status_processo');
            $table->longText('process_message')->nullable()->comment('Antigo campo - msg_processo');
            $table->string('proposal_number')->nullable()->comment('Antigo campo - proposta');
            $table->string('cnpj')->nullable()->comment('Antigo campo - numerocnpj');
            $table->string('ticket')->nullable()->comment('Antigo campo - bilhete');
            $table->string('tags')->nullable()->comment('Antigo campo - busca_tags');
            $table->string('softx')->nullable()->comment('Antigo campo - busca_softx');
            $table->string('date_time')->nullable()->comment('Antigo campo - data_horario');
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });

        /*
        Schema::table('units_solicitations', function($table) {
            $table->foreign('units_service_id')->references('id')->on('units_services');
            $table->foreign('units_operation_id')->references('id')->on('units_operations');
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_solicitations');
    }
}
