<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataToMenuOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units_menu_orders', function (Blueprint $table) {
            DB::table('units_menu_orders')->where('id', '1')->update([
                'menu_order' => '[{"id":1},{"id":2,"children":[{"id":3,"children":[{"id":4},{"id":5,"children":[{"id":7},{"id":6}]}]},{"id":8},{"id":9}]},{"id":10},{"id":11},{"id":12},{"id":13,"children":[{"id":14,"children":[{"id":15}]}]},{"id":16},{"id":17,"children":[{"id":23,"children":[{"id":24}]},{"id":18,"children":[{"id":19}]}]},{"id":20,"children":[{"id":21}]},{"id":22}]'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_order', function (Blueprint $table) {
            //
        });
    }
}
