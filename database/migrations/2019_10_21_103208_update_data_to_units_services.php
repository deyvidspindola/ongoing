<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateDataToUnitsServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('units_services')->where('id', '2')->update([
            'description' => 'REINSTALAÇÃO / CORREÇÃO DE STATUS'
        ]);

        DB::table('units_services')->where('permission_name', 'units_servico_pint_excluir_area_local')->update([
            'slug' => 'portabilidade-interna'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
