<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_services', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('description')->nullable()->comment('Antigo campo - descricao');
            $table->string('description_short')->nullable()->comment('Antigo campo - descricao_curta');
            $table->text('description_detailed')->nullable()->comment('Antigo campo - descricao_detalhada ');
            $table->string('permission_name')->nullable();
            $table->integer('permission')->nullable()->comment('Antigo campo - permissao');
            $table->string('script_file')->nullable()->comment('Antigo campo - script');
            $table->char('online_process')->default('S')->comment('Antigo campo - processo_online');
            $table->string('menu')->nullable();
            $table->string('slug')->nullable();
            $table->char('additional_fields')->nullable()->default('N')->comment('Antigo campo - campos_adicionais');
            $table->integer('flag_fields')->nullable()->default(0)->comment('Antigo campo - flag_campos');
            $table->string('send_ebt_service')->nullable()->default('N')->comment('Antigo campo - envio_servico_ebt');
            $table->integer('units_group_id')->nullable()->unsigned()->comment('Antigo campo - id_grupo');
            $table->integer('units_ebt_service_id')->nullable()->unsigned()->comment('Antigo campo - tipo_servico_ebt');
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });

        /*
        Schema::table('units_services', function($table) {
            $table->foreign('units_group_id')->references('id')->on('units_groups');
            $table->foreign('units_ebt_service_id')->references('id')->on('units_ebt_services');
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_services');
    }
}
