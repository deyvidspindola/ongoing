<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataToPermissionGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_permission', function (Blueprint $table) {
            DB::table('group_permission')->insert([
                ['group_id' => 2, 'permission_id' => 59],
                ['group_id' => 1, 'permission_id' => 59],
                ['group_id' => 3, 'permission_id' => 59],
                ['group_id' => 4, 'permission_id' => 59],
                ['group_id' => 2, 'permission_id' => 60],
                ['group_id' => 1, 'permission_id' => 60],
                ['group_id' => 3, 'permission_id' => 60],
                ['group_id' => 4, 'permission_id' => 60]
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permission_group', function (Blueprint $table) {
            //
        });
    }
}
