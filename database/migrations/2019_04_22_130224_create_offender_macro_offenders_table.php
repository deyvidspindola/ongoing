<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenderMacroOffendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offender_macro_offenders', function (Blueprint $table) {
	        $table->integer('offender_id')->unsigned();
	        $table->integer('macro_offender_id')->unsigned();

	        $table->index('offender_id');
	        $table->index('macro_offender_id');

	        $table->foreign('offender_id')->references('id')->on('offenders')->onDelete('cascade');
	        $table->foreign('macro_offender_id')->references('id')->on('macro_offenders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offender_macro_offenders');
    }
}
