<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataToUnitsServicesCorrecaoAcentuacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('units_services')->where('id', '21')->update([
            'description_detailed' => 'Esta opção deve ser utilizada somente para telefones, onde há necessidade de alterar o FQDN e PORTA para telefones portados e netfones. O(s) terminal(s) deve(m) estar ativo, para que se possível realizar a troca com sucesso.'
        ]);

        DB::table('units_services')->where('id', '7')->update([
            'description' => 'EXCLUIR ÁREA LOCAL'
        ]);

        DB::table('units_services')->where('id', '11')->update([
            'description' => 'HISTÓRICO DE PORTAB.'
        ]);

        DB::table('units_services')->where('id', '14')->update([
            'description' => 'INSERE ÁREA LOCAL'
        ]);

        DB::table('units_services')->where('id', '17')->update([
            'description' => 'EXCLUIR ÁREA LOCAL'
        ]);

        DB::table('units_services')->where('id', '19')->update([
            'description' => 'INCLUIR ÁREA LOCAL'
        ]);

        DB::table('units_services')->where('id', '24')->update([
            'description' => 'INCLUIR ÁREA LOCAL CTV'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
