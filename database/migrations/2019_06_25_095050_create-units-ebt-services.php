<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsEbtServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_ebt_services', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->comment('Antigo campo - tipo_servico');
            $table->string('service_host')->nullable()->comment('Antigo campo - host_servico_ebt');
            $table->integer('service_port')->nullable()->comment('Antigo campo - port_servico_ebt');
            $table->string('service_username')->nullable()->comment('Antigo campo - username_servico_ebt');
            $table->string('service_password')->nullable()->comment('Antigo campo - password_servico_ebt');
            $table->string('service_endpoint')->nullable()->comment('Antigo campo - end_point_servico_ebt');
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_ebt_services');
    }
}
