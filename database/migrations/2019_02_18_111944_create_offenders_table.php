<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offenders', function (Blueprint $table) {

	        $table->increments('id');
	        $table->string('offender_id', 20)->nullable();
	        $table->text('offender')->nullable();
	        $table->string('type', 7);
	        $table->string('status', 20)->nullable();
	        $table->string('origin', 255)->nullable();
	        $table->string('retention_level', 10)->nullable();
	        $table->string('valid', 20)->nullable();
	        $table->text('detailed_explanation_offender')->nullable();
	        $table->text('answer_for_the_user_in_the_call')->nullable();
	        $table->text('preventive_offender')->nullable();
	        $table->string('project_pack', 255)->nullable();
	        $table->string('classification', 255)->nullable();
	        $table->string('predict_definitive', 255)->nullable();
	        $table->string('num_patch', 10)->nullable();
	        $table->string('patch_identification', 255)->nullable();
	        $table->string('unit_c', 255)->nullable();
	        $table->string('ocurrence_id', 10)->nullable();
	        $table->string('procedure_id', 10)->nullable();
	        $table->text('observation')->nullable();
	        $table->string('instruction_id', 10)->nullable();
	        $table->string('instruction_file', 255)->nullable();
	        $table->string('ppm_id', 100)->nullable();
	        $table->string('event_id', 20)->nullable();

	        #Ofensores direcionados
	        $table->string('proceeding_id', 20)->nullable();
	        $table->text('scenario_validation')->nullable();
	        $table->text('description_and_procedure_id')->nullable();
	        $table->text('action_solution_targeting_for_responsible_team')->nullable();
	        $table->text('exception')->nullable();

	        #Campos obrigatórios do CA
	        $table->text('symptom')->nullable();
	        $table->text('fact')->nullable();
	        $table->text('cause')->nullable();
	        $table->text('action')->nullable();
	        $table->string('system_legacy', 20)->nullable();
	        $table->string('group_identification', 200)->nullable();
	        $table->integer('solver_group_id')->nullable();
	        $table->integer('ppm')->nullable();
            $table->string('sheet', 255)->nullable();
	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');
	        $table->timestamps();
	        $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offenders');
    }
}
