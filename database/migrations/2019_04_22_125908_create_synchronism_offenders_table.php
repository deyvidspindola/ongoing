<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSynchronismOffendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('synchronism_offenders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offender_id')->unsigned();
            $table->string('synchronism_offender_type', 50);
            $table->integer('possible_treatment');
            $table->integer('definitely_treated');
            $table->integer('treated_percentage');

	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');

            $table->timestamps();

	        $table->foreign('offender_id')->references('id')->on('offenders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('synchronism_offenders');
    }
}
