<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertGroupsToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {

            DB::table('groups')->insert([

                ['company_id' => null, 'name' => 'admin', 'description' => 'Administrador Geral' , 'is_admin' => 1],
                ['company_id' => 1, 'name' => 'ongoing_admin', 'description' => 'Ongoing Admin' , 'is_admin' => 0],
                ['company_id' => 1, 'name' => 'ongoing_now', 'description' => 'Ongoing Now' , 'is_admin' => 0],
                ['company_id' => 1, 'name' => 'ongoing_in', 'description' => 'Ongoing In' , 'is_admin' => 0],
                ['company_id' => 1, 'name' => 'ongoing_portabilidade', 'description' => 'Ongoing Portabilidade', 'is_admin' => 0],

            ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::select("TRUNCATE groups");
    }
}
