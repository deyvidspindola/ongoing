<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOrderToUnitsServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units_services', function (Blueprint $table) {
            $table->integer('order')->unsigned();
        });

        DB::table('units_services')->where('id', '1')->update(['order' => '1']);
        DB::table('units_services')->where('id', '2')->update(['order' => '1']);
        DB::table('units_services')->where('id', '3')->update(['order' => '1']);
        DB::table('units_services')->where('id', '4')->update(['order' => '1']);
        DB::table('units_services')->where('id', '5')->update(['order' => '1']);
        DB::table('units_services')->where('id', '6')->update(['order' => '1']);
        DB::table('units_services')->where('id', '7')->update(['order' => '3']);
        DB::table('units_services')->where('id', '8')->update(['order' => '2']);
        DB::table('units_services')->where('id', '9')->update(['order' => '1']);
        DB::table('units_services')->where('id', '10')->update(['order' => '1']);
        DB::table('units_services')->where('id', '11')->update(['order' => '5']);
        DB::table('units_services')->where('id', '12')->update(['order' => '1']);
        DB::table('units_services')->where('id', '13')->update(['order' => '1']);
        DB::table('units_services')->where('id', '14')->update(['order' => '2']);
        DB::table('units_services')->where('id', '15')->update(['order' => '2']);
        DB::table('units_services')->where('id', '16')->update(['order' => '4']);
        DB::table('units_services')->where('id', '17')->update(['order' => '4']);
        DB::table('units_services')->where('id', '18')->update(['order' => '3']);
        DB::table('units_services')->where('id', '19')->update(['order' => '1']);
        DB::table('units_services')->where('id', '20')->update(['order' => '1']);
        DB::table('units_services')->where('id', '21')->update(['order' => '1']);
        DB::table('units_services')->where('id', '22')->update(['order' => '2']);
        DB::table('units_services')->where('id', '23')->update(['order' => '1']);
        DB::table('units_services')->where('id', '24')->update(['order' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units_services', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
