<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenderInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offender_instructions', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('filename', 255)->nullable();
	        $table->string('file', 255)->nullable();
	        $table->string('instruction_id', 200)->nullable();
	        $table->integer('offender_id')->unsigned();
	        $table->integer('user_id')->unsigned();
	        $table->integer('is_three_days_email_sent')->default(0);
	        $table->integer('is_five_days_email_sent')->default(0);

	        $table->foreign('user_id')->references('id')->on('users');
	        $table->foreign('offender_id')->references('id')->on('offenders');

	        $table->integer('company_id')->unsigned();
	        $table->foreign('company_id')->references('id')->on('companies');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offender_instructions');
    }
}
