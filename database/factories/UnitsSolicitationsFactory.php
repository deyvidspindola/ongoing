<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\Models\Units\UnitsSolicitation;
use Faker\Generator as Faker;

$factory->define(UnitsSolicitation::class, function (Faker $faker) {
    return [
        'username' => 'drspindola',
        'remote_ip' => $faker->ipv4,
        'units_service_id' => 2,
        'units_operation_id' => 2,
        'contract_number' => $faker->randomDigit(),
        'terminal' => 19981119983,
        'date_time' => $faker->date('Y-m-d')
    ];
});
