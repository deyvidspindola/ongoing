<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SolverGroupsGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = [

            [1, 1, 1],
            [2, 1, 2],
            [3, 1, 3],
            [4, 1, 4],
            [5, 2, 1],
            [6, 2, 2],
            [7, 2, 3],
            [8, 2, 4],
            [9, 5, 1],
            [10, 3, 2],
            [11, 4, 3],
            [12, 4, 4],

        ];


        foreach($array as $row){

            $data[] = [
                'id'               => $row[0],
                'group_id'         => $row[1],
                'solver_group_id'  => $row[2],
                'created_at'       => Carbon::now(),
            ];

        }

        DB::table('solver_groups_group')->insert($data);
    }

}
