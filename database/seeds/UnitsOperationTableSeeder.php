<?php

use Illuminate\Database\Seeder;
use App\Models\Units\UnitsOperation;

class UnitsOperationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units_operations')->delete();
        $json = File::get("database/data/net_operations.json");
        $data = json_decode($json);
        foreach($data as $obj){
            UnitsOperation::create(array(
                'base_name' => $obj->base_name,
                'base_code' => $obj->base_code,
                'city_contract' => $obj->city_contract,
                'city_name' => $obj->city_name,
                'uf' => $obj->uf,
	            'company_id' => 1
            ));
        }
    }
}
