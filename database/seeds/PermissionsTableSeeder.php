<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [

            // ADMINISTRAÇÃO

                // Usuários
                [1, 'user_list' , 'Administrador / Usuários / Listar'],
                [2, 'user_add'  , 'Administrador / Usuários / Criar'],
                [3, 'user_edit' , 'Administrador / Usuários / Editar'],
                [4, 'user_del'  , 'Administrador / Usuários / Deletar'],

                // Grupos
                [5, 'group_list', 'Administrador / Grupos / Listar'],
                [6, 'group_add' , 'Administrador / Grupos / Criar'],
                [7, 'group_edit', 'Administrador / Grupos / Editar'],
                [8, 'group_del' , 'Administrador / Grupos / Deletar'],

                // Empresas
                [9, 'company_list', 'Administrador / Empresa / Listar'],
                [10, 'company_edit', 'Administrador / Empresa / Editar'],

                //Ofensores
                [11, 'offender_list', 'Ofensores / Listar'],
                [12, 'offender_add' , 'Ofensores / Criar'],
                [13, 'offender_edit', 'Ofensores / Editar'],
                [14, 'offender_del' , 'Ofensores / Deletar'],
                [15, 'offender_import' , 'Ofensores / Carga de Dados'],
                [16, 'offender_export' , 'Ofensores / Extração de Dados'],

                //Sincronismo
                [17, 'synchronism_list', 'Sincronismo / Listar'],
                [18, 'synchronism_add' , 'Sincronismo / Criar'],
                [19, 'synchronism_edit', 'Sincronismo / Editar'],
                [20, 'synchronism_del' , 'Sincronismo / Deletar'],

                //Units 
                [21, 'units_dashboard' , 'Units / Dashboard / Visualizar'],
                [22, 'units_site_shortcuts' , 'Units / Atalhos / Visualizar'],

        ];


        foreach($array as $row){

            $data[] = [
                'id' => $row[0],
                'name' => $row[1],
                'description' => $row[2],
                'created_at' => Carbon::now(),
            ];

        }

        DB::table('permissions')->insert($data);
    }
}
