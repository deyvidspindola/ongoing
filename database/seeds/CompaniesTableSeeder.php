<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'art_it',
            'description' => 'ART IT Intelligent Technology',
            'domain' => 'artit.com.br',
            'created_at' => Carbon::now(),
        ]);



    }
}
