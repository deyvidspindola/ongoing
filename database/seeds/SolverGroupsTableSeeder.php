<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SolverGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = [

            [1, 'NETSMS - PORTABILIDADE'],
            [2, 'ONGOING NOW'],
            [3, 'ONGOING BI ARTIT'],
            [4, 'SINCRONISMO'],

        ];


        foreach($array as $row){

            $data[] = [
                'id'         => $row[0],
                'name'       => $row[1],
 	            'company_id' => 1,
                'created_at' => Carbon::now(),
            ];

        }

        DB::table('solver_groups')->insert($data);
    }
}
