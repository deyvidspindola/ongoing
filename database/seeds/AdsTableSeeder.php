<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('ads')->insert([
            'company_id' => 1,
            'connection' => '10.230.230.6',
            'username' => 'artauth',
            'password' => 'artit@2016',
            'domain' => 'artdc.local',
            'port' => '3268',
            'dn_users' => 'OU=Usuarios,OU=Art,DC=artdc,DC=local',
            'dn_groups' => 'OU=Art,DC=artdc,DC=local',
            'fields' => 'givenName,sn,mail,samaccountname,memberof',
            'created_at' => Carbon::now(),
        ]);

    }
}
