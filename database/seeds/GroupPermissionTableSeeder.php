<?php

use Illuminate\Database\Seeder;

class GroupPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [

            // OFENSORES

            // Admin
            [2,11],
            [2,15],
	        [2,17],
	        [2,18],
	        [2,19],
	        [2,20],
	        [2,21],
	        [2,22],
	        [2,65],

            // Now
            [3,11],
            [3,15],
	        [3,17],
	        [3,18],
	        [3,19],
	        [3,20],
	        [3,21],
	        [3,22],

            // In
            [4,11],
            [4,15],
	        [4,17],
	        [4,18],
	        [4,19],
	        [4,20],
	        [4,21],
	        [4,22],

            // Portabilidade
            [5,11],
            [5,15],
	        [5,17],
	        [5,18],
	        [5,19],
	        [5,20],
	        [5,21],
	        [5,22],

        ];

        $data = [];

        foreach($array as $row){

            $data[] = [
                'group_id'      => $row[0],
                'permission_id' => $row[1],
            ];

        }

        DB::table('group_permission')->insert($data);
    }
}
