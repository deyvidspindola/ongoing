<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data[] = [
            'id'         => 1,
            'company_id' => 1,
            'name'       => 'Administrador Geral',
            'username'   => 'admin',
            'password'   => bcrypt('artit@2019'),
            'created_at' => Carbon::now(),
        ];

        DB::table('users')->insert($data);

    }
}
