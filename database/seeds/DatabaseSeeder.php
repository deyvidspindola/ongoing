<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        $this->call(CompaniesTableSeeder::class);
        $this->call(ParametersTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
//        $this->call(PermissionsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(UserGroupTableSeeder::class);
//        $this->call(SolverGroupsTableSeeder::class);
//        $this->call(SolverGroupsGroupTableSeeder::class);
        $this->call(UnitsOperationTableSeeder::class);
	    $this->call(GroupPermissionTableSeeder::class);

        // Novas aplicações

    }
}
