<?php

use Illuminate\Database\Seeder;

class UserGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    $data = [
		    ['user_id' => 1, 'group_id' => 1],
//		    ['user_id' => 2, 'group_id' => 1],
//		    ['user_id' => 2, 'group_id' => 2],
//		    ['user_id' => 2, 'group_id' => 3],
//		    ['user_id' => 2, 'group_id' => 4],
	    ];

        DB::table('user_group')->insert($data);
    }
}
