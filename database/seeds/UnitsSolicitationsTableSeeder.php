<?php

use Illuminate\Database\Seeder;

class UnitsSolicitationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Units\UnitsSolicitation::class, 100)->create();
    }
}
