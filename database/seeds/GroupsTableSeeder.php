<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('groups')->insert([
	        ['company_id' => null, 'name' => 'teste_unit', 'description' => 'Teste Unit', 'is_admin' => 0],
        ]);

    }
}
