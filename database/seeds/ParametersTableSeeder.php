<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parameters')->insert([
            'company_id' => 1,
            'auth_ad' => 1,
            'created_at' => Carbon::now(),
        ]);
    }
}
